About pescado
=============

First version of pescado

Installation
============

Run the command ::

    python setup.py build_ext --inplace

This will compile the cython files located in pescado and allow for the import of the module ``pescado``.

Testing
=======

Testing using pytest is supported.

Uses the library pytest-optional-tests
        
        pip install pytest-optional-tests

Slow-running tests are disabled by default.  To enable them, use the runslow
option::

        python -m pytest --runslow


Documentation
=============

To build the documentation, go to the folder ``doc`` and run the command ::

        make html

This requires a recent version of sphinx, and jupyter-sphinx.
The built documentation can be found in ``doc/build/html/index.html``.
