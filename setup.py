"""
    Execute with python3 setup.py build_ext --inplace
"""
import sys
import numpy
from setuptools import setup, find_packages, Extension

from Cython.Build import cythonize
from distutils.core import setup as setup_cy

if sys.version_info < (3, 6):
    print("Pescado requires Python 3.6 or above.")
    sys.exit(1)

install_requires = [
    "scipy",
    "numpy",
    "matplotlib",
]

exts = dict(
    [
        (
            "pescado.tools._meshing",
            dict(
                sources=["pescado/tools/_meshing.pyx"],
                include_dirs=[numpy.get_include()],
            ),
        )
    ]
)

# Stop relying on numpy deprecated API.
for ext in exts.values():
    ext["define_macros"] = [("NPY_NO_DEPRECATED_API", "NPY_1_7_API_VERSION")]

exts = cythonize(
    [Extension(name, **kwargs) for name, kwargs in exts.items()],
    language_level=3,
    compiler_directives={"linetrace": True},
)

packages = find_packages(".")
setup(
    name="Pescado",
    description="Poisson equation solver using finite volume",
    author="Pescado authors",
    license="BSD",
    classifiers=[
        "Development Status :: Pre-alpha",
        "Programming Language :: Python :: 3.7.3",
    ],
    packages=packages,
    package_data={p: ["*.pxd", "*.h"] for p in packages},
    install_requires=install_requires,
    ext_modules=exts
)
