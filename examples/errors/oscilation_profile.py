import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl

from pescado import mesher, poisson, tools, self_consistent
from scipy import constants

import pickle

def poisson_problem():

    eps1 = 9
    epgate = 2
    ep1 = 30
    epgas = 1

    origin = -0.6
    d0 = origin + epgas
    d1 = d0 + ep1

    xsize = 100

    pattern_fine = mesher.patterns.Rectangular.constant(
        element_size=(1, 1))

    shape_all = mesher.shapes.Box(
        lower_left=(-1 * xsize / 2, -1 * xsize / 2),
        size=(xsize, xsize))

    pp_builder = poisson.ProblemBuilder()
    pp_builder.add_sub_mesh(
        shape_inst=shape_all, pattern_inst=pattern_fine)

    gate_up = mesher.shapes.Box(
        lower_left=(-1 * xsize / 2, d1),
        size=(xsize, epgate))

    dielectric = mesher.shapes.Box(
        lower_left=(-1 * xsize / 2, d0),
        size=(xsize, ep1))

    dopant_reg = mesher.shapes.Box(
        lower_left=(-1 * xsize / 3, d0 + 10),
        size=(2 * xsize / 3, 5))

    pattern_gaz = mesher.patterns.Rectangular.constant(element_size=(0.25, 1))
    gaz = mesher.shapes.Box(
        lower_left=(-1 * xsize / 2, origin),
        size=(xsize, epgas))

    pp_builder.refine(
        shape_inst=gaz, pattern_inst=pattern_gaz, ext_bd=4)

    air = (shape_all - (gate_up | gaz | dielectric))

    rel_die_list = [1e4, 1e4, eps1, eps1]
    shapes_list = [air, gate_up, dielectric, gaz]
    for rel_die, shape_inst in zip(rel_die_list, shapes_list):
        pp_builder.set_relative_permittivity(
            val=rel_die, shape_inst=shape_inst)

    pp_builder.set_dirichlet(
        shape=gate_up, setup_tag='gate')

    pp_builder.set_neumann(
        shape=dopant_reg, setup_tag='dopant')

    pp_builder.set_flexible(
        shape=gaz, setup_tag='gaz')

    pp_problem = pp_builder.finalized(parallelize=True)

    return pp_problem


def ildos_gen(b_field, gas_surf):

    lb = np.sqrt(constants.hbar / (constants.elementary_charge * b_field))
    w_c = constants.elementary_charge * b_field / (
        constants.electron_mass * 0.5)

    charge_step = 1 / (2 * constants.pi * (lb ** 2))
    volt_step = (1 / 2) * constants.hbar * w_c / constants.elementary_charge

    dens = constants.electron_mass * 0.5 / (np.pi * (constants.hbar ** 2))

    ildos = np.empty((9, 2), dtype=float)
    ildos[:, 0] = np.array(
        [-1, 0, 0, 1/2, 1/2, 3/2, 3/2, 5/2, 5/2]) * volt_step
    ildos[:, 1] = np.array(
        [0,  0, 1,  1,   2,   2,   3,   3,   4]) * charge_step * 1e-18
    ildos[:, 1] = ildos[:, 1] * gas_surf

    return ildos


def solve(ildos, pp_problem, v_gate):
    sp_problem = self_consistent.SchrodingerPoisson(
        ildos=ildos, poisson_problem_inst=pp_problem)

    sv_gate = pp_problem.sparse_vector(
        val=-0.23, name='gate')

    sv_dop = pp_problem.sparse_vector(
        val=3 * 1e15 * 1e-18, name='dopant')

    sp_problem.initialize(
        initial_config=np.ones(len(pp_problem.flexible_indices), dtype=int),
        voltage=sv_gate, charge_electrons=sv_dop, return_poisson_output=False)

    sp_problem.iterate(return_poisson_output=False)

    ite_max = 100
    ite = 1
    while (np.any(
        sp_problem.iteration_data[-1][2]
        != sp_problem.iteration_data[-2][2]) and (ite < ite_max)):

        voltage_res, charge_res = sp_problem.iterate(
            return_poisson_output=True)

        ite += 1

    return sp_problem, voltage_res, charge_res


def main(b_field_list, file_name):

    res = list()
    v_gate = -0.23
    pp_problem = poisson_problem()
    for num, b_field in enumerate(b_field_list):

        ildos = ildos_gen(b_field=b_field, gas_surf=0.25 * 1)

        sp_problem, voltage_res, charge_res = solve(
            ildos=ildos, pp_problem=pp_problem, v_gate=v_gate)

        res.append(
            {'sp_problem_ite':sp_problem.iteration_data,
             'last_volt':voltage_res[pp_problem.flexible_indices],
             'last_charge':charge_res[pp_problem.flexible_indices],
             'info':[b_field, v_gate]})

    with open(file_name, 'wb') as file:
        pickle.dump(res, file)

# main(b_field_list=np.linspace(0.25, 0.38, 26),
#      file_name='data_osc_pro_B_field_0')

# main(b_field_list=np.linspace(0.4, 0.45, 10),
#      file_name='data_osc_pro_B_field_1')

# main(b_field_list=np.linspace(0.55, 0.64, 18),
#      file_name='data_osc_pro_B_field_2')

main(b_field_list=np.linspace(0.7, 2, 130),
     file_name='data_osc_pro_B_field_6')
