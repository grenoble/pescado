import cycler
import numpy as np

import matplotlib.pyplot as plt
import matplotlib as mpl

from pescado import mesher, poisson


pp_builder = poisson.builder.ProblemBuilder()

pattern = mesher.patterns.Rectangular.constant(element_size=(1, 1))
shape_all = mesher.shapes.Box(lower_left=(-50, -50), size=(100, 100))

# We then proceed to add each mesh to the ProblemBuilder
pp_builder.add_sub_mesh(shape_inst=shape_all, pattern_inst=pattern)

circ_gate = mesher.shapes.Ellipsoid.hypersphere(center=(-25, -25), radius=10)
pp_builder.set_dirichlet(shape=circ_gate, setup_tag='circ')

up_gate = mesher.shapes.Box(lower_left=(-25, 20), size=(50, 10))
pp_builder.set_dirichlet(shape=up_gate, setup_tag='up')

for sh, ep in zip(
    [circ_gate | up_gate, shape_all - (circ_gate | up_gate)], [1e4, 8.1]):
    pp_builder.set_relative_permittivity(val=ep, shape_inst=sh)

pp_problem = pp_builder.finalized(parallelize=True)

volt_sv = pp_problem.sparse_vector(val=-3, shape_inst=up_gate)
volt_sv.extend(pp_problem.sparse_vector(val=0.5, shape_inst=circ_gate))

volt, charge = pp_problem.solve(voltage=volt_sv)


voltage_die, idx_die = pp_problem.read_sparse_vector(
    sparse_vector_inst=volt, shape_inst=shape_all - (circ_gate | up_gate),
    return_indices=True)

charge_gate, idx_gate = pp_problem.read_sparse_vector(
    sparse_vector_inst=charge, shape_inst=(circ_gate | up_gate),
    return_indices=True)


coordinates = pp_problem.mesh_inst.coordinates(idx_die)

normalize = mpl.colors.Normalize(vmin=voltage_die.min(), vmax=voltage_die.max())
colormap = plt.cm.viridis
scalarmappaple = plt.cm.ScalarMappable(norm=normalize, cmap=colormap)
scalarmappaple.set_array(len(voltage_die))

plt.scatter(coordinates[:, 0], coordinates[:, 1],
            c=colormap(normalize(voltage_die)), s=5)

plt.ylabel('y(nm)')
plt.xlabel('x(nm)')
cbar = plt.colorbar(scalarmappaple)
cbar.set_label(r'Voltage $(V)$', rotation=270, labelpad=20)

plt.show()

non_zero = charge_gate != 0.0

coordinates = pp_problem.mesh_inst.coordinates(idx_gate)

normalize = mpl.colors.Normalize(vmin=charge_gate.min(), vmax=charge_gate.max())
colormap = plt.cm.viridis
scalarmappaple = plt.cm.ScalarMappable(norm=normalize, cmap=colormap)
scalarmappaple.set_array(len(charge_gate))

plt.scatter(coordinates[non_zero, 0], coordinates[non_zero, 1],
            c=colormap(normalize(charge_gate[non_zero])), s=5)

plt.ylabel('y(nm)')
plt.xlabel('x(nm)')
cbar = plt.colorbar(scalarmappaple)
cbar.set_label(r'Charge', rotation=270, labelpad=20)

plt.show()

##############
print('First done')

dopant = mesher.shapes.Box(lower_left=(-5, -10), size=(10, 20))

charge_sv = pp_problem.sparse_vector(
    val=-10e15*1e-18, shape_inst=dopant, density_to_charge=True)

volt, charge = pp_problem.solve(voltage=volt_sv, charge_electrons=charge_sv)


voltage_die, idx_die = pp_problem.read_sparse_vector(
    sparse_vector_inst=volt, shape_inst=shape_all - (circ_gate | up_gate),
    return_indices=True)

charge_die = pp_problem.read_sparse_vector(
    sparse_vector_inst=charge, shape_inst=shape_all - (circ_gate | up_gate))

charge_gate, idx_gate = pp_problem.read_sparse_vector(
    sparse_vector_inst=charge, shape_inst=(circ_gate | up_gate),
    return_indices=True)

coordinates = pp_problem.mesh_inst.coordinates(idx_die)

normalize = mpl.colors.Normalize(vmin=voltage_die.min(), vmax=voltage_die.max())
colormap = plt.cm.viridis
scalarmappaple = plt.cm.ScalarMappable(norm=normalize, cmap=colormap)
scalarmappaple.set_array(len(voltage_die))

plt.scatter(coordinates[:, 0], coordinates[:, 1],
            c=colormap(normalize(voltage_die)), s=5)

plt.ylabel('y(nm)')
plt.xlabel('x(nm)')
cbar = plt.colorbar(scalarmappaple)
cbar.set_label(r'Voltage $(V)$', rotation=270, labelpad=20)

plt.show()

normalize = mpl.colors.Normalize(vmin=charge_die.min(), vmax=charge_die.max())
colormap = plt.cm.viridis
scalarmappaple = plt.cm.ScalarMappable(norm=normalize, cmap=colormap)
scalarmappaple.set_array(len(charge_die))

plt.scatter(coordinates[:, 0], coordinates[:, 1],
            c=colormap(normalize(charge_die)), s=5)

plt.ylabel('y(nm)')
plt.xlabel('x(nm)')
cbar = plt.colorbar(scalarmappaple)
cbar.set_label(r'Charge', rotation=270, labelpad=20)

plt.show()

non_zero = charge_gate != 0.0

coordinates = pp_problem.mesh_inst.coordinates(idx_gate)

normalize = mpl.colors.Normalize(vmin=charge_gate.min(), vmax=charge_gate.max())
colormap = plt.cm.viridis
scalarmappaple = plt.cm.ScalarMappable(norm=normalize, cmap=colormap)
scalarmappaple.set_array(len(charge_gate))

plt.scatter(coordinates[non_zero, 0], coordinates[non_zero, 1],
            c=colormap(normalize(charge_gate[non_zero])), s=5)

plt.ylabel('y(nm)')
plt.xlabel('x(nm)')
cbar = plt.colorbar(scalarmappaple)
cbar.set_label(r'Charge', rotation=270, labelpad=20)

plt.show()


###############

print('Second done')

side_gate = (mesher.shapes.Box(lower_left=(15, -25), size=(30, 25))
             - mesher.shapes.Ellipsoid.hypersphere(center=(25, -25), radius=10))

pp_builder.reset_regions() # Reset the Dirichlet / Neumann regions

pp_builder.set_dirichlet(shape=side_gate, setup_tag='side_gate')
pp_builder.set_dirichlet(shape=circ_gate, setup_tag='circ')
pp_builder.set_dirichlet(shape=up_gate, setup_tag='up')
print('test')
pp_problem_2 = pp_builder.finalized(parallelize=True)
print('test')
volt_sv = pp_problem.sparse_vector(val=-3, shape_inst=up_gate)
for v, sh in zip([2, 0.5], [side_gate, circ_gate]):
    volt_sv.extend(pp_problem.sparse_vector(val=v, shape_inst=sh))

charge_sv = pp_problem.sparse_vector(
    val=1e15*1e-18, shape_inst=dopant, density_to_charge=True)

volt, charge = pp_problem.solve(voltage=volt_sv, charge_electrons=charge_sv)

voltage_die, idx_die = pp_problem.read_sparse_vector(
    sparse_vector_inst=volt, shape_inst=shape_all - (circ_gate | up_gate | side_gate),
    return_indices=True)

charge_die = pp_problem.read_sparse_vector(
    sparse_vector_inst=charge, shape_inst=shape_all - (circ_gate | up_gate | side_gate))

charge_gate, idx_gate = pp_problem.read_sparse_vector(
    sparse_vector_inst=charge, shape_inst=(circ_gate | up_gate | side_gate),
    return_indices=True)


coordinates = pp_problem.mesh_inst.coordinates(idx_die)

normalize = mpl.colors.Normalize(vmin=voltage_die.min(), vmax=voltage_die.max())
colormap = plt.cm.viridis
scalarmappaple = plt.cm.ScalarMappable(norm=normalize, cmap=colormap)
scalarmappaple.set_array(len(voltage_die))

plt.scatter(coordinates[:, 0], coordinates[:, 1],
            c=colormap(normalize(voltage_die)), s=5)

plt.ylabel('y(nm)')
plt.xlabel('x(nm)')
cbar = plt.colorbar(scalarmappaple)
cbar.set_label(r'Voltage $(V)$', rotation=270, labelpad=20)

plt.show()

normalize = mpl.colors.Normalize(vmin=charge_die.min(), vmax=charge_die.max())
colormap = plt.cm.viridis
scalarmappaple = plt.cm.ScalarMappable(norm=normalize, cmap=colormap)
scalarmappaple.set_array(len(charge_die))

plt.scatter(coordinates[:, 0], coordinates[:, 1],
            c=colormap(normalize(charge_die)), s=5)

plt.ylabel('y(nm)')
plt.xlabel('x(nm)')
cbar = plt.colorbar(scalarmappaple)
cbar.set_label(r'Charge', rotation=270, labelpad=20)

plt.show()

non_zero = charge_gate != 0.0

coordinates = pp_problem.mesh_inst.coordinates(idx_gate)

normalize = mpl.colors.Normalize(vmin=charge_gate.min(), vmax=charge_gate.max())
colormap = plt.cm.viridis
scalarmappaple = plt.cm.ScalarMappable(norm=normalize, cmap=colormap)
scalarmappaple.set_array(len(charge_gate))

plt.scatter(coordinates[non_zero, 0], coordinates[non_zero, 1],
            c=colormap(normalize(charge_gate[non_zero])), s=5)

plt.ylabel('y(nm)')
plt.xlabel('x(nm)')
cbar = plt.colorbar(scalarmappaple)
cbar.set_label(r'Charge', rotation=270, labelpad=20)

plt.show()

##############

def show_cells(pattern, shape_all, gate_p, gate_n, dopant_1, dopant_2=None):
    pts_all = pattern(pattern.inside(shape_all))
    pts_gate = pattern(pattern.inside(gate_n|gate_p))
    pts_1 = pattern(pattern.inside(dopant_1))

    plt.plot(pts_all[:, 0], pts_all[:, 1], 'y.')
    plt.plot(pts_gate[:, 0], pts_gate[:, 1], 'k.')
    plt.plot(pts_1[:, 0], pts_1[:, 1], 'b.')
    if dopant_2!=None:
        pts_2 = pattern(pattern.inside(dopant_2))
        plt.plot(pts_2[:, 0], pts_2[:, 1], 'r.')

def show_values(val, index, value_tag, title, pp_pro, non_zero=False):

    coordinates = pp_pro.mesh_inst.coordinates(index)

    normalize = mpl.colors.Normalize(vmin=val.min(), vmax=val.max())
    colormap = plt.cm.viridis
    scalarmappaple = plt.cm.ScalarMappable(norm=normalize, cmap=colormap)
    scalarmappaple.set_array(len(val))

    if non_zero:
        non_zero = val != 0.0
        plt.scatter(coordinates[non_zero, 0], coordinates[non_zero, 1],
            c=colormap(normalize(val[non_zero])), s=5)
    else:
        plt.scatter(coordinates[:, 0], coordinates[:, 1],
            c=colormap(normalize(val)), s=5)

    plt.title(title)
    plt.ylabel('y(nm)')
    plt.xlabel('x(nm)')
    cbar = plt.colorbar(scalarmappaple)
    cbar.set_label(value_tag, rotation=270, labelpad=20)

    plt.show()

pp_builder.reset_mesh()
pp_builder.reset_regions()

shape_all = mesher.shapes.Box(lower_left=(-60, -60), size=(120, 120))
pp_builder.add_sub_mesh(shape_inst=shape_all, pattern_inst=pattern)

gate_p = mesher.shapes.Box(lower_left=(20,20), size=(40,40)) - mesher.shapes.Ellipsoid.hypersphere(center=(-100,140), radius=40*np.sqrt(18)) - mesher.shapes.Ellipsoid.hypersphere(center=(140,-100), radius=40*np.sqrt(18))
gate_n = mesher.shapes.Box(lower_left=(-60,-60), size=(40,40)) - mesher.shapes.Ellipsoid.hypersphere(center=(100,-140), radius=40*np.sqrt(18)) - mesher.shapes.Ellipsoid.hypersphere(center=(-140,100), radius=40*np.sqrt(18))

dopant_1 = mesher.shapes.Ellipsoid.hypersphere(center=(0,0), radius=20) - mesher.shapes.Ellipsoid.hypersphere(center=(0,0), radius=16) - mesher.shapes.Box(lower_left=(-5,-60), size=(10,120)) - mesher.shapes.Box(lower_left=(-60,-5), size=(120,10))
dopant_2 = mesher.shapes.Ellipsoid.hypersphere(center=(0,0), radius=np.pi)
dopant = dopant_1|dopant_2


pp_builder.set_neumann(shape=dopant, setup_tag='Dopant')
pp_builder.set_dirichlet(shape=gate_p, setup_tag='positive gate')
pp_builder.set_dirichlet(shape=gate_n, setup_tag='negative gate')
pp_builder.set_relative_permittivity(val=12, shape_inst=dopant)
pp_problem_3 = pp_builder.finalized(parallelize=True)

sv_volt_p = pp_problem_3.sparse_vector(val=10, shape_inst=gate_p)
sv_volt_n = pp_problem_3.sparse_vector(val=0, shape_inst=gate_n)
sv_volt = sv_volt_p
sv_volt.extend(sv_volt_n)

sv_charge = pp_problem_3.sparse_vector(val=-1e15*1e-18*5, shape_inst=dopant_1, density_to_charge=True)
sv_charge.extend(pp_problem_3.sparse_vector(val=1e15*1e-18*5, shape_inst=dopant_2, density_to_charge=True))

volt, charge = pp_problem_3.solve(voltage=sv_volt, charge_electrons=sv_charge, method='scipy')

charge_all, idx_all = pp_problem_3.read_sparse_vector(
    sparse_vector_inst=charge, shape_inst=shape_all,
    return_indices=True)

voltage_all_2, idx_v_2 = pp_problem_3.read_sparse_vector(
    sparse_vector_inst=volt, shape_inst=shape_all - (gate_p|gate_n),
    return_indices=True)

charge_die, idx_die = pp_problem_3.read_sparse_vector(
    sparse_vector_inst=charge, shape_inst=dopant,
    return_indices=True)

charge_gate, idx_gate = pp_problem_3.read_sparse_vector(
    sparse_vector_inst=charge, shape_inst=(gate_p|gate_n),
    return_indices=True)

show_values(voltage_all_2, idx_v_2,
            'Voltage $(V)$', ' The structure ', pp_pro=pp_problem_3)
