pescado.poisson.builder
====================================================

class ProblemBuilder
-------------------------------
    Builds a Poisson problem by constructing the Mesh; defining the Dirichlet,
    Neumann, Mixed and Helmholtz and constructing the capacitance matrix.

.. autoclass:: pescado.poisson.builder.ProblemBuilder
    :members:
