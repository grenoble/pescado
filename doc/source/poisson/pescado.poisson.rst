pescado.poisson
========================================

.. toctree::
    :maxdepth: 5

    pescado.poisson.builder
    pescado.poisson.problem
    pescado.poisson.solver
