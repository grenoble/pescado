pescado.poisson.solver
==========================================================================


method solve_scipy
-------------------------------
    Uses scipy.linal.spsolve

.. autoclass:: pescado.poisson.solver.solve_scipy
    :members:


class Mumps
-------------------------------
    Uses kwant.linalg.mumps

.. autoclass:: pescado.poisson.solver.Mumps
    :members:
