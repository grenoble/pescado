pescado.poisson.problem
====================================================

class Problem
-------------------------------
    Poisson problem constructed by an instance of ProblemBuilder. Here
    one can freeze the Mixed region configuration (into Dirichlet / Neumann /
    Helmholtz) and set the voltage / charge / helmholtz density inputs to solve
    the problem.

.. autoclass:: pescado.poisson.problem.Problem
    :members:


class LinearProblem
-------------------------------
    Defines the Linear problem solved in poisson.Problem.

.. autoclass:: pescado.poisson.problem.LinearProblem
    :members:
