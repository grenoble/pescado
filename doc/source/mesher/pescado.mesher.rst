pescado.mesher
========================================

.. toctree::
    :maxdepth: 5

    pescado.mesher.shapes
    pescado.mesher.patterns
    pescado.mesher.mesh
    pescado.mesher.assembler
    pescado.mesher.voronoi
