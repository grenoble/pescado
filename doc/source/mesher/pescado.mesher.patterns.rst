pescado.mesher.patterns - Patterns Object
========================================


ABC class Patterns - superclass
-----------------------------

.. autoclass:: pescado.mesher.patterns.Pattern
    :members:

class Finite - Pattern subclass
-------------------------------
    Generates a Finite pattern from a given set of coordinates.
    Can be used to generate irregular / regular finite patterns.

.. autoclass:: pescado.mesher.patterns.Finite
    :members:


class Rectangular - Pattern subclass
-------------------------------
    Generates a Rectangular infinite pattern

.. autoclass:: pescado.mesher.patterns.Rectangular
    :members:
