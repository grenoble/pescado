pescado.mesher.shapes - Shape object
========================================


ABC class Shape - superclass
-----------------------------

.. autoclass:: pescado.mesher.shapes.Shape
    :members:

class General - Shape subclass
-------------------------------
    Generates a Shape from any given function.

.. autoclass:: pescado.mesher.shapes.General
    :members:


class Box - Shape subclass
-------------------------------
    Generates a Box

.. autoclass:: pescado.mesher.shapes.Box
    :members:


class Ellipsoid - Shape subclass
-------------------------------
    Generates an Ellipsoid

.. autoclass:: pescado.mesher.shapes.Ellipsoid
    :members:


class Delaunay - Shape subclass
-------------------------------
    Generates an instance of Shape from a given set of
    from a set of vertices by performing Delaunay triangulation.

.. autoclass:: pescado.mesher.shapes.Delaunay
    :members:
