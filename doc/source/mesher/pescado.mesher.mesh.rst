pescado.mesher.mesh - Mesh and Builder Object
==============================================

class Builder
-------------------------------
    Builds a Mesh by assembling sub_meshes. Each sub_mesh is an instance
    of pescado.mesher.patterns.Pattern paired to an instance of
    pescado.mesher.shapes.Shape.

.. autoclass:: pescado.mesher.mesh.Builder
    :members:

class Mesh
-------------------------------
    Generates a finite volume mesh Mesh

.. autoclass:: pescado.mesher.mesh.Mesh
    :members:

method generate
-------------------------------
    Generates a mesh by sequentially adding sub_meshes. Each sub_mesh added
    takes preference over previous sub_meshes occupying the same region in space. Can be used to progressively refine a mesh.

.. autoclass:: pescado.mesher.mesh.generate
    :members:

method validate_mesh_continuity
--------------------------------
    Tests whether the local voronoi cell in Mesh forms a continuous pattern or not. That is, looks for holes in the Mesh.

.. autoclass:: pescado.mesher.mesh.validate_mesh_continuity
    :members:

method plot_2d_finalized_mesh
-------------------------------
    Plots a 2 dimensional instance of Mesh

.. autoclass:: pescado.mesher.mesh.plot_2d_finalized_mesh
    :members:
