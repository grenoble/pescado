pescado.tools.sparse_vector
====================================================

class SparseVector
-----------------------------
Container. Each element of a SparseVector has a float and an integer associated to it. The float is the value and the integer the index of element. Addition
operations between instances of SparseVector is possible. Multiplication of
the value by a scalar as weel.


.. autoclass:: pescado.tools.sparse_vector.SparseVector
    :members:
