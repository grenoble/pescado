Core modules
============

.. toctree::
    :maxdepth: 2

    mesher/pescado.mesher
    poisson/pescado.poisson
    tools/pescado.tools
