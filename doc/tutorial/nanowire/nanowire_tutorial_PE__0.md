---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.14.5
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

```python
#from IPython.display import Latex

import numpy as np
import copy
import matplotlib.pyplot as plt
import matplotlib as mpl

from pescado import tools
from pescado import poisson
from pescado.mesher import shapes
from pescado.mesher import patterns

```

#  Nanowire Tutorial - Poisson Equation - 0


### In this tutorial we will solve the Poisson equation, i.e.

![sphere profile](Eq_11_.png)

for the following geometry :

![sphere profile](nanowire.png)


### In principle, using the PESCADO solver can be devided into four steps :

    1) Defining the geometry in the continuum;

    2) Defining the meshing pattern;

    3) Building the mesh and defining the status of each point (fixed charge - Neumann  or fixed voltage - Dirichlet);

    4) Setting the values of the boundary conditions for the poisson problem. That is, attributing voltage values for the Dirichlet sites and charge values for the Neuamnn sites;

    5) Calling the solver.

    Repeat steps 4 and 5 for different values of boundary conditions, e.g. charge distributions



### Helper functions


### Function returning True if a coordinate is inside a hexagon of "side" and centered around "center".

```python
def inside_hexagon(side, center=None):

    a = (np.cos(np.pi / 3) / np.sin(np.pi / 3))
    b = side * (np.cos(np.pi / 3) + 1/2)
    c = side * np.sin(np.pi / 3)

    if center is None:
        center = np.array([0, 0])

    hexagon_equation = np.array([[a, -1, b], [-a, -1, b],
                                 [-a, 1, b], [a, 1, b],
                                 [1, 0, c], [-1, 0, c]]).T
    def fun(coord):
        centered_coord = np.hstack([coord - center, np.ones(len(coord))[:, None]])

        cond = (np.matmul(centered_coord, hexagon_equation) > 0)

        return np.all(cond, axis=1)
    return fun
```

### Plotting the Shapes

```python
def plot_shape(shapes, colors, grid_bbox):

    grid = tools.meshing.grid(bbox=grid_bbox,
    step = [1, ] * 2)

    plt.plot(grid[:, 0], grid[:, 1], 'm.')

    for shape, color in zip(shapes, colors):
        plt.plot(grid[shape(grid), 0], grid[shape(grid), 1], '{0}.'.format(color))

    plt.xlabel('x (nm)')
    plt.ylabel('y (nm)')
```

### Plotting the solution of the Poisson equation

```python
def plot_map(val, coord, colormap=None, cbar_label=None):

    normalize = mpl.colors.Normalize(
        vmin=-1 * np.abs(val).max(),
        vmax=1 * np.abs(val).max())

    if colormap is None:
        colormap = plt.cm.viridis_r

    scalarmappaple = plt.cm.ScalarMappable(
        norm=normalize, cmap=colormap)

    scalarmappaple.set_array(len(val))

    plt.scatter(coord[:, 0], coord[:, 1],
                c=colormap(normalize(val)), s=10)

    cbar = plt.colorbar(scalarmappaple)

    plt.ylim([-40, 30])
    plt.xlabel('x(nm)')
    plt.ylabel('y(nm)')

    if cbar_label is not None:
        cbar.set_label(cbar_label)
```

## The entire code representing all the 5 steps :


```python
############## 0. Define the geometry parameters ###############

side_nanowire = 20
side_top_gate = 20
center_top_gate = np.array([0, 10])

die_thickness = 0.5 * side_nanowire
bkg_thickness = 0.3 * side_nanowire

system_size = 10 * side_nanowire

origin_die = -side_nanowire * (1/2 + np.cos(np.pi / 3)) - die_thickness
origin_bkg = origin_die - bkg_thickness


############## 1. Define the geometry regions ###############

## Nanowire
nanowire = shapes.General(
    func=inside_hexagon(side=side_nanowire), #Function defining an hexagonal region
    bbox=np.array([[-1, -1], [1, 1]]) * side_nanowire)

## top_gate
top_gate = shapes.General(
    func=inside_hexagon(side=side_top_gate, center=center_top_gate),
    bbox=np.array([[-1, -1], [1, 1]]) * (side_top_gate + center_top_gate))

top_gate = top_gate - nanowire

## Back_gate
back_gate = shapes.Box(lower_left=[-system_size / 2, origin_bkg],
                       size=[system_size, bkg_thickness])

## Dielectric
dielectric = shapes.Box(lower_left=[-system_size / 2, origin_die],
                        size=[system_size, die_thickness])

dielectric = (dielectric - (nanowire | back_gate))

## Air
air = shapes.Box(lower_left=[-system_size / 2, origin_bkg], size=[system_size, ] * 2)

# Function to visualize the shapes
plot_shape(shapes=[air, top_gate, nanowire, dielectric, back_gate],
           colors=['w', 'g', 'r', 'y', 'k'],
           grid_bbox=np.array([[-5.5, -3], [5.5, 8]]) * side_nanowire)

plt.show()


############## 2. Define the meshing pattern ###############


# Coarse step of 4
coarse_pattern = patterns.Rectangular.constant(element_size=[4, 4])

# Fine step of 1
fine_pattern = patterns.Rectangular.constant(element_size=[1, 1])

fine = shapes.Box(lower_left=[-side_nanowire * 2, origin_bkg],
                  size=[side_nanowire * 4, side_nanowire * 6])

############## 3. Build the Poisson Problem ###############


pp_builder = poisson.ProblemBuilder()

## Build the mesh

pp_builder.initialize_mesh(simulation_region=air, pattern=coarse_pattern)
pp_builder.refine_mesh(region=fine, pattern=fine_pattern)

## Set the site types (Neumann by default)

pp_builder.set_dirichlet(region=top_gate, setup_tag='top_gate')
pp_builder.set_dirichlet(region=back_gate, setup_tag='back_gate')

## Set the relative dielectric permittivity (1 by default)

pp_builder.set_relative_permittivity(val=8, region=dielectric)
pp_builder.set_relative_permittivity(val=5.23, region=nanowire)
pp_builder.set_relative_permittivity(val=1e4, region=top_gate | back_gate)

## Construct the capacitance matrix
pp_problem = pp_builder.finalized(parallelize=True)


############## 4. Solve the Poisson Problem ###############

V_back = 1
V_top = -0.8

# Charge values set to zero by default
voltage_sv = pp_problem.sparse_vector(val=V_back, name='back_gate')
voltage_sv.extend(pp_problem.sparse_vector(val=V_top, name='top_gate'))

voltage_res, charge_res = pp_problem.solve(voltage=voltage_sv)


############## 4". Recover the data in the finer mesh and plot it ###############


charge_val, index = pp_problem.read_sparse_vector(
    sparse_vector_inst=charge_res, region=fine, return_indices=True)

voltage_val = pp_problem.read_sparse_vector(
    sparse_vector_inst=voltage_res, region=fine)

coord = pp_problem.coordinates[index]

# Function to plot a 2D colormap for a numpy array
plot_map(val=voltage_val, coord=coord, cbar_label='Voltage (V)')

plt.show()

plot_map(val=charge_val, coord=coord, colormap=plt.cm.PuOr, cbar_label='Charge per cell')

plt.show()

```
