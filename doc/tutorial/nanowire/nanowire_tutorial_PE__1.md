---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.14.5
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

```python
#from IPython.display import Latex

import numpy as np
import copy
import matplotlib.pyplot as plt
import matplotlib as mpl

from pescado import tools
from pescado import poisson
from pescado.mesher import shapes
from pescado.mesher import patterns

```

#  Nanowire Tutorial - Poisson Equation - 1


### In this tutorial we will go into more detail ont how to define the following geometry using shapes :

![sphere profile](nanowire.png)


## 1. Defining the sample geometry

Constructing the sample geometry means defining the different regions composing the system to be solved. It is "abstract" in the sense no coordinate value is stored in the memory. Only functions defining the contour of each region are stored. For the case of the nanowire, we have :

        a) The top gate;
        b) the back gate;
        c) the nanowire;
        d) the dielectric underneath the nanowire;
        e) the air.

Hence we will define six functions that define the contour of each region in the previous list. This is done by the intermediary of pescado.mesher.shapes, where each function is an instance of pescado.mesher.shapes.Shape.

Each instance of Shape behaves as a function that :

    i) Takes as input a set of coordinates

    ii) Returns a boolean array, where the element "i" is true iff the coordinate "i" is inside the contour defined by the shape


### 1.0 The code nescessary for this step is :

```python
def plot_shape(shapes, colors, grid_bbox):

    grid = tools.meshing.grid(bbox=grid_bbox,
    step = [1, ] * 2)

    plt.plot(grid[:, 0], grid[:, 1], 'm.')

    for shape, color in zip(shapes, colors):
        plt.plot(grid[shape(grid), 0], grid[shape(grid), 1], '{0}.'.format(color))

    plt.xlabel('x (nm)')
    plt.ylabel('y (nm)')
```

```python
def inside_hexagon(side, center=None):

    a = (np.cos(np.pi / 3) / np.sin(np.pi / 3))
    b = side * (np.cos(np.pi / 3) + 1/2)
    c = side * np.sin(np.pi / 3)

    if center is None:
        center = np.array([0, 0])

    hexagon_equation = np.array([[a, -1, b], [-a, -1, b],
                                 [-a, 1, b], [a, 1, b],
                                 [1, 0, c], [-1, 0, c]]).T
    def fun(coord):
        centered_coord = np.hstack([coord - center, np.ones(len(coord))[:, None]])

        cond = (np.matmul(centered_coord, hexagon_equation) > 0)

        return np.all(cond, axis=1)
    return fun

############## 0. Define the geometry parameters ###############

side_nanowire = 20
side_top_gate = 20
center_top_gate = np.array([0, 10])

die_thickness = 0.5 * side_nanowire
bkg_thickness = 0.3 * side_nanowire

system_size = 10 * side_nanowire

origin_die = -side_nanowire * (1/2 + np.cos(np.pi / 3)) - die_thickness
origin_bkg = origin_die - bkg_thickness

############## 1. Define the geometry regions ###############


## Nanowire
nanowire = shapes.General(
    func=inside_hexagon(side=side_nanowire), #Function defining an hexagonal region
    bbox=np.array([[-1, -1], [1, 1]]) * side_nanowire)

## top_gate
top_gate = shapes.General(
    func=inside_hexagon(side=side_top_gate, center=center_top_gate),
    bbox=np.array([[-1, -1], [1, 1]]) * (side_top_gate + center_top_gate))

top_gate = top_gate - nanowire

## Back_gate
back_gate = shapes.Box(lower_left=[-system_size / 2, origin_bkg],
                       size=[system_size, bkg_thickness])

## Dielectric
dielectric = shapes.Box(lower_left=[-system_size / 2, origin_die],
                        size=[system_size, die_thickness])

dielectric = (dielectric - (nanowire | back_gate))


## Air
air = shapes.Box(lower_left=[-system_size / 2, origin_bkg], size=[system_size, ] * 2)


# Function to visualize the shapes
plot_shape(shapes=[air, top_gate, nanowire, dielectric, back_gate],
           colors=['w', 'g', 'r', 'y', 'k'],
           grid_bbox=np.array([[-5.5, -3], [5.5, 8]]) * side_nanowire)

plt.show()
```

### 1.1 Defining a region using shapes.General

General is the simplest subclass of the Shapes family. And it is also a good example of how the Shapes subclass works.

    The shapes.General takes a "func" and a "bbox" as input :

        i) The "func" is a function that specifies the region in space. It takes as input a "coordinate" and returns a boolean array where the element "i" is True if the coordinate "i" is inside the region defined by the "func".

        ii) The "bbox" defines a bounding box that contains the region defined by "func"

In the following cells we will creat the shapes.General defining :

    i) The nanowire region;
    ii) The top gate region.



### We start by writing the equation defining  hexagon of "side" and translated by "translate", i.e.

```python
side = 20
center = np.array([0, 0])

a = (np.cos(np.pi / 3) / np.sin(np.pi / 3))
b = side * (np.cos(np.pi / 3) + 1/2)
c = side * np.sin(np.pi / 3)

hexagon_equation = np.array([[a, -1, b], [-a, -1, b],
                             [-a, 1, b], [a, 1, b],
                             [1, 0, c], [-1, 0, c]]).T
```

### Hence we can define the following function to verify if a given "coordinate" is inside the hexagon

```python
def fun(coordinate):
    centered_coord = np.hstack([coordinate - center, np.ones(len(coord))[:, None]])

    cond = (np.matmul(centered_coord, hexagon_equation) > 0)

    return np.all(cond, axis=1)
```

### To makes things easier, we define the "inside_hexagon" method that generates "fun" for a given hexagon of "side" and centered around "center".

```python
def inside_hexagon(side, center=None):

    a = (np.cos(np.pi / 3) / np.sin(np.pi / 3))
    b = side * (np.cos(np.pi / 3) + 1/2)
    c = side * np.sin(np.pi / 3)

    if center is None:
        center = np.array([0, 0])

    hexagon_equation = np.array([[a, -1, b], [-a, -1, b],
                                 [-a, 1, b], [a, 1, b],
                                 [1, 0, c], [-1, 0, c]]).T
    def fun(coord):
        centered_coord = np.hstack([coord - center, np.ones(len(coord))[:, None]])

        cond = (np.matmul(centered_coord, hexagon_equation) > 0)

        return np.all(cond, axis=1)
    return fun
```

### We then define the functions for the nanowire and the top gate respectively

```python
side_nanowire = 20
side_top_gate = 20
center_top_gate = np.array([0, 10])

fun_nanowire = inside_hexagon(side=side_nanowire)

fun_top_gate = inside_hexagon(side=side_nanowire, center=center_top_gate)

grid = tools.meshing.grid(bbox=np.array(
    [[-1.25, -1], [1.25, 1.5]]) * side_nanowire,
    step = [1, ] * 2)

plt.plot(grid[:, 0], grid[:, 1], 'k.')
plt.plot(grid[fun_top_gate(grid), 0], grid[fun_top_gate(grid), 1], 'g.')
plt.plot(grid[fun_nanowire(grid), 0], grid[fun_nanowire(grid), 1], 'r.')


```

### We can then define the shapes.General

```python
nanowire = shapes.General(
    func=fun_nanowire,
    bbox=np.array([[-1, -1], [1, 1]]) * side_nanowire)

top_gate = shapes.General(
    func=fun_top_gate,
    bbox=np.array([[-1, -1], [1, 1]]) * (side_top_gate + center_top_gate))

grid = tools.meshing.grid(bbox=np.array(
    [[-1.25, -1], [1.25, 1.5]]) * side_nanowire,
    step = [1, ] * 2)

plt.plot(grid[:, 0], grid[:, 1], 'k.')
plt.plot(grid[top_gate(grid), 0], grid[top_gate(grid), 1], 'go')
plt.plot(grid[nanowire(grid), 0], grid[nanowire(grid), 1], 'rx')

```

### As one can see, the top_gate and the nanowire regions are intersecting. This is not allowed

In order to fix this we will apply logical operations between instances of the Shapes class, that is, for this case, "top_gate - nanowire".

```python
top_gate = top_gate - nanowire
```

```python
plt.plot(grid[:, 0], grid[:, 1], 'k.')
plt.plot(grid[top_gate(grid), 0], grid[top_gate(grid), 1], 'go')
plt.plot(grid[nanowire(grid), 0], grid[nanowire(grid), 1], 'rx')

```

### As we can see, the top_gate shape no longer intersects with the nanowire shape. In fact, four different logical operations can be performed between instances of Shapes :
    i) or -> | (union between two shapes)
    ii) and -> & (intersection between two shapes)
    iii) sub -> - (substract the second shape from the first)
    iv) xor -> ^ (logical xor operation between two shapes)


### Thus we have :

```python
def inside_hexagon(side, center=None):

    a = (np.cos(np.pi / 3) / np.sin(np.pi / 3))
    b = side * (np.cos(np.pi / 3) + 1/2)

    if center is None:
            center = np.array([0, 0])

    def fun(coord):

        centered_coord = coord - center

        inside = centered_coord[:, 0] > (-side * np.sin(np.pi / 3))
        inside = inside * (centered_coord[:, 0] < (side * np.sin(np.pi / 3)))

        for sign_a, sign_b in [[1, 1], [1, -1], [-1, 1], [-1, -1]]:

            y_val = (centered_coord[:, 0] * a * sign_a
                     + np.ones(len(centered_coord[:, 0])) * b * sign_b)

            inside = inside * ((sign_b * (y_val - centered_coord[:, 1])) > 0)

        return inside
    return fun

side_nanowire = 20
side_top_gate = 20
center_top_gate = np.array([0, 10])

nanowire = shapes.General(
    func=inside_hexagon(side=side_nanowire),
    bbox=np.array([[-1, -1], [1, 1]]) * side_nanowire)

top_gate = shapes.General(
    func=inside_hexagon(side=side_nanowire, center=center_top_gate),
    bbox=np.array([[-1, -1], [1, 1]]) * (side_top_gate + center_top_gate))


top_gate = top_gate - nanowire
```

### 1.2 Define the geometry elements with Rectangular shape

The remaining elements of the system, i.e.

        b) the back gate;
        c) the nanowire;
        d) the dielectric underneath the nanowire;
        e) the air;

can be defined with a rectangular shape, that is shapes.Box. The latter allows one to define a shape without having to define an equation delimiting the shape region. One needs to give the "lower_left" corner and the "size" of the Box.

```python
die_thickness = 0.5 * side_nanowire
bkg_thickness = 0.3 * side_nanowire

system_size = 10 * side_nanowire

origin_die = -side_nanowire * (1/2 + np.cos(np.pi / 3)) - die_thickness
origin_bkg = origin_die - bkg_thickness
```

```python
back_gate = shapes.Box(lower_left=[-system_size / 2, origin_bkg],
                       size=[system_size, bkg_thickness])
```

```python
grid = tools.meshing.grid(bbox=np.array(
    [[-2, -2], [2, 2]]) * side_nanowire,
    step = [1, ] * 2)

plt.plot(grid[:, 0], grid[:, 1], 'k.')
plt.plot(grid[top_gate(grid), 0], grid[top_gate(grid), 1], 'g.')
plt.plot(grid[nanowire(grid), 0], grid[nanowire(grid), 1], 'r.')
plt.plot(grid[back_gate(grid), 0], grid[back_gate(grid), 1], 'y.')
```

```python
dielectric = shapes.Box(lower_left=[-system_size / 2, origin_die],
                        size=[system_size, die_thickness])
```

#### To be certain that dielectric, nanowire and back_gate regions do not intersect :

```python
dielectric = (dielectric - (nanowire | back_gate))
```

```python
grid = tools.meshing.grid(bbox=np.array(
    [[-2, -2], [2, 2]]) * side_nanowire,
    step = [1, ] * 2)

plt.plot(grid[:, 0], grid[:, 1], 'k.')
plt.plot(grid[top_gate(grid), 0], grid[top_gate(grid), 1], 'g.')
plt.plot(grid[nanowire(grid), 0], grid[nanowire(grid), 1], 'r.')
plt.plot(grid[back_gate(grid), 0], grid[back_gate(grid), 1], 'y.')
plt.plot(grid[dielectric(grid), 0], grid[dielectric(grid), 1], 'b.')
```

#### Finally , we define the air region -> that is, the region in space to be simulated

```python
air = shapes.Box(lower_left=[-system_size / 2, origin_bkg], size=[system_size, ] * 2)

```

### We need to then use the logical operations in shape so that none of the regions are intersecting

```python
grid = tools.meshing.grid(bbox=np.array(
    [[-5.5, -3], [5.5, 8]]) * side_nanowire,
    step = [1, ] * 2)

plt.plot(grid[:, 0], grid[:, 1], 'm.')
plt.plot(grid[air(grid), 0], grid[air(grid), 1], 'w.')
plt.plot(grid[top_gate(grid), 0], grid[top_gate(grid), 1], 'g.')
plt.plot(grid[nanowire(grid), 0], grid[nanowire(grid), 1], 'r.')
plt.plot(grid[dielectric(grid), 0], grid[dielectric(grid), 1], 'y.')
plt.plot(grid[back_gate(grid), 0], grid[back_gate(grid), 1], 'k.')
```

### Thus we have :

```python
## Back_gate
back_gate = shapes.Box(lower_left=[-system_size / 2, origin_bkg],
                       size=[system_size, bkg_thickness])

## Dielectric
dielectric = shapes.Box(lower_left=[-system_size / 2, origin_die],
                        size=[system_size, die_thickness])

dielectric = (dielectric - (nanowire | back_gate))

## Air
air = shapes.Box(lower_left=[-system_size / 2, origin_bkg], size=[system_size, ] * 2)


```

### Therefore, the code we have written so far :

```python
def inside_hexagon(side, center=None):

    a = (np.cos(np.pi / 3) / np.sin(np.pi / 3))
    b = side * (np.cos(np.pi / 3) + 1/2)
    c = side * np.sin(np.pi / 3)

    if center is None:
        center = np.array([0, 0])

    hexagon_equation = np.array([[a, -1, b], [-a, -1, b],
                                 [-a, 1, b], [a, 1, b],
                                 [1, 0, c], [-1, 0, c]]).T
    def fun(coord):
        centered_coord = np.hstack([coord - center, np.ones(len(coord))[:, None]])

        cond = (np.matmul(centered_coord, hexagon_equation) > 0)

        return np.all(cond, axis=1)
    return fun

############## 0. Define the geometry parameters ###############

side_nanowire = 20
side_top_gate = 20
center_top_gate = np.array([0, 10])

die_thickness = 0.5 * side_nanowire
bkg_thickness = 0.3 * side_nanowire

system_size = 10 * side_nanowire

origin_die = -side_nanowire * (1/2 + np.cos(np.pi / 3)) - die_thickness
origin_bkg = origin_die - bkg_thickness

############## 1. Define the geometry regions ###############


## Nanowire
nanowire = shapes.General(
    func=inside_hexagon(side=side_nanowire), #Function defining an hexagonal region
    bbox=np.array([[-1, -1], [1, 1]]) * side_nanowire)

## top_gate
top_gate = shapes.General(
    func=inside_hexagon(side=side_top_gate, center=center_top_gate),
    bbox=np.array([[-1, -1], [1, 1]]) * (side_top_gate + center_top_gate))

top_gate = top_gate - nanowire

## Back_gate
back_gate = shapes.Box(lower_left=[-system_size / 2, origin_bkg],
                       size=[system_size, bkg_thickness])

## Dielectric
dielectric = shapes.Box(lower_left=[-system_size / 2, origin_die],
                        size=[system_size, die_thickness])

dielectric = (dielectric - (nanowire | back_gate))


## Air
air = shapes.Box(lower_left=[-system_size / 2, origin_bkg], size=[system_size, ] * 2)

# Function to visualize the shapes
plot_shape(shapes=[air, top_gate, nanowire, dielectric, back_gate],
           colors=['w', 'g', 'r', 'y', 'k'],
           grid_bbox=np.array([[-5.5, -3], [5.5, 8]]) * side_nanowire)

plt.show()
```

```python

```
