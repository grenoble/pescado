---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.14.5
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

```python
#from IPython.display import Latex

import numpy as np
import copy
import matplotlib.pyplot as plt
import matplotlib as mpl

from pescado import tools
from pescado import poisson
from pescado.mesher import shapes
from pescado.mesher import patterns

```

#  Nanowire Tutorial - Poisson Equation - 3 | 4


## In this tutorial we will construct the Mesh, attribute the site types and solve the Poisson problem


### 1 & 2 - First run the previous code to define the shapes / patterns we use

```python
def inside_hexagon(side, center=None):

    a = (np.cos(np.pi / 3) / np.sin(np.pi / 3))
    b = side * (np.cos(np.pi / 3) + 1/2)
    c = side * np.sin(np.pi / 3)

    if center is None:
        center = np.array([0, 0])

    hexagon_equation = np.array([[a, -1, b], [-a, -1, b],
                                 [-a, 1, b], [a, 1, b],
                                 [1, 0, c], [-1, 0, c]]).T
    def fun(coord):
        centered_coord = np.hstack([coord - center, np.ones(len(coord))[:, None]])

        cond = (np.matmul(centered_coord, hexagon_equation) > 0)

        return np.all(cond, axis=1)
    return fun
```

```python
def plot_shape(shapes, colors, grid_bbox):

    grid = tools.meshing.grid(bbox=grid_bbox,
    step = [1, ] * 2)

    plt.plot(grid[:, 0], grid[:, 1], 'm.')

    for shape, color in zip(shapes, colors):
        plt.plot(grid[shape(grid), 0], grid[shape(grid), 1], '{0}.'.format(color))

    plt.xlabel('x (nm)')
    plt.ylabel('y (nm)')
```

```python
############## 0. Define the geometry parameters ###############

side_nanowire = 20
side_top_gate = 20
center_top_gate = np.array([0, 10])

die_thickness = 0.5 * side_nanowire
bkg_thickness = 0.3 * side_nanowire

system_size = 10 * side_nanowire

origin_die = -side_nanowire * (1/2 + np.cos(np.pi / 3)) - die_thickness
origin_bkg = origin_die - bkg_thickness


############## 1. Define the geometry regions ###############


## Nanowire
nanowire = shapes.General(
    func=inside_hexagon(side=side_nanowire), #Function defining an hexagonal region
    bbox=np.array([[-1, -1], [1, 1]]) * side_nanowire)

## top_gate
top_gate = shapes.General(
    func=inside_hexagon(side=side_top_gate, center=center_top_gate),
    bbox=np.array([[-1, -1], [1, 1]]) * (side_top_gate + center_top_gate))

top_gate = top_gate - nanowire

## Back_gate
back_gate = shapes.Box(lower_left=[-system_size / 2, origin_bkg],
                       size=[system_size, bkg_thickness])

## Dielectric
dielectric = shapes.Box(lower_left=[-system_size / 2, origin_die],
                        size=[system_size, die_thickness])

dielectric = (dielectric - (nanowire | back_gate))

## Air
air = shapes.Box(lower_left=[-system_size / 2, origin_bkg], size=[system_size, ] * 2)


# Function to visualize the shapes
plot_shape(shapes=[air, top_gate, nanowire, dielectric, back_gate],
           colors=['w', 'g', 'r', 'y', 'k'],
           grid_bbox=np.array([[-5.5, -3], [5.5, 8]]) * side_nanowire)

plt.show()


############## 2. Define the meshing pattern ###############


# Coarse step of 4
coarse_pattern = patterns.Rectangular.constant(element_size=[4, 4])

# Fine step of 1
fine_pattern = patterns.Rectangular.constant(element_size=[1, 1])

fine = shapes.Box(lower_left=[-side_nanowire * 2, origin_bkg],
                  size=[side_nanowire * 4, side_nanowire * 6])
```

## 3. Constructing the Poisson problem

Once we define the Patterns and geometry (tutorials 1 and 2) , we need to assemble it in order to define the discretized poisson problem.

This means :

    i) Building the Mesh using Patterns and Shapes (defined in 2.)

    ii) defining the status of each point (fixed charge - Neumann  or fixed voltage -Dirichlet)

    iii) Defining the dielectric constant


These three steps are handled by an instance of pescado.ProblemBuilder.

It will allow us to construct a capacitance matrix and Linear Problem associated to the system.

```python
pp_builder = poisson.ProblemBuilder() # Initialize it
```

### We then need to add an instance of Pattern and an instance of Shape.

The pattern defines the properties of the mesh, the shape where the mesh is located in space. A couple (pattern, shape) defines a sub_mesh of the ProblemBuilder.

We add a (pattern, shape) element to the ProblemBuilder instance using the ".add_sub_mesh" method.


#### Here we will therefore set the "coarse_pattern" at the region defined by the "coarse" shape

```python
pp_builder.initialize_mesh(simulation_region=air, pattern=coarse_pattern)
```

#### The previous pattern is too coarse for the nanowire region, hence we will refine the mesh around the region defined by the "fine" shape. This is done using the ".refine"  method

```python
pp_builder.refine_mesh(region=fine, pattern=fine_pattern)
```

### Now that the mesh is defined, we need to define the status of each region.

In order to do so we use the methods :

    i) set_dirichlet : where the voltage is fixed
    ii) set_neumann : where the charge is fixed
    iii) set_helhomtz
    iv) set_flexible

This tutorial only covers i) and ii). See .. ... for .. ..

By default a region is set to Neumann, hence we only need to explicitly use set_dirichlet

The set_dirichlet (and all set methods) take as input a shape and a setup_tag. That is :

    set_dirichlet(shape=top_gate, setup_tag='top_gate')

Sets the region defined by "top_gate" to Dirichlet and names it 'top_gate'.

```python
# Neumann by default
pp_builder.set_dirichlet(region=top_gate, setup_tag='top_gate')
pp_builder.set_dirichlet(region=back_gate, setup_tag='back_gate')
```

### Finally, one needs to set the relative permittivity.

The default value is 1. In order to change it one can use the set_relative_permittivity method.

```python

# One by default
pp_builder.set_relative_permittivity(val=8, region=dielectric)
pp_builder.set_relative_permittivity(val=5.23, region=nanowire)
pp_builder.set_relative_permittivity(val=1e4, region=top_gate | back_gate)

#
pp_problem = pp_builder.finalized(parallelize=True)
```

## 4. Fixing the boundary condition and solving the Poisson problem

Once the system has been discretized, we can now fix the voltage and charge values for the, respectively, Dirichlet and Neumann sites.

This is done using the instance of pescado.poisson.Problem, output of .finalize()



```python
V_back = 1
V_top = -0.8

# Charge values set to zero by default
voltage_sv = pp_problem.sparse_vector(val=V_back, name='back_gate')
voltage_sv.extend(pp_problem.sparse_vector(val=V_top, name='top_gate'))

voltage_res, charge_res = pp_problem.solve(voltage=voltage_sv)

```

```python
charge_val, index = pp_problem.read_sparse_vector(
    sparse_vector_inst=charge_res, region=fine, return_indices=True)

voltage_val = pp_problem.read_sparse_vector(
    sparse_vector_inst=voltage_res, region=fine)

coord = pp_problem.coordinates[index]

normalize = mpl.colors.Normalize(
    vmin=-1 * np.abs(voltage_val).max(),
    vmax=1 * np.abs(voltage_val).max())

colormap = plt.cm.viridis_r

scalarmappaple = plt.cm.ScalarMappable(
    norm=normalize, cmap=colormap)

scalarmappaple.set_array(len(voltage_val))

plt.scatter(coord[:, 0], coord[:, 1],
            c=colormap(normalize(voltage_val)), s=350)

cbar = plt.colorbar(scalarmappaple)

plt.ylim([-40, 30])
plt.show()
```
