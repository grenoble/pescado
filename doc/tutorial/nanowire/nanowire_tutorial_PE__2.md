---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.14.5
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

```python
#from IPython.display import Latex

import numpy as np
import copy
import matplotlib.pyplot as plt
import matplotlib as mpl

from pescado import tools
from pescado import poisson
from pescado.mesher import shapes
from pescado.mesher import patterns

```

#  Nanowire Tutorial - Poisson Equation - 2


### In this tutorial we will show how to define the Mesh for the geometry :

![sphere profile](nanowire.png)

```python
def inside_hexagon(side, center=None):

    a = (np.cos(np.pi / 3) / np.sin(np.pi / 3))
    b = side * (np.cos(np.pi / 3) + 1/2)
    c = side * np.sin(np.pi / 3)

    if center is None:
        center = np.array([0, 0])

    hexagon_equation = np.array([[a, -1, b], [-a, -1, b],
                                 [-a, 1, b], [a, 1, b],
                                 [1, 0, c], [-1, 0, c]]).T
    def fun(coord):
        centered_coord = np.hstack([coord - center, np.ones(len(coord))[:, None]])

        cond = (np.matmul(centered_coord, hexagon_equation) > 0)

        return np.all(cond, axis=1)
    return fun
```

```python
def plot_shape(shapes, colors, grid_bbox):

    grid = tools.meshing.grid(bbox=grid_bbox,
    step = [1, ] * 2)

    plt.plot(grid[:, 0], grid[:, 1], 'm.')

    for shape, color in zip(shapes, colors):
        plt.plot(grid[shape(grid), 0], grid[shape(grid), 1], '{0}.'.format(color))

    plt.xlabel('x (nm)')
    plt.ylabel('y (nm)')
```

## 2. Defining the mesh

We need to build the mesh onto which we can discretize the system defined in "Nanowire_tutorial_PE_1".

The first step is to define the mesh patterns, that is :

    i) The mesh points spacing;
    ii) The finite volume cell for each mesh point;
    iii) The neighbours for each mesh point

This is handled by the pescado.mesher.patterns submodule.

One can have a regular mesh, such that i) and ii) are the same for all points, or a varying mesh. In the latter we can define a finer mesh near the nanowire, and a coarser mesh outside of it. The regions where we will use the coarser and finer mesh are defined by instances of Shapes


```python
side_nanowire = 20
side_top_gate = 20
center_top_gate = np.array([0, 10])

die_thickness = 0.5 * side_nanowire
bkg_thickness = 0.3 * side_nanowire

system_size = 10 * side_nanowire

origin_die = -side_nanowire * (1/2 + np.cos(np.pi / 3)) - die_thickness
origin_bkg = origin_die - bkg_thickness

air = shapes.Box(lower_left=[-system_size / 2, origin_bkg], size=[system_size, ] * 2)

```

#### For simplicity we will make a regular Rectangular mesh for both coarse and fine mesh regions

```python
# Coarse step of 4
coarse_pattern = patterns.Rectangular.constant(element_size=[4, 4])

# Fine step of 1
fine_pattern = patterns.Rectangular.constant(element_size=[1, 1])
```

#### We then need to specify where in the system we want to have a "coarse_pattern" and a "fine_pattern"

```python
fine = shapes.Box(lower_left=[-side_nanowire * 2, origin_bkg],
                  size=[side_nanowire * 4, side_nanowire * 6])

```

#### To obtain the pattern points, i.e. the center of the finite volume cell, one needs an instance of Shape, and the method "inside" of the pattern instance, e.g.

```python
fine_pts = fine_pattern.inside(fine)
```

#### The points are not in real space coordinates, but are indexed using an indexing unique to each pattern, to recover the real space coordinate, one needs to call the pattern using as parameter the pattern specific index

```python
fine_coord = fine_pattern(fine_pts)
```

#### Hence we can  visualize the mesh points

```python
coarse_coord = coarse_pattern(coarse_pattern.inside(air))

plt.plot(coarse_coord[:, 0], coarse_coord[:, 1], 'r.')
plt.plot(fine_coord[:, 0], fine_coord[:, 1], 'g.')
```

### Therefore, the code we have written so far :

```python
############## 0. Define the geometry parameters ###############

side_nanowire = 20
side_top_gate = 20
center_top_gate = np.array([0, 10])

die_thickness = 0.5 * side_nanowire
bkg_thickness = 0.3 * side_nanowire

system_size = 10 * side_nanowire

origin_die = -side_nanowire * (1/2 + np.cos(np.pi / 3)) - die_thickness
origin_bkg = origin_die - bkg_thickness


############## 1. Define the geometry regions ###############


## Nanowire
nanowire = shapes.General(
    func=inside_hexagon(side=side_nanowire), #Function defining an hexagonal region
    bbox=np.array([[-1, -1], [1, 1]]) * side_nanowire)

## top_gate
top_gate = shapes.General(
    func=inside_hexagon(side=side_top_gate, center=center_top_gate),
    bbox=np.array([[-1, -1], [1, 1]]) * (side_top_gate + center_top_gate))

top_gate = top_gate - nanowire

## Back_gate
back_gate = shapes.Box(lower_left=[-system_size / 2, origin_bkg],
                       size=[system_size, bkg_thickness])

## Dielectric
dielectric = shapes.Box(lower_left=[-system_size / 2, origin_die],
                        size=[system_size, die_thickness])

dielectric = (dielectric - (nanowire | back_gate))

## Air
air = shapes.Box(lower_left=[-system_size / 2, origin_bkg], size=[system_size, ] * 2)


# Function to visualize the shapes
plot_shape(shapes=[air, top_gate, nanowire, dielectric, back_gate],
           colors=['w', 'g', 'r', 'y', 'k'],
           grid_bbox=np.array([[-5.5, -3], [5.5, 8]]) * side_nanowire)

plt.show()


############## 2. Define the meshing pattern ###############


# Coarse step of 4
coarse_pattern = patterns.Rectangular.constant(element_size=[4, 4])

# Fine step of 1
fine_pattern = patterns.Rectangular.constant(element_size=[1, 1])

fine = shapes.Box(lower_left=[-side_nanowire * 2, origin_bkg],
                  size=[side_nanowire * 4, side_nanowire * 6])

```
