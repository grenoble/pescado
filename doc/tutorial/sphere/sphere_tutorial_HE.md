---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.14.5
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

```python
#from IPython.display import Latex

from collections import Sequence

import numpy as np

import copy
import matplotlib.pyplot as plt
import matplotlib as mpl

from scipy import constants
from scipy.optimize import approx_fprime

from pescado import tools
from pescado import poisson
from pescado.mesher import shapes
from pescado.mesher import patterns
```

# Sphere Tutorial - Helmholtz Equation

<!-- #region -->
### In this tutorial we will solve the linear Helmholtz equation, i.e.


![equation_pois](Eq_5.png)

For the following geometry

![sphere profile](profile_sphere__.png)
<!-- #endregion -->

### In principle, using the PESCADO solver can be devided into four steps :

    1) Defining the geometry in the continuum;

    2) Defining the meshing pattern;

    3) Building the mesh and defining the status of each region (fixed charge - Neumann  or fixed voltage - Dirichlet);

    4) Setting the values of the boundary conditions for the poisson problem. That is, attributing voltage values for the Dirichlet sites and charge values for the Neuamnn sites;

    5) Calling the solver.

    Repeat steps 4 and 5 for different values of boundary conditions, e.g. charge distributions


## The entire code representing all the 5 steps :

```python
############## 0. Define the geometry parameters ###############

system_size, bottom_gate, radius = [100, 50, 30]

############## 1. Define the geometry regions ###############

circ_shape = shapes.General(
    func=lambda x: np.sum(x ** 2, axis=1) <= (radius ** 2),
    bbox=np.array([[-1.1, -1.1], [1.1, 1.1]]) * radius)

gate = shapes.Box(
    lower_left=np.array([-system_size / 2, -bottom_gate]), size=[system_size, 6])

space = shapes.Box(
    lower_left=np.array([-system_size / 2, -bottom_gate]), size=[system_size, ] * 2)

############## 2. Define the meshing pattern ###############

coarse_pattern = patterns.Rectangular.constant(element_size=[1, 1])

############## 3. Build the Poisson Problem ###############

pp_builder = poisson.ProblemBuilder()

pp_builder.initialize_mesh(simulation_region=space, pattern=coarse_pattern)

## Set the site types (Neumann by default)
pp_builder.set_dirichlet(region=gate, setup_tag='gate')

# Add linear dependance on the potential
pp_builder.set_helmholtz(region=circ_shape, setup_tag='circ') # Set the sites to Helmhotlz

pp_problem = pp_builder.finalized(parallelize=True)

############## 4. Solve the Poisson Problem ###############

voltage_sv = pp_problem.sparse_vector(val=2, region=gate)

# Define the dos for the helmholtz sites
helmholtz_dens = pp_problem.sparse_vector(val=-1e23 * 1e-27, region=circ_shape)


voltage_res, charge_res = pp_problem.solve(
    voltage=voltage_sv, helmholtz_density=helmholtz_dens)

############## 4". Recover the data in the finer mesh and plot it ###############

coord = pp_problem.coordinates

charge = charge_res[np.arange(len(coord))]

charge[pp_problem.helmholtz_indices] += (
    voltage_res[pp_problem.helmholtz_indices] * (-1e23 * 1e-27))

tools.plotting.plot_map(
    val=voltage_res[np.arange(len(coord))], coord=coord, colormap=plt.cm.PuOr, cbar_label='Voltage (V)')
plt.show()

tools.plotting.plot_map(
    val=charge, coord=coord, colormap=plt.cm.PuOr, cbar_label='Charge per cell')
plt.show()
```
