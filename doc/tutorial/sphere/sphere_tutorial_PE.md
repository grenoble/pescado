---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.14.5
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

```python
from IPython.display import Latex

import numpy as np
import copy
import matplotlib.pyplot as plt
import matplotlib as mpl

from pescado import tools
from pescado import poisson
from pescado.mesher import shapes
from pescado.mesher import patterns
```

# Sphere Tutorial - Poisson Equation

<!-- #region -->
### In this tutorial we will solve the Poisson equation, i.e.


![equation_pois](Eq_11_.png)

For the following geometry

![sphere profile](profile_sphere__.png)
<!-- #endregion -->

### In principle, using the PESCADO solver can be devided into four steps :

    1) Defining the geometry in the continuum;

    2) Defining the meshing pattern;

    3) Building the mesh and defining the status of each point (fixed charge - Neumann  or fixed voltage - Dirichlet);

    4) Setting the values of the boundary conditions for the poisson problem. That is, attributing voltage values for the Dirichlet sites and charge values for the Neuamnn sites;

    5) Calling the solver.

    Repeat steps 4 and 5 for different values of boundary conditions, e.g. charge distributions


## The entire code representing all the 5 steps :

```python
############## 0. Define the geometry parameters ###############
size, bg, r = [120, 70, 20]

############## 1. Define the geometry regions ###############
gate = shapes.Box(lower_left=np.array([-size / 2, -bg]), size=[size, 6]) # Defines the region III
circ_gate = shapes.General(
    func=lambda x: np.sum(x ** 2, axis=1) <= (r ** 2),
    bbox=np.array([[-1, -1], [1, 1]]) * r) # Defines the region II
space = shapes.Box(lower_left=np.array([-size / 2, -bg]), size=[size, ] * 2) # Defines the region I

############## 2. Define the meshing pattern ###############
coarse_pattern = patterns.Rectangular.constant(element_size=[1, 1]) # Create a rectangular parttern

############## 3. Build the Poisson Problem ###############
p_builder = poisson.ProblemBuilder() # Initialize the ProblemBuilder instance
p_builder.initialize_mesh(simulation_region=space, pattern=coarse_pattern) # Define the Finite Volume Mesh
p_builder.set_dirichlet(region=gate | circ_gate, setup_tag='dir') # Set the sites in the gates as Dirichlet

p_problem = p_builder.finalized(parallelize=True) # Construct the Mesh / Capacitance Matrix / Linear problem

############## 4. Solve the Poisson Problem ###############
voltage_input = p_problem.sparse_vector(val=1, region=circ_gate) # Define the circle voltage
voltage_input.extend(p_problem.sparse_vector(val=-1, region=gate)) # Define the bottom gate voltage
voltage_res, charge_res = p_problem.solve(voltage=voltage_input) # Solve the problem

############## 4". Recover the data in the finer mesh and plot it ###############
coord = p_problem.coordinates # Recover the mesh coordinates

tools.plotting.plot_map( # Plot the voltage
    val=voltage_res[np.arange(len(coord))], coord=coord, colormap=plt.cm.PuOr, cbar_label='Voltage (V)')
plt.show()

tools.plotting.plot_map( # Plot the charge
    val=charge_res[np.arange(len(coord))], coord=coord, colormap=plt.cm.PuOr, cbar_label='Charge per cell')
plt.show()
```

## Change the voltage values of the gates

```python
voltage_sv = p_problem.sparse_vector(val=1, region=circ_gate)
voltage_sv.extend(p_problem.sparse_vector(val=-2, region=gate))

voltage_res, charge_res = p_problem.solve(voltage=voltage_sv)

coord = p_problem.coordinates # Recover the mesh coordinates

tools.plotting.plot_map( # Plot the voltage
    val=voltage_res[np.arange(len(coord))], coord=coord, colormap=plt.cm.PuOr, cbar_label='Voltage (V)')
plt.show()

tools.plotting.plot_map( # Plot the charge
    val=charge_res[np.arange(len(coord))], coord=coord, colormap=plt.cm.PuOr, cbar_label='Charge per cell')
plt.show()
```

## 1. Define the geometry in the continuum

```python
size, bg, r = [120, 70, 20]

gate = shapes.Box(lower_left=np.array([-size / 2, -bg]), size=[size, 6]) # Defines the region III
circ_gate = shapes.General(
    func=lambda x: np.sum(x ** 2, axis=1) <= (r ** 2),
    bbox=np.array([[-1, -1], [1, 1]]) * r) # Defines the region II
space = shapes.Box(lower_left=np.array([-size / 2, -bg]), size=[size, ] * 2) # Defines the region I

```

## 2. Define the mesh pattern

```python
coarse_pattern = patterns.Rectangular.constant(element_size=[1, 1]) # Create a rectangular parttern
```

## 3. Build the mesh and define the status of each point

```python
p_builder = poisson.ProblemBuilder() # Initialize the ProblemBuilder instance
p_builder.initialize_mesh(simulation_region=space, pattern=coarse_pattern) # Define the Finite Volume Mesh
p_builder.set_dirichlet(region=gate | circ_gate, setup_tag='dir') # Set the sites in the gates as Dirichlet

p_problem = p_builder.finalized(parallelize=True) # Construct the Mesh / Capacitance Matrix / Linear problem

```

## 4. Set the voltage values and solve the problem

```python
voltage_input = p_problem.sparse_vector(val=1, region=circ_gate) # Define the circle voltage
voltage_input.extend(p_problem.sparse_vector(val=-1, region=gate)) # Define the bottom gate voltage
voltage_res, charge_res = p_problem.solve(voltage=voltage_input) # Solve the problem

```

### Plot the results

```python
coord = p_problem.coordinates # Recover the mesh coordinates

tools.plotting.plot_map( # Plot the voltage
    val=voltage_res[np.arange(len(coord))], coord=coord, colormap=plt.cm.PuOr, cbar_label='Voltage (V)')
plt.show()

tools.plotting.plot_map( # Plot the charge
    val=charge_res[np.arange(len(coord))], coord=coord, colormap=plt.cm.PuOr, cbar_label='Charge per cell')
plt.show()
```
