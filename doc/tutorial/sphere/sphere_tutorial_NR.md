---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.14.5
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

```python
#from IPython.display import Latex

from collections import Sequence

import numpy as np

import copy
import matplotlib.pyplot as plt
import matplotlib as mpl

from scipy import constants
from scipy.optimize import approx_fprime

from pescado import tools
from pescado import poisson
from pescado.mesher import shapes
from pescado.mesher import patterns
```

# Sphere Tutorial - Helmholtz Equation

<!-- #region -->
### In this tutorial we will solve the non-linear Helmholtz equation, i.e.


![equation_pois](Eq_3.png)

With the n given as :

![ildos](ildos.png)

For the following geometry

![sphere profile](profile_sphere__.png)
<!-- #endregion -->

## There are three steps into solving the "non-linear Generalized Helmholtz equation"

    i) Defining the Helmholtz problem (c.f. Sphere_tutorial_HE)

    ii) Defining the ILDOS / DOS function

    iii) Solving the Helmholtz problem with an initial guess

    iv) Iterating over the solution of the Helmholtz problem and
    the input from the ILDOS until convergence


## The entire code representing all the 5 steps :


##  Defining the ILDOS Function

```python
mass_eff = 0.067

m_star = constants.electron_mass * mass_eff
q = 1 * constants.elementary_charge
hbar = constants.hbar

dos = ((m_star ** (3/2)) / ((np.pi ** 2) *  (hbar ** 3))) * (4/3) * (q ** (3/2)) * 1e-27
dos = dos / 5e3

delta = 2e-1

def ildos(x):

    if not isinstance(x, (np.ndarray, Sequence)):
        x = np.array([x, ])

    in_gap = np.abs(x) > delta

    charge = np.zeros(len(x))
    charge[in_gap] = (dos
                      * (np.abs(np.abs(x[in_gap]) - delta) ** (3/2))
                      * np.sign(x[in_gap]))

    return charge
```

```python
plt.plot(
    np.linspace(-1.5, 1.5, 1000),
    ildos(np.linspace(-1.5, 1.5, 1000)), 'k-', linewidth=3)

plt.xlabel(r'$U = \mu / e$')
plt.ylabel('Charge per cell')
plt.title('ILDOS')
```

## Defining a function to get the DOS from the ILDOS

One can either define a dos function, or calculate the derivative from the ILDOS function (
using scipy.approx_fprime for instance)

```python
def get_dos_ildos(sites, potential_out, ildos):

    origin_sv = tools.SparseVector(
        indices=sites, values=np.zeros(len(sites)))
    slope_sv = tools.SparseVector(
        indices=sites, values=np.zeros(len(sites)))

    epsilon=1e-6
    for site in sites:

        point = potential_out[site]
        slope_sv[site] = approx_fprime( # Use scipy.approx_fprime to get the derivative of the ILDOS
            xk=np.array(point), f=ildos, epsilon=epsilon)[0]

        origin_sv[site] = (ildos(point) - slope_sv[site] * point)[0]

    return origin_sv, slope_sv
```

## Defining the Helmholtz problem

```python
############## 0. Define the geometry parameters ###############

system_size, bottom_gate, radius = [100, 50, 30]

############## 1. Define the geometry regions ###############

circ_shape = shapes.General(
    func=lambda x: np.sum(x ** 2, axis=1) <= (radius ** 2),
    bbox=np.array([[-1.1, -1.1], [1.1, 1.1]]) * radius)

gate = shapes.Box(
    lower_left=np.array([-system_size / 2, -bottom_gate]), size=[system_size, 6])

space = shapes.Box(
    lower_left=np.array([-system_size / 2, -bottom_gate]), size=[system_size, ] * 2)

############## 2. Define the meshing pattern ###############

coarse_pattern = patterns.Rectangular.constant(element_size=[1, 1])

############## 3. Build the Poisson Problem ###############

p_builder = poisson.ProblemBuilder()

p_builder.initialize_mesh(simulation_region=space, pattern=coarse_pattern)

## Set the site types (Neumann by default)
p_builder.set_dirichlet(region=gate, setup_tag='gate')
p_builder.set_helmholtz(region=circ_shape, setup_tag='circ')

p_problem = p_builder.finalized(parallelize=True)

############## 4. Solve the Poisson Problem ###############

voltage_sv = p_problem.sparse_vector(val=2, region=gate)
helmholtz_dens = p_problem.sparse_vector(val=-1e23 * 1e-27, region=circ_shape)

voltage_res, charge_res = p_problem.solve(
    voltage=voltage_sv, helmholtz_density=helmholtz_dens)

############## 4". Recover the data in the finer mesh and plot it ###############

coord = p_problem.coordinates

charge = charge_res[np.arange(len(coord))]

charge[p_problem.helmholtz_indices] += (
    voltage_res[p_problem.helmholtz_indices] * (-1e23 * 1e-27))

tools.plotting.plot_map(
    val=voltage_res[np.arange(len(coord))], coord=coord, colormap=plt.cm.PuOr, cbar_label='Voltage (V)')
plt.show()

tools.plotting.plot_map(
    val=charge, coord=coord, colormap=plt.cm.PuOr, cbar_label='Charge per cell')
plt.show()
```

## Iterating over the Helmholtz / ILDOS

Here we will define a "iterate" function.

It takes as input the voltage result from the  Helmholtz problem defined above. It will then find the new dos / charge values for the Helmholtz sites. That is the values to put in "helmholtz_density" and "charge_density" parameters in the "p_problem.solve()" method;

```python
def iterate(voltage_res, plot=False):

    origin_sv, slope_sv = get_dos_ildos(
        sites=p_problem.helmholtz_indices, potential_out=voltage_res, ildos=ildos)

    ############## 4. Solve the Poisson Problem ###############
    voltage_res, charge_res = p_problem.solve(
        voltage=voltage_sv, helmholtz_density=slope_sv, charge_density=origin_sv)

    ############## 4". Recover the data in the finer mesh and plot it ###############

    coord = p_problem.coordinates
    charge = charge_res[np.arange(len(coord))]
    charge[p_problem.helmholtz_indices] += (
        voltage_res[p_problem.helmholtz_indices] * (-1e23 * 1e-27))

    if plot :
        tools.plotting.plot_map(
            val=voltage_res[np.arange(len(coord))], coord=coord, colormap=plt.cm.PuOr, cbar_label='Voltage (V)')
        plt.show()

        tools.plotting.plot_map(
            val=charge, coord=coord, colormap=plt.cm.PuOr, cbar_label='Charge per cell')
        plt.show()

    return voltage_res, charge_res, charge
```

```python
for i in range(10):

    old_charge = charge

    voltage_res, charge_res, charge = iterate(voltage_res=voltage_res)

    print('Mean charge rrror : {0}'.format(np.mean(np.abs(old_charge - charge))))
```
