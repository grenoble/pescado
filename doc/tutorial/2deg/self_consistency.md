---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.14.5
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

```python
import copy

import numpy as np

from scipy import constants

from pescado.mesher import shapes
from pescado.mesher import patterns
from pescado import poisson
from pescado.self_consistent import problem as sp_problem

import matplotlib.pyplot as plt
```

# In this tutorial we shall solve the Self Consistent Schrodinger Poisson problem in the Thomas Fermi approximation for a 2DEG

<!-- #region -->
# In this tutorial we will solve the non-linear Helmholtz equation, i.e.


![equation_pois](../sphere/Eq_3.png)


### For the following system:

![sim_system](schem_sim.png)

### With the n at the 2DEG given by the function bellow :

<!-- #endregion -->

```python
def disc_ildos(surface):
    dos = (surface * 1e-18) * (constants.m_e * 0.067) \
          / (constants.pi * constants.hbar ** 2) * constants.e
    # The value of A corresponds to a one-dimension unit cell

    coord = np.empty((5, 2), dtype=float)
    coord[:, 0] = np.array([-2, -1, 0, 1, 2])
    coord[:, 1] = np.array([0, 0, 0, dos, 2 * dos])

    return coord
```

## 1: Define the System Geometry

```python
system_width, system_height, system_depth = 400, 300, 300 #700, 176, 1000

gate_width, gate_height, gate_depth = 200, 40, 100 #300, 50, 200

(top_AlGaAs_height, doped_AlGaAs_height,
 middle_AlGaAs_height, gas_height, bottom_AlGaAs_height) = 10, 50, 20, 1, 100 #5, 70, 30, 1, 20

space_dimensions = (1200, 900, 900)#(900, 360, 1300)
```

```python
gate_ll = sum([top_AlGaAs_height, doped_AlGaAs_height, 
               middle_AlGaAs_height, gas_height / 2])

gate = shapes.Box(
    lower_left=np.array([-1 * gate_width / 2, gate_ll]),
    size=[gate_width, gate_height])

AlGaAs_ll = -1 * (bottom_AlGaAs_height + gas_height / 2)
# * 0.995 so the gate sits just above the AlGaAs layer
AlGaAs_height = gate_ll * 0.995 + -1 * (AlGaAs_ll)

AlGaAs = shapes.Box(
    lower_left=np.array([-system_width/2, AlGaAs_ll]),
    size=[system_width, AlGaAs_height])

doped_AlGaAs = shapes.Box(
    lower_left=np.array([-system_width/2, gas_height / 2 + middle_AlGaAs_height]),
    size=[system_width, doped_AlGaAs_height])

gas = shapes.Box(              #0.95 so only one cell is selected
    lower_left=np.array([-system_width/2, -0.95 * gas_height / 2]),
    size=[system_width, 0.95 * gas_height]) 

system = shapes.Box(
    lower_left=np.array([-1 * system_width / 2, -1 * system_height / 2]), 
    size=[system_width, system_height])

space = shapes.Box(
    lower_left=np.array([-1 * space_dimensions[0] / 2, 
                         -1 * space_dimensions[1] / 2]), 
    size=[space_dimensions[0], space_dimensions[1]])
```

## 2: Define the Meshing Patterns

```python
coarse_lattice = [40, 40]
fine_lattice = [10, 5]
gas_lattice = [10, gas_height]

coarse_pattern = patterns.Rectangular.constant(element_size=coarse_lattice)
fine_pattern = patterns.Rectangular.constant(element_size=fine_lattice)
gas_pattern = patterns.Rectangular.constant(element_size=gas_lattice)
```

## 3: Build the Helmholtz Problem

```python
pp_builder = poisson.ProblemBuilder()

# refine pattern at the two DEG
pp_builder.initialize_mesh(simulation_region=space, pattern=coarse_pattern)
pp_builder.refine_mesh(region=system, pattern=fine_pattern)
pp_builder.refine_mesh(region=gas, pattern=gas_pattern, ext_bd=5)

pp_builder.set_dirichlet(region=gate, setup_tag="gate")
pp_builder.set_neumann(region=doped_AlGaAs, setup_tag="dopant")
pp_builder.set_flexible(region=gas, setup_tag="twodeg")

AlGaAs_epsilon = 12.7
# dielectric constant of metals >> dielectric constant of other materials
gate_epsilon = AlGaAs_epsilon * 1e3

# loop on all the regions to assign dielectric constant
for dielec, region_inst in zip([AlGaAs_epsilon, gate_epsilon], 
                               [AlGaAs, gate]):
    pp_builder.set_relative_permittivity(val=dielec, region=region_inst)
    
pp_problem = pp_builder.finalized()
```

### We can check if each region is well defined

```python
def plot_geometry(region, color):
    for sh, clr in zip(region, color):
        coord = pp_problem.points_inside(region=sh)
        plt.plot(pp_problem.coordinates[coord, 0],
                 pp_problem.coordinates[coord, 1],
                 '.', color=clr)
    plt.show()

region_instance = [space, gate, AlGaAs - (doped_AlGaAs | gas), doped_AlGaAs, gas]
color = ['blue', 'orange', 'forestgreen', 'black', 'violet']

plot_geometry(region_instance, color)
plot_geometry(region_instance[1:], color[1:])
plot_geometry(region_instance[-2:], color[-2:])
```

## 4 : Define the Thomas Fermi ILDOS

### Notice to transform it into Helmhotlz Problem units we multiple the Thomas Fermi charge by the 2DEG surface. This is done here using the cell volume - we have a 2D mesh. 

```python
twod_surface = pp_problem.volume[pp_problem.points_inside(region=gas)]

assert np.all(twod_surface == twod_surface[0]), (
    '2DEG voronoi cell not regular')

print('2DEG surface is {0}'.format(twod_surface[0]))

coord_ILDOS = disc_ildos(twod_surface[0])
```

## 5: Define the Self Consistent problem 


### 5.1 : Initialize an instance of 'sp_problem.SchrodingerPoisson' 

```python
sp_problem_dildos = sp_problem.SchrodingerPoisson(
    ildos=coord_ILDOS, poisson_problem_inst=pp_problem)
```

### 5.2 : Initialize a sp_problem_dildos  by filling up the Helmholtz boundary conditions on each site and giving a initial guess for the ILDOS interval

```python
# Initialize poisson input
# m^{-2} to nm^{-2}
volumic_density = 1e15 * 1e-18
dopant_density = pp_problem.sparse_vector(
    val=volumic_density,
    indices=pp_problem.sub_region_index(name='dopant'))

# vector with voltage
vg = -0.07
sv_gate = pp_problem.sparse_vector(val=vg, name='gate')

poisson_problem_input = {'voltage': sv_gate,
                         'charge': dopant_density}

# Put all gas on branch 0
initial_config = pp_problem.sparse_vector(
    val=0, indices=pp_problem.sub_region_index(name='twodeg'), dtype=int)

sp_problem_dildos.initialize(
    initial_guess=copy.deepcopy(initial_config),
    poisson_problem_input=poisson_problem_input,
    return_poisson_output=False)
```

## 6: Solve the sp_problem_dildos 

```python
sp_problem_dildos.iterate(return_poisson_output=False)

# while all sites do not stay on a fixed branch
n_count = 0
while np.any(
        np.abs(sp_problem_dildos.iteration_data[-1]['interval']
               - sp_problem_dildos.iteration_data[-2]['interval']) != 0):
    sp_problem_dildos.iterate(return_poisson_output=False)
    n_count += 1
    print(n_count)
```

## 7: Recover the converged results and plot them

```python
# Take the charge of the last iteration
charge = sp_problem_dildos.charge(iteration=-1)
# Take the potential of the last iteration
potential = sp_problem_dildos.potential(iteration=-1)
```

```python
plt.plot(charge.values, 'r.')
plt.ylabel('Charge at 2DEG')
plt.show()
plt.plot(potential.values, 'k.')
plt.ylabel('Potential at 2DEG')
plt.show()
```
