import copy
import numpy as np

from collections.abc import Sequence

from pescado.tools import SparseVector
from pescado.tools import error
from scipy.optimize import approx_fprime

class NaiveSchrodingerPoisson():
    ''' Solves the ScrodingerPoisson self consistent problem.
    '''

    def __init__(self,
    ildos, poisson_problem_inst, sites_ildos=None,
    potential_precision=1e-6, tolerance=None):
        ''' Returns an instance of SchrodingerPoisson

        Parameters
        -----------

        ildos: function

        poisson_problem_inst: instance of poisson.Problem

        potential_precision: float (default is None)
            Used for continuous ILDOS. It is used for two things :

            i) Step used in scipy.optimize.approx_fprime to calculate the
            tangent from a 'functional_point', i.e. the 'epsilon' parameter

            ii) Precision used to verify if the new potential (solution of
            the PESCADO) is added as a new functional point.

                np.abs(
                    self.functional_pts - possible_pot) > potential_precision

            It can be interpreted as the maximal resolution used to discretize
            the ildos, in case the latter is continuous.

        Returns
        --------
        instance of SchrodingerPoisson

        Note
        -----

        Important parameters:

            self.poisson_input: dict passed to
                poisson_problem_inst.solve()

            self.iteration_data: list containing
                    [(value_type, input), output, interval]

                    !!! TODO: Update this description !!!

                    interval: sparse vector with the ILDOS interval for
                    each quantum site

                    output: poisson.Problem result for each quantum site

                    value_type: sparse vector with type of quantum site
                    (Dirichlet - 0 / Neumann - 1 / Helmholtz - 2)
                    Defined by the ildos

                    input: sparse vector with value of quantum site as defined
                    by the Ildos and used as input for
                    poisson_problem_inst.solve()

        !!! Both self.poisson_input and self.iteration_data reset after !!!
        !!!             self.initialize() is called                     !!!
        '''

        if isinstance(ildos, (tuple, list)):
            self.ildos_list = ildos
        elif callable(ildos):
            self.ildos_list = [ildos, ]
        else:
            raise NotImplementedError(
                ('tuple, list or callable was expected for "ildos" however'
                 + ' {0} was given'.format(ildos)))

        self.sites_ildos = sites_ildos
        self.quantum_functional_list = list()
        self.pp_inst = poisson_problem_inst
        self.potential_precision=potential_precision
        self.poisson_input = None # For initialization purposes

    def initialize(self,
        poisson_problem_input, initial_potential,
        return_poisson_output=False):
        ''' Initialize self consistency

        Parameters
        -----------

        initial_guess: instance of 'pescado.tools.sparsevector.SparseVector'

            If 'self.ildos' is discrete

                Contains the Ildos interval used to initialize the points in
                the self consistent SP problem

            If 'self.ildos' is continuous

                Contains the 'chemical potential' used to initialize the
                points in the self consistent SP problem.

        poisson_problem_input: dict

            Elements are passed as input to poisson_problem_inst.solve()

        return_poisson_output: boolean (default False)

            If True return the raw result for poisson_problem_inst.solve()

        Returns
        -------

            None if return_poisson_output is False,

            (voltage, charge) sparse vectors output of
             poisson_problem_inst.solve()

        '''

        self.input = None
        self.output = None

        self.poisson_input = poisson_problem_input
        self.iteration_data = list()

        if self.sites_ildos is None:
            self.sites_ildos = SparseVector(
                values=np.zeros(len(initial_potential.indices)),
                indices=initial_potential.indices)

        for ildos in self.ildos_list:

            pot_pre = self.potential_precision
            def quant_fun(point):

                if isinstance(point, (np.ndarray, Sequence)):
                    slope = np.empty(len(point))
                    origin = np.empty(len(point))

                    for i, pt in enumerate(point):
                        slope[i] = approx_fprime(
                            xk=np.array([pt]), f=ildos,
                            epsilon=pot_pre)[0]

                        origin[i] = (ildos(pt) - slope[i] * pt)[0]
                else:

                    slope = approx_fprime(
                            xk=np.array([point]), f=ildos,
                            epsilon=pot_pre)[0]

                    origin = (ildos(point) - slope * point)[0]

                return origin, slope

            self.quantum_functional_list.append(quant_fun)

        return self.iterate(
            initial_potential=initial_potential,
            return_iteration_data=return_poisson_output)

    def iterate(
        self, initial_potential=None,
        return_iteration_data=False):
        ''' Self consistent iteration

        Parameters
        ----------

        condition_fun: function (default None)

            condition_fun(error, wrong_interval) -> new_interval

            input
            ------
                error: instance of 'pescado.tools.sparsevector.SparseVector'
                    Contains the error from what is expected of the ILDOS and
                    the result from the poisson_problem_inst.solve()

                wrong_interval: instance of
                    'pescado.tools.sparsevector.SparseVector'

                    Contains the value of the wrong interval

            Returns
            -------
                new_interval: instance of
                    'pescado.tools.sparsevector.SparseVector'

                    Contains the new interval for the quantum sites in
                    'wrong_interval'

            Default behaviour: Moves the interval up if the sign of error
            is positive, down if negative

        return_poisson_output: boolean (default False)

            If True return the raw result for poisson_problem_inst.solve()

        Returns
        -------

            None if return_poisson_output is False,

            (voltage, charge) sparse vectors output of
             poisson_problem_inst.solve()

        '''

        # Recover the origin / slope from the continuous ildos
        origin_sv = SparseVector()
        slope_sv = SparseVector()

        if initial_potential is None:
            self.input = self.output
        else:
            self.input = initial_potential

        origin_sv, slope_sv = self._get_dos_ildos(potential=self.input)

        # New poisson problem
        ps_pot_out, (voltage_output, charge_output) = self._solve_poisson(
            origin_sv=origin_sv,
            slope_sv=slope_sv,
            return_full_output=True)

        self.output = ps_pot_out

        try:
            self.iteration_data.append(
                {'input':self.input.values,
                 'output':self.output.values,
                 'indices':self.sites_ildos.indices})
        except IndexError:
            pass

        if return_iteration_data:
            return ps_pot_out, (voltage_output, charge_output), (origin_sv, slope_sv)

    def iteration_error(self, ite1=None, ite2=None):
        ''' Calculates the absolute difference between 'ite1' and 'ite2'

        Parameters
        ----------
            ite1: integer, default None
                Defaults to the last iteration (-1)

            ite2: integer, default None
                Defaults to the 2nd last iteration (-2)

        Returns
        --------
            err_pot: pescado.SparseVector instance
                np.abs(potential_of_ite1 - potential_of_ite2)

            err_char: pescado.SparseVector instance
                np.abs(charge_of_ite1 - charge_of_ite2)
        '''

        return error.iteration_error(instance_1=self, ite1=ite1, ite2=ite2)

    def _solve_poisson(self,
        origin_sv, slope_sv, return_full_output=False):
        ''' Solves the poisson problem
        '''

        # Set charge sign convetion to pescado
        origin_sv.values = origin_sv.values * -1
        slope_sv.values = slope_sv.values * -1

        # Reset flexible sites configuration
        self.pp_inst.linear_problem_inst.reset_flexible_configuration()

        self.pp_inst.assign_flexible(
            helmholtz_index=self.sites_ildos.indices)

        self.pp_inst.freeze()

        voltage_sv = SparseVector()
        for key, input_sv in zip(
                ['voltage', 'charge', 'helmholtz_density'],
                [voltage_sv, origin_sv, slope_sv]):
            if key in self.poisson_input.keys():
                s_vect = self.poisson_input[key]
                if s_vect is not None:
                    input_sv.extend(s_vect)

        default = {'charge_density':None,
                   'method':'scipy',
                   'kwargs':dict()}

        for key in ['charge_density', 'method', 'kwargs']:
            if key not in self.poisson_input.keys():
                self.poisson_input.update({key:default[key]})

        # Change charge sign so it respects pescado convention
        voltage_output, charge_output = self.pp_inst.solve(
            voltage=voltage_sv,
            charge=origin_sv,
            charge_density=self.poisson_input['charge_density'],
            helmholtz_density=slope_sv,
            method=self.poisson_input['method'],
            **self.poisson_input['kwargs'])

        output = SparseVector(
            values=voltage_output[self.sites_ildos.indices],
            indices=self.sites_ildos.indices)

        if return_full_output:
            return output, (voltage_output, charge_output)
        else:
            return output

    def _get_dos_ildos(self, potential):
        ''' Gets the DOS and the origin for a given 'potential' SV

        Parameters
        -----------
            potential: instance of SparseVector

        Returns
        --------

            origin_sv: instance of SparseVector
                containing the origin

            slope_sv: instance of SparseVector
                containing the slope (DOS)

        '''

        origin_sv = SparseVector()
        slope_sv = SparseVector()

        for num, quant_fun in enumerate(self.quantum_functional_list):

            sites = self.sites_ildos.find(value=num)
            try:
                origin, slope = quant_fun(potential[sites])
            except AttributeError:
                raise RuntimeError(
                    ('No continuous Ildos for sites {0}, '.format(sites)
                    + 'hence can"t perform stupid iteration '))

            origin_sv.extend(SparseVector(values=origin, indices=sites))
            slope_sv.extend(SparseVector(values=slope, indices=sites))

        return origin_sv, slope_sv

    def charge(self, iteration):
        ''' Gets the charge value for 'iteration'

        Parameters
        -----------
            iteration: integer
                iteration number

        Returns
        --------
            instance of SparseVector
        '''

        input_ = self.iteration_data[iteration]['input']

        all_sites = self.sites_ildos.indices

        pot = SparseVector(values=copy.deepcopy(input_), indices=all_sites)

        origin_sv, slope_sv = self._get_dos_ildos(potential=pot)

        char = origin_sv[all_sites] + slope_sv[all_sites] * pot[all_sites]

        return SparseVector(values=char, indices=all_sites)

    def potential(self, iteration):
        ''' Gets the potential value for 'iteration'

        Parameters
        -----------
            iteration: integer
                iteration number

        Returns
        --------
            instance of SparseVector
        '''
        output_ = self.iteration_data[iteration]['output']

        all_sites = self.sites_ildos.indices

        return SparseVector(values=copy.deepcopy(output_), indices=all_sites)
