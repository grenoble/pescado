import copy
import numpy as np

from pescado.tools import SparseVector, DiscreteIldos
from pescado.tools import discretized_ildos, error


def new_functional_points_ii(
    input_type, input, output, interval, ildos, tolerance, verbose=False):
    ''' Returns a np.array of points to be added to self.functional_pts

    Notes
    ------

        It verifies calculates the difference between the output potential
        and existing functional points.

        Then:

            If there is None for which the difference is smaller than
            self.potential_precision; the output potential is sent as a
            new_functional point

            Otherwise the output potential is not sent
    '''

    param_1 = 1e-3

    idx = np.arange(len(interval))

    pt = list()

    # If it is a Neumann or Helmholtz point
    # If neumann then the slope should just be zero.

    if np.any(input_type != 0):
        pt = output[idx[input_type != 0]]
    elif np.any(input_type == 0):
        pt = input[idx[input_type == 0], 0]

    if verbose:
        print(pt)
        print('------------')
        print(ildos.functional_points)

    pt = pt[np.all((np.abs(pt[:, None] - ildos.functional_points)
                    > ildos.potential_precision),
                   axis=1)]
    if verbose:
        print(pt)

    for i, p in enumerate(pt):

        if input_type[i] != 0:
            tp = 0
        else:
            tp = 1

        idx = np.searchsorted(ildos[:, tp], p)

        if verbose:
            print(ildos[:, tp])
            print(idx)

            print(np.abs(
                ildos[idx - 1][tp] - p) < param_1)

        if np.abs(
            ildos[idx - 1][tp] - p) < param_1:
            if verbose:
                print(pt[i])
                print((ildos[idx - 1][tp]
                     + ildos[idx][tp]) / 2)
                print('---------')

            pt[i] = (ildos[idx - 1][tp]
                     + ildos[idx][tp]) / 2

    if verbose:
        print(pt)

    return pt

def new_functional_points(
    input_type, output, interval, ildos, tolerance, verbose=False):
    ''' Returns a np.array of points to be added to self.functional_pts

    Notes
    ------

        It verifies calculates the difference between the output potential
        and existing functional points.

        Then:

            If there is None for which the difference is smaller than
            self.potential_precision; the output potential is sent as a
            new_functional point

            Otherwise the output potential is not sent
    '''

    idx = np.arange(len(interval))

    pt = list()

    # If it is a Neumann or Helmholtz point
    # If neumann then the slope should just be zero.

    if np.any(input_type != 0):
        pt = output[idx[input_type != 0]]

    if verbose:
        print(pt)
        print('------------')
        print(ildos.functional_points)

    pt = pt[np.all((np.abs(pt[:, None] - ildos.functional_points)
                    > ildos.potential_precision),
                   axis=1)]
    if verbose:
        print(pt)

    return pt

def get_ildos_interval(
        ildos, input_interval, output, input_type,condition_fun=None):
        ''' Generates the new interval values for a given 'output',
        'input_interval', 'input_type' and 'ildos'

        '''

        wrong_interval, error = compare_with_ildos(
            ildos=ildos,
            poisson_out=output,
            interval=input_interval,
            input_type=input_type)

        default_cond_fun = lambda error, wrong_interval: SparseVector(
            values=(wrong_interval.values
                    + np.sign(error.values)).astype(int),
            indices=wrong_interval.indices, dtype=int)

        if condition_fun is None:
            condition_fun = default_cond_fun

        interval = SparseVector(
            indices=input_interval.indices,
            values=input_interval.values, dtype=int)

        interval[wrong_interval.indices] = condition_fun(
            error=error, wrong_interval=wrong_interval)[
            wrong_interval.indices].astype(int)

        return interval, error


def interval_type(ildos, interval, tolerance=None):
    ''' Returns the type of the 'interval' for a given piecewise 'ildos'.

    Parameters
    ----------

    ildos: numpy.array of floats with shape(m, 2)
        Coordinates forming the piece_wise ildos
        First dimension is voltage, second the charge

    interval: sequence of n integers

    tolerance: float or sequence of 2 floats (default 1e-13)
        tolerance for voltage and charge compairison.

        [tol_voltage, tol_charge]

    Returns
    -------
    numpy array of n integers

        0 -> Dirichlet
        1 -> Neumann
        2 -> Helmholtz
    '''

    if tolerance is None:
        tolerance = [1e-13, 1e-13]
    elif isinstance(tolerance, int):
        tolerance = [tolerance, ] * 2

    tol_voltage, tol_charge = tolerance

    diff = np.abs(ildos[:-1] - ildos[1:])

    dirichlet = np.arange(0, len(interval), dtype=int)[
        diff[interval, 0] < tol_voltage]
    neumann =  np.arange(0, len(interval), dtype=int)[
        diff[interval, 1] < tol_charge]

    type_ = np.ones(len(interval)) * 2
    type_[dirichlet] = np.zeros(len(dirichlet))
    type_[neumann] = np.ones(len(neumann))

    return type_


def interval_info(ildos, interval):
    ''' Returns the type of the 'interval' for a given piecewise 'ildos'.

    Parameters
    ----------

    ildos: numpy.array of floats with shape(m, 2)
        Coordinates forming the piece_wise ildos
        First dimension is voltage, second the charge

    interval: sequence of n integers

    Returns
    -------
    value_type: numpy array of n integers

        0 -> Dirichlet
        1 -> Neumann
        2 -> Helmholtz

    value: numpy array of floats with shape(n, 2)
        Interval value given by the ildos

        For type_ Dirichlet or Neumann:
            voltage = value[:, 0]
            charge = value[:, 1]

        For type_ Helmholtz
            density = value[:, 0]
            charge_0 = value[:, 1]
    '''

    value_type = SparseVector(dtype=np.int64)
    voltage = SparseVector()
    charge = SparseVector()
    helmholtz_density = SparseVector()

    value_type[interval.indices] = interval_type(
        ildos=ildos, interval=interval.values)

    for i in range(3):
        idx = value_type.find(value=i)
        if i == 0:
            voltage[idx] = ildos[interval[idx], i]
        elif i == 1:
            charge[idx] = ildos[interval[idx], i]
        elif i == 2:
            helm_int = interval[idx]
            if len(helm_int) > 0:

                slope = ((ildos[helm_int, 1] - ildos[helm_int + 1, 1])
                         / (ildos[helm_int, 0] - ildos[helm_int + 1, 0]))

                origin = ildos[helm_int, 1] - ildos[helm_int, 0] * slope

                charge[idx] = origin
                helmholtz_density[idx] = slope

    return value_type, voltage, charge, helmholtz_density


def compare_with_ildos(ildos, poisson_out, interval, input_type):
    ''' Compares 'poisson_out' with 'ildos' expected value for a given
    'poisson_input'

    Parameters
    -----------

    ildos: numpy.array of floats with shape(m, 2)
        Coordinates forming the piece_wise ildos
        First dimension is voltage, second the charge

    poisson_out: numpy array of floats with shape(n,)

        For Dirichlet type: Charge values

        For Neumann or Helmholtz type: Voltage values

    poisson_input: numpy array of floats with shape(n, 2)
        Interval value given by the ildos

        For type_ Dirichlet or Neumann:
            voltage = poisson_input[:, 0]
            charge = poisson_input[:, 1]

        For type_ Helmholtz
            density = poisson_input[:, 0]
            charge_0 = poisson_input[:, 1]

    in_type: sequence of n integers

        0 -> Dirichlet
        1 -> Neumann
        2 -> Helmholtz

    Returns
    --------
    wrong_interval: numpy.array of integers with shape(t, )
        The index of poisson_input that have the wrong poisson_out when
        compared to the ildos

    direction: numpy.array of integers with shape(t, )
        If the poisson_out value is smaller or larger than the expected
        ildos value

            -1 if smaller
             1 if larger
    '''
    out_type = SparseVector(
        values=(input_type.values + 1) % 2 - input_type.values // 2,
        indices=input_type.indices,
        dtype=np.int64)

    # Check the boundary conditions
    wrong_interval, error = check_boundary(
        ildos=ildos, poisson_out=poisson_out,
        interval=interval, out_type=out_type)

    # Check the middle intervals
    idx = interval.indices[
        (interval.values != 0) * (interval.values != ildos.shape[0] - 2)]

    mid_wrg_idx, mid_error = check_mid_interval(
        ildos=ildos, poisson_out=poisson_out[idx],
        interval=interval[idx], out_type=out_type[idx])

    wrong_interval[idx[mid_wrg_idx]] = interval[idx[mid_wrg_idx]]
    error[idx[mid_wrg_idx]] = mid_error

    return wrong_interval, error

def compare_continuous_ildos(ildos, input_charge, poisson_out):
    ''' Compares the expected charge / slope for a given 'poisson_out'
    with the 'poisson_in' value.
    '''

    charge_val = ildos(poisson_out.values)
    error = SparseVector(
        values=(input_charge[:] - charge_val) / charge_val,
        indices=poisson_out.indices)

    return error


def check_mid_interval(ildos, poisson_out, interval, out_type):
    ''' Verifies that the poisson_output belong to interval for
    intervals in between [1, ildos.shape[0] - 3].

    Parameters
    -----------

    ildos: numpy.array of floats with shape(m, 2)
        Coordinates forming the piece_wise ildos
        First dimension is voltage, second the charge

    poisson_out: numpy array of floats with shape(n,)

        For Dirichlet type: Charge values

        For Neumann or Helmholtz type: Voltage values

    interval: sequence of integers with shape(n, )
        interval to which poisson_out should belong
        Must be in between [1, ildos.shape[0] - 3]

    out_type: sequence of n integers

        0 -> Dirichlet
        1 -> Neumann / Helmholtz

    Returns
    --------
    wrong_interval: numpy.array of integers with shape(t, )
        The index of poisson_input that have the wrong poisson_out when
        compared to the ildos

    direction: numpy.array of integers with shape(t, )
        If the poisson_out value is smaller or larger than the expected
        ildos value

            -1 if smaller
             1 if larger
    '''

    wrong_idx = list()
    error = list()

    ldiff = poisson_out - ildos[interval, out_type]
    rdiff = poisson_out - ildos[interval + 1, out_type]

    lmap = ldiff <= 0.0
    rmap = rdiff > 0.0

    for map_, diff in zip([lmap, rmap], [ldiff, rdiff]):
        wrong_idx.append(np.arange(len(interval))[map_])
        error.append(diff[map_])

    return np.concatenate(wrong_idx), np.concatenate(error)


def check_boundary(ildos, poisson_out, interval, out_type):
    ''' Verifies that the poisson_output belong to the first
    or last interval of a given 'ildos'.

    Parameters
    -----------

    ildos: numpy.array of floats with shape(m, 2)
        Coordinates forming the piece_wise ildos
        First dimension is voltage, second the charge

    poisson_out: numpy array of floats with shape(n,)

        For Dirichlet type: Charge values

        For Neumann or Helmholtz type: Voltage values

    interval: sequence of integers with shape(n, )
        interval to which poisson_out should belong
        Must be the first (0) or the last (ildos.shape[0] - 2) 'ildos' interval

    out_type: sequence of n integers

        0 -> Dirichlet
        1 -> Neumann / Helmholtz

    Returns
    --------
    wrong_interval: instance of SparseVector
        The wrong interval values

    direction: instance of SparseVector
        The difference between the expected ildos value and the one
        given by poisson_out

    '''

    wrong_interval = SparseVector(dtype=np.int64)
    error = SparseVector()

    for j, i in enumerate([ildos.shape[0] - 2, 0]):

        idx = interval.find(value=i)
        difference = poisson_out[idx] - ildos[(i + j), out_type[idx]]

        if j == 0:
            wrong_idx = np.arange(0, len(idx), dtype=int)[difference <= 0.0]

        elif j == 1:
            wrong_idx = np.arange(0, len(idx), dtype=int)[difference > 0.0]

        error[idx[wrong_idx]] = difference[wrong_idx]
        wrong_interval[idx[wrong_idx]] = interval[idx[wrong_idx]]

    return wrong_interval, error


class SchrodingerPoisson():
    ''' Solves the ScrodingerPoisson self consistent problem.
    '''

    def __init__(self,
    ildos, poisson_problem_inst=None, linear_problem_inst=None, helmholtz_cutoff=None,
    sites_ildos=None, tolerance=None):
        ''' Returns an instance of SchrodingerPoisson

        Parameters
        -----------

        ildos: list of, or instance of  :

            tools.discretized_ildos.DiscreteIldos

                    or

            numpy.array of floats with shape(m, 2)

                Coordinates forming the piece_wise ildos
                First dimension is chemical potential, second the charge

            Units: i) chemical potential in electron volts
                  ii) charge in electrons per cell

        poisson_problem_inst: instance of poisson.Problem

        linear_problem_inst: instance of LinearProblem
            Optional, can be given instead of poisson_problem_inst

        tolerance: float or sequence of 3 floats (default 1e-13)
            tolerance for voltage, charge and density of states.

            [tol_voltage, tol_charge, tol_dos]

        potential_precision: float (default is None)
            Used for continuous ILDOS. It is used for two things :

            i) Step used in scipy.optimize.approx_fprime to calculate the
            tangent from a 'functional_point', i.e. the 'epsilon' parameter

            ii) Precision used to verify if the new potential (solution of
            the PESCADO) is added as a new functional point.

                np.abs(
                    self.functional_pts - possible_pot) > potential_precision

            It can be interpreted as the maximal resolution used to discretize
            the ildos, in case the latter is continuous.

        Returns
        --------
        instance of SchrodingerPoisson

        Note
        -----

        Important parameters:

            self.poisson_input: dict passed to
                poisson_problem_inst.solve()

            self.iteration_data: list containing
                    [(value_type, input), output, interval]

                    !!! TODO: Update this description !!!

                    interval: sparse vector with the ILDOS interval for
                    each quantum site

                    output: poisson.Problem result for each quantum site

                    value_type: sparse vector with type of quantum site
                    (Dirichlet - 0 / Neumann - 1 / Helmholtz - 2)
                    Defined by the ildos

                    input: sparse vector with value of quantum site as defined
                    by the Ildos and used as input for
                    poisson_problem_inst.solve()

        !!! Both self.poisson_input and self.iteration_data reset after !!!
        !!!             self.initialize() is called                     !!!
        '''

        ildos_types = (
            np.ndarray, discretized_ildos.DiscreteIldos, DiscreteIldos)

        if isinstance(ildos, (tuple, list)):
            self.ildos_list = ildos
        elif isinstance(ildos, ildos_types):
            self.ildos_list = [ildos, ]
        else:
            raise NotImplementedError(
                ('tuple, list or {0} was expected for "ildos" however'.format(
                    ildos_types)
                 + ' {0} was given'.format(ildos)))

        self.sites_ildos = sites_ildos

        if poisson_problem_inst is not None:
            self.lp_inst = poisson_problem_inst.linear_problem_inst
        else:
            self.lp_inst = linear_problem_inst

        self.poisson_input = None # For initialization purposes

        self.helmholtz_cutoff = helmholtz_cutoff

        if tolerance is None:
            self.tolerance = [1e-13, ] * 3
        elif isinstance(tolerance, int):
            self.tolerance = [tolerance, ] * 3

    def initialize(self,
        poisson_problem_input, initial_guess,
        condition_fun=None, return_poisson_output=False):
        ''' Initialize self consistency

        Parameters
        -----------

        initial_guess: instance of 'pescado.tools.sparsevector.SparseVector'

            If 'self.ildos' is discrete

                Contains the Ildos interval used to initialize the points in
                the self consistent SP problem

            If 'self.ildos' is continuous

                Contains the 'chemical potential' used to initialize the
                points in the self consistent SP problem.

        poisson_problem_input: dict

            Elements are passed as input to poisson_problem_inst.solve()

        return_poisson_output: boolean (default False)

            If True return the raw result for poisson_problem_inst.solve()

        Returns
        -------

            None if return_poisson_output is False,

            (voltage, charge) sparse vectors output of
             poisson_problem_inst.solve()

        '''

        self.interval = None
        self.input_type = None
        self.input = None
        self.output = None
        self.error = None

        self.poisson_input = poisson_problem_input
        self.iteration_data = list()

        if self.sites_ildos is None:
            self.sites_ildos = SparseVector(
                values=np.zeros(len(initial_guess.indices)),
                indices=initial_guess.indices)

        return self.iterate(
            condition_fun=condition_fun,
            initial_guess=initial_guess.extract(initial_guess.indices),
            return_poisson_output=return_poisson_output)

    def iterate(
        self, condition_fun=None,
        initial_guess=None,
        return_poisson_output=False):
        ''' Self consistent iteration

        Parameters
        ----------

        condition_fun: function (default None)

            condition_fun(error, wrong_interval) -> new_interval

            input
            ------
                error: instance of 'pescado.tools.sparsevector.SparseVector'
                    Contains the error from what is expected of the ILDOS and
                    the result from the poisson_problem_inst.solve()

                wrong_interval: instance of
                    'pescado.tools.sparsevector.SparseVector'

                    Contains the value of the wrong interval

            Returns
            -------
                new_interval: instance of
                    'pescado.tools.sparsevector.SparseVector'

                    Contains the new interval for the quantum sites in
                    'wrong_interval'

            Default behaviour: Moves the interval up if the sign of error
            is positive, down if negative

        return_poisson_output: boolean (default False)

            If True return the raw result for poisson_problem_inst.solve()

        Returns
        -------

            None if return_poisson_output is False,

            (voltage, charge) sparse vectors output of
             poisson_problem_inst.solve()

        '''

        data = self._iterate_discrete(
            condition_fun=condition_fun,
            interval=initial_guess,
            return_poisson_output=return_poisson_output)

        # Transform the old iteration data into normal numpy arrays to reduce
        # space
        try:
            self.iteration_data.append(
                {'input_type':self.input_type.values,
                 'input':self.input_value,
                 'output':self.output.values,
                 'interval':self.interval.values,
                 'error':(self.error.values, self.error.indices)})
        except IndexError:
            pass

        return data

    def update_ildos(self):

        for i, ildos in enumerate(self.ildos_list):
            if isinstance(
                ildos,
                (discretized_ildos.DiscreteIldos, DiscreteIldos)):

                idx = self.sites_ildos.find(value=i)
                verbose = False

                # fun_pts = new_functional_points_ii(
                #     input_type=self.input_type[idx],
                #     input=self.input_value,
                #     output=self.output[idx],
                #     interval=self.interval[idx], verbose=verbose,
                #     ildos=ildos, tolerance=self.tolerance)

                fun_pts = new_functional_points(
                    input_type=self.input_type[idx],
                    output=self.output[idx],
                    interval=self.interval[idx], verbose=verbose,
                    ildos=ildos, tolerance=self.tolerance)

                if verbose:
                    print(fun_pts)

                if len(fun_pts) > 0:

                    ildos.update(functional_points=fun_pts)

                    if self.input_type[idx] == 0:
                        pot = self.input_value[i][0]
                        charge = self.output[idx]
                    else:
                        pot = self.output[idx][0]
                        charge = self.input_value[i][1]

                    if self.input_type[idx] == 2:
                        charge += self.input_value[i][2] * pot

                    self.interval[idx] = ildos.interval(
                        coordinates=np.array([pot, charge])[None, :])[0]

    def iteration_error(self, ite1=None, ite2=None):
        ''' Calculates the absolute difference between 'ite1' and 'ite2'

        Parameters
        ----------
            ite1: integer, default None
                Defaults to the last iteration (-1)

            ite2: integer, default None
                Defaults to the 2nd last iteration (-2)

        Returns
        --------
            err_pot: pescado.SparseVector instance
                np.abs(potential_of_ite1 - potential_of_ite2)

            err_char: pescado.SparseVector instance
                np.abs(charge_of_ite1 - charge_of_ite2)
        '''

        return error.iteration_error(
            instance_1=self, ite1=ite1, ite2=ite2)

    def _iterate_discrete(
        self, condition_fun=None, interval=None, return_poisson_output=False):

        if interval is None:

           self.interval, self.error = self._get_ildos_interval(
               condition_fun=condition_fun)
        else:
            self.error = SparseVector()
            self.interval = interval

        (self.input_type,
         voltage, charge, helmholtz_density) = self._interval_info()

        flex_input = [voltage, charge, helmholtz_density]

        data = self._solve(
            flex_input=flex_input, return_poisson_output=return_poisson_output)

        return data

    def _solve(self, flex_input, return_poisson_output=False):
        ''' Solves the poisson problem with ILDOS built input
        '''

        # To save the input as a numpy array of shape(m, 3)
        self.input_value = np.zeros((len(self.input_type.indices), 3))
        for i, sv in enumerate(flex_input):
            ign, idx_vt, idx_sv = np.intersect1d(
                self.input_type.indices, sv.indices, return_indices=True)
            self.input_value[idx_vt, i] = sv.values[idx_sv]

        poisson_res = self._solve_poisson(
            flex_input=flex_input, flex_type=self.input_type,
            return_full_output=return_poisson_output)

        if return_poisson_output:
            self.output, (voltage_output, charge_output) = poisson_res
        else:
            self.output = poisson_res

        if return_poisson_output:
            return voltage_output, charge_output

    def _solve_poisson(self,
        flex_input, flex_type, return_full_output=False):
        ''' Solves the poisson problem
        '''

        # Set charge sign convetion to pescado
        flex_input[1] = flex_input[1] * -1
        flex_input[2] = flex_input[2] * -1

        # Reset flexible sites configuration
        self.lp_inst.assign_flexible(
            dirichlet_index=flex_input[0].indices,
            neumann_index=np.setdiff1d(
                flex_input[1].indices, flex_input[2].indices),
            helmholtz_index=flex_input[2].indices)

        self.lp_inst.build()

        for num, key in enumerate(
            ['voltage', 'charge', 'helmholtz_density']):
            if key in self.poisson_input.keys():
                s_vect = self.poisson_input[key]
                if s_vect is not None:
                    flex_input[num].extend(s_vect)

        default = {'charge_density':None,
                   'method':'mumps',
                   'kwargs':dict()}

        for key in ['charge_density', 'method', 'kwargs']:
            if key not in self.poisson_input.keys():
                self.poisson_input.update({key:default[key]})

        # Change charge sign so it respects pescado convention
        voltage_output, charge_output = self.lp_inst.solve(
            voltage=flex_input[0],
            charge=flex_input[1],
            charge_density=self.poisson_input['charge_density'],
            helmholtz_density=flex_input[2],
            method=self.poisson_input['method'],
            **self.poisson_input['kwargs'])

        output = SparseVector()
        for i in range(3):
            idx = flex_type.find(value=i)
            if i == 0:
                # Set sign convention to SP
                output[idx] = -1 * charge_output[idx]
            elif i in [1, 2]:
                output[idx] = voltage_output[idx]

        if return_full_output:
            return output, (voltage_output, charge_output)
        else:
            return output

    def _get_ildos_interval(self, condition_fun=None):
        ''' Generates the new interval values for the next iteration
        '''

        interval = SparseVector(dtype=int)
        error = SparseVector()

        for i, ildos in enumerate(self.ildos_list):

            idx = self.sites_ildos.find(value=i)
            out = get_ildos_interval(
                ildos=ildos, condition_fun=condition_fun,
                input_interval=self.interval.extract(idx),
                output=self.output.extract(idx),
                input_type=self.input_type.extract(idx))

            for sv, out in zip([interval, error], out):
                sv.extend(out)

        return interval, error

    def _interval_info(self):

        input_type = SparseVector(dtype=int)
        voltage = SparseVector()
        charge = SparseVector()
        helmholtz_density = SparseVector()

        for i, ildos in enumerate(self.ildos_list):

            idx = self.sites_ildos.find(value=i)

            out = interval_info(
                 ildos, interval=self.interval.extract(idx))

            for sv, out in zip(
                [input_type, voltage, charge, helmholtz_density], out):
                sv.extend(out)

        return input_type, voltage, charge, helmholtz_density

    def charge(self, iteration):
        ''' Gets the charge value for 'iteration'

        Parameters
        -----------
            iteration: integer
                iteration number

        Returns
        --------
            instance of SparseVector
        '''

        input_ = self.iteration_data[iteration]['input']
        output_ = self.iteration_data[iteration]['output']
        type_ = self.iteration_data[iteration]['input_type']

        all_sites = self.sites_ildos.indices

        char = copy.deepcopy(output_)

        char[type_ == 1] = input_[type_ == 1, 1]

        char[type_ == 2] = (
            input_[type_ == 2, 1] + input_[type_ == 2, 2] * output_[type_ == 2])

        return SparseVector(values=char, indices=all_sites)

    def potential(self, iteration):
        ''' Gets the potential value for 'iteration'

        Parameters
        -----------
            iteration: integer
                iteration number

        Returns
        --------
            instance of SparseVector
        '''

        input_ = self.iteration_data[iteration]['input']
        output_ = self.iteration_data[iteration]['output']
        type_ = self.iteration_data[iteration]['input_type']

        all_sites = self.sites_ildos.indices

        pot = copy.deepcopy(output_)
        pot[type_ == 0] = input_[type_ == 0, 0]

        return SparseVector(values=pot, indices=all_sites)
