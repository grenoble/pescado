''' Tests 1D result obtained with self_consistent with the analytical
result. Assures that the error remains the same
'''
from collections.abc import Sequence

import numpy as np

from scipy import constants
from scipy import optimize

from pescado import mesher, poisson

from pescado.self_consistent import solver
from pescado.self_consistent import ildos as sc_ildos

from pescado.tools import SparseVector

def solve_continuous_pp(sp_problem, p_problem, vg):
    ''' Solves the Generalized helmlholtz problem for
    a continuous ildos for the parallel plate capacitor with
    gate voltage vg.

    Parameters
    -----------

        sp_problem_disc: instance of solver.ScrodingerPoisson
            with a cotinuous ildos

        vg: float

    Returns
    ---------

    charge, (mean_charge, difference_max)
    '''
    
    initial_guess = SparseVector(
        values=np.zeros(len(p_problem.flexible_indices)),
        indices=p_problem.flexible_indices, dtype=float)

    sv_gate = p_problem.sparse_vector(val=vg, name='gate')
    poisson_problem_input = {'voltage':sv_gate,}

    initial_fpoints = np.array([-0.8, 0., 0.8])

    sp_problem.solve(
        poisson_input=poisson_problem_input,
        initial_guess=initial_guess,
        initial_fpoints=initial_fpoints)

    charge = sp_problem.quantum_charge(-1)
    
    mean_char = np.mean(charge.values)
    diff_max = np.max(charge.values - mean_char)

    return charge, (mean_char, diff_max)


def solve_discrete_pp(sp_problem_disc, p_problem, vg):
    ''' Solves the Generalized helmlholtz problem for
    a discrete ildos for the parallel plate capacitor with
    gate voltage vg.

    Parameters
    -----------

        sp_problem_disc: instance of solver.ScrodingerPoisson
            with a discrete ildos

        vg: float

    Returns
    ---------

    charge, (mean_charge, difference_max)
    '''

    initial_guess = SparseVector(
        values=np.zeros(len(p_problem.flexible_indices)),
        indices=p_problem.flexible_indices, dtype=float)

    sv_gate = p_problem.sparse_vector(val=vg, name='gate')
    poisson_problem_input = {'voltage':sv_gate,}

    sp_problem_disc.solve(
        poisson_input=poisson_problem_input, 
        initial_guess=initial_guess, max_ite=50)

    charge = sp_problem_disc.quantum_charge(-1)
    
    mean_char = np.mean(charge.values)
    diff_max = np.max(charge.values - mean_char)

    return charge, (mean_char, diff_max)


def make_poisson_parrallel_plate(rel_perm, gas_ep, d):
    ''' Initializes a helmholtz solver PESCADO instance for the
    parallel plate capacitor problem with a 2DEG as the second
    electrode.

    Parameters
    -----------

        rel_perm: relative permittivity

        gas_ep: 2DEG single cell layer thickness
        gas_len: 2DEG cell length

        d: distance between gate and the 2DEG

        size: system size

    Returns
    --------

    Instance of pescado.poisson.Problem()

    '''
    pt = np.array(
        [0 - gas_ep * 3, 0 - gas_ep * 2,
         0 - gas_ep, 0, 0 + gas_ep,
         0.5, d - gas_ep, d, d + gas_ep, d + gas_ep * 2,
         d + gas_ep * 3,  d + gas_ep * 4])[:, None]

    p_builder = poisson.ProblemBuilder()

    mesh_box = mesher.shapes.Box(
        lower_left=(-gas_ep / 2, ), size=(d + gas_ep * 2, ))

    finite_pat = mesher.patterns.Finite.points(pt)

    p_builder.initialize_mesh(
        simulation_region=mesh_box, pattern=finite_pat)

    gaz = mesher.shapes.Box(
        lower_left=(-gas_ep / 2.1, ), size=(gas_ep, ))
    
    gate = mesher.shapes.Box(
        lower_left=(d - gas_ep / 2.1, ), size=(gas_ep,))

    die = mesh_box - (gate | gaz)

    for sh, ep_r in zip([gaz, die], [rel_perm, rel_perm]):
        p_builder.set_relative_permittivity(
            val=ep_r, region=sh)

    p_builder.set_metal(region=gate, setup_name='gate')
    p_builder.set_neumann(region=die, setup_name='dielectric')
    p_builder.set_flexible(region=gaz, setup_name='gaz')

    return p_builder.finalized()


def qhe_disc_ildos(b, m_eff):

    lb = np.sqrt(constants.hbar / (constants.elementary_charge * b))
    w_c = constants.elementary_charge * b / (constants.electron_mass * m_eff)

    charge_step = 1 / (2 * constants.pi * (lb ** 2))
    volt_step = constants.hbar * w_c / constants.elementary_charge

    coord_QHE = np.empty((20, 2), dtype=float)

    coord_QHE[:, 0] = np.array([-1,  0, 1/2, 1/2,
                                3/2, 3/2, 5/2, 5/2,
                                7/2, 7/2, 9/2, 9/2,
                                11/2, 11/2, 13/2, 13/2,
                                15/2, 15/2, 17/2, 17/2]) * volt_step

    coord_QHE[:, 1] = np.array([ 0,  0,  0,   1,  1,
                                2,   2,   3,   3,
                                4, 4, 5, 5,
                                6, 6, 7, 7,
                                8, 8, 9]) * charge_step

    coord_QHE[:, 1] = coord_QHE[:, 1] * 1e-18

    return coord_QHE


def theta(x):
    return (np.sign(x) + 1) / 2.


def make_ildos_QH(B, m_eff):

    # In Si
    omega = constants.e * B / (m_eff * constants.m_e)
    phi0 = constants.e * B / (constants.hbar * 2 * np.pi)

    def ildos(mu):
        ildos_ = np.sum(
            phi0
            * theta((mu * constants.e)
                    - constants.hbar * omega * (np.arange(0, 50000) + 0.5)))
        return ildos_

    return ildos


def make_ildos(m_eff):
    def ildos(mu):

        dens = (constants.m_e * m_eff) / (np.pi * (constants.hbar ** 2))

        #Get to the correct units
        dens = dens * 1e-18 * constants.elementary_charge

        if not isinstance(mu, (np.ndarray, Sequence)):
            return dens, dens * mu
        else:
            return (
                dens * np.ones(len(mu)), 
                dens * np.asarray(mu))

    return ildos


def test_parallel_plate_2DEG_QHE():
    ''' Compares graphic solution for QHE in 2DEG in a parallel
    plate capacitance geometry with that obtained with the discrete
    version of PESCADO.
    '''
    b = 5
    m_eff = 0.067
    rel_perm = 7
    d = 1

    gas_ep = 1e-4
    p_problem = make_poisson_parrallel_plate(
        rel_perm=rel_perm, gas_ep=gas_ep, d=d)

    coord_QHE = qhe_disc_ildos(b=b, m_eff=m_eff)
    
    sp_problem = solver.PieceWiseNewthonRaphson(
        ildos=coord_QHE, poisson_problem=p_problem)

    vg_list = np.linspace(0, 0.05, 4)
    mean_char = np.zeros(len(vg_list))
    diff_max = np.zeros(len(vg_list))

    for i, vg in enumerate(vg_list):
        ig, (mean_char[i], diff_max[i]) = solve_discrete_pp(
            sp_problem_disc=sp_problem, p_problem=p_problem, vg=vg)

    assert np.all(np.abs(diff_max) < 1e-15)

    # Get reference
    ildos = make_ildos_QH(B=b, m_eff=m_eff)

    ref_char = np.zeros(len(vg_list))
    cont_ildos_char = np.zeros(len(vg_list))

    for i, vg in enumerate(vg_list):
        def n_pois(mu):
            a = ((constants.epsilon_0 * rel_perm)
                / (constants.elementary_charge * d * 1e-9))

            return a * (vg - mu)

        fun = lambda x: n_pois(x) - ildos(x)

        res, c_info = optimize.brentq(fun, -2, 2, full_output=True)

        # Take only the n_pois value since the other can oscillate
        # quite a lot due to the step_wise character of the ILDOS in QHE
        ref_char[i] = n_pois(res) * 1e-18

    print(mean_char[1:])
    print(ref_char[1:])
    rel_error = np.abs(mean_char[1:] - ref_char[1:]) / ref_char[1:]

    assert np.all(rel_error < 6e-5), (
        'Relative error larger than {0}%'.format(6-5 * 100))


def test_parallel_plate_2DEG_no_field():
    ''' Compares graphic solution for the 2DEG with zero field in a parallel
    plate capacitance geometry with that obtained with the discrete
    version of PESCADO.

        Notice this also tests the ildos in  an interesting case, 
        where a linear ILDOS is discretized by linear intervals.
    '''

    m_eff = 0.067
    rel_perm = 7
    d = 1

    gas_ep = 1e-4

    p_problem = make_poisson_parrallel_plate(
        rel_perm=rel_perm, gas_ep=gas_ep, d=d)

    ildos = make_ildos(m_eff=m_eff)

    disc_ildos = sc_ildos.PContinuousIldos(
        coordinates=np.array([[-.8, ildos(-.8)[1]],
                              [0.8, ildos(0.8)[1]]]),
        functions=ildos)
    
    sp_problem = solver.PieceWiseDichotomy(
        ildos=disc_ildos, poisson_problem=p_problem)
    
    vg_list = np.linspace(0, 1, 4)
    mean_char = np.zeros(len(vg_list))
    diff_max = np.zeros(len(vg_list))

    for i, vg in enumerate(vg_list):

        ig, (mean_char[i], diff_max[i]) = solve_continuous_pp(
            sp_problem=sp_problem, p_problem=p_problem, vg=vg)

    assert np.all(np.abs(diff_max) < 1e-15)

    # Get reference
    a = ((constants.elementary_charge * d * 1e-9)
        / (constants.epsilon_0 * rel_perm))

    b = (np.pi * (constants.hbar ** 2)
        / (constants.elementary_charge * constants.m_e * m_eff))

    ref_slope = 1 / (a + b)
    ref_char = (ref_slope * 1e-18 ) * vg_list

    rel_error = np.abs(mean_char[1:] - ref_char[1:]) / ref_char[1:]

    print(rel_error)
    assert np.all(rel_error < 3e-5), (
        'Relative error larger than {0}%'.format(3e-5 * 100))

# test_parallel_plate_2DEG_no_field()

# test_parallel_plate_2DEG_QHE()