''' Tests ILDOS

    I) Tests adding (u, dos, origin).

    II) Compare with continuous reference

    III) Notice test_analytical_1d_ppc uses the ildos and it
        captures an interesting case, where a linear ILDOS
        is discretized by linear intervals.
'''

from collections.abc import Sequence
import numpy as np
from scipy import constants

from pescado.self_consistent import ildos

from numpy.testing import assert_almost_equal

def disc_ildos(surf):

    dos = 1 * (0.067 * constants.m_e) / (np.pi * constants.hbar ** 2)
    dens_helm = dos * 1e-18 * constants.elementary_charge * surf
    # The value of A corresponds to a one-dimension unit cell

    coord = np.empty((3, 2), dtype=float)
    coord[:, 0] = np.array([-1, 0, 1])
    coord[:, 1] = np.array([0, 0, dens_helm])

    return coord

def ildos_pesca(mu):

    aq = 1e-9
    t = constants.hbar**2 / (2. * constants.m_e * 0.067 * aq**2)
    dens = (1 / t ** 2) * (1 / (2 * np.pi)) * 2 * (
        constants.elementary_charge ** 2)

    if not isinstance(mu, (np.ndarray, Sequence)):
        if mu <= 0:
            dens_ =  0
            ildos_ =  0
        else:
            dens_ = 2 * dens * mu
            ildos_ = dens * (mu ** 2)
    else:
        mu_zero = mu <= 0
        ildos_ = dens * (mu ** 2)
        ildos_[mu_zero] = 0
        dens_ = 2 * np.ones(len(mu)) * dens * mu

    return dens_, ildos_

def ildos_zero(mu):
    if not isinstance(mu, (np.ndarray, Sequence)):
        return 0, 0
    else:
        return np.zeros(len(mu)), np.zeros(len(mu))

def test_PLinearIldos():
    ''' Test I - compare to hardcoded reference
    '''

    ildos_c = ildos.PLinearIldos.from_array(coordinates=disc_ildos(surf=10))

    dos = 1 * (0.067 * constants.m_e) / (np.pi * constants.hbar ** 2)
    dens_helm = dos * 1e-18 * constants.elementary_charge * 2 * 2

    ildos_c.add_interval(
        u=0.3, dos=dens_helm / 1.25, origin=0)

    ildos_c.add_interval(
        u=0.36, dos=0, origin=1)

    ildos_c.add_interval(
        u=0.9, dos= 3 * dens_helm, origin=0)

    ildos_c.add_interval(
        u=-.7, dos=dens_helm, origin=0)

    ildos_c.add_interval(
        u=-0.33, dos=0, origin=0)

    ref_origin = np.array([0., 0., 0., 0., 0., 1., 0., 0., 0., 0.])

    ref_fpoints = np.array(
        [[-0.7, -0.78366424],
        [np.nan, np.nan],
        [-0.5, 0.        ],
        [ 0.3, 0.26868488],
        [np.nan, np.nan],
        [ 0.36, 1.        ],
        [np.nan, np.nan],
        [ 0.5, 1.39940043],
        [np.nan, np.nan],
        [ 0.9, 3.02270492]])

    ref_dos = np.array(
        [1.11952034, np.infty, 0. , 0.89561627, np.infty,
        0. , np.infty, 2.79880085, np.infty, 3.35856102])

    ref_coordinates = np.array(
        [[-1., -1.11952034],
        [-0.6, -0.6717122 ],
        [-0.6,  0.],
        [-0.,  0.],
        [ 0.33,  0.29555337],
        [ 0.33,  1.],
        [ 0.43,  1.],
        [ 0.43,  1.20348437],
        [ 0.7,  1.9591606 ],
        [ 0.7,  2.35099271],
        [ 1.,  3.35856102]])

    assert_almost_equal(ref_origin, ildos_c.origin)
    assert_almost_equal(ref_dos, ildos_c.dos)
    assert_almost_equal(ref_fpoints, ildos_c.fpoints)

    assert_almost_equal(ref_coordinates, ildos_c.coordinates)

def test_PContinuousIldos():
    '''
    '''

    coord = np.empty((3, 2), dtype=float)
    coord[:, 0] = np.array([-1, 0, 1])
    coord[:, 1] = np.array([0, 0, ildos_pesca(1)[1]])

    ildos_cont = ildos.PContinuousIldos(
        coordinates=coord, functions=[ildos_zero, ildos_pesca])


    fpoints = np.array([-0.8,-0.2, 0.1, .2, .3, .45, .6, .8, .9])
    u = np.linspace(-1, 1, 100)

    dos, q = ildos_cont(u)

    ildos_disc = ildos_cont.linearize(fpoints=fpoints)

    ref_coordinates = np.array(
        [[-1., 0.],
         [ 0.05, 0.],
         [ 0.15, 0.0196872],
         [ 0.25, 0.05906159],
         [ 0.375, 0.13288857],
         [ 0.525, 0.26577714],
         [ 0.7, 0.47249269],
         [ 0.85, 0.70873904],
         [ 1., 0.97451618]])

    ref_fpoints = np.array(
        [[-0.8, 0.],
         [ 0.1, 0.0098436],
         [ 0.2, 0.03937439],
         [ 0.3, 0.08859238],
         [ 0.45, 0.19933285],
         [ 0.6, 0.35436952],
         [ 0.8, 0.62999026],
         [ 0.9, 0.79733142]])

    assert_almost_equal(ref_fpoints, ildos_disc.fpoints)
    assert_almost_equal(ref_coordinates, ildos_disc.coordinates)

#    plt.plot(u, q, 'k.-')
#    plt.plot(ildos_disc.fpoints[:, 0], ildos_disc.fpoints[:, 1], 'go')
#    plt.plot(ildos_disc.coordinates[:, 0], ildos_disc.coordinates[:, 1], 'r.-')

# test_PLinearIldos()