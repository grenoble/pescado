''' - Compares the self consistent charge with that obtained by
    using the self consistent potential to calculated the charge with the ILDOS.

    - Compares the charge obtained with PESCADO when using the self consistent potential
    as input to the poisson problem

    - Compares the potential obtained with PESCADO when using the self consistent charge
    as input to the poisson problem.

    - Compares the result obtained using DiscreteIldos.from_continuos() with that DiscreteIldos()

    - Does it for 2D (
        There is no reason to do it in 3D - the SchrodingerPoisson problem has no idea of it).
'''
from collections.abc import Sequence

import numpy as np
from scipy import constants

from pescado.mesher import patterns
from pescado.mesher import shapes
from pescado.poisson import ProblemBuilder

from pescado.self_consistent import solver
from pescado.self_consistent import ildos as sc_ildos

from pescado.tools import SparseVector

from matplotlib import pyplot as plt

def gen_ildos_pesca(ef=0):
    def ildos_pesca(mu):
        aq = 1e-9
        t = constants.hbar**2 / (2. * constants.m_e * 0.067 * aq**2)
        dens = ((1 / t ** 2) 
                * (1 / (2 * np.pi))
                * 2 * (constants.elementary_charge ** 2))

        if not isinstance(mu, (np.ndarray, Sequence)):
            if mu <= ef:
                dens_ =  0
                ildos_ =  0
            else:
                dens_ = 2 * dens * (mu - ef)
                ildos_ = dens * ((mu - ef) ** 2)
        else:
            mu_zero = mu <= ef
            ildos_ = dens * ((mu - ef * np.ones(len(mu))) ** 2)
            ildos_[mu_zero] = 0
            dens_ = 2 * np.ones(len(mu)) * dens * (mu - ef * np.ones(len(mu)))

        return dens_, ildos_
    return ildos_pesca

def ildos_zero(mu):
    if not isinstance(mu, (np.ndarray, Sequence)):
        return 0, 0
    else:
        return np.zeros(len(mu)), np.zeros(len(mu))

def test_all_flex_quantum_self_consistent():
    ''' See description on the beginning of the script.
    '''

    ###########################################################################
    ######################## Define the Poisson System ########################
    ###########################################################################

    # Define the ProblemBuilder
    pb = ProblemBuilder()
    
    ### Define the Finite volume mesh
    # Begin defining the mesh spacing
    rect_pattern = patterns.Rectangular.constant(element_size=(4, 10))
    # Define the space occupied by the system
    system = shapes.Box(lower_left=[-1000, -55], size=[2000, 600])

    # Make the mesh
    pb.initialize_mesh(simulation_region=system, pattern=rect_pattern)

    ### Set relative dielectric permittivity
    dielectric = shapes.Box(lower_left=[-1000, -55], size=[2e3, 160])
    pb.set_relative_permittivity(val=12, region=dielectric)

    gate = (shapes.Box(lower_left=[-1000, 105], size=[650, 10])
            | shapes.Box(lower_left=[-100, 105], size=[200, 10])
            | shapes.Box(lower_left=[350, 105], size=[650, 10]))
                                
    ### Split sites into N, D, H and F.
    # Define the remaining regions
    dopants = shapes.Box(lower_left=[-1000, 35], size=[2000, 20])
    twodeg = shapes.Box(lower_left=[-1000, -4.9], size=[2000, 9.8])

    # Define local boundary conditions
    pb.set_metal(region=gate, setup_name='gate')
    pb.set_flexible(region=twodeg, setup_name='2deg')
    #pb.set_helmholtz(region=twodeg, setup_name='2deg')
    pb.set_neumann(region=dopants, setup_name='dopants')

    pp_problem = pb.finalized()

    ###########################################################################
    ########################    Define the SC inputs   ########################
    ###########################################################################
    ildos_pesca_0 = gen_ildos_pesca()
    
    disc_ildos4cont = sc_ildos.PContinuousIldos(
        coordinates=np.array([[-.8, ildos_pesca_0(-.8)[1]],
                              [0.8, ildos_pesca_0(0.8)[1]]]),
        functions=ildos_pesca_0)

    disc_ildos4func = sc_ildos.PContinuousIldos(
        coordinates=np.array([[-.8, 0],
                              [.0, 0],
                              [0.8, ildos_pesca_0(0.8)[1]]]),
        functions=[ildos_zero, ildos_pesca_0])

    e_f = 0.2
    ildos_pesca_ef = gen_ildos_pesca(e_f)

    disc_ildos4func_ef = sc_ildos.PContinuousIldos(
        coordinates=np.array([[-.8, 0],
                              [e_f, 0],
                              [0.8, ildos_pesca_ef(0.8)[1]]]),
        functions=[ildos_zero, ildos_pesca_ef])

    sv_gate = pp_problem.sparse_vector(val=-0.2, name='gate')
    sv_dop = pp_problem.sparse_vector(val=1e23 * 1e-27, name='dopants')
    poisson_input = {'voltage':sv_gate, 'charge_density':sv_dop}

    initial_guess = SparseVector(
        values=np.zeros(len(pp_problem.flexible_indices)),
        indices=pp_problem.flexible_indices, dtype=float)

    ###########################################################################
    ######################## Initialize the SC inputs #########################
    ###########################################################################

    sp_problem_cont = solver.PieceWiseDichotomy(
        ildos=disc_ildos4cont, poisson_problem=pp_problem)
    
    sp_problem_cont.solve(
        poisson_input=poisson_input,
        initial_guess=initial_guess,
        initial_fpoints=np.array([-1e-4, 1e-2]))

    charge_cont = sp_problem_cont.quantum_charge(-1)
    pot_cont = sp_problem_cont.chemical_potential(-1)

    sp_problem_func = solver.PieceWiseNewthonRaphson(
        ildos=disc_ildos4func, poisson_problem=pp_problem)

    sp_problem_func.solve(
        poisson_input=poisson_input,
        initial_guess=initial_guess)
    
    charge_func = sp_problem_func.quantum_charge(-1)
    pot_func = sp_problem_func.chemical_potential(-1)
    
    sp_problem_func_ef = solver.PieceWiseNewthonRaphson(
        ildos=disc_ildos4func_ef, poisson_problem=pp_problem, e_f=e_f)

    sp_problem_func_ef.solve(
        poisson_input=poisson_input,
        initial_guess=initial_guess)
    
    charge_func_ef = sp_problem_func_ef.quantum_charge(-1)
    pot_func_ef = sp_problem_func_ef.chemical_potential(-1)

    # plt.plot(charge_cont[pp_problem.flexible_indices], 'k.')
    # plt.plot(charge_func[pp_problem.flexible_indices], 'r.')
    # plt.plot(charge_func_ef[pp_problem.flexible_indices], 'm.')

    # plt.show()

    # plt.plot(pot_cont[pp_problem.flexible_indices], 'k.')
    # plt.plot(pot_func[pp_problem.flexible_indices], 'r.')
    # plt.plot(pot_func_ef[pp_problem.flexible_indices], 'm.')

    # plt.show()

    # plt.plot(ildos_pesca_0(pot_func[pp_problem.flexible_indices])[1], 'r.')
    # plt.plot(ildos_pesca_ef(pot_func_ef[pp_problem.flexible_indices])[1], 'm.')

    # plt.show()

    if np.max(np.abs(charge_cont[pp_problem.flexible_indices]
                     - charge_func[pp_problem.flexible_indices])) > 1e-14:

        raise RuntimeError(
            ('PieceWiseDichotomy vs PieceWiseNewthonRaphson'
             + 'do not share the same charge result'))

    if np.max(np.abs(charge_func[pp_problem.flexible_indices]
                        - charge_func_ef[pp_problem.flexible_indices])) > 1e-14:

        raise RuntimeError(
            ('DiscreteIldos.fromcontinus vs PieceWiseNewthonRaphson with ef'
             + 'do not share the same charge result'))

    if np.max(np.abs(pot_cont[pp_problem.flexible_indices]
                     - pot_func[pp_problem.flexible_indices])) > 1e-14:

        raise RuntimeError(
            ('PieceWiseDichotomy vs PieceWiseNewthonRaphson '
             + 'do not share the same potential result'))

    if np.max(np.abs(ildos_pesca_0(pot_cont.values)[1]
                     - charge_cont.values)) > 1e-14:

        raise RuntimeError(
            ('PieceWiseDichotomy vs ildos reference'
             + 'do not share the same charge result'))
    
    if np.max(np.abs(ildos_pesca_ef(pot_func_ef.values)[1]
                     - charge_func_ef.values)) > 1e-14:

        raise RuntimeError(
            ('PieceWiseDichotomy with ef vs ildos reference with ef'
             + 'do not share the same charge result'))    

    ###########################################################################
    ######################## Run pescado with full D  #########################
    ###########################################################################

    pp_problem.linear_problem_inst.reset_flexible_configuration()

    pp_problem.assign_flexible(dirichlet_index=pp_problem.flexible_indices)
    pp_problem.freeze()

    sv_volt = pot_cont.extract(pp_problem.flexible_indices)
    sv_volt.extend(sv_gate)

    potential_full_d, charge_full_d = pp_problem.solve(
        voltage=sv_volt, 
        charge_density=sv_dop)

    if np.max(np.abs(
        charge_cont[pp_problem.flexible_indices]
        - -1 * charge_full_d[pp_problem.flexible_indices])) > 1e-14:

        raise RuntimeError(
            ('Non linear Helmholtz with PESCADO with D initialization'
             + 'do not share the same charge result'))

    if np.max(np.abs(pot_cont[pp_problem.flexible_indices]
                     - potential_full_d[pp_problem.flexible_indices])) > 1e-14:

        raise RuntimeError(
            ('Non linear Helmholtz with PESCADO with D initialization'
             + 'do not share the same potentail result'))

    ###########################################################################
    ######################## Run pescado with full N  #########################
    ###########################################################################

    pp_problem.linear_problem_inst.reset_flexible_configuration()

    pp_problem.assign_flexible(neumann_index=pp_problem.flexible_indices)
    pp_problem.freeze()

    sv_gate = pp_problem.sparse_vector(val=-0.2, name='gate')

    sv_char = charge_func.extract(pp_problem.flexible_indices) * -1

    potential_full_n, charge_full_n = pp_problem.solve(
        voltage=sv_gate,
        charge=sv_char, 
        charge_density=sv_dop)

    if np.max(np.abs(
        charge_cont[pp_problem.flexible_indices]
        - -1 * charge_full_n[pp_problem.flexible_indices])) > 1e-14:

        raise RuntimeError(
            ('Non linear Helmholtz with PESCADO with N initialization'
             + 'do not share the same charge result'))

    if np.max(np.abs(pot_cont[pp_problem.flexible_indices]
                     - potential_full_n[pp_problem.flexible_indices])) > 1e-13:

        print(np.abs(pot_cont[pp_problem.flexible_indices]
                     - potential_full_n[pp_problem.flexible_indices]))
        raise RuntimeError(
            ('Non linear Helmholtz with PESCADO with N initialization'
             + 'do not share the same potentail result'))

def test_partial_flex_quantum_self_consistent():
    ''' See description on the beginning of the script.
    '''

    ###########################################################################
    ######################## Define the Poisson System ########################
    ###########################################################################

    # Define the ProblemBuilder
    pb_partial = ProblemBuilder()
    pb_full = ProblemBuilder()
    
    ### Define the Finite volume mesh
    # Begin defining the mesh spacing
    rect_pattern = patterns.Rectangular.constant(element_size=(4, 10))
    # Define the space occupied by the system
    system = shapes.Box(lower_left=[-1000, -55], size=[2000, 600])

    # Make the mesh
    pb_full.initialize_mesh(simulation_region=system, pattern=rect_pattern)
    pb_partial.initialize_mesh(simulation_region=system, pattern=rect_pattern)

    ### Set relative dielectric permittivity
    dielectric = shapes.Box(lower_left=[-1000, -55], size=[2e3, 160])
    pb_partial.set_relative_permittivity(val=12, region=dielectric)
    pb_full.set_relative_permittivity(val=12, region=dielectric)

    gate = (shapes.Box(lower_left=[-1000, 105], size=[650, 10])
            | shapes.Box(lower_left=[-100, 105], size=[200, 10])
            | shapes.Box(lower_left=[350, 105], size=[650, 10]))
                                
    ### Split sites into N, D, H and F.
    # Define the remaining regions
    dopants = shapes.Box(lower_left=[-1000, 35], size=[2000, 20])
    twodeg = shapes.Box(lower_left=[-1000, -4.9], size=[2000, 9.8])

    # Define local boundary conditions
    pb_partial.set_metal(region=gate, setup_name='gate')
    pb_full.set_metal(region=gate, setup_name='gate')
    
    pb_partial.set_flexible(region=twodeg | dopants, setup_name='flexible')
    pb_full.set_flexible(region=twodeg, setup_name='flexible')

    pb_full.set_neumann(region=dopants, setup_name='dopants')

    pp_full = pb_full.finalized()
    pp_partial = pb_partial.finalized()

    ###########################################################################
    ########################    Define the SC inputs   ########################
    ###########################################################################
    e_f = 0.2
    ildos_pesca_ef = gen_ildos_pesca(e_f)

    disc_ildos4func_ef = sc_ildos.PContinuousIldos(
        coordinates=np.array([[-.8, 0],
                              [e_f, 0],
                              [0.8, ildos_pesca_ef(0.8)[1]]]),
        functions=[ildos_zero, ildos_pesca_ef])

    sv_gate = pp_partial.sparse_vector(val=-0.2, name='gate')
    sv_dop_charge = pp_partial.sparse_vector(
        val=1e23 * 1e-27, region=dopants, density_to_charge=True)
    sv_dop = pp_partial.sparse_vector(val=1e23 * 1e-27, region=dopants)
    ###########################################################################
    ######################## Initialize the SC inputs #########################
    ###########################################################################
    sp_problem_func_ef_p = solver.PieceWiseNewthonRaphson(
        ildos=disc_ildos4func_ef, poisson_problem=pp_partial, e_f=e_f)

    quantum_sites_partial = pp_partial.points_inside(region=twodeg)
    initial_guess_partial = SparseVector(
        values=np.zeros(len(quantum_sites_partial)),
        indices=quantum_sites_partial, dtype=float)
    

    poisson_input_partial = {
        'voltage':sv_gate, 
        'flexible_input':{'charge':sv_dop_charge}}

    sp_problem_func_ef_p.solve(
        poisson_input=poisson_input_partial,
        initial_guess=initial_guess_partial)
    
    charge_func_ef_p = sp_problem_func_ef_p.quantum_charge(-1)
    pot_func_ef_p = sp_problem_func_ef_p.chemical_potential(-1)
    
    sp_problem_func_ef_f = solver.PieceWiseNewthonRaphson(
        ildos=disc_ildos4func_ef, poisson_problem=pp_partial, e_f=e_f)

    quantum_sites_full = pp_full.points_inside(region=twodeg)
    initial_guess_full = SparseVector(
        values=np.zeros(len(quantum_sites_full)),
        indices=quantum_sites_full, dtype=float)
    
    poisson_input_full = {
        'voltage':sv_gate, 'charge_density':sv_dop}

    sp_problem_func_ef_f.solve(
        poisson_input=poisson_input_full,
        initial_guess=initial_guess_full)
    
    charge_func_ef_f = sp_problem_func_ef_f.quantum_charge(-1)
    pot_func_ef_f = sp_problem_func_ef_f.chemical_potential(-1)

    # plt.plot(charge_func_ef_p[quantum_sites_partial], 'm.')
    # plt.plot(charge_func_ef_f[quantum_sites_full], 'm.')
    # plt.plot(ildos_pesca_ef(pot_func_ef_f[quantum_sites_full])[1], 'm.')
    # plt.show()
    # plt.plot(pot_func_ef_p[quantum_sites_partial], 'm.')
    # plt.plot(pot_func_ef_f[quantum_sites_full], 'm.')
    # plt.show()

    if np.max(np.abs(charge_func_ef_p[quantum_sites_partial]
                     - charge_func_ef_f[quantum_sites_full])) > 1e-14:

        raise RuntimeError(
            ('PieceWiseNewthonRaphson partial vs PieceWiseNewthonRaphson'
             + 'full do not share the same charge result'))    

    if np.max(np.abs(ildos_pesca_ef(pot_func_ef_f.values)[1]
                     - charge_func_ef_f.values)) > 1e-14:

        raise RuntimeError(
            ('PieceWiseNewthonRaphson with ef vs ildos reference with ef'
             + 'do not share the same charge result'))    


def test_constructor():
    '''
    '''
    ###########################################################################
    ######################## Define the Poisson System ########################
    ###########################################################################

    # Define the ProblemBuilder
    pb = ProblemBuilder()
    
    ### Define the Finite volume mesh
    # Begin defining the mesh spacing
    rect_pattern = patterns.Rectangular.constant(element_size=(4, 10))
    # Define the space occupied by the system
    system = shapes.Box(lower_left=[-1000, -55], size=[2000, 600])

    # Make the mesh
    pb.initialize_mesh(simulation_region=system, pattern=rect_pattern)

    ### Set relative dielectric permittivity
    dielectric = shapes.Box(lower_left=[-1000, -55], size=[2e3, 160])
    pb.set_relative_permittivity(val=12, region=dielectric)

    gate = (shapes.Box(lower_left=[-1000, 105], size=[650, 10])
            | shapes.Box(lower_left=[-100, 105], size=[200, 10])
            | shapes.Box(lower_left=[350, 105], size=[650, 10]))
                                
    ### Split sites into N, D, H and F.
    # Define the remaining regions
    dopants = shapes.Box(lower_left=[-1000, 35], size=[2000, 20])
    twodeg = shapes.Box(lower_left=[-1000, -4.9], size=[2000, 9.8])

    # Define local boundary conditions
    pb.set_metal(region=gate, setup_name='gate')
    pb.set_flexible(region=twodeg, setup_name='2deg')
    #pb.set_helmholtz(region=twodeg, setup_name='2deg')
    pb.set_neumann(region=dopants, setup_name='dopants')

    pp_problem = pb.finalized()

    ###########################################################################
    ########################    Define the SC inputs   ########################
    ###########################################################################
    e_f = 0.2
    ildos_pesca_ef = gen_ildos_pesca(e_f)

    disc_ildos4func_ef = sc_ildos.PContinuousIldos(
        coordinates=np.array([[-.8, 0],
                              [e_f, 0],
                              [0.8, ildos_pesca_ef(0.8)[1]]]),
        functions=[ildos_zero, ildos_pesca_ef])

    sv_gate = pp_problem.sparse_vector(val=-0.2, name='gate')
    sv_dop = pp_problem.sparse_vector(val=1e23 * 1e-27, name='dopants')
    poisson_input = {'voltage':sv_gate, 'charge_density':sv_dop}

    initial_guess = SparseVector(
        values=np.zeros(len(pp_problem.flexible_indices)),
        indices=pp_problem.flexible_indices, dtype=float)

    ###########################################################################
    ######################## Initialize the SC inputs #########################
    ###########################################################################

    sp_problem_cont = solver.PieceWiseDichotomy(
        ildos=disc_ildos4func_ef, poisson_problem=pp_problem, e_f=e_f)
    sp_problem_cont.solve(
        poisson_input=poisson_input,
        initial_guess=initial_guess,
        initial_fpoints=np.array([-1e-4, 0.3]))
    
    charge_cont = sp_problem_cont.quantum_charge(-1)
    pot_cont = sp_problem_cont.chemical_potential(-1)

    sp_problem_cont_wr = solver.thomas_fermi(
        ildos=disc_ildos4func_ef, poisson_problem=pp_problem, e_f=e_f,
        method='PieceWiseDichotomy')
    
    sp_problem_cont_wr.solve(
        poisson_input=poisson_input,
        initial_guess=initial_guess,
        initial_fpoints=np.array([-1e-4, 0.3]))
    
    charge_cont_wr = sp_problem_cont_wr.quantum_charge(iteration=-1)
    pot_cont_wr = sp_problem_cont_wr.chemical_potential(iteration=-1)
    
    sp_problem_func_wr = solver.thomas_fermi(
        ildos=disc_ildos4func_ef, poisson_problem=pp_problem, e_f=e_f,
        method='PieceWiseNewthonRaphson')
    
    sp_problem_func_wr.solve(
        poisson_input=poisson_input,
        initial_guess=initial_guess,
        initial_fpoints=np.array([-1e-4, 0.3]))
    
    charge_func_wr = sp_problem_func_wr.quantum_charge(iteration=-1)
    pot_func_wr = sp_problem_func_wr.chemical_potential(iteration=-1)

    # plt.plot(charge_cont[pp_problem.flexible_indices], 'k.')
    # plt.plot(charge_func_wr[pp_problem.flexible_indices], 'r.')
    # plt.plot(charge_cont_wr[pp_problem.flexible_indices], 'm.')

    # plt.show()

    # plt.plot(pot_cont[pp_problem.flexible_indices], 'k.')
    # plt.plot(pot_cont_wr[pp_problem.flexible_indices], 'r.')
    # plt.plot(pot_func_wr[pp_problem.flexible_indices], 'm.')

    # plt.show()

    if np.max(np.abs(charge_cont[pp_problem.flexible_indices]
                     - charge_func_wr[pp_problem.flexible_indices])) > 1e-14:

        raise RuntimeError(
            ('PieceWiseDichotomy vs ThomasFermi Dichothomy'
             + 'do not share the same charge result'))

    if np.max(np.abs(charge_cont[pp_problem.flexible_indices]
                     - charge_cont_wr[pp_problem.flexible_indices])) > 1e-14:

        raise RuntimeError(
            ('PieceWiseDichotomy vs ThomasFermi NR'
             + 'do not share the same charge result'))

    if np.max(np.abs(pot_cont[pp_problem.flexible_indices]
                     - pot_cont_wr[pp_problem.flexible_indices])) > 1e-14:

        raise RuntimeError(
            ('PieceWiseDichotomy vs ThomasFermi Dichothomy'
             + 'do not share the same charge result'))

    if np.max(np.abs(pot_cont.values
                     - pot_func_wr.values)) > 1e-14:

        raise RuntimeError(
            ('PieceWiseDichotomy vs ThomasFermi NR'
             + 'do not share the same charge result'))

# test_all_flex_quantum_self_consistent()
# test_partial_flex_quantum_self_consistent()
# test_wrapper()