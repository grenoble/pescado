from collections.abc import Sequence

import numpy as np

from scipy import constants

import copy

from pescado import mesher, poisson, tools
from pescado.self_consistent import solver
from pescado.self_consistent import ildos as sc_ildos

from pescado.tools import SparseVector

from matplotlib import pyplot as plt

def ildos_pesca(mu):

    aq = 4e-9
    t = constants.hbar**2 / (2. * constants.m_e * 0.067 * aq**2)
    
    dens = (
        (1 / t ** 2) * (1 / (2 * np.pi)) * 16
         * (constants.elementary_charge ** 2))
    tang = 2 * dens * mu

    if not isinstance(mu, (np.ndarray, Sequence)):
        if mu <= 0:
            val =  (0, 0)
        else:
            val = (tang, dens * (mu ** 2))
    else:
        mu_zero = mu <= 0
        val = (tang * np.ones(len(mu)), dens * (mu ** 2))
        
        val[0][mu_zero] = np.zeros(sum(mu_zero))
        val[1][mu_zero] = np.zeros(sum(mu_zero))

    return val


def test_nr_vs_pescado_hot_dog():
    ''' Compares the result using Newton-Raphson to that
    using PESCADO for the hot dog geometry. ILDOS is
    a quadratic ildos.
    '''

    ###########################################################################
    ######################## Define the Poisson System ########################
    ###########################################################################

    eps1 = 12
    epgate = 30
    or_gate = 150

    ep_dop = 50
    or_dop = 50

    size = np.array([800, 800])

    pattern_coarse = mesher.patterns.Rectangular.constant(
        element_size=(40, 40))

    shape_all = mesher.shapes.Box(lower_left=-1 * size / 2, size=size)

    shape_fine = mesher.shapes.Box(
        lower_left=(-140, -40), size=(360, 260))

    pp_builder = poisson.ProblemBuilder()
    pp_builder.initialize_mesh(
        simulation_region=shape_all, pattern=pattern_coarse)

    gate_c = mesher.shapes.Box(
        lower_left=(-11, or_gate), size=(24, epgate))
    gate_l = mesher.shapes.Box(
        lower_left=(-90, or_gate), size=(30, epgate))
    gate_r = mesher.shapes.Box(
        lower_left=(60, or_gate), size=(30, epgate))

    dopant = mesher.shapes.Box(
        lower_left=(-110, or_dop), size=(220, ep_dop))

    dielectric = (shape_all - (gate_c | gate_l | gate_r | dopant))

    gaz = mesher.shapes.Box(
        lower_left=(-80 + 1, -0.5), size=(160 - 2, 1))

    pattern_fine = mesher.patterns.Rectangular.constant(
        element_size=(10, 6))

    pp_builder.refine_mesh(
        region=shape_fine, pattern=pattern_fine)

    rel_die_list = [eps1, eps1]
    shapes_list = [dielectric, gaz]

    for rel_die, shape_inst in zip(rel_die_list, shapes_list):
        pp_builder.set_relative_permittivity(
            val=rel_die, region=shape_inst)

    pp_builder.set_metal(
        region=gate_c, setup_name='gate_c')
    pp_builder.set_metal(
        region=gate_l, setup_name='gate_l')
    pp_builder.set_metal(
        region=gate_r, setup_name='gate_r')

    pp_builder.set_flexible(
        region=gaz, setup_name='gaz')

    pp_problem = pp_builder.finalized()

    ###########################################################################
    ########################     Define the Inputs     ########################
    ###########################################################################

    disc_ildos = sc_ildos.PContinuousIldos(
        coordinates=np.array([[-.8, ildos_pesca(-.8)[1]],
                              [0.8, ildos_pesca(0.8)[1]]]),
        functions=ildos_pesca)
    
    functional_points = np.array([-1e-4, 1e-2])

    sv_gate = pp_problem.sparse_vector(val=0.0, region=gate_c) #-0.7
    sv_gate.extend(pp_problem.sparse_vector(val=-0.25, region=gate_r)) #-3
    sv_gate.extend(pp_problem.sparse_vector(val=-0.25, region=gate_l)) #-3

    # at 0.2 it spends a lot of time !!!!! see NR for compârison
    sv_dop = pp_problem.sparse_vector(
        val=0.4 * 1e23 * 1e-27, region=dopant, density_to_charge=True) # 2

    poisson_problem_input = {
        'voltage':sv_gate,
        'charge_electrons':sv_dop}

    initial_guess = SparseVector(
        values=np.zeros(len(pp_problem.flexible_indices)),
        indices=pp_problem.flexible_indices, dtype=float)

    ###########################################################################
    ########################      PESCADO METHOD       ########################
    ###########################################################################

    sp_problem = solver.PieceWiseDichotomy(
        ildos=disc_ildos, poisson_problem=pp_problem)

    sp_problem.solve(
        poisson_input=poisson_problem_input,
        initial_guess=initial_guess,
        initial_fpoints=functional_points)
    
    charge_PESCADO = sp_problem.quantum_charge(-1)
    potential_PESCADO = sp_problem.chemical_potential(-1)

    ###########################################################################
    ########################   Newton-Raphson METHOD   ########################
    ###########################################################################

    # plt.plot(charge_PESCADO[pp_problem.flexible_indices])
    # plt.show()
    # plt.plot(potential_PESCADO[pp_problem.flexible_indices])
    # plt.show()

    disc_ildos = sc_ildos.PContinuousIldos(
        coordinates=np.array([[-.8, ildos_pesca(-.8)[1]],
                              [.0, ildos_pesca(.0)[1]],
                              [0.8, ildos_pesca(0.8)[1]]]),
        functions=[ildos_pesca, ildos_pesca])

    sp_problem_nr = solver.PieceWiseNewthonRaphson(
        ildos=disc_ildos, poisson_problem=pp_problem)

    sp_problem_nr.solve(
        poisson_input=poisson_problem_input, 
        initial_guess=initial_guess)

    charge_NR = sp_problem_nr.quantum_charge(-1)
    potential_NR = sp_problem_nr.chemical_potential(-1)

    non_zero_idx = pp_problem.flexible_indices[
        charge_PESCADO[pp_problem.flexible_indices] != 0]

    # plt.plot(charge_PESCADO.values, 'r.')
    # plt.plot(charge_NR.values, 'k.')
    # plt.show()
    # plt.plot(potential_PESCADO.values, 'r.')
    # plt.plot(potential_NR.values, 'k.')
    # plt.show()

    err_char = (
        np.abs(charge_PESCADO[non_zero_idx] - charge_NR[non_zero_idx])
        / np.abs(charge_PESCADO[non_zero_idx])) * 100

    if len(non_zero_idx) > 0:
        err_char = (
            np.abs(charge_PESCADO[non_zero_idx] - charge_NR[non_zero_idx])
            / np.abs(charge_PESCADO[non_zero_idx]))
    else:
        err_char = np.abs(
            charge_PESCADO[pp_problem.flexible_indices] 
            - charge_NR[pp_problem.flexible_indices])

    # Error for 10 NR iterations
    msg = 'Charge relative error larger than expected'
    assert (np.max(err_char) < 1e-10), msg

    non_zero_idx = pp_problem.flexible_indices[
        potential_PESCADO[pp_problem.flexible_indices] != 0]

    if len(non_zero_idx) > 0:
        err_pot = (
            np.abs(potential_PESCADO[non_zero_idx]- potential_NR[non_zero_idx])
            / np.abs(potential_PESCADO[non_zero_idx])) * 100
    else:
        err_pot = np.abs(
            potential_PESCADO[pp_problem.flexible_indices] 
            - potential_NR[pp_problem.flexible_indices])

    msg = 'Potential relative error larger than expected'
    assert (np.max(err_pot) < 1e-10), msg
    

# test_nr_vs_pescado_hot_dog()
