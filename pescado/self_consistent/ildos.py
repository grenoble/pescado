from abc import ABC, abstractmethod

from collections.abc import Sequence

import copy
import numpy as np
import warnings

from pescado.tools import meshing

def piecewise_linear_function(
        right_tangent, left_tangent, bounds,
        tol_dos=1e-14, tol_q=1e-10, tol_u=1e-10):
    ''' Generates a piecewise linear function connecting 'left_tangent' to
    'right_tangent'. Here 'left_tangent' specify the tangent values at left
    'bounds' and 'right_tangent' the ones in the right 'bounds'

    !!! The resulting piecewise linear function is monotonically increasing) !!!
    
    TODO :: REFACTOR THIS CODE !!!!
    
    Parameters
    -----------

    right_tangent: sequence of two floats

        right_tangent[0]: linear coefficient ('dos_r')
        right_tangent[1]: constant           ('origin_r)

                q := origin_r + dos_r * u

    left_tangent: idem as 'right_tangent'

        Defines the behaviour of the linear interval defined within
        'bounds'

        left_tangent[0]: linear coefficient ('dos_l')
        left_tangent[1]: constant           ('origin_l)

                q := origin_l + dos_l * u

    bounds: Sequence of two floats
        Interval where the current linear interval is defined

        ps: q_l_min = origin_l + dos_l * bounds[0]
            q_r_max = origin_r + dos_r * bounds[1]

    tol_dos, tol_q, tol_u = float (default 1e-14)

    Returns
    --------
    new_interval: Sequence of two np.arrays or None

        a) sequence of two np.arrays:

            i) np.array of floats with shape(n, 2)

                Coordinates defining the n piecewise linear intervals

                n = 1 - one interval, right_tangent intersects with left_tangent
                    within 'bounds'.

                n = 2 - two intervals, right_tangent does not intersect with
                    left_tangent within 'bounds'. Hence a second interval is
                    added to ensure the linear interval defined by the
                    left tangent is continuously connected to the one defined
                    by the right tangent.

            ii) np.array of floats with shape(2, ) or None

                if n == 1: then None

                else: return the 'dos' and 'origin' for the linear
                interval added to connect the two tangents

        b) None:
            It means:
                np.abs(right_tangent[0] - left_tangent[0]) < tol_dos
                np.abs(right_tangent[1] - left_tangent[1]) < tol_q

    '''

    dos_l, origin_l = left_tangent
    dos_r, origin_r = right_tangent

    dos_origin = np.array(
        [[dos_l, origin_l],
         [dos_r, origin_r]])

    # if ((np.abs(dos_l - dos_r) < tol_dos)
    #      and (np.abs(origin_l - origin_r) < tol_q)):
    #     new_interval = None
    # else:

    if np.all(dos_origin[:, 0] == np.infty):
        intersection = np.ones((1, 2)) * np.infty
    elif np.any(dos_origin[:, 0] == np.infty):
        map2infty = dos_origin[:, 0] == np.infty
        intersection = (
            np.ones((1, 2)) * bounds[:, 0][map2infty])
        intersection[0, 1] = (
            dos_origin[:, 1][np.logical_not(map2infty)]
            + (dos_origin[:, 0][np.logical_not(map2infty)]
                * intersection[0, 0]))
    elif (np.abs(origin_r - origin_l) <= tol_q
          and np.abs(dos_l - dos_r) <= tol_dos):
        intersection = np.average(bounds, axis=0)[None, :]

    else:
        intersection = (
            np.ones((1, 2)) * (origin_r - origin_l) / (dos_l - dos_r))
        intersection[0, 1] = (
            dos_origin[0, 1] + dos_origin[0, 0] * intersection[0, 0])

    if (np.all((intersection - bounds[0]) >= [-tol_u, -tol_q])
        and np.all((intersection - bounds[1]) <= [tol_u, tol_q])):
        new_interval = [intersection, None]
    else:

        # Evaluate
        # f(u) := (origin_l - origin_r) + (dos_l - dos_r) * u
        monotonicity = np.sign(dos_l - dos_r)
        type_ = None

        if np.all(dos_origin[:, 0] == np.infty):
            type_ = 'neumann'
        elif monotonicity < 0 and np.any(intersection > bounds[1, :]):
            type_ = 'neumann'
        elif monotonicity > 0 and np.any(intersection < bounds[0, :]):
            type_ = 'neumann'
        elif np.abs(dos_origin[0, 0] - dos_origin[1, 0]) < tol_dos:
            type_ = 'dirichlet'
        elif monotonicity < 0 and np.any(intersection < bounds[0, :]):
            type_ = 'dirichlet'
        elif monotonicity > 0 and np.any(intersection > bounds[1, :]):
            type_ = 'dirichlet'
        else:
            raise RuntimeError('Failed to find intermediary interval')

        if type_ == 'dirichlet':

            new_u = np.sum(bounds[:, 0]) / 2
            new_interval = [
                np.array([
                    [new_u, origin_l + dos_l * new_u],
                    [new_u, origin_r + dos_r * new_u]]),
                np.array([np.infty, 0])]

            for i, (dos, origin) in enumerate(dos_origin):
                if dos == np.infty:
                    new_interval[0][i, 1] = bounds[i, 1]

        elif type_ == 'neumann':

            new_q = np.sum(bounds[:, 1]) / 2
            new_interval = [
                np.array([[bounds[0, 0], new_q], [bounds[1, 0], new_q]]),
                np.array([0, new_q])]

            for i, (dos, origin) in enumerate(dos_origin):
                if origin != np.infty:
                    new_interval[0][i, 0] = (new_q - origin) / dos
        else:
            raise RuntimeError('Unknown type of interval')

    if new_interval is not None:
        if (np.any((new_interval[0] - bounds[0]) < [-tol_u, -tol_q])
            or np.any((new_interval[0] - bounds[1]) > [tol_u, tol_q])):

            print('new_interval: {0}'.format(new_interval))
            print('bounds: {0}'.format(bounds))
            print('dos_origin: {0}'.format(dos_origin))

            raise RuntimeError(
                'Failed to find intersection inside the bounds')

    return new_interval


class Ildos(ABC):
    ''' Defines a minimal behaviour for SchrodingerPoisson ildos
    By definition monotonically increasing function
    '''

    def interval(self, u):
        ''' Returns the interval in which 'u' is located

        Parameters
        -----------

        u: np.ndarray of floats with shape(n, )
            n: number of chemical potentials

        Returns
        --------
        np.ndarray of integers of shape(n, ) containing the interval
            for the chemical potential 'u'.

        '''

        if not isinstance(u, (np.ndarray, Sequence)):
            u_ = [u, ]
        else:
            u_ = u

        int_r = np.searchsorted(self.coordinates[1:, 0], u_, side='right')
        int_l = np.searchsorted(self.coordinates[1:, 0], u_, side='left')

        if np.any(int_r - int_l) > 2:
            raise RuntimeError(
                'Error when finding the intervals for u={0}'.format(
                    u_[(int_r - int_l) > 2]))

        intervals = int_l.astype(int)

        # This ensures if it borders a Dirichlet interval
        # the 'u' interval is found correctly
        intervals[(int_r - int_l) == 2] += 1
        intervals[intervals == (len(self.coordinates) - 1)] -= 1

        if not isinstance(u, (np.ndarray, Sequence)):
            intervals = intervals[0]

        return intervals

    def __call__(self, u, return_intervals=False):
        ''' Returns the DOS and ILDOS for a set of 'u'

        Parameters
        -----------

        u: np.array of float with shape(n, )
            Values of chemical potential - in eV.

        return_intervals: boolean (default False)
            If True also return the intervals

        Returns
        --------

        dos: np.ndarray of float with shape(n, )
            Values of density of states - in e.eV^-1

        ildos: np.ndarray of float with shape(n, )

            Values of the integrated density of
            states up to u - in elementary charge

        If return intervals is true - also returns

        intervals: np.ndarray of integers with shape(n, )

            Index of the intervals for 'u'

        -------------------------- Note -----------------------------

            Calls abstract method 'self._dos_ildos(u, intervals)'
        '''

        intervals = self.interval(u)
        dos, ildos = self._dos_ildos(u=u, intervals=intervals)

        if return_intervals:
            return dos, ildos, intervals
        else:
            return dos, ildos

    @abstractmethod
    def _dos_ildos(self, u, interval):
        ''' Returns the 'dos', 'ildos' for a given 'u' located at a
        given 'interval' of 'self'.

        Parameters
        -----------

        u: np.array of float with shape(n, )
            Values of chemical potential - in eV.

        intervals: np.ndarray of integers with shape(n, )
            Index of the intervals for 'u'

        Returns
        --------

        dos: np.ndarray of float with shape(n, )
            Values of density of states - in e.eV^-1

        ildos: np.ndarray of float with shape(n, )

            Values of the integrated density of
            states up to u - in elementary charge

        '''

        pass

class PLinearIldos(Ildos):
    ''' Piecewise linear Ildos
    '''

    def __init__(
        self, coordinates, dos, origin, fpoints=None,
        tol_dos=1e-15, tol_q=1e-15, tol_u=1e-15):
        ''' Ildos

        Parameters
        -----------

        coordinates: np.ndarray of float with shape(n+1, 2)

            Beginning and end of each ildos interval

            [coordinates[i, 0], coordinates[i+1, 0]) the chemical potential
             bounds for interval 'i' -> in eV

            [coordinates[i, 1], coordinates[i+1, 1]) the charge
             bounds for interval 'i' -> in elementary charge

        dos: np.ndarray of floats with shape(n, )
            Density of states for the 'i'th interval

        origin: np.ndarray of floats with shape(n, )
            Charge value origin of the linear 'i'th interval

        fpoints: np.ndarray of floats with shape(n, ) (default None)

            Defines the (u, q) inside the coordinates interval
            where the tangent defined by 'dos, origin' is assumed exact.

            If default, it takes the center of the interval.

        tol_dos, tol_u, tol_q = float (default 1e-14)
            tolerance regarding dos, u and q:= origin + dos * u

        Note q[i] = origin[i] + dos[i] * u

        !!  Notice !!!

            If coordinates[i, 0] = coordinates[i+1, 0] then it is a
            constant potential interval -> \rho = infty

            If coordinates[i, 1] = coordinates[i+1, 1] then it is a
            constant charge interval -> \rho = 0 and n = coordinates[i, 1]

        MODIFICATIONS ?? -> It does not know the initial continuous ildos
            Would it be usefull for it to know about it ???? 

        '''

        self.coordinates = coordinates

        if not isinstance(dos, (np.ndarray, Sequence)):
            dos = np.array([dos, ])
            origin = np.array([origin, ])
            if fpoints is not None:
                fpoints = fpoints[None, :]

        self.dos = dos
        self.origin = origin

        if fpoints is None:
            fpoints = (
                self.coordinates
                + np.roll(self.coordinates, shift=-1, axis=0))[:-1, :] / 2

        self.fpoints = fpoints

        self.tol_dos = tol_dos
        self.tol_q = tol_q
        self.tol_u = tol_u

    def functional_interval(self, u, origin, dos):
        ''' Returns the intervals containing the two closest
        fpoints. Also returns the coordinates of the ILDOS containing
        (u,q) and the closest fpoints.

        Parameters
        -----------

        u: float
            chemical potential (eV)

        dos: float
            charge per chemical potential (eV^-1)

        origin: float
            charge

        Returns
        --------

        surrounding_intervals: np.array of integers with shape(n)

            Intervals containing the two functional points

            n: number of intervals

        surrounding_coordinates: np.array of floats with shape(n, 2)

            n: number of intervals

        If n=1 it means (u,q) is at one of the borders.
        '''

        if np.any(np.isnan(self.fpoints[:, 0]) != np.isnan(self.fpoints[:, 1])):
            raise RuntimeError('Error in self.fpoints {0}'.format(self.fpoints))

        fpoint_intervals = np.arange(len(self.fpoints), dtype=int)[
            ~np.isnan(self.fpoints[:, 0])]

        sign_diff = np.sign(u - self.fpoints[fpoint_intervals, 0])
        sign_diff = sign_diff[sign_diff >= 0]
        interval = len(sign_diff[sign_diff >= 0])

        # Belong to the border
        if interval == 0:
            surrouding_intervals = np.array([fpoint_intervals[0], ])
            surrouding_coordinates = [
                self.coordinates[surrouding_intervals + 1, :], ]
        elif interval == len(fpoint_intervals):
            surrouding_intervals = np.array([fpoint_intervals[-1], ])
            surrouding_coordinates = [
                self.coordinates[surrouding_intervals, :], ]
        else:
            
            surrouding_intervals = np.array(
                [fpoint_intervals[interval - 1],
                 fpoint_intervals[interval]])

            surrouding_coordinates = [
                self.coordinates[surrouding_intervals[0], :][None, :],
                self.coordinates[surrouding_intervals[1]+1, :][None, :]]
            
        surrouding_intervals = np.array(surrouding_intervals, dtype=int)

        return surrouding_intervals, surrouding_coordinates

    def _dos_ildos(self, u, intervals=False):
        ''' Returns the DOS and ILDOS for a set of 'u'

        Parameters
        -----------

        u: np.array of float with shape(n, )
            Values of chemical potential - in eV.

        intervals: boolean (default False)
            If True also return the intervals

        Returns
        --------

        dos: np.ndarray of float with shape(n, )
            Values of density of states - in e.eV^-1

        ildos: np.ndarray of float with shape(n, )

            Values of the integrated density of
            states up to u - in elementary charge

        If return intervals is true - also returns

        intervals: np.ndarray of integers with shape(n, )

            Index of the intervals for 'u'

        '''
        ildos = self.origin[intervals] + self.dos[intervals] * u
        return self.dos[intervals], ildos

    def add_interval(self, u, dos, origin, q=None):
        ''' Adds the interval defined from the tangent at
        'u', with slope 'dos' and 'origin'.

        TODO: Fix bug appearing when two redundent 
        points are added. See paper_ii/..../SCQE.ipynb for the 
        code reproducing it (Oct 5 2024)

        TODO: Refactor this method !!!

        TODO: Change algorithm using an "imaginary functional point"
            -> internal list of functional points
            -> external list of functional points

        TODO: Deal with border conditions !!
        
        Parameters
        -----------

        u: float
            Chemical potential (in eV)

        q: float (optional)
            charge at 'u'

        dos: float
            Slope of the tangent at 'u', (in eV^-1)

        origin: float
            Origin of the tangent at 'u'

        '''

        if dos < 0:
            raise ValueError(
                'Ildos is an increasing function, however dos:{0}'.format(dos))
        elif (dos == np.infty and q is None):
            raise ValueError(
                'dos is {0}, please define a q'.format(dos))

        if np.any(np.abs(self.fpoints[:, 0] - u) <  self.tol_u):
            raise RuntimeError(
                'u:{0} already exists.'.format(u))
        if (q is None) and (dos != np.infty):
            q = origin + dos *  u
        elif (q is None) and (dos == np.infty):
            raise RuntimeError(
                'q is None when the dos is {0}'.format(dos))

        ########### Find interval and surrouding functional points ###########
        surrouding_intervals, surrouding_coordinates = self.functional_interval(
            u=u, dos=dos, origin=origin)
        
        quadrant = np.sign(
            np.array([u, q])
            - self.fpoints[surrouding_intervals[-1], :]).astype(int)

        if (np.prod(quadrant) < 0):

            raise RuntimeError(
                'Adding (u:'
                +'{0}, q:{1}) to {2} breaks its monotonicity.'.format(
                    u, q, self))

        # Check if adding this functional point is redundant
        not_redundant = True
        int_dos, int_ildos = self._dos_ildos(u, self.interval(u))

        # If it has the same dos / origin
        if np.all(
            np.abs([dos - int_dos, q - int_ildos]) <= [
                    self.tol_dos, self.tol_q]):
            not_redundant = False

        ########  Connect the tangent 'dos, origin' defined at 'u' to ########
        ################       its surrouding tangents       ################
        if not_redundant:

            dos_origin = [(dos, origin), ]
            fpoints = [np.array([u, q]), ]

            # Recover the tangents and fpoints
            for int_ in surrouding_intervals:
                fpoints.append(self.fpoints[int_])
                dos_origin.append((self.dos[int_], self.origin[int_]))
            
            ordering = np.argsort(np.array(fpoints)[:, 0])

            fpoints = np.array(fpoints)[ordering]
            dos_origin = np.array(dos_origin)[ordering]

            pos_p_interval = list()
            fppoints = list()
            pdos_origin = list()
            not_redundant = False

            # Connect the tangents
            skip = True
            for i in range(len(fpoints) - 1):
                dos_l, origin_l = dos_origin[i]
                dos_r, origin_r = dos_origin[i+1]
                if ((np.abs(dos_l - dos_r) > self.tol_dos)
                    or (np.abs(origin_l - origin_r) > self.tol_q)):
                    skip = False
            
            if not skip:
                for i in range(len(fpoints) - 1):

                    ni = piecewise_linear_function(
                        right_tangent=dos_origin[i+1], left_tangent=dos_origin[i],
                        bounds=fpoints[i:i+2], 
                        tol_dos=self.tol_dos, tol_q=self.tol_q, tol_u=self.tol_u)

                    # If None is because 'dos' = 'self.dos[neig_int]' and
                    # 'origin' = 'self.origin[neig_int]'
                    if ni != None:
                        # If an additional interval is needed to connect
                        # the two tangents
                        if ni[0].shape[0] == 2:
                            pos_p_interval.append(i+1)
                            fppoints.append(np.array([np.nan, np.nan]))
                            pdos_origin.append(ni[1])
                        
                        # The nan is added to keep the number of 
                        # intervals the same as number of fpoints
                        surrouding_coordinates.append(ni[0])

            if len(pos_p_interval) > 0:
                fpoints = np.insert(
                    fpoints, pos_p_interval, fppoints, axis=0)
                dos_origin = np.insert(
                    dos_origin, pos_p_interval, pdos_origin, axis=0)

            surrouding_coordinates = meshing.sequencial_sort(
                np.concatenate([c for c in surrouding_coordinates]),
                order=(0, 1))

            # This means a point is at the border
            if len(surrouding_intervals) == 1:

                if ((u - fpoints[0, 0]) <= self.tol_u
                    and ((q - fpoints[0, 1]) <= self.tol_dos 
                         or (q == fpoints[0, 1]))):
                    pos, sign, add_pos = 0, -1, 0
                elif ((u - fpoints[-1, 0]) <= self.tol_u
                      and ((q - fpoints[-1, 1]) <= self.tol_dos 
                           or (q == fpoints[-1, 1]))):
                    pos, sign, add_pos = len(surrouding_coordinates), +1, -1
                else:
                    raise RuntimeError('Failed to add u:{0}'.format(u))
                # Extend the border
                if np.any(
                    sign * (self.coordinates[add_pos] - np.array([u, q])) >= [
                        self.tol_u, self.tol_q]):
                    add_coord = self.coordinates[add_pos]
                else:
                    add_coord = copy.deepcopy(surrouding_coordinates[add_pos])
                    add_coord[0] += np.abs(
                        surrouding_coordinates[add_pos, 0] - u) * sign

                if dos == np.infty:
                    add_coord[1] += np.abs(add_coord[1] - q) * sign
                else:
                    add_coord[1] = origin + add_coord[0] * dos

                surrouding_coordinates = np.insert(
                    surrouding_coordinates, pos, add_coord, axis=0)

            ###########  Update the ildos section sourrounding 'u' ###########
            self.update(
                section=(
                    np.min(surrouding_intervals),
                    np.max(surrouding_intervals) + 2),
                coordinates=surrouding_coordinates,
                dos=dos_origin[:, 0], origin=dos_origin[:, 1], fpoints=fpoints)

        elif (u - self.coordinates[0, 0]) < -self.tol_u:
            # Extend bounds to the left
            self.coordinates[0, 0] = u + (u - self.fpoints[0, 0])
            self.fpoints[0, 0] = u
        elif (u - self.coordinates[-1, 0]) > self.tol_u:
            # Extend bounds to the right
            self.coordinates[-1, 0] = u + (u - self.fpoints[-1, 0])
            self.fpoints[-1, 0] = u

    def update(self,
        section, coordinates, dos, origin, fpoints):
        ''' Replaces a 'section' of the current ildos by
        'coordinates', 'dos', 'origin' and 'fpoints'.

        Parameters
        -----------

        section: sequence of two integers
            Defines the beginning and end of the section of the ildos
            to replace

        coordinates: np.ndarray of float with shape(n+1, 2)

            Beginning and end of each ildos interval

            [coordinates[i, 0], coordinates[i+1, 0]) the chemical potential
             bounds for interval 'i' -> in eV

            [coordinates[i, 1], coordinates[i+1, 1]) the charge
             bounds for interval 'i' -> in elementary charge

        dos: np.ndarray of floats with shape(n, )
            Density of states for the 'i'th interval

        origin: np.ndarray of floats with shape(n, )
            Charge value origin of the linear 'i'th interval

        fpoints: np.ndarray of floats with shape(n, )
            Defines the (u, q) inside the coordinates interval
            where the tangent defined by 'dos, origin' is assumed exact.

        '''
        section = np.arange(section[0], section[1], dtype=int)

        if len(section) == len(self.coordinates):
            self.coordinates = coordinates
            self.dos = dos
            self.origin = origin
            self.fpoints = fpoints
        else:
            self.coordinates = np.delete(self.coordinates, section, axis=0)
            self.dos = np.delete(self.dos,section[:-1], axis=0)
            self.origin = np.delete(self.origin, section[:-1], axis=0)
            self.fpoints = np.delete(self.fpoints, section[:-1],axis=0)

            self.coordinates = np.insert(
                self.coordinates, section[0], coordinates, axis=0)

            self.dos = np.insert(self.dos, section[0], dos, axis=0)
            self.origin = np.insert(self.origin, section[0], origin, axis=0)
            self.fpoints = np.insert(self.fpoints, section[0], fpoints, axis=0)

        if len(self.coordinates) <= len(self.fpoints):
            raise RuntimeError(
                'Too many functional points '
                + '{0} for the # of coordinates {1}'.format(
                    self.fpoints, self.coordinates))

    @classmethod
    def from_array(cls, coordinates, tol_charge=1e-13, tol_chem_pot=1e-13):
        ''' Intializes an ILDOS instance from
        'coordinates', containing the discrete ildos
        vertices (chemical potential, charge)

        Parameters
        -----------

        coordinates: np.ndarray of float with shape(n+2, 2)
            n+2: number of intervals

            axis 0: chemical potential in eV units
            axis 1: charge in elementary charge units

        tol_charge: float
            Tolerance on the difference between the charge boundaries
            of an interval bellow which it is considered Neumann

        tol_chem_pot: float
            Tolerance on the difference between the chemical potential
            boundaries of an interval bellow which it is considered Neumann

        Returns
        --------
        instance of Ildos
        '''

        diff = np.abs(coordinates[:-1] - coordinates[1:])

        dirichlet = np.arange(0, len(coordinates) - 1, dtype=int)[
            diff[:, 0] < tol_chem_pot]
        neumann =  np.arange(0, len(coordinates) - 1, dtype=int)[
            diff[:, 1] < tol_charge]

        if len(np.intersect1d(dirichlet, neumann)) > 0:
            raise ValueError(
                'coordinates fail to define an Ildos. The coordinates defining'
                + ' the intervals {0} are too close'.format(
                    np.intersect1d(dirichlet, neumann)))

        helmholtz = np.setdiff1d(
            np.arange(0, len(coordinates) -1, dtype=int),
            np.concatenate([neumann, dirichlet]))

        # Define as all neumann
        origin = copy.deepcopy(coordinates[:-1, -1])
        dos = np.zeros(len(coordinates) - 1)

        # Set the values for Dirichlet
        origin[dirichlet] = np.ones(len(dirichlet)) * np.nan
        dos[dirichlet] = np.ones(len(dirichlet)) * np.infty

        # Set the values for the Helmholtz
        if len(helmholtz) > 0:
            dos[helmholtz] = diff[helmholtz, 1] / diff[helmholtz, 0]
            origin[helmholtz] = (
                coordinates[helmholtz, 1]
                - coordinates[helmholtz, 0] * dos[helmholtz])

        return cls(coordinates=coordinates, dos=dos, origin=origin)


class PContinuousIldos(Ildos):
    ''' Piecewise continuous Ildos
    '''

    def __init__(
        self, coordinates, functions):
        ''' Ildos

        Parameters
        -----------

        coordinates: np.ndarray of float with shape(n+1, 2)

            Beginning and end of each ildos interval

            [coordinates[i, 0], coordinates[i+1, 0]) the chemical potential
             bounds for interval 'i' -> in eV

            [coordinates[i, 1], coordinates[i+1, 1]) the charge
             bounds for interval 'i' -> in elementary charge

        functions: list of python functions or sequence of 2 floats

            Let f the element 'i' of 'functions' and u the chemical
            potential contained within the interval defined by
            (coordinates[i, :], coordinates[i+1, :]];

                f(u) = (n, \rho)

                with \rho and n respectively the DOS and charge at u.

        !!  Notice !!!

            If coordinates[i, 0] = coordinates[i+1, 0] then it is a
            constant potential interval -> \rho = infty

            If coordinates[i, 1] = coordinates[i+1, 1] then it is a
            constant charge interval -> \rho = 0 and n = coordinates[i, 1]

        '''

        self.coordinates = coordinates

        if isinstance(functions, Sequence):
            self.functions = functions
        else:
            self.functions = [functions, ]

    def _dos_ildos(self, u, intervals=False):
        ''' Returns the DOS and ILDOS for a set of 'u'

        Parameters
        -----------

        u: np.array of float with shape(n, )
            Values of chemical potential - in eV.

        intervals: boolean (default False)
            If True also return the intervals

        Returns
        --------

        dos: np.ndarray of float with shape(n, )
            Values of density of states - in e.eV^-1

        ildos: np.ndarray of float with shape(n, )

            Values of the integrated density of
            states up to u - in elementary charge

        If return intervals is true - also returns

        intervals: np.ndarray of integers with shape(n, )

            Index of the intervals for 'u'


        '''

        if isinstance(u, (np.ndarray, Sequence)):
            dos = np.empty(len(intervals))
            ildos = np.empty(len(intervals))
            for i, int_ in enumerate(intervals):
                dos[i], ildos[i] = self.functions[int_](u[i])
        else:
            dos, ildos = self.functions[intervals](u)
        return dos, ildos

    def linearize(self, fpoints):
        ''' Returns an instance of PLinearIldos that
        linearizes current ildos around 'fpoints'

        Parameters
        ------------

        fpoints: np.ndarray of floats(n, )
            Values of chemical potential around which the current ildos
            is linearized (tangent)

        Returns
        --------
        instance of PLinearIldos
        '''

        if (np.any((fpoints - self.coordinates[0, 0]) < -1e-10)
            or np.any((fpoints - self.coordinates[-1, 0]) > 1e-10)):
            raise RuntimeError(
                'fpoints found otuside of ILDOS boundaries {0}'.format(
                [self.coordinates[0, 0], self.coordinates[-1, 0]]))

        # Initialize using a first 'fpoint'
        dos, ildos = self(fpoints)

        origin = ildos - dos * fpoints

        init_pt = np.arange(len(dos), dtype=int)[
           np.logical_not(np.isin(dos, [0., np.infty]))]

        if len(init_pt) == 0:
            raise ValueError(
                " At least one finite non zero dos is required, however"
                +"The dos found for fpoints {0} is {1}.".format(
                    fpoints, dos) )

        coord = self.coordinates[[0, -1]]
        coord[:, 1] = (
            np.ones(2) * origin[init_pt[0]]
            + dos[init_pt[0]] * coord[:, 0])

        plinear_ildos = PLinearIldos(
            coordinates=coord, dos=dos[init_pt[0]],
            origin=origin[init_pt[0]],
            fpoints=np.array([fpoints[init_pt[0]], ildos[init_pt[0]]]))

        for i in np.setdiff1d(
            np.arange(len(fpoints), dtype=int), [init_pt[0], ]):

            q = ildos[i]
            u = fpoints[i]
            if q == np.infty:
                q = np.average(self.coordinates[
                    [self.interval(u), self.interval(u) + 1], 1])

            plinear_ildos.add_interval(
                u=u, dos=dos[i], origin=origin[i], q=q)

        return plinear_ildos
