import sys
import numbers

from abc import ABC, abstractmethod
from collections.abc import Sequence

import copy

from pescado.self_consistent import ildos as pescado_ildos
import numpy as np
from pescado.tools import SparseVector
import warnings
import matplotlib.pyplot as plt

from numpy.testing import assert_array_equal


def is_dos_infty(dos_value, max_dos):

    if isinstance(dos_value, numbers.Integral):
        
        if (np.abs(dos_value) >= np.abs(max_dos)
            or dos_value == sys.maxsize
            or (dos_value - 1) == sys.maxsize):
            return True
        else:
            return False

    else:
        
        map_bool = np.abs(dos_value) < np.abs(max_dos)
        map_bool[map_bool] = dos_value[map_bool] != sys.maxsize
        map_bool[map_bool] = (dos_value[map_bool] - 1) != sys.maxsize
        
        return np.logical_not(map_bool)


def make_new_chem(tf_solver):
    ''' Notice this increases by one iteration in case of Hemlholtz sites. 
    Can be optmized by treating helmholtz, dirichlet and neumann sites
    diferently. 
    '''

    ildos_list = tf_solver.ildos_list
    sites_ildos = tf_solver.sites_ildos
    tol_ch= tf_solver.q_tol
    max_dos = tf_solver.max_dos

    charge = tf_solver.quantum_charge(-1)
    potential = tf_solver.chemical_potential(-1)

    charge_res = SparseVector(
        values=np.zeros(len(charge.indices)),
        indices=charge.indices, dtype=int)

    dos_res = SparseVector(
        values=np.zeros(len(charge.indices)),
        indices=charge.indices, dtype=int)

    for idx, ildos in enumerate(ildos_list):
        sites = charge_res.indices[sites_ildos[charge_res.indices] == idx]
        dos_res[sites], charge_res[sites] = ildos(
            potential[sites], return_intervals=False)
    
    ref_interval = tf_solver.intervals(-1)

    charge_err = charge[charge.indices] - charge_res[charge.indices]
    
    wrong_sites = charge.indices[np.abs(charge_err) > tol_ch]

    new_chem = potential.extract(charge.indices)
    
    for site in wrong_sites:
        
        ildos = ildos_list[sites_ildos[site]]

        if is_dos_infty(dos_value=dos_res[site], max_dos=np.abs(max_dos)):
            test_val = charge[site]
            axis = 1 # Compare the charge boundary
        else:
            test_val = potential[site]
            axis = 0 # Compare the potential boundary

        if test_val > ildos.coordinates[ref_interval[site] + 1, axis]:
            new_chem[site] = (
                ildos.coordinates[ref_interval[site] + 1, 0]
                + ildos.coordinates[ref_interval[site] + 2, 0]) / 2
        elif test_val <= ildos.coordinates[ref_interval[site], axis]:
            new_chem[site] = (
                ildos.coordinates[ref_interval[site], 0]
                + ildos.coordinates[ref_interval[site] - 1, 0]) / 2
    
    return new_chem


class AbstractThomasFermi(ABC):
    ''' Self Consistent Thomas Fermi solver
    '''

    def __init__(
            self, ildos, poisson_problem, sites_ildos=None,
            e_f=0, dos_tol=1e-14, u_tol=1e-14, q_tol=1e-14, max_dos=np.infty):
        ''' Returns an instance child class from AbstractThomasFermi

        Parameters
        -----------

        ildos: list or instance of -
            
            i) np.array of floats with shape (n+2, 2)
                
                Coordinates defining the ildos intervals
                
                n - number of intervals
                
                ildos[:, 0]: chemical potential in eV
                ildos[:, 1]: charge in eV^{-1}

            ii) instance of pescado.self_consistent.ildos.Ildos

        sites_ildos: instance of SparseVector
            Indicates the element in 'ildos' defined for a given 
            'poisson_problem' point. 

            sites_ildos[i] = j
                site 'i' of 'poisson_problem' has 'ildos[j]'

            Required if 'ildos' is a sequence. 

        poisson_problem: instance of pescado.poisson.Problem

        e_f: float (default 0)
            Fermi level in eV
        
        max_dos : int (default np.infty)
            
            if dos >= max_dos, 
                the interval is considered Dirichlet.
        ''' 

        if not isinstance(ildos, Sequence):
            self.ildos_list = [ildos, ]
        else:
            self.ildos_list = ildos
        
        for i, ildos in enumerate(self.ildos_list):
            if isinstance(ildos, np.ndarray):
                self.ildos_list[i] = pescado_ildos.PLinearIldos.from_array(
                    coordinates=ildos)
        
        if sites_ildos is None:
           sites_ildos = SparseVector(default=0, dtype=int)

        self.sites_ildos = sites_ildos
        self.poisson_problem = poisson_problem

        self.e_f = e_f

        self.dos_tol, self.q_tol, self.u_tol, self.max_dos = (
            dos_tol, q_tol, u_tol, max_dos)
        
        self.iteration_info = None
        self.iteration_data = None
    
    @abstractmethod
    def solve(self):
        ''' Solves Self consistency
        '''
        pass

    @abstractmethod
    def initialize(self):
        ''' Initializes self consistency
        '''

        pass

    @abstractmethod
    def update(self):
        ''' Updates Poisson problem and ILDOS input using
        the results from the last iteration
        '''

        pass

    @abstractmethod
    def iterate(self):
        ''' Self consistent Thomas Fermi iteration
        '''

        pass

    def solve_poisson(self, poisson_input, chem_quant, **kwargs):
        ''' Solves the Poisson equation for a given 'poisson_input'
        and 'chemical_potential' fixed at the quantum sites.

        Notice 'chemical_potential.indices' sets the quantum sites for
        the current iteration of the Thomas Fermi problem. The quantum 
        sites are those whose charge density is set by the ILDOS. 

        Parameters
        -----------

        poisson_problem_input: dictionary
            Set the poisson problem input for the pescado problem sites not 
            defined in 'chemical_potential.indices', i.e. classical sites.

            The classical sites can be of Helmholtz, Neumann, Dirichlet and
            flexible sites. Hence, poisson problem input can have as elements:

                'voltage': SparseVector
                'charge': SparseVector
                'charge_density': SparseVector
                'helmholtz_density': SparseVector
                
                'flexible_input': Dictionary with elements:
                    
                    'voltage': SparseVector
                    'charge': SparseVector
                    'charge_density': SparseVector
                    'helmholtz_density': SparseVector

            See poisson.Problem.solve() for more information. 

        chem_quant: instance of SparseVector
            chem_quant.indices -> sites index in the 'self.poisson_problem'
            chem_quant.values -> chemical potential value at the site defined 
                in 'chem_quant.indices'. 

            The chem_quant[i] value is passed down to the ILDOS defined for the 
        site 'i'. From this the 'dos', 'ildos' and 'interval' is recovered. They
        define the site 'i' poisson type (D, H or N) and input value.

        kwargs: passed down to self.poisson_problem.solve()
        '''

        dos, ildos_u, intervals = self._get_interval_dos_ildos(
            chem_potential=chem_quant)

        qflex_input = self._generate_qflex_input(
            chem_potential=chem_quant, dos=dos, ildos_u=ildos_u)

        data = {'qflex_input':qflex_input, 
                'intervals':intervals,
                'chem':chem_quant, 
                'dos':dos,
                'ildos_u':ildos_u}
        
        voltage, charge, total_charge = self._solve_poisson(
            poisson_problem_input=poisson_input, 
            qflex_input=qflex_input, **kwargs)

        quantum_potential = voltage.extract(chem_quant.indices)
        quantum_potential.values += self.e_f

        quantum_charge = total_charge.extract(chem_quant.indices) * -1
        
        data.update({
            'poisson_voltage':voltage, 'poisson_charge':charge,
            'poisson_total_charge':total_charge,
            'quantum_potential':quantum_potential,
            'quantum_total_charge':quantum_charge})

        return data

    def _get_interval_dos_ildos(self, chem_potential):
        ''' Recover 'dos', 'ildos_u', 'intervals' for a given 
        'chem_potential'. 
        '''
        dos = SparseVector()
        ildos_u = SparseVector()
        intervals = SparseVector(dtype=int)

        for idx, ildos in enumerate(self.ildos_list):
            sites = chem_potential.indices[
                self.sites_ildos[chem_potential.indices] == idx]

            (dos[sites], ildos_u[sites], intervals[sites]) = ildos(
                chem_potential[sites], return_intervals=True)
                
        return dos, ildos_u, intervals
                        
    def _generate_qflex_input(self, chem_potential, dos, ildos_u):
        ''' Define the site type (N, D, H) and input value (q, \rho, U)
        for a given 'chem_potential', 'dos, 'ildos_u'

        Parameters
        -----------

        chem_potential, dos, ildos_u: instances of SparseVector
            
            chem_potential: User input self.solve_poisson()
            dos, ildos_u: output of self._get_interval_dos_ildos()

        Returns
        --------
            dictionary of 'voltage', 'charge', 'hemholtz_density' for the 
        sites defined in chem_potential.indices. 
            
            Values defined in units of Poisson problem:
                'voltage'-> V
                'charge'-> 1 
                    (number of elementary charge)
                'helmholtz_density'-> V^{-1}
                    (number of elementary charge per voltage)
        '''

        voltage = chem_potential.extract(chem_potential.indices)
        voltage.values -= self.e_f
        
        qflex_input = { # Set the voltage at the Dirichlet sites
                'voltage':voltage.extract(dos.indices[
                    is_dos_infty(dos.values, max_dos=self.max_dos)])}

        # Temporary        
        assert_array_equal(
            np.abs(dos.values) >= np.abs(self.max_dos),
            is_dos_infty(dos.values, max_dos=self.max_dos))

        charge = ildos_u.extract(
            dos.indices[np.logical_not(
                is_dos_infty(dos.values, max_dos=self.max_dos))])
        
        if len(charge.indices) > 0:
            helmholtz_sites = charge.indices[
                np.abs(dos[charge.indices]) >= self.dos_tol]
        else:
            helmholtz_sites = list()
        
        if len(helmholtz_sites) > 0:
            helmholtz_density = dos.extract(helmholtz_sites)
            
            # Calculate the origin q_0 = q_u - \rho * (u - e_f)
            # s.t. q = \rho * U + q_O with U = u - e_f
            charge[helmholtz_sites] = (
                charge[helmholtz_sites]
                -(helmholtz_density[helmholtz_sites]
                  * voltage[helmholtz_sites])) # Account for self.e_f

            qflex_input.update({
                'charge':charge * -1,
                'helmholtz_density':helmholtz_density * -1})
        else:
            qflex_input.update({'charge':charge * -1})
        
        return qflex_input

    def _solve_poisson(self, poisson_problem_input, qflex_input, **kwargs):
        ''' Solves the poisson problem

        Parameters
        -----------
        
        poisson_problem_input: dictionary
            Set the poisson problem input for the classical sites.

            The classical sites can be of Helmholtz, Neumann, Dirichlet and
            flexible sites. Hence, poisson problem input can have as elements:

                'voltage': SparseVector
                'charge': SparseVector
                'charge_density': SparseVector
                'helmholtz_density': SparseVector
                
                'flexible_input': Dictionary with elements:
                    
                    'voltage': SparseVector
                    'charge': SparseVector
                    'charge_density': SparseVector
                    'helmholtz_density': SparseVector

            See poisson.Problem.solve() for more information. 

            Set 'voltage', 'charge', 'charge_density', 'helmholtz_density'
                (instances of SparseVectors, see poisson.Problem.solve())

        qflex_input: dictionary
            Set the quantum sites type and values

            set 'voltage', 'charge', 'charge_density' and 'helmholtz_density'
                (instances of SparseVectors, see poisson.Problem.solve())

        kwargs: passed down to self.poisson_problem.solve()

        Returns
        --------
        voltage, charge - output of self.poisson_problem.solve()
        '''
        
        input = {
            'voltage':SparseVector(), 
            'charge':SparseVector(), 
            'charge_density':SparseVector(), 
            'helmholtz_density':SparseVector(),
            'flexible_input':None}
        
        for key in input.keys():
            if key in poisson_problem_input.keys():
                input[key] = copy.deepcopy(poisson_problem_input[key])
        flexible_input = input.pop('flexible_input')
        
        if flexible_input is not None:
            for key, val in qflex_input.items():
                if key in flexible_input.keys():
                    if len(np.intersect1d(
                        val.indices, flexible_input[key].indices)) > 0:
                        raise RuntimeError(
                            '{0} set as classical and quantum site(s)'.format(
                                np.intersect1d(
                                    val.indices, flexible_input[key].indices)))
                    flexible_input[key].extend(val)
                else:
                    flexible_input.update({key:val})
        else:
            flexible_input = copy.deepcopy(qflex_input)

        assign_flexible = {'contain_all':True}
        # Those not defined set to Neumann (contain_all = True)
        for key_idx, key_val in zip(
            ['helmholtz_index', 'dirichlet_index'], 
            ['helmholtz_density', 'voltage']):
            if key_val in flexible_input.keys():
                val = flexible_input[key_val].indices
            else:
                val = np.array([])
            assign_flexible.update({key_idx:val})

        # Reset flexible sites configuration
        self.poisson_problem.assign_flexible(**assign_flexible)
        self.poisson_problem.freeze()

        for key, val in flexible_input.items():
            if len(np.intersect1d(
                val.indices, input[key].indices)) > 0:
                raise RuntimeError(
                    '{0} set as D/N/F and flexible in key:{1}'.format(
                        np.intersect1d(val.indices, input[key].indices), key))
            input[key].extend(val)
        
        _kwargs = copy.deepcopy(kwargs)
        if _kwargs is not None:
            _kwargs.update(input)
        else:
            _kwargs = input

        voltage, charge = self.poisson_problem.solve(**_kwargs)
        
        total_charge = SparseVector(
            values=charge.values, indices=charge.indices, 
            default=0, dtype=np.float64)
        
        helm_idx = input['helmholtz_density'].indices
        if len(helm_idx) > 0:
            total_charge[helm_idx] += (
                input['helmholtz_density'][helm_idx] * voltage[helm_idx])
        
        return voltage, charge, total_charge

    def charge(self, iteration):
        ''' Gets the charge value at the classical and quantum sites 
        for 'iteration'
        Parameters
        -----------
            iteration: integer
                iteration number

        Returns
        --------
            instance of SparseVector
        '''
        return self.iteration_data[iteration]['poisson_total_charge']

    def potential(self, iteration):
        ''' Gets the potential value at the classical and quantum sites 
        for 'iteration'

        Parameters
        -----------
            iteration: integer
                iteration number

        Returns
        --------
            instance of SparseVector
        '''
        return self.iteration_data[iteration]['poisson_voltage']
        
    def quantum_charge(self, iteration):
        ''' Gets the charge value at the quantum sites 
        for 'iteration'

        Parameters
        -----------
            iteration: integer
                iteration number

        Returns
        --------
            instance of SparseVector
        '''
        qcharge = self.iteration_data[iteration]['quantum_total_charge']
        qcharge.default = np.nan
        return qcharge

    def chemical_potential(self, iteration):
        ''' Gets the chemical_potential value at the quantum sites 
        for 'iteration'

        Parameters
        -----------
            iteration: integer
                iteration number

        Returns
        --------
            instance of SparseVector
        '''
        chem_pot = self.iteration_data[iteration]['quantum_potential']
        chem_pot.default = np.nan
        return chem_pot
    
    def intervals(self, iteration):
        ''' Gets the chemical_potential value at the quantum sites 
        for 'iteration'

        Parameters
        -----------
            iteration: integer
                iteration number

        Returns
        --------
            instance of SparseVector
        '''
        return self.iteration_data[iteration]['intervals']


class PieceWiseNewthonRaphson(AbstractThomasFermi):
    ''' Solves Self Consistent Thomas Fermi using a piecewise version
    of the Newton Raphson algorithm.
    '''

    def initialize(self, initial_guess):
        ''' Initialize self consistency

        Parameters
        -----------
        
        initial_guess: SparseVector
            Contains the chemical potential values at the quantum sites
            set at the first iteration. 
        '''

        self.iteration_info = {
            'type':'PieceWiseNewthonRaphson', 
            'chem_pot':list(), 
            'poisson_input':list(), 
            'kwargs':list()}
        
        self.iteration_data = list()

        self.current_chem_pot = initial_guess

    def update(self, update_chem_method=make_new_chem):
        ''' Updates the chemical potential for the next iteration

        Parameters
        -----------

        update_chem_method: python function
            Takes as input 'self' and returns a SparseVector containing
            the chemical potential values to be set at the next
            self consistet iteration.
        '''
        
        self.current_chem_pot = update_chem_method(tf_solver=self)
        
    def iterate(self, poisson_input, **kwargs):
        ''' Iterate for a given set of 'poisson_input'

        Parameters
        -----------
        
        poisson_input: dictionary
            Set the poisson problem input for the Helmholtz, Neumann
            and Dirichlet sites

            Set 'voltage', 'charge', 'charge_density', 'helmholtz_density'
                (instances of SparseVectors, see poisson.Problem.solve())

        kwargs: passed down to poisson.Problem.solve()

        '''
        
        self.iteration_info['poisson_input'].append(poisson_input)
        self.iteration_info['chem_pot'].append(self.current_chem_pot)
        self.iteration_info['kwargs'].append(kwargs)

        self.iteration_data.append(
            self.solve_poisson(
                poisson_input=poisson_input, 
                chem_quant=self.current_chem_pot, **kwargs))
            
    def solve(
            self, poisson_input, initial_guess, max_ite=15, **kwargs):
        ''' Solves the self consistent problem for 'self.ildos_list'
        for a given 'initial_guess' using the 'Discretized' method. 

        Parameters
        -----------
        
        poisson_input: dictionary
            Set the poisson problem input for the Helmholtz, Neumann
            and Dirichlet sites

            Set 'voltage', 'charge', 'charge_density', 'helmholtz_density'
                (instances of SparseVectors, see poisson.Problem.solve())

        initial_guess: SparseVector
            Contains the chemical potential values for the initial guess

        kwargs: passed down to poisson.Problem.solve()

        Returns
        --------

        dictionary containing iteration data and metadata
        '''

        self.initialize(initial_guess)
        self.iterate(poisson_input=poisson_input, **kwargs)
        print('initialize')
        not_converged = True
        ite = 0
        while not_converged and (ite <= max_ite):
            
            print('##############'.format(ite))
            print('Iteration {0}'.format(ite))
            ite += 1
            
            self.update(update_chem_method=make_new_chem)
            self.iterate(poisson_input=poisson_input, **kwargs)

            not_converged = np.any(
                np.abs(self.iteration_data[-1]['quantum_potential'].values
                       - self.iteration_data[-2]['quantum_potential'].values) 
                > self.u_tol)

            print('Max Err on u: {0}'.format(
                np.max(
                    np.abs(self.iteration_data[-1]['quantum_potential'].values
                    - self.iteration_data[-2]['quantum_potential'].values))))

            print('##############'.format(ite))

        return self.iteration_data, self.iteration_info


class PieceWiseDichotomy(AbstractThomasFermi):
    ''' Solves Self Consistent Thomas Fermi using a piecewise
     dichotomy algorithm.
    '''

    def initialize(
            self, sites_index, initial_fpoints, tf_pwnr_tol=None):
        ''' Initializes 'PieceWiseNewthonRaphson' instance 
        and 'self.iteration_info'. 
        
        To set the ildos of the 'PieceWiseNewthonRaphson' instance 
        it linearizes each element of self.ildos_list using 'initial_fpoints'.
        
        For each element 'i' of 'sites_index' it linearizes the
        self.ildos_list[self.sites_ildos[sites_index[i]]] using the
        initial_fpoints[i] np.ndarray. 
         
        Parameters
        -----------

        sites_index: np.ndarray of integers

        initial_fpoints: Sequence of np.ndarray of floats with shape(n, )
            Contains the chemical potential defining the functional points
            to linearize the self.ildos_list

        '''
        default_tf_pwnr_tol = {
                'dos_tol':self.dos_tol,
                'q_tol':self.q_tol,
                'u_tol':self.u_tol}
        if tf_pwnr_tol is None:
            tf_pwnr_tol = default_tf_pwnr_tol
        else:
            for key in default_tf_pwnr_tol.keys():
                if key not in tf_pwnr_tol.keys():
                    tf_pwnr_tol.update({key:default_tf_pwnr_tol[key]})
        
        self.iteration_info = {
            'type':'PieceWiseDichotomy', 
            'chem_pot_tf_pwnr':list(),
            'poisson_input':list(),
            'initial_fpoints':initial_fpoints,
            'iteration_fpoints':list(),
            'NR_iteration':list()}
        
        self.iteration_data = list()
        
        # Make linear ildos from 'initial_fpoints'
        if not isinstance(initial_fpoints, Sequence):
            initial_fpoints = [initial_fpoints, ] * len(sites_index)

        # Linearize them
        linear_ildos_list = list()
        for i, fpoint in enumerate(initial_fpoints):
             linear_ildos_list.append(self.ildos_list[
                self.sites_ildos[sites_index[i]]].linearize(fpoint))
        
        # Initialize a DiscretizedNR
        self.tf_pwnr = PieceWiseNewthonRaphson(
            ildos=linear_ildos_list, 
            sites_ildos=SparseVector(
                values=np.arange(len(initial_fpoints)),
                indices=sites_index),
            poisson_problem=self.poisson_problem, 
            e_f=self.e_f, **tf_pwnr_tol)
        
        self.current_chem_pot = None

    def update(self, add_chem_pot=None):
        ''' Updates the 'self.tf_pwnr.ildos_list'. 
        
        For the site defined by the 'i'-th element of 'add_chem_pot.indices', it adds an interval centered around 'add_chem_pot.values[i]' to its
        ildos:
            
            'self.tf_pwnr.ildos_list[
                self.tf_pwnr.sites_ildos[add_chem_pot.indices[i]]]'

        using the 'self.tf_pwnr.ildos_list[...].add_interval(...)' method.

        Parameters
        -----------

        add_chem_pot: instance of SparseVector (default None)

        '''

        if add_chem_pot is None: # Looks for converged results
            if self.previous_chem_pot is not None:
                mod_idx = self.current_chem_pot.indices[np.abs(
                    self.current_chem_pot[self.current_chem_pot.indices]
                    - self.previous_chem_pot[self.current_chem_pot.indices])
                    > self.u_tol]
            else:
                mod_idx = self.current_chem_pot.indices
            
            add_chem_pot = self.current_chem_pot.extract(mod_idx)

        for site_idx in add_chem_pot.indices:
            site_chem_pot = add_chem_pot[site_idx]
            
            dos, ildos = self.ildos_list[
                self.sites_ildos[site_idx]](site_chem_pot)
            
            origin = ildos - dos * site_chem_pot
            try:
                disc_ildos = self.tf_pwnr.ildos_list[
                    self.tf_pwnr.sites_ildos[site_idx]]
                disc_ildos.add_interval(
                    u=site_chem_pot, dos=dos, origin=origin, q=ildos)

            except RuntimeError as e:
                fpts = disc_ildos.fpoints

                if np.any(
                    np.abs(fpts[:, 0] - site_chem_pot) < disc_ildos.tol_u):

                    idx = np.arange(len(fpts))[np.abs(fpts[:, 0] - site_chem_pot) < disc_ildos.tol_u]
                    warnings.warn(
                        "Point u:{0}, qO:{1}, dos:{2} not added. ".format(
                            site_chem_pot, origin, dos)
                        + "Since u:{0}, qO:{1}, dos:{2} already in the ILDOS".format(
                            fpts[idx, 0], 
                            disc_ildos.origin[idx], 
                            disc_ildos.dos[idx]))
                else:
                    raise RuntimeError(str(e))


    def iterate(
        self, poisson_input, chem_pot_tf_pwnr, custom_NR_solve=None, **kwargs):
        ''' Solves the current instance of 'self.tf_pwnr' for a given
        'poisson_input', 'chem_pot_tf_pwnr', and 'tol_pot'.

        Parameters
        -----------

        poisson_input: dictionary
            
            Set the poisson problem input for the Helmholtz, Neumann
            and Dirichlet sites

            Set 'voltage', 'charge', 'charge_density', 'helmholtz_density'
                (instances of SparseVectors, see poisson.Problem.solve())

        chem_pot_tf_pwnr: SparseVector
            Contains the chemical potential values for the initial guess

        custom_NR_solve: function
            Custom piece wise newthon raphson solve method. Replaced
            the default PieceWiseNewthonRaphson.solve().

            Parameters
            -----------
                self.tf_pwnr
                poisson_input
                chem_pot_tf_pwnr
                **kwargs

            Returns
            --------
                iteration_info: dictionary
                    dict elements:
                        'type' -> type of iteration
                        'chem_pot' -> chemical potential initial guess
                        'poisson_input'
                        'kwargs' -> Passed down to Problem
                iteration_data: list of dictionaries
                    Each element is the output of 
                        AbstractThomasFermi.solve_poisson()

        kwargs: passed down to poisson.Problem.solve() (default None)
        '''

        self.iteration_info['poisson_input'].append(poisson_input)
        self.iteration_info['chem_pot_tf_pwnr'].append(chem_pot_tf_pwnr)
        self.iteration_info['iteration_fpoints'].append(
            [self.tf_pwnr.ildos_list[
                    self.tf_pwnr.sites_ildos[site_idx]].fpoints
             for site_idx in chem_pot_tf_pwnr.indices])
        
        if custom_NR_solve is None:
            # Solve the SCQP - TF
            self.iteration_info['NR_iteration'].append(
                self.tf_pwnr.solve(
                    poisson_input=poisson_input, 
                    initial_guess=chem_pot_tf_pwnr, **kwargs))
        else:
            self.iteration_info['NR_iteration'].append(
                custom_NR_solve(
                    tf_pwnr=self.tf_pwnr,
                    poisson_input=poisson_input, 
                    initial_guess=chem_pot_tf_pwnr, **kwargs))

        self.iteration_data.append(
            self.iteration_info['NR_iteration'][-1][0][-1])

        self.previous_chem_pot = copy.deepcopy(self.current_chem_pot)
        self.current_chem_pot = self.iteration_data[-1]['quantum_potential']

    def solve(self, poisson_input, initial_guess, initial_fpoints,
              max_ite=10, tf_pwnr_tol=None, **kwargs):
        ''' Solves the self consistent problem for a linearized
        'self.ildos_list'.

        Parameters
        -----------

        poisson_input: dictionary
            Set the poisson problem input for the Helmholtz, Neumann
            and Dirichlet sites

            Set 'voltage', 'charge', 'charge_density', 'helmholtz_density'
                (instances of SparseVectors, see poisson.Problem.solve())
        
        initial_fpoints: np.array or Sequence of np.array of floats
            
            Defines the chemical potential around which to linearize 
            each element in 'self.ildos_list'. 

            If a np.array is given, apply the same 'intial_fpoints'
            to all 'ildos_list'. 

            If a Sequence is given, apply the ith element of 'initial_fpoitns'
            to the ith 'ildos_list'. 

        initial_guess: SparseVector
            Contains the chemical potential values for the initial guess

        kwargs: passed down to poisson.Problem.solve()
        '''

        self.initialize(
            sites_index=initial_guess.indices, 
            initial_fpoints=initial_fpoints, 
            tf_pwnr_tol=tf_pwnr_tol)
        
        self.iterate(
            poisson_input=poisson_input, 
            chem_pot_tf_pwnr=initial_guess, **kwargs)

        not_converged = True
        ite = 0
        while not_converged and (ite <= max_ite):
            
            print('##############'.format(ite))
            print('Iteration {0}'.format(ite))
            ite += 1
            
            self.update()
            
            self.iterate(
                poisson_input=poisson_input, 
                chem_pot_tf_pwnr=initial_guess, **kwargs)

            not_converged = np.any(
                np.abs(self.previous_chem_pot.values 
                       -self.current_chem_pot.values) > self.u_tol)

            print('Max Err on u: {0}'.format(
                np.max(np.abs(self.previous_chem_pot.values 
                       -self.current_chem_pot.values))))

            print('##############'.format(ite))

        return self.iteration_data, self.iteration_info


def thomas_fermi(
        ildos, poisson_problem, sites_ildos=None,
        e_f=0, method='PieceWiseNewthonRaphson', 
        dos_tol=1e-14, u_tol=1e-14, q_tol=1e-14, max_dos=np.infty):
        ''' Class constructor. Returns a Thomas Fermi solver, 
        either an instance of 'PieceWiseNewthonRaphson' or 'PieceWiseDichotomy'.

        Parameters
        -----------

        ildos: list or instance of -
            
            i) np.array of floats with shape (n+2, 2)
                
                Coordinates defining the ildos intervals
                
                n - number of intervals
                
                ildos[:, 0]: chemical potential in eV
                ildos[:, 1]: charge in eV^{-1}

            ii) instance of pescado.self_consistent.ildos.Ildos

        sites_ildos: instance of SparseVector
            Indicates the element in 'ildos' defined for a given 
            'poisson_problem' point. 

            sites_ildos[i] = j
                site 'i' of 'poisson_problem' has 'ildos[j]'

            Required if 'ildos' is a sequence. 

        method: string (default PieceWiseNewthonRaphson)
            
            If 'PieceWiseNewthonRaphson' solves the Thomas Fermi
                problem using piecewise Newthon Raphson method

            If 'PieceWiseDichotomy' solves the Thomas Fermi
                problem using piecewise dichotomy method

        poisson_problem: instance of pescado.poisson.Problem

        e_f: float (default 0)
            Fermi level in eV

        max_dos : int (default np.infty)
            
            if dos >= max_dos, 
                the interval is considered Dirichlet.        
        '''

        possible_methods = {
            'PieceWiseNewthonRaphson':PieceWiseNewthonRaphson,
            'PieceWiseDichotomy':PieceWiseDichotomy}
        
        return possible_methods[method](
            poisson_problem=poisson_problem, sites_ildos=sites_ildos,
            ildos=ildos, e_f=e_f, dos_tol=dos_tol, u_tol=u_tol, q_tol=q_tol, 
            max_dos=max_dos)
