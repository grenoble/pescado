'''
    Pescado package module
'''

from .mesher import Shape
from .tools.sparse_vector import SparseVector

__all__ = (['Shape', 'SparseVector'])

version_info = (0, 0, 1)
__version__ = '.'.join(str(c) for c in version_info)
