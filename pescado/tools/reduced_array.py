''' Methods to work with 'reduced array' numpy arrays.

A = [[1, 2, 3], [2, 1]]

RDofA = ([1, 2, 3, 2, 1], [0, 3, 5])


!!! Use copy, deepcopy before using !!!

'''
import collections

import numpy as np
import matplotlib.pyplot as plt

from pescado.tools import meshing


######### Use reduced_arrays to "compress" data
def zip(values, return_kdtree=False):
    ''' Compress 'values' into 'unique_values'. 
        'values' = 'unique_values'['values_index']

    Parameters
    ----------
    values: np.ndarray with shape(m, )

    return_kdtree: boolean - Default False
        If True returns the KDtree for 'unique_values'
        
    Returns
    -------
    unique_values: np.ndarray of - values.dtype

    values_index: np.ndarray of integers with shape(m, )
    
    If return_kdtree == True
        kdtree: instance of scipy.spatial.KDtree 

      
    '''
    if isinstance(values, collections.abc.Sequence):
        values = np.asarray(values)
    elif not isinstance(values, np.ndarray):
        raise ValueError('Values must be a numpy array or Sequence')
        
    # Get the vertices
    unique_values = meshing.unique(values)[0]

    # Update the indexing
    if len(values.shape) == 1:
        values_index = np.searchsorted(unique_values, values, side='right') -1
    else: 
        
        values_index, inside, kdtree = meshing.kd_where(
            array=values, ref_array=unique_values, return_tree=True)
            
        if not np.all(inside):
            raise RuntimeError(
                'Failed to zip - values -.'
                ' values {0} not found in kdtree.'.format(
                values[np.logical_not(inside)]))
    
    if return_kdtree:
        return unique_values, values_index, kdtree
    else:
        return unique_values, values_index


######### Create Reduced_Array
def make_rand(low, high, size, cnts=[2, 3, 4]):
    '''Makes a random reduced array - usefull for testing
    
    Parameters
    ----------
    low: integer lowest 'values'
    
    high: integer highest 'values'
    
    size: integer number of 'values'
    
    cnts: Sequence of integers
        possible counts for the reduced array elements. 
    
    Returns
    --------
    values: np.ndarray of integers with shape(size, )
    
    indices: np.ndarray of integers with shape(nelements, )

    Note - 
         make_rand divides size into len(cnts) equal bins, 
         divided them by each element in cnts. The sum of 
         the resulting divisions + len(cnts) is the number
         of elements in the reduced_array

    ------- Not Tested
    
    '''
   
    bin_size = np.linspace(0, size, len(cnts) + 1, dtype=int)
    bin_size = (bin_size - np.roll(bin_size, shift=1))[1:]
    
    val = np.zeros(len(cnts) * 2, dtype=int)
    rep = np.ones(len(cnts) * 2, dtype=int)
    for b, bs in enumerate(bin_size):
        
        rep[2 * b] = int(bs // cnts[b])        
        val[2 * b] = cnts[b]
        val[2 * b + 1] = bs - (rep[2 * b] * cnts[b])

    indices = np.repeat(val, repeats=rep)
    np.random.shuffle(indices)
    indices = np.insert(np.cumsum(indices), obj=0, values=[0, ])

    assert max(indices) == size

    values = np.random.randint(low=low, high=high, size=size)

    return values, indices
    

def make_from(list_values, order=None):
    ''' Create the reduced dimension array from 'list_values'.
    If 'order' not None, then shuffle to elements in list
    to match 'order'. 

    ------- Not Tested

    '''
    if order is None:
        order = np.arange(len(list_values), dtype=int)
    
    indices = np.cumsum([len(list_values[i]) for i in order])
    indices = np.insert(indices, obj=0, values=0)   
    values = np.concatenate(
        [list_values[i] for i in order])

    return values, indices


def concatenate(red_arrays):
    ''' Concatenates reduced arrays
    
    red_arrays: list of (values, indices)
    '''
    
    ind = red_arrays[0][1]
    val = [red_arrays[i][0] for i in range(len(red_arrays))]

    for i in range(1, len(red_arrays)):
        ind = np.concatenate((ind, ind[-1] + red_arrays[i][1][1:]))
    val = np.concatenate(val)
    
    assert len(val) == ind[-1]
    return val, ind


def extract_elements(elements, values, indices):
    ''' Extract part of a reduced array as another
    new reduced_array.
    
    Notice - much more efficient ifc'elements' IS a sorted array. 
    Otherwise on current implementation it goes through a python for loop. 
    '''        
    if np.any(np.bincount(elements) > 1):
        raise ValueError('can not have repeated elements')

    if max(elements) > (len(indices) - 1):
        idx = np.arange(len(elements), dtype=int)[elements > (len(indices) -1)]
        raise ValueError("elements {0}:{1} not in 'indices'".format(
            idx, elements[idx]))
    
    arg_s = np.argsort(elements)
    if np.any(arg_s != np.arange(len(elements))):
        elements = elements[arg_s]
        # raise NotImplementedError(
        #     'Extract unsorted elements not implemented'
        # )
    
    idx_map, idx_count = map2elements(
        elements=elements, indices=indices, return_counts=True)
    
    new_values = values[idx_map == 0]
    new_indices = np.insert(np.cumsum(idx_count), obj=0, values=[0, ])
    
    # Shuffle them to get in the unsorted order
    if np.any(arg_s != np.arange(len(elements))):
        new_values, new_indices = shuffle(
            indices=new_indices, values=new_values, 
            positions=np.argsort(arg_s))
    return new_values, new_indices


######### Acess properties of RA
def counts(indices, elements=None):
    ''' Returns the number of values per 'elements' 
    in 'indices'.

    If 'elements' is None, then return counts for all
    elements in 'indices'

    ------- Not Tested

    '''
    if elements is None:
        elements = np.arange(len(indices) - 1, dtype=int)
    return (indices - np.roll(indices, shift=1))[1:][elements]


def elements_value(
        elements, values, indices, assume_sorted=False):
    ''' Return the 'values' for the 'elements'
    Notice - 'elements' has to be a sorted array. 
    '''
    val = None
    if not assume_sorted:
        if np.any(np.sort(elements) != elements):
            val, ind = extract_elements(
                elements=elements, indices=indices, values=values)
    
    if val is None:
        idx_map = map2elements(elements=elements, indices=indices)
        val = values[idx_map == 0]
    
    return val


def map2elements(
        elements, indices, return_counts=False):
    ''' Find where are the 'values' of 'elements'
        
        with element_values = 'values'['idx_map' == 0]

    Returns
    --------
    idx_map: np.ndarray of booleans with shape(n, )
        n: max(indices)
    
    If return_counts is True then also returns :
    idx_num: np.ndarray of integers with shape(e, )
        e: len(elements)
        Number of values found for each 'element'. 

    ------- Not Tested

    '''
    
    if np.any(np.bincount(elements) > 1):
        raise ValueError('can not have repeated elements')
        
    idx_num = counts(indices=indices)
    arr = np.ones(len(indices) - 1, dtype=int)
    arr[elements] = np.zeros(len(elements), dtype=int)
    idx_map = np.repeat(arr, idx_num)
    
    if return_counts:
        return idx_map, idx_num[elements]
    else:
        return idx_map


######### Manipulate RA
def argsort(indices, values):
    ''' Returns an np.ndarray sorting the 'values'
    independently for each element - interval defined in 'indices'.

    ------- Not Tested

    '''
    val_idx = np.repeat(
        np.arange(len(indices) -1, dtype=int), repeats=counts(indices))

    return np.lexsort((values, val_idx))


def shuffle(indices, values, positions, return_idx=False):
    ''' Shuffle elements according to 'positions'.

        Such that elements[positions] is the new ordering. 

    Parameters
    ----------
    indices: np.array of integers shape(n+1, )
            n: total number of elements
    
    values: np.array of shape(m, )
            m: total number of values
                m = indices[-1]
    
    positions: np.array of shape(n, )
    
    return_idx: boolean (default False)
        If return_idx is True, return the indices shuffling the 'values'

    Returns
    --------
    new_indices: np.array of integers shape(n+1, )
            n: total number of elements
    
    new_values: np.array of shape(m, )
            m: total number of values
                m = indices[-1]

    shuffle_idx: np.array of shape(m, )
        Indexing shuffling the 'values' input. 
    '''

    cnts = counts(indices)

    rep = np.repeat(np.argsort(positions), repeats=cnts)
    shuffle_idx = np.lexsort((np.arange(len(values), dtype=int), rep))
    
    new_values = values[shuffle_idx]
    new_indices = np.insert(np.cumsum(cnts[positions]), obj=0, values=[0, ])
    
    if return_idx:
        return new_values, new_indices, shuffle_idx
    else:
        return new_values, new_indices


def update_indices(elements, counts, indices):
    ''' Update 'indices' of 'elements' by 'counts'
    
    ------- Not Tested

    '''
    
    num_removed = np.zeros(len(indices) - 1, dtype=int)
    num_removed[elements] = counts
    indices[1:] = indices[1:] + np.cumsum(num_removed)
    
    return indices


def remove_all_values(elements, indices, values):
    ''' Remove all 'values' for 'elements'. 
    
    ------- Not Tested
    
    '''

    idx_map, idx_num = map2elements(
        elements=elements, indices=indices, 
        return_counts=True)
    
    # Remove the neighbours
    values = values[idx_map == 1]
    
    # Update the indexing        
    indices = update_indices(
        elements=elements, counts=-1 * idx_num, 
        indices=indices)
    
    return indices, values

    
def update_element(
        elements, new_values, new_indices, values, indices, 
        assume_sorted=False):
    ''' Replace 'values' of 'elements' by 'new_values'

    Parameters
    -----------
        indices: np.array of shape(m, )
            m: total number of neighbours
            Indices of the neighbours

        values: np.array of shape(n, )
            n: total amount of points in the mesh
            to which 'neighbours' belong.

        elements: np.array of shape(t, )
            Index to be added to 'neighbours'

        new_values: sequence of shape(t, a)
            Index of the neighbours of 'element'
        new_indices

    Returns
    --------
        Updated neighbours and neig_idx

    Notice neighbours[neig_idx[i]:neig_idx[i+1]] is the neighbours of
    point 'i' in the mesh to wich 'neighbours' belongs.
    
    ------- Not Tested

    '''

    if not assume_sorted:
        if np.any(np.sort(elements) != elements):
            raise NotImplementedError('Please sort "elements".')
        
    ######### Replacing the neighbours
    if new_indices is None:
        replace_element_indices = [len(n) for n in new_values]
        replace_element_values = np.concatenate([n for n in new_values])
    else:
        replace_element_indices = counts(new_indices)
        replace_element_values = new_values

    # Remove the values
    indices, values = remove_all_values(
        elements, indices=indices, values=values)
    
    # Add the values - Start by updating the indexing
    indices = update_indices(
        elements=elements, counts=replace_element_indices, 
        indices=indices)
    
    # Find where they are
    idx_map = map2elements(
        elements=elements, indices=indices)
    # Replace them
    n_n = np.zeros(indices[-1], dtype=int)
    n_n[idx_map==0] = replace_element_values
    n_n[idx_map==1] = values
    
    values = n_n

    return values, indices


def remove_element(elements, values, indices):
    ''' Removes 'element' from 'values' and 'indices'

    ------- Not Tested

    '''
        
    if max(elements) > max(values):
        raise RuntimeError('elements not found in neig')
    
    ######### Remove element
    indices, values = remove_all_values(
        elements=elements, indices=indices, values=values)
    
    # Then remove it from indices
    indices = np.delete(indices, obj=(elements + 1))   


    # Update the other values of values
    values, indices, upd_ele = remove_values(
        val2remove=elements, values=values, indices=indices)

    if len(values) != 0:
        # Shift the neighbour value
        correction = np.zeros(max(values) + 1, dtype=int)
        correction[elements[elements <= max(values)]] = np.ones(
            sum(elements <= max(values)))    
        correction = np.cumsum(correction)


        argsort = np.argsort(values)
        val, counts = np.unique(values[argsort], return_counts=True)    
        values = np.repeat(
            val - correction[val], counts)[np.argsort(argsort)].astype(int)   

    return values, indices, upd_ele


def add_elements(elements, elements_values, elements_indices,
                  values, indices):
    ''' Adds 'element' with 'element_values' and 'elements_indices'
    to 'values' and 'indices'. 

    ------- Not Tested

    '''
    if len(elements_values) > 0:
        
        # Add to values the elements with a values in element_values
        # Update the indexing accordingly

        idx_in_neig = np.arange(len(elements_values))[
            elements_values < (len(indices) - 1)]

        pos_ele = np.searchsorted(
            elements_indices, idx_in_neig, side='right') - 1

        values = np.insert(
            values, obj=indices[elements_values[idx_in_neig] + 1],
            values=elements[pos_ele])

        pos, idx, counts = meshing.unique(
            elements_values[idx_in_neig], return_counts=True)

        # Update the indexing
        indices = update_indices(
            elements=pos, counts=counts, indices=indices)
    elif sum(elements_indices) != 0:
        raise RuntimeError(
            'element_values is empty however sum(element_indices) is not zero')

    # Add the elements to values 
    # Update the indexing    
    indices = np.concatenate((indices, len(values) + elements_indices[1:]))
    if len(elements_values) > 0:
        values = np.concatenate((values, elements_values))
    
    return values, indices


def remove_values(val2remove, values, indices):
    ''' Removes 'val2remove' from 'values'. 

    Paramters
    ----------
        value: sequence of integers
            neighbour value to be removed

        values: np.array of integers
            values[0] output of - initialize_mesh()

        indices: np.array of integers
            values[1] output of - initialize_mesh()

    Returns
    --------
        updated values, updated indices, upd_ele

    ------- Not Tested

    '''

    remove_map = np.isin(values, test_elements=val2remove)
    remove_idx = np.arange(0, len(values))[remove_map]

    upd_ele, ignore, counts = meshing.unique(
        (np.searchsorted(indices, remove_idx, side='right') - 1),
         return_counts=True)

    # Update the indexing
    indices = update_indices(
        elements=upd_ele, counts=-1 * counts, indices=indices)
    
    return values[np.logical_not(remove_map)].astype(int), indices, upd_ele