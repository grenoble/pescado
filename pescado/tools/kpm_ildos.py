''' Makes ILDOS using kpm
'''

import copy
import pickle

from collections.abc import Sequence

from scipy.spatial import KDTree
from scipy import constants

import numpy as np
from numpy.polynomial.chebyshev import chebval

import kwant

from pescado.self_consistent import ildos


def kwant_chem_pot(
        kwant_system, chem_pot, chem_pot_coordinates, t, cutoff, tol=1e-12):
    ''' Generates a chemical potential in kwant
    units for every site in 'kwant_sites'

    Parameters
    -----------

    kwant_system: instance of kwant.Builder().finalized
        Finite system without leads

    chem_pot: np.ndarray of floats with shape(n, )
        Value for the chemical potential in Pescado units

    chem_pot_coordinates: np.ndarray of floats with shape(n, d)
        Coordinates for the values of chem_pot
        n: number of coordinates
        d: dimension of 'kwant_system'

    t: float
        Kwant energy to energy conversion

        E_SI = E_kwant * t

    kcp_cutoff: float
        Cutoff applied to 'chem_pot' in Pescado units

        chem_pot[chem_pot < cutoff] = cutoff * np.ones(sum(chem_pot < cutoff))

    tol: float
        Acts on coordinates

    Returns
    ---------
    dictionary with key the 'kwant.sites[i].tag' and value its chemical
        potential value in kwant units
    '''

    kwant_chem_pot = copy.deepcopy(chem_pot)

    # Apply cutoff to chem_pot
    kwant_chem_pot[chem_pot < cutoff] = cutoff * np.ones(sum(chem_pot < cutoff))

    # Transform into kwant units
    kwant_chem_pot *= -1 * constants.elementary_charge / t

    ks_coord = np.array([s.pos for s in kwant_system.sites])

    kdtree = KDTree(chem_pot_coordinates)
    dis, idx = kdtree.query(ks_coord, k=1)

    return {
        site.tag:val
        for site, val in zip(kwant_system.sites, kwant_chem_pot[idx])}


def kwant_kpm(
    kwant_system, kwant_sites, kwant_params, t, num_moments):
    ''' Returns a spectral density from kwant built
    using kwant.kpm.SpectralDensity

    Parameters
    -----------

    kwant_system: instance of kwant.Builder().finalized
        Finite system without leads

    kwant_sites: tuple of kwant sites

    kwant_params: dictionary
        Passed down to kwant.kpm.SpectralDensity

    num_moments: integer

        Number of moments passed down to kwant.kpm.SpectralDensity

        Notice, for Sites who are placed at a distance < 'num_moments / 2'
        from the boundary of the 'kwant_system', the dos might oscillate.

    t: float
        Kwant energy to energy conversion

        E_SI = E_kwant * t

    '''

    spec_dens = kwant.kpm.SpectralDensity(
        kwant_system,
        vector_factory=kwant.kpm.LocalVectors(kwant_system, where=kwant_sites),
        mean=False, num_vectors=None, accumulate_vectors=False,
        num_moments=num_moments, params=kwant_params)

    return make_PContinuousIldos(t=t, spec_dens=spec_dens, file_name=None)


def make_PContinuousIldos(
        t=None, spec_dens=None, kwant_chem_pot=None, kwant_sites=None,
        file_name=None, tol=1e-8):
    ''' Use this as a model function to make a ILDOS to PESCADO
        from kwant.
    '''

    if spec_dens is not None:
        spec_energies = spec_dens.energies
        spec_moments = spec_dens._moments()
        spec_a = spec_dens._a
        spec_b = spec_dens._b
    elif file_name is not None:
        with open(file_name, 'rb') as f:
            spec_dens = pickle.load(f)
        spec_energies = spec_dens['energies']
        spec_moments = spec_dens['_moments']
        spec_a = spec_dens['_a']
        spec_b = spec_dens['_b']
        
        kwant_chem_pot = spec_dens['kwant_chem_pot']
        kwant_chem_pot = {key:-val for key, val in kwant_chem_pot.items()}
        t = spec_dens['t']
        kwant_sites = spec_dens['kwant_sites']

    pcont_ildos_list = list()
    
    kw_bounds = np.array([np.min(spec_energies), np.max(spec_energies)])
    to_kwant = 1 * constants.elementary_charge / t

    spin_dof = 2

    for i in range(len(kwant_sites)):
        kwant_pot = kwant_chem_pot[kwant_sites[i].tag]

        def dos_ildos(mu, idx=i, _bounds=kw_bounds, _kwant_pot=kwant_pot):

            anl_ildos = analytical_ildos_kpm(
                spec_moments=spec_moments,
                spec_a=spec_a, spec_b=spec_b, idx=idx)

            dos_spec = dos_kpm(spec_moments=spec_moments,
                spec_a=spec_a, spec_b=spec_b, idx=idx)

            mu_kwant = (mu * to_kwant - _kwant_pot)

            if isinstance(mu_kwant, (Sequence, np.ndarray)):

                dos = np.zeros(len(mu_kwant))
                q = np.zeros(len(mu_kwant))

                in_bounds = np.arange(len(mu_kwant))[
                    (mu_kwant - _bounds[0]) >= tol]

                if np.any((mu_kwant - _bounds[1]) >= tol):
                    raise RuntimeError((
                        'Chemical potential '
                        + '{0} outside of integral bounds'.format(
                        mu_kwant[(mu_kwant - _bounds[1]) >= -tol])))

                if len(in_bounds) > 0:
                    dos[in_bounds] = dos_spec(mu_kwant[in_bounds])
                    q[in_bounds] = anl_ildos(e_f=mu_kwant[in_bounds])

            else:
                if (mu_kwant - _bounds[1]) >= tol:
                    raise RuntimeError((
                        'Chemical potential '
                        + '{0} outside of integral bounds'.format(
                        mu_kwant)))
                elif (mu_kwant - _bounds[0]) >= tol:
                    dos = dos_spec(mu_kwant)
                    q = anl_ildos(e_f=mu_kwant)
                else:
                    dos, q = 0, 0

            return spin_dof * dos * to_kwant, spin_dof * q

        bounds = np.zeros((4, 2))
        bounds[[1,2], 0] = kw_bounds + kwant_pot
        bounds[[1,2], 0] *= 1 / to_kwant
        
        bounds[[1,2], 1] = dos_ildos(mu=bounds[[1,2], 0])[1]
        
        bounds[3, 0] = bounds[2, 0]
        bounds[3, 1] = 2 * bounds[2 , 1]

        bounds[0, 0] = np.min([2 * bounds[0, 0], -bounds[3, 0]])
        bounds[0, 1] = 0

        pcont_ildos_list.append(
            ildos.PContinuousIldos(
                coordinates=bounds, 
                functions=[
                    neumann_bd, dos_ildos, dirichlet_bd]))

    return pcont_ildos_list


def analytical_ildos_kpm(spec_moments, spec_a, spec_b, idx):
    ''' Generates a kpm ILDOS using analytical integral
    for Heaviside distribution (Fermi Dirac at T=0).

    See Apendix B, Eq.B.8 from A.Lacerda-Santos PhD Manuscript

    Parameters
    -----------
    spec_dens: instance of kwant.kpm.SpectralDensity

    idx: integer (default None)
        spec_dens vector basis element to get the ILDOS

    Returns
    --------
    Ildos function
    '''

    def analytical_integration(e_f):

        moments = spec_moments[:, idx]

        lower_bound_int = moments[0]
        es_f = (e_f - spec_b) / spec_a

        if not isinstance(e_f, (Sequence, np.ndarray)):
            es_f = np.array([es_f, ])

        c_i = (-1 / np.pi) * np.ones((len(moments[:]), len(es_f)))
        m = np.arange(1, len(moments[:]))

        c_i[0, :] = moments[0] * c_i[0, :] * np.arccos(es_f)
        c_i[1:, :] = (
            2 * c_i[1:, :] * (moments[1:] / m)[:, None]
            * np.sin(m[:, None] * np.arccos(es_f).T))

        if not isinstance(e_f, (Sequence, np.ndarray)):
            upper_bound_int = np.sum(c_i)
        else:
            upper_bound_int = np.sum(c_i, axis=0)

        return (upper_bound_int + lower_bound_int) * spec_a

    return analytical_integration

def dos_kpm(spec_moments, spec_a, spec_b, idx):
    ''' Calculates the KPM density of states
    Uses the same equation as the spec_dens.__call__ method
    '''

    def dos(e):

        moments = copy.deepcopy(spec_moments[:, idx])
        e_tilde = (e - spec_b) / spec_a

        if not isinstance(e, (Sequence, np.ndarray)):
            e_tilde = np.array([e_tilde, ])

        factor = np.pi * np.sqrt(1 - e_tilde) * np.sqrt(1 + e_tilde)
        moments[1:] *=2

        return chebval(e_tilde, moments) / factor

    return dos

def dirichlet_bd(mu):
    '''
    '''
    dos, q = np.infty, np.infty

    if isinstance(mu, (Sequence, np.ndarray)):
        dos = np.ones(len(mu)) * dos
        q = np.ones(len(mu)) * q

    return dos, q


def neumann_bd(mu):
    '''
    '''
    dos, q = 0, 0

    if isinstance(mu, (Sequence, np.ndarray)):
        dos = np.ones(len(mu)) * dos
        q = np.ones(len(mu)) * q

    return dos, q
