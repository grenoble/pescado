from collections.abc import Sequence

import copy
import numpy as np


class SparseVector():
    def __init__(self,
        indices=None, values=None, default=None,
        assume_sorted=False, dtype=None):
        ''' Returns an instance of SparseVector

        Parameters
        -----------

        indices: np.array of intergers (default None)
            Index of the non default values

        values: np.array of floats (default None)

        default: float or None (optional, default is None)
            Value assigned to any index no in 'indices'

        assume_sorted: bool (optional, default False)
            Wether to assume that 'indices' are sorted.

        dtype: numpy.dtype for the self.values array (default None)

        Returns
        --------
        instance of pescado.tools.sparse_vector.SparseVector

        Notes
        ------
        Can be initialized without any 'indices' or 'values'.
        The result SparseVector will be an empty instance.

        TODO: Add test for 'dtype'.

        '''

        if (indices is None) and (values is None):
            indices = np.array([])
            values  = np.array([])

        self.indices = np.asarray(indices).astype(int)
        self.values = np.asarray(values)
        self.default = default

        if dtype is not None:
            self.values = self.values.astype(dtype)
        self.dtype = self.values.dtype

        unique, counts = np.unique(self.indices, return_counts=True)
        if np.any(counts > 1):
            raise IndexError('"indices" is not unique')

        if assume_sorted is False:
            self._sort()

    def _sort(self):
        ''' Sorts self.values and self.indices
        '''

        argsorted = np.argsort(self.indices)
        self.indices = self.indices[argsorted].astype(int)
        self.values = self.values[argsorted].astype(self.dtype)

    def __call__(self):
        ''' Returns the values for the 'indices'

        Parameters
        -----------
        indices: sequence of integers

        Returns
        -------
        np.ndarray of integers

        '''
        raise NotImplementedError('Use [] instead.')
        # default_indices = np.setdiff1d(
        #     indices, self.indices)

        # if (self.default is None) and (len(indices) != 0):
        #     raise ValueError(
        #         'Indices {0} are not defined'.format(
        #             default_indices))

        # indices, indices_pos, self_indices_pos = np.intersect1d(
        #     indices, self.indices, return_indices=True)

    def __getitem__(self, indices):
        ''' Returns the values for the 'indices'

        Parameters
        -----------
        indices: sequence of integers

        Returns
        -------
        np.ndarray of integers

        TODO: TETEST NOT UNIQUE THING
        '''

        sequence_arr = True
        if not isinstance(indices, (np.ndarray, Sequence)):
            sequence_arr = False
            indices = [indices, ]
            #print(type(indices))

        argsorted = np.argsort(indices)

        num_idx = len(indices)
        indices = np.asarray(indices)
        indices, idx, counts = np.unique(
            indices[argsorted], return_index=True, return_counts=True)

        non_default, self_pos, pos = np.intersect1d(
            self.indices, indices, return_indices=True)

        if (self.default is None) and (len(indices) != len(non_default)):
            if len(non_default) > 0:
                err = np.setdiff1d(indices, non_default)
            else:
                print(type(indices))
                err = indices
            raise ValueError(
                'Indices {0} are not defined'.format(err))

        if self.default is not None:
            values = np.ones(len(indices)) * self.default
            values[pos] = self.values[self_pos]
        else:
            values = self.values[self_pos]
        
        all_values = np.empty(num_idx)
        for i, (id, num) in enumerate(zip(idx, counts)):
            all_values[id:id+num] = values[i] * np.ones(num)
        
        if not sequence_arr:
            return all_values.astype(self.dtype)[np.argsort(argsorted)][0]
        else:
            return all_values.astype(self.dtype)[np.argsort(argsorted)]

    def find(self, value, tol=1e-12):
        ''' Finds the indices where 'self.values' == 'value'

        Parameters
        -----------

        value: float

        tol: float (default is 1e-12)

        Returns
        --------

        np.array of integers with shape(m, )

        Notes
        ------
        'value' can't be equal to the default, otherwise
        it raises ValueError

        TODO: Not tested yet.
        '''

        if self.default is not None:
            if np.abs(self.default - value) < tol:
                raise ValueError(
            '"value" is equal to current default value of {0}'.format(
                self.default))

        return self.indices[np.abs(self.values - value) < tol].astype(int)

    def __setitem__(self, indices, values):
        ''' Sets the values of 'indices'

        Parameters
        -----------

        indices: sequence of integers

        values: sequence of floats

        '''
        if not isinstance(indices, (np.ndarray, Sequence)):
            indices = [indices, ]
        if not isinstance(values, (np.ndarray, Sequence)):
            values = [values, ]

        indices = np.asarray(indices)
        values = np.asarray(values)

        # Remove the indices that we are going to reset
        self.delete(indices=indices)

        # Remove those equal to default from the input
        not_default_map = (values != self.default)
        self.indices = np.concatenate(
            [self.indices, indices[not_default_map]]).astype(int)
        self.values = np.concatenate(
            [self.values, values[not_default_map]]).astype(self.dtype)

        self._sort()

    def delete(self, indices):
        ''' Deletes the 'indices' from the sparse vector

        Parameters
        ----------

        indices: sequence of integers

        TODO: TEST IT !!!!
        '''
        indices = np.asarray(indices)
        argsorted = np.argsort(indices)

        # Remove the indices that we are going to reset
        ignore, to_remove_self, to_remove = np.intersect1d(
            self.indices, indices[argsorted], return_indices=True)

        self.indices = np.delete(self.indices, to_remove_self).astype(int)
        self.values = np.delete(self.values, to_remove_self).astype(self.dtype)

    def extend(self, sv_inst):
        ''' Extends the 'self.indices' and 'self.values' of self
        to acomodate for the values in 'sv_inst' not in 'self'.

        Parameters
        ----------

        sv_inst: instance of pescado.SparseVector
        '''

        unique_other = np.setdiff1d(sv_inst.indices, self.indices)
        ignore, pos, ignore = np.intersect1d(
            sv_inst.indices, unique_other, return_indices=True)

        self.indices = np.concatenate(
            [self.indices, sv_inst.indices[pos]]).astype(int)
        self.values = np.concatenate(
            [self.values, sv_inst.values[pos]]).astype(self.dtype)

        self._sort()

    def extract(self, indices):
        ''' Extracts the 'indices' of self into a new SparseVector instance

        Parameters
        ----------

        indices: sequence of integers

        Returns
        -------

        sv_inst: instance of pescado.SparseVector
        TODO: TEST IT
        '''
        indices = np.asarray(indices)

        return SparseVector(
            values=self[indices], indices=indices, dtype=self.dtype)

    def __add__(self, other_pv):
        ''' Addition between two SparseVectors

            Parameters
            ----------

            other_pv: instance of pescado.SparseVector

            Both non default and default values are added.

            Addition between two None values is not allowed
            Addition between a None value and a float is not allowed
        '''

        if not isinstance(other_pv, SparseVector):
            raise ValueError(
                'Addition is only allowed between instances of SparseVector'
                + " however 'other_pv' is {0}".format(type(other_pv)))

        new_s_v = SparseVector(
            indices=copy.deepcopy(self.indices),
            values=copy.deepcopy(self.values),
            default=copy.deepcopy(self.default),
            dtype=self.dtype)

        # Indices is sorted for any instance of SparseVectors
        common = np.intersect1d(other_pv.indices, new_s_v.indices)
        unique_other_idx = np.setdiff1d(other_pv.indices, new_s_v.indices)
        unique_self_idx = np.setdiff1d(new_s_v.indices, other_pv.indices)
        
        new_s_v[common] += other_pv[common]

        if ((len(unique_other_idx) > 0 or len(unique_self_idx) > 0)
            and ((self.default is None) or (other_pv.default is None))):
            raise ValueError(
                'Addition between {0} and {1} is not possible'.format(
                    self.default, other_pv.default)
                + '{0}.default is {1} where '.format(
                    self, self.default)
                + '{0}.default is {1} where. \n'.format(
                    other_pv, other_pv.default))
        elif (self.default is not None) and (other_pv.default is not None):
            
            new_s_v[unique_self_idx] += other_pv.default
            # Only extend if both are not None, otherwise the indices should
            # match exactly
            # Only extend for the unique_other_idx !! 

            new_s_v.extend(sv_inst=other_pv.extract(unique_other_idx))

            new_s_v[unique_other_idx] += new_s_v.default
            new_s_v.default += other_pv.default

        return new_s_v

    def __mul__(self, scalar):
        ''' Multiplication between an instance of SparseVector and a scalar

        Parameters
        ----------

        scalar: float or int

        Notes
        -----

        Equivalent to :
            self.values *= scalar
            self.default *= scalar
        '''

        if not isinstance(
            scalar, (float, int, np.integer, np.floating)):
            raise ValueError(
                'scalar is {0} however float or int was expected'.format(
                    type(scalar)))

        if self.default is not None:
            return SparseVector(
                indices=copy.deepcopy(self.indices),
                values=copy.deepcopy(self.values) * scalar,
                default=copy.deepcopy(self.default) * scalar,
                dtype=self.dtype)
        else:
            return SparseVector(
                indices=copy.deepcopy(self.indices),
                values=copy.deepcopy(self.values) * scalar,
                dtype=self.dtype)
