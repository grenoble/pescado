'''
    Tools module
'''

from .assertions import (
    assert_coord_convention, assert_is_inside_output, assert_unique,
    assert_not_unique, custom_issubdtype)

from .meshing import (
    grid, polygons_surface, points_distance, polygons_volume, )
from ._meshing import unique, repeated_values
from .sparse_vector import SparseVector
from .discretized_ildos import DiscreteIldos
from .plotting import plot_map
#from .error import stupid_iteration_error

__all__ = (['assert_coord_convention',
            'assert_is_inside_output',
            'assert_unique',
            'assert_not_unique',
            'SparseVector',
            'DiscreteIldos'])
