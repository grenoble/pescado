''' TODO: Testing'''
import copy
from collections.abc import Sequence
import numpy as np
import bisect

from scipy.optimize import approx_fprime


class DiscreteIldos():
    ''' Discretizes a continuously increasing ILDOS
    '''

    def __init__(self,
        quantum_functional, functional_points,
        potential_precision, charge_precision=None,
        tolerance=None):
        ''' Constructs a 'discretized_ildos' by finding the tangents around the
        'functional_points' with a precision of 'potential_precision'. The
        coordinates of the discrete ildos are the intersections of such
        tangents.

        Parameters
        -----------

        quantum_functional: function (default is None)
            Returns the (origin, slope) for a given functional point
            The (origin, slope) defines the tangent around the functional point,
            respecting the equation :

                charge = origin + slope * potential

        potential_precision: float (default is None)
            Used for continuous ILDOS. It is used for two things :

            i) Step used in scipy.optimize.approx_fprime to calculate the
            tangent from a 'functional_point', i.e. the 'epsilon' parameter

            ii) Precision used to verify if the new potential (solution of
            the PESCADO) is added as a new functional point.

                np.abs(
                    self.functional_pts - possible_pot) > potential_precision

            It can be interpreted as the maximal resolution used to discretize
            the ildos, in case the latter is continuous.

        functional_points: sequence of floats
            Potential values around which the ildos will be discretized

        tolerance: float or sequence of 2 floats (default 1e-13)
            tolerance for voltage, charge and density of states.

            [tol_voltage, tol_charge, tol_dos]

         '''

        self.quantum_functional = quantum_functional

        self.potential_precision = potential_precision
        self.charge_precision = charge_precision

        if self.charge_precision is None:
            self.slope_cutoff = None
        else:
            self.slope_cutoff = self.charge_precision / self.potential_precision

        if tolerance is None:
            tolerance = [1e-10, ] * 3
        elif isinstance(tolerance, int):
            tolerance = [tolerance, ] * 3

        self.tolerance = tolerance

        self.functional_points = np.array([])
        self.slope = np.array([])
        self.origin = np.array([])

        self.discrete_ildos = None

        self.update(functional_points=functional_points)

    def update(self, functional_points):
        ''' Updates the 'self.discrete_ildos' by adding 'functional_points'
        to the list of 'self.functional_points'.

        Parameters
        -----------
        functional_points: sequence of floats
            Potential values around which the ildos will be discretized
        '''

        self._update_tangent(functional_points=functional_points)

        # Get new discretized Ildos
        discrete_ildos = self._discretize()
        # Check for coordinates outside of functional points bounds

        discrete_ildos = self.check_bounds_intersection(
            discrete_ildos=discrete_ildos)

        # Check that the lines connecting the ildos coordinates
        # have a slope smaller than self.slope_cutoff OR are straight lines
        if self.slope_cutoff is not None:
            discrete_ildos = self._account4slopecutoff(discrete_ildos=discrete_ildos)

        self.discrete_ildos = np.zeros((len(discrete_ildos) + 2, 2))

        self.discrete_ildos[0] = np.array(
            [self.functional_points[0] * 2 - self.functional_points[1],
             ((self.functional_points[0] * 2 - self.functional_points[1]) * self.slope[0]
              + self.origin[0])])

        self.discrete_ildos[-1] = np.array(
            [self.functional_points[-1] * 2 - self.functional_points[-2],
             ((self.functional_points[-1] * 2 - self.functional_points[-2]) * self.slope[-1]
              + self.origin[-1])])

        self.discrete_ildos[1:-1, :] = discrete_ildos

    def _account4slopecutoff(self, discrete_ildos):
        ''' Calculates the slope separating the points in self.discrete_ildos.
        If larger then self.slope_cutoff, replace them by straight lines.

        Returns
        --------
            new_coord: New potential / charge coordinates for the discrete ildos.
        '''

        slope = ((discrete_ildos[1:, 1] - discrete_ildos[:-1, 1])
                  / (discrete_ildos[1:, 0] - discrete_ildos[:-1, 0]))

        outside_intervals = np.arange(len(slope))[
            slope >= self.slope_cutoff]

        insert_pos = np.insert(
            outside_intervals + 1,
            obj=np.arange(1, len(outside_intervals) + 1),
            values=outside_intervals + 2)

        insert_val = discrete_ildos[insert_pos -1, :]
        for i in np.arange(0, len(outside_intervals), 2):
            insert_val[[i, i+1], 0] = np.array(
                [np.mean(insert_val[[i, i+1], 0]), ] * 2)

            insert_pos[i + 1] -= 1

        new_coord = np.empty((
            len(discrete_ildos) + len(insert_pos), 2))

        new_coord[:, 0] = np.insert(
            discrete_ildos[:, 0], obj=insert_pos, values=insert_val[:, 0])
        new_coord[:, 1] = np.insert(
            discrete_ildos[:, 1], obj=insert_pos, values=insert_val[:, 1])

        return new_coord

    def _discretize(self):
        ''' Generates the coordinates for a discrete version of
        self.ildos based on self.functional_points.

        Returns
        --------

            numpy array of floats with shape(m, 2)
                with m len(self.functional_points)

        Notes
        ------

            The points are chosen as the intersections of the
            tangents around each functional point.

        '''
        char, pot = [np.zeros(len(self.slope)), ] * 2

        pot = (self.functional_points[:len(self.slope) - 1]
               + self.functional_points[1:len(self.slope)]) / 2

        for idx in range(len(self.slope) - 1):

            sl, org = (self.slope[idx], self.origin[idx])
            sl_rg, org_rg = (self.slope[idx + 1], self.origin[idx + 1])

            if  ((np.abs(sl - sl_rg) > self.tolerance[2])
                 and ((sl != 0) or (sl_rg != 0))):

                pot[idx] = (org_rg - org) / (sl - sl_rg)

        char = self.origin[:-1] + self.slope[:-1] * pot

        return np.concatenate(
            [pot[:, None], char[:, None]], axis=1)

    def check_bounds_intersection(self, discrete_ildos):
        ''' Verifies that the iéme coordinate in discrete_ildos are
        in between the iémé and iémé + 1 functional points.
        Returns the indices that do not respect such condition
        and the new functional point that solves the issue.

        Parameters
        -----------

        discrete_ildos: numpy array of floats with shape(m, 2)
            With m len(self.functional_points)

            The (pot, char) coordinates of the discrete ildos.

            Must respect the order in self.functional_points

        Returns
        -------

        idx: np.array of integers
            Index of the (pot_coord, char_coord)[i] not in between the
            self.functional_points[i], self.functional_points[i + 1]

        add_fun_pts: np.array of floats
            Functional points to be added such that the all points in
            the discretize_ildos are in betwen
            (self.functional_points[:-1], self.functional_points[1:])

        Notes
        ------
        The condition used to select the new functional points in add_fun_pts is:

            TODO:

            OBS: For this to work the self.ildos must be a monotonically
            increasing function.

        '''
        wrg_idx = list()

        charge = self.slope * self.functional_points + self.origin

        for i in range(len(discrete_ildos)):

            b_min = [self.functional_points[i], charge[i]]

            b_max = [self.functional_points[i + 1], charge[i + 1]]

            if (np.any((discrete_ildos[i, :] - b_min) <= 0)
                or np.any((b_max - discrete_ildos[i, :]) <= 0)):
                wrg_idx.append(i)

        wrg_idx = np.array(wrg_idx)
        counts = 0
        for j in wrg_idx:

            sl_lf, org_lf = (self.slope[j], self.origin[j])
            sl_rg, org_rg = (self.slope[j + 1], self.origin[j + 1])

            if sl_rg == 0:
                sign_lf = 1
            else:
                sign_lf = (self.functional_points[j] - ((charge[j] - org_rg) / sl_rg))

            if sl_lf == 0:
                sign_rg = -1
            else:
                sign_rg = (self.functional_points[j + 1] - ((charge[j + 1]- org_lf) / sl_lf))

            if ((sign_lf * sign_rg) < 0 or (sl_lf * sl_rg == 0)):

                coord_i = [self.functional_points[j], charge[j]]
                coord_ii = [self.functional_points[j + 1], charge[j + 1]]

                if j > 0:
                    if np.all(discrete_ildos[j + counts - 1, :] == coord_i):
                        coord_i = coord_ii

                discrete_ildos[j + counts, :] = coord_i

                if coord_i != coord_ii:

                    discrete_ildos = np.insert(
                        arr=discrete_ildos, obj=j + counts + 1,
                        values=coord_ii, axis=0)

                    counts += 1

        return discrete_ildos

    def interval(self, coordinates):
        ''' Returns the interval inside the ILDOS in which 'coordinates'
        are found.

        Checks where coordinates + tolerance > self[:]

        Parameters
        -----------

        coordinates: np.array of floats with shape(m, 2)
            m number of points
            axis 0 -> potential values
            axis 1 -> charge values

        Returns
        -------

        np.array of ints with shape(m, )
            Ildos intervals where each point coordinate[m, :] is found.

        Notes
        ------
            Points outside of the self[:] max / min points are set to the
            first / last intervals

        TODO: ADD TEST
        '''
        potential_cond = (coordinates[:, 0] + self.tolerance[0]) > self[:, 0][:, None]
        charge_cond = (coordinates[:, 1] + self.tolerance[1]) > self[:, 1][:, None]

        cond = (potential_cond * charge_cond).T

        interval = np.max(cond * np.arange(cond.shape[1]), axis=1).astype(int)

        # If smaller than the first ildos coordinate than 0
        # If larger than the first ildos coordinate then len(self[:]) - 1
        # Hence set it to len(self[:]) - 2 -> number of intervals
        interval[interval == (len(self[:]) - 1)] -= 1

        return interval

    def __getitem__(self, indices):
        ''' Returns the values for the 'indices'

        Parameters
        -----------
        indices: sequence of integers or slice (like for a numpy array)

        Returns
        --------
        np.array of floats

        '''

        types = (int, np.int64, np.int32, slice)

        ax_1, ax_2 = (None, ) * 2

        if isinstance(indices, Sequence):
            if isinstance(indices[0], types):
                ax_1 = indices
            else:
                ax_1, ax_2 = indices
        elif isinstance(indices, types):
            ax_1 = indices
        else:
            raise NotImplementedError(
                ('indices can only be Sequence or integer however'
                 + '{0} was given'.format(type(indices))))

        if ax_2 is None:
            return self.discrete_ildos[ax_1]
        else:
            return self.discrete_ildos[ax_1, ax_2]

    def _update_tangent(self, functional_points):
        ''' Updates the tangent information ('self.slope', 'self.origin')
        to incorporate the 'new_functional_points' into 'self.functional_points'

        Parameters
        ------------

        functional_points: np.ndarray of floats

            Potential points to be added to self.functonal_points
        '''

        # Sort and remove repeated points
        new_fun_pts = np.setdiff1d(
            functional_points, self.functional_points)

        updated_functional_points = np.empty(
            len(new_fun_pts) + len(self.functional_points))

        slope = np.empty(len(updated_functional_points))
        origin = np.empty(len(updated_functional_points))

        updated_functional_points[
            :len(self.functional_points)] = self.functional_points

        slope[:len(self.slope)] = self.slope
        origin[:len(self.origin)] = self.origin

        # Added dirhclet intervals
        idx_inf = np.arange(len(self.functional_points))[
            slope[:len(self.functional_points)] == np.inf]

        # Added Neumann intervals
        idx_nan = np.arange(len(self.functional_points))[
            np.isnan(slope[:len(self.functional_points)])]

        for index in np.concatenate([idx_inf, idx_nan]):
            origin[index], slope[index] = self.quantum_functional(
                updated_functional_points[index])

        for i, point in enumerate(new_fun_pts):
            if len(self.functional_points) > 0:
                index = bisect.bisect_left(
                    updated_functional_points[
                        :len(self.functional_points) + i], point)
            else:
                index = i

            for vect in [updated_functional_points, slope, origin]:
                vect[index + 1:] = vect[index:-1]

            updated_functional_points[index] = point
            origin[index], slope[index] = self.quantum_functional(point)

        if np.any(slope < 0):
            print(slope)
            print(updated_functional_points)
            print(updated_functional_points[(slope < 0)[:, 0]])
            raise RuntimeError(
                ('continuous_ildos must be increasing function'
                    + ' negative slope found for tangent around {0}'.format(
                    updated_functional_points[(slope < 0)[:, 0]])))

        arg_sort = np.argsort(updated_functional_points)

        self.slope = slope[arg_sort]
        self.origin = origin[arg_sort]
        self.functional_points = updated_functional_points[arg_sort]

        # Test for redundent points

        # # Remove redundant points based on potential

        decimals = str(self.potential_precision)[::-1].find('.')
        if decimals == -1:
            decimals = str(self.potential_precision)[
                str(self.potential_precision).find('e-') + 2:]

        #This is already sorted
        ignore, unq_idx, counts = np.unique(
            np.around(self.functional_points, decimals=int(decimals)),
            return_index=True, return_counts=True)

        minim_index_dir = copy.deepcopy(unq_idx)

        # Only two nescessary to define a Dirichlet interval
        for c, pos in enumerate(np.arange(len(counts))[counts > 1]):
            minim_index_dir = np.insert(
                arr=minim_index_dir,
                obj=pos + c + 1, values=unq_idx[pos] + counts[pos] -1)

        self.functional_points = self.functional_points[minim_index_dir]
        self.slope = self.slope[minim_index_dir]
        self.origin = self.origin[minim_index_dir]

        # Only two nescessary to define a Dirichlet interval

        # must be sorted already
        charge = self.slope * self.functional_points + self.origin

        if np.any(np.abs(np.sort(charge) - charge) > self.tolerance[1]):
            raise RuntimeError(
                ('Charge : {0} \n'.format(charge)
                 + 'calculated from self.functional_points : {0}\n'.format(self.functional_points)
                 + 'is not ordered'))

        decimals = str(self.tolerance[-1])[::-1].find('.')
        if decimals == -1:
            decimals = str(self.tolerance[-1])[
                str(self.tolerance[-1]).find('e-') + 2:]

        ignore, unq_idx, counts = np.unique(
            np.around(charge, decimals=int(decimals)),
            return_index=True, return_counts=True)

        # Only two nescessary to define a Neumann interval
        minim_index_neu = copy.deepcopy(unq_idx)
        for c, pos in enumerate(np.arange(len(counts))[counts > 1]):
            minim_index_neu = np.insert(
                arr=minim_index_neu,
                obj=pos + c + 1, values=unq_idx[pos] + counts[pos] -1)

        self.functional_points = self.functional_points[minim_index_neu]
        self.slope = self.slope[minim_index_neu]
        self.origin = self.origin[minim_index_neu]

    @property
    def shape(self):

        return self.discrete_ildos.shape

    @classmethod
    def from_continuous(
        cls, ildos, potential_precision,
        functional_points, charge_precision=None, tolerance=None):
        ''' Discretizes the 'ildos' by finding the tangents around the
        'functional_points' with a precision of 'potential_precision'. The
        coordinates of the discrete ildos are the intersections of such
        tangents.

        Parameters
        -----------

        ildos: continously increasing function

        potential_precision: float (default is None)
            Used for continuous ILDOS. It is used for two things :

            i) Step used in scipy.optimize.approx_fprime to calculate the
            tangent from a 'functional_point', i.e. the 'epsilon' parameter

            ii) Precision used to verify if the new potential (solution of
            the PESCADO) is added as a new functional point.

                np.abs(
                    self.functional_pts - possible_pot) > potential_precision

            It can be interpreted as the maximal resolution used to discretize
            the ildos, in case the latter is continuous.

        functional_points: sequence of floats
            Potential values around which the ildos will be discretized

        tolerance: float or sequence of 2 floats (default 1e-13)
            tolerance for voltage, charge and density of states.

            [tol_voltage, tol_charge, tol_dos]

        TODO: Change the use of ildos , it requires a stupid
        if not instance everytime you define it

            def ildos(mu):

                dens = m_eff / (np.pi * (constants.hbar ** 2))

                #Get to the correct units
                dens = (dens / gaz_ep) * 1e-18
                mu_si = mu * constants.elementary_charge

                if not isinstance(mu, (np.ndarray, Sequence)):
                    return [dens * mu_si, ]
                else:
                    return dens * mu_si

        '''

        def quant_fun(point):

            if isinstance(point, (np.ndarray, Sequence)):
                slope = np.empty(len(point))
                origin = np.empty(len(point))

                for i, pt in enumerate(point):
                    slope[i] = approx_fprime(
                        xk=np.array([pt]), f=ildos,
                        epsilon=potential_precision)[0]

                    origin[i] = (ildos(pt) - slope[i] * pt)[0]
            else:

                slope = approx_fprime(
                        xk=np.array([point]), f=ildos,
                        epsilon=potential_precision)[0]

                origin = (ildos(point) - slope * point)[0]

            return origin, slope

        return cls(
            quantum_functional=quant_fun, charge_precision=charge_precision,
            potential_precision=potential_precision,
            functional_points=functional_points,
            tolerance=tolerance)


