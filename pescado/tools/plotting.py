import numpy as np
from scipy.spatial import KDTree

import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib as mpl

from pescado.tools import reduced_array as ra
from pescado.tools import meshing
from pescado import mesher


def discrete_value_plot(
        region_plot_fun, extent, values, names, **kwargs):        
    ''' Generates a function plotting an imshow of discrete integer values, 
    where each integer value has a specific color and 'name'. Generates
    a label for each value folowing 'name'.

    Parameter
    ----------

    region_plot_fun: output of 'region_plot()'

    extent: np.array of floats with shape(2, 2)
        Same as extent parameter of plt.imshow()

    values: np.ndarray of integers

    names: sequence of strings
        len(names) = max(values)

    label: boolean
        If False does not show label (default is True)

    ref_y: float
        Y position of the label (default is 0.1)

    rel_h: float
        Thickness of the label (default is 0.05)
    
    rel_w: float
        Length of the label (default is 0.1)

    rel_ws: float
        Space between two rows of labels (default is 0.025)

    n_label_row: integer
        Number of labels per row. If len(names) > n_label_row
        then the labels are stack horizontally. 

    Returns
    --------
    function plotting the imshow()
        Parameters
        -----------
        
        cut_position: float -> default is None
            In case slice of 3D system - specify the 
            value where to slice the system along the invariant direction

        ax: instance of matplotlib.pyplot.gca() (default is None)

        kwargs: parameters transfered to matplotlib.pyplot.imshow()
    '''

    dk = {
        'label':True, 'rel_h':0.05, 'n_label_row':4,
        'rel_w':0.1 , 'ref_y':0.1 , 'rel_ws':0.025}

    for key, val in kwargs.items():
        dk[key] = val
    
    def plot_fun(cut_position=None, ax=None, **kwargs):
        
        if ax is None:
            ax = plt.gca()
        if 'cmap' not in kwargs.keys():
            kwargs.update(
                {'cmap':cm.ScalarMappable(cmap='Set1').cmap})

        cmap = kwargs['cmap']
        c = cmap(np.arange(0, len(names), dtype=int))
        kwargs.update({'colors':c})           
        n_reg = len(c)

        y = np.repeat(
            np.arange(1, 1 + ((n_reg // dk['n_label_row']) + 1) * 2, 2),
            repeats=dk['n_label_row']) * dk['ref_y']
        
        x = np.tile(
            np.linspace(0.1, 0.7, dk['n_label_row']), 
            reps=(n_reg // dk['n_label_row']) + 1)
        
        xy = [(x[i], y[i]) for i in range(n_reg)]
        
        size_x = extent[1, 0] - extent[0, 0]
        size_y = extent[1, 1] - extent[0, 1]
        
        if dk['label']:
            for i in range(len(c)):

                x = extent[0, 0] + size_x * xy[i][0]
                y = extent[1, 1] + size_y * xy[i][1]
                width = size_x * dk['rel_w']
                height = size_y * dk['rel_h']
                
                ax.add_patch(
                    mpl.patches.Rectangle(
                        xy=(x, y), width=width, height=height,
                        facecolor=c[i], clip_on=False))
                ax.text(x=x, y=y * (1 + dk['rel_h'] + dk['rel_ws']), s=names[i])

        return region_plot_fun(
            values=values, cut_position=cut_position, ax=ax, **kwargs)
    return plot_fun


def region_plot(
        region, values_coord, grid_step, direction=None,
        grid_center=None,  tol=1e-12):
    ''' Generates the function 'plot_fun'. It plots the 'values' inside
    'region'. 
    
    If direction is None: 'plot_fun(values)'

    If direction is not None: it slices 'values_coord' at a 
    constant 'cut_position' along 'direction' :
            
                'plot_fun(cut_position, values)'

    Parameters
    -----------
    
    region: instance of 'pescado.mesher.shapes'
        Notice -> dimension of the 'region' must be smaller than 'd'

    grid_step: float
        Distance between grid points used for plotting
    
    grid_center: sequence of floats (default None)
        Coordinates of the center of the grid used for plotting

    values_coord: np.ndarray of floats with shape(n, d)

    direction: integer or sequence of integers (default is None)
        Slicing direction

            Example: Take a 3D system, then if:
                direction = 2 -> make a 2D plot along (0, 1)
                direction = (1, 2) -> make a 1D slice along 0

    tol: float (default is 1e-12)

    Returns
    --------
    plot_fun:

        Parameters
        -----------

        values: np.ndarray of integer or floats with shape(n, d)
            Notice -> values[i] refer to the value at coordinate 
            'values_coord[i]'

        cut_position: floats or sequence of floats (default is None)
            Same format as 'direction'

        ax: instance of matplotlib.pyplot.gca() (default is None)

        cmap: str (default 'viridis')
            c.f. https://matplotlib.org/examples/color/colormaps_reference.html

            Ignored if 1D plot

        kwargs: arguments passed down to 
            i) ax.plot if 1D plot
            ii) ax.imshow if 2D plot

        Returns
        --------

        if 1D plot -> 'ax'
        if 2D plot -> 'ax' and 'ax.imshow(-)'
       
    '''
    
    pattern = mesher.patterns.Rectangular.constant(
        element_size=(grid_step, ) * region.bbox.shape[1], 
        center=grid_center)
    coord_reg = pattern(pattern.inside(region))
    
    ### In case the region is not rectangular
    # Get the min and max
    ll_plt = np.min(coord_reg, axis=0)
    size_plt = np.abs(ll_plt - np.max(coord_reg, axis=0))
    bbox_reg = mesher.shapes.Box(lower_left=ll_plt, size=size_plt)

    # Get the plotting coord
    coord_plot = pattern(pattern.inside(bbox_reg))
    coord_plot_in_reg = region(coord_plot) 

    dim = coord_plot.shape[1]
    
    if direction is None:
        closest = KDTree(values_coord).query(
            coord_plot[coord_plot_in_reg])[1]
    else:
        if isinstance(direction, (int, np.integer)):
            direction = [direction, ]
        if dim not in [1, 2]:
            raise ValueError(
                'Region must be a'
                ' {0}D shape instance - however {1}D was found'.format(
                    values_coord.shape[1] - len(direction), 
                    coord_plot.shape[1]))
        
        closest = meshing.slice_closest(
            slice_coord=coord_plot[coord_plot_in_reg], direction=direction,
            values_coord=values_coord, tol=tol)

    def plot_fun(values, cut_position=None, ax=None, **kwargs):
        
        if len(values) != len(values_coord):
            raise ValueError(
                'Values do not have the same size as values_coord')
        if ax is None:
            ax = plt.gca()

        plot_values = np.zeros(len(coord_plot), dtype=float) * np.nan
        if cut_position is None:
            plot_values[coord_plot_in_reg] = values[closest]
        else:
            plot_values[coord_plot_in_reg] = values[closest(cut_position)]
        
        res = None
        if dim == 1:       
            ax.plot(coord_plot[:, 0], plot_values, **kwargs)
        elif dim == 2:
            
            if 'colors' in kwargs.keys():
                colors = kwargs.pop('colors')
            else:
                colors = None
            
            if 'cmap' in kwargs.keys():
                cmap = kwargs.pop('cmap')
            else:
                cmap = 'viridis'

            res = plot_imshow(
                slice_coord=coord_plot, values=plot_values, ax=ax, 
                colors=colors, cmap=cmap, **kwargs)
        else:
            raise RuntimeError(
                'Only 1D or 2D cuts allowed, however plotting dimension'
                + 'is {0}'.format(dim))

        if res is None:
            return ax
        else:            
            return ax, res
        
    return plot_fun
            

def plot_imshow(
    slice_coord, values, ax, cmap=None, colors=None, **kwargs):
    ''' Makes an imshow plot for 'slice_coord' and 'values'
    '''

    bbox = np.ravel(np.vstack(
        [np.min(slice_coord, axis=0), np.max(slice_coord, axis=0)]).T)    

    sorted_idx = meshing.sequencial_sort(
        slice_coord, order=[1, 0], return_indices=True)
    ncol = int(sum(
        slice_coord[sorted_idx, 1] == slice_coord[sorted_idx[0], 1]))
    nrow = int(len(slice_coord) / ncol)

    fv = values[sorted_idx].reshape(nrow, ncol)[::-1, :]
    
    if cmap is None:
        cmap = 'viridis'
    
    if colors is None:
        if isinstance(cmap, str):
            cmap = plt.cm.ScalarMappable(cmap=cmap).cmap
        elif not callable(cmap):
            raise ValueError('cmap must be callable or a string')

        cmap_nan_color = kwargs.get('cmap_nan_color', 'k')
        cmap_nan_alpha = kwargs.get('cmap_nan_alpha', 0.5)
        cmap.set_bad(color=cmap_nan_color, alpha=cmap_nan_alpha)

        img = ax.imshow(
            fv, extent=bbox, cmap=cmap, **kwargs)
    else:
        
        if 'cmap' in kwargs.keys():
            kwargs.pop('cmap')
        
        ## Set to white if it is np.nan
        for i in range(len(colors)):
            if np.all(
                colors[i] == np.zeros(4)):
                raise RuntimeError('White is default - can"t be set by user')
        
        colors_ = np.zeros((len(colors)+1, 4))
        colors_[:len(colors)] = colors
        
        fv_ = np.ones(fv.shape, dtype=int) * (len(colors_) - 1)
        fv_[~np.isnan(fv)] = fv[~np.isnan(fv)]

        img = ax.imshow(colors_[fv_], extent=bbox, **kwargs)

    return img


def plot_2d_finalized_mesh(
    mesh_inst, ax=None, x_lim=None, y_lim=None,
    plot_region=None, ratio=3, regions=None, c=None,  **kwargs):
    ''' Plots voronoi diagram for 2D finalized mesh.

    Parameters
    -----------
    mesh_inst: instance of Mesh()

    x_lim: sequence of 2 floats
        Selects the coordinates of the mesh along x

    y_lim: sequence of 2 floats
        Selects the coordinates of the mesh along y

    plot_region: instance of shapes.Shape
        Defines the region to be plotted

        !! Overwrites y_lim and x_lim !!

    ratio: integer
        The mesh point size is 'ratio' times larger
        than the vertex point.

    kwargs: passed to ax.plot()
        Notice, for the vertex points the ms is
        divided by 'ratio'.

    regions: instances of pescado.shape.Shape

    c: marker color(s)

    '''

    for param, val in zip(['ms', 'c'], [3, 'k']):
        if param not in kwargs.keys():
            kwargs.update({param:val})

    if ax is None:
        ax = plt.gca()

    if plot_region is None:
        if x_lim is None and y_lim is None:
            plot_region = mesh_inst._simulation_region
        else:
            plot_bbox = mesh_inst._simulation_region.bbox
            if x_lim is not None:
                plot_bbox[:, 0]
                plot_bbox[:, 0] = x_lim
            if y_lim is not None:
                plot_bbox[:, 1] = y_lim
            plot_region = mesher.shapes.Box(
                lower_left=plot_bbox[0, :],
                size=(plot_bbox[1, :] - plot_bbox[0, :]))

    idx = mesh_inst.inside(plot_region)
    coord = mesh_inst.coordinates(points=idx)

    if c is not None:
        if c is int:
            c = [c, ] * len(regions)

    for i, reg in enumerate(regions):
        if c is not None:
            kwargs['c'] = c[i]
        reg_coord = coord[reg(coord)]

        ax.plot(reg_coord[:, 0],
                reg_coord[:, 1], 'o', **kwargs)

    kwargs['ms'] = kwargs['ms'] / ratio
    kwargs.pop('c')

    (vertices, neighbours,
     ridge_vertices, ridges, 
     region_vertices, regions,
    point_ridges, point_neighbours) = mesh_inst._ridges_and_vertices(
        points=idx)

    for pt in range(len(point_neighbours) - 1):
        vert, vert_idx = ra.extract_elements(
            elements=point_ridges[point_neighbours[pt]:point_neighbours[pt+1]],
            indices=ridges, values=ridge_vertices)
        
        for r in range(len(vert_idx) - 1):
            rv = vertices[vert[vert_idx[r]:vert_idx[r+1]]]
            ax.plot(rv[:, 0], rv[:, 1], 'k.-', **kwargs)


def plot_map(val, coord, colormap=None, cbar_label=None):
    ''' Plots a 2D colormap with x, y axis from 'coord' and values from 'val'

    Parameters
    -----------

    val: np.ndarray of floats with shape (n, )

    coord: np.ndarray of floats with shape (n, 2)

    colormap: plt.cm.*

    cbar_label: str

    !!!!! THIS IS A TEMPORARY HELPER FUNCTION !!!!!
    !!!!! THIS WILL CHANGE !!!!!!!!!!!!!!!!!!!!!!!!

    '''
    normalize = mpl.colors.Normalize(
        vmin=-1 * np.abs(val).max(),
        vmax=1 * np.abs(val).max())
    if colormap is None:
        colormap = plt.cm.viridis_r
    
    scalarmappaple = plt.cm.ScalarMappable(
        norm=normalize, cmap=colormap)

    scalarmappaple.set_array(len(val))

    plt.scatter(coord[:, 0], coord[:, 1],
                c=colormap(normalize(val)), s=10)

    cbar = plt.colorbar(scalarmappaple)
    plt.xlabel('x(nm)')
    plt.ylabel('y(nm)')

    if cbar_label is not None:
        cbar.set_label(cbar_label)
