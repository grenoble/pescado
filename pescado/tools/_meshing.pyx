
# TODO: Clean up. simplify and remove unecessary functionality
# once the mesher.py and voronoi.py are done.
from cpython.mem cimport PyMem_Malloc, PyMem_Realloc, PyMem_Free

import numpy as np
cimport numpy as cnp


cdef (int *, int) c_repeated_values(
    double *array_1, double [:, :] array_2, float tol):
    ''' Returns a boolean map comparing array_1 to array_2

    Parameters
    -----------
    array_1: sequence of d doubles

    array_2: np.ndarray of doubles with shape(m, d)

    tol: float

    Returns
    --------
    repeated: list of booleans of size(m,)
        m: array_1.shape[0]
        n: array_2.shape[0]

    Notes
    ------
    if abs(array_1[:] - array_2[0, :]) < tol:
        repeated[0, 0] = True

    Can be made faster by using pure double array. But probably
    too much trouble for what's worth.
    '''

    cdef Py_ssize_t x_size_2 = array_2.shape[0]
    cdef Py_ssize_t y_size_2 = array_2.shape[1]

    cdef Py_ssize_t x_test, y_test, x_ref

    cdef int *repeated = <int *>PyMem_Malloc(x_size_2 * sizeof(int))
    cdef int num_repeated = 0

    cdef int equal
    cdef int component_equal
    cdef double diff

    for x_ref in range(x_size_2):
        equal = 1
        for y_test in range(y_size_2):

            diff = abs(array_1[y_test]
                        - array_2[x_ref, y_test])

            if diff < tol:
                component_equal = 1
            else:
                component_equal = 0

            equal *= component_equal

        repeated[x_ref] = equal
        num_repeated += equal

    return repeated, num_repeated



cdef (char, long long) is_inside_ordered(
    double *value, double [:, :] array_2,
    float tol, long *ordering):
    ''' Returns 1 if value inside array_2, 0 if not.

    Parameters
    -----------
    value: sequence of d doubles

    array_2: Ordered np.ndarray of doubles with shape(m, d)

    tol: float

    Returns
    --------
    boolean
    interger :  position at which the value was found.

    Notes
    ------
        Implements bisect searchsorted alghorithm.

        array_2 is ordered, d=0, then d=1, then d=2.
    '''

    cdef Py_ssize_t y_len = array_2.shape[1]
    cdef Py_ssize_t y

    cdef long long pos
    cdef long long min_int = 0
    cdef long long max_int = array_2.shape[0]

    cdef char component_equal
    cdef char not_equal = 1

    cdef double diff

    while not_equal:

        pos = (min_int + max_int) // 2

        for y in range(y_len):

            diff = (value[ordering[y]]
                    - array_2[pos, ordering[y]])

            if abs(diff) < tol:
                component_equal = 1
            else:
                component_equal = 0

            if component_equal == 0:
                break

        not_equal -= component_equal

        if (max_int - min_int) == 1:

            if component_equal == 0:
                pos = -1

            not_equal = 0

        if diff > tol:
            min_int = pos
        else:
            max_int = pos

    return component_equal, pos


cdef (char, long long) is_inside(
    double *value, double [:, :] array_2, float tol):
    ''' Returns 1 if value inside array_2, 0 if not.

    Parameters
    -----------
    value: sequence of d doubles

    array_2: np.ndarray of doubles with shape(m, d)

    tol: float

    Returns
    --------
    boolean
    interger :  position at which the value was found.
    '''

    cdef Py_ssize_t x_size_2 = array_2.shape[0]
    cdef Py_ssize_t y_size_2 = array_2.shape[1]

    cdef Py_ssize_t y_test, x_ref

    cdef char equal
    cdef long long pos
    cdef char component_equal
    cdef double diff

    for x_ref in range(x_size_2):
        equal = 1
        for y_test in range(y_size_2):

            diff = abs(value[y_test]
                       - array_2[x_ref, y_test])

            if diff < tol:
                component_equal = 1
            else:
                component_equal = 0

            equal *= component_equal

        if equal == 1:
            break

    pos = x_ref
    return equal, pos


cdef long long * c_where(
    double [:, :] array_1, double [:, :] array_2,
    float tol, long *ordering):
    ''' Returns the indices where the values in array_1 are
    found in array2.

    Parameters
    -----------
    array_1: F-contiguous np.ndarray of floats with shape(m, d)

    array_2: F-contiguous np.ndarray of floats with shape(n d)

    tol: float

    Returns
    --------
    repeated_position: list of booleans of int(m,)

    '''

    cdef Py_ssize_t test_x_size = array_1.shape[0]
    cdef Py_ssize_t test_y_size = array_1.shape[1]

    cdef Py_ssize_t reference_x_size = array_2.shape[0]
    cdef Py_ssize_t reference_y_size = array_2.shape[1]

    cdef long long *repeated_position = <long long *>PyMem_Malloc(
        test_x_size * sizeof(long long))

    cdef Py_ssize_t x, y
    cdef double *current_val =  <double *>PyMem_Malloc(
        test_y_size * sizeof(double))

    cdef long long pos
    cdef char inside

    for x in range(test_x_size):

        for y in range(test_y_size):
            current_val[y] = array_1[x, y]

        if ordering[0] == -1:
            inside, pos = is_inside(
                current_val, array_2, tol=tol)
        else:
            inside, pos = is_inside_ordered(
                current_val, array_2, tol=tol,
                ordering=ordering)

        if inside == 1:
            repeated_position[x] = pos
        elif inside == 0:
            repeated_position[x] = -1

    PyMem_Free(current_val)
    return repeated_position


cpdef where(test_array, reference_array, tol=1e-15, ordering=[-1, ]):
    ''' Returns the indices where the values in test_array
    are found in reference_array.

    Parameters:
    ------------

    test_array: np.ndarray with shape(m, d)

    reference_array: np.ndarray with shape(n, d)

    tol: float with deffault 1e-15

    ordering: sequence of int with default to None
        If the reference array is ordered, it is the sense of "ordering" :

            e.g. if the array is ordered first in d = 2,
            then d = 1 and finally d = 0;

                then ordering  = [2, 1, 0]

    Returns:
    --------

    repeated_position: np.ndarray of integers with shape(m, )

        Position in reference_array where test_array[repeated_position]
        are found.
        If the value is -1, it means the value is not found in reference_array.

    calls function c_where
    '''

    arrays = [test_array, reference_array]
    for i, array in enumerate(arrays):
        assert (np.issubdtype(array.dtype, np.floating)
                or np.issubdtype(array.dtype, np.integer)), (
            '{0}.dtype not a np.interger or np.float'.format(array))
        arrays[i] = np.asfortranarray(array).astype(float)

    cdef Py_ssize_t test_size = test_array.shape[0]
    cdef Py_ssize_t ref_size = reference_array.shape[0]

    cdef long long *c_repeated_position = <long long *>PyMem_Malloc(
        test_size * sizeof(long long))

    cdef long *c_ordering = <long *> PyMem_Malloc(
        len(ordering) * sizeof(long))
    cdef Py_ssize_t y
    for y in range(len(ordering)):
        c_ordering[y] = ordering[y]

    c_repeated_position = c_where(
        array_1=arrays[0], array_2=arrays[1],
        tol=tol, ordering=c_ordering)

    cdef cnp.ndarray[cnp.int64_t, ndim=1] repeated_map = np.empty(
        (test_size, ), dtype=np.int64)

    cdef Py_ssize_t x
    for x in range(test_size):
        repeated_map[x] = c_repeated_position[x]

    PyMem_Free(c_repeated_position)

    return repeated_map


cdef c_unique(
    double[::1, :] array, float tol, char return_idx):
    ''' Returns array of unique values

    Parameters
    -----------
    array_1: F-contiguous np.ndarray of floats

    tol: float

    return_idx: char
        If 1 the returned array is the indices of the first elements
        in 'array' equal to the unique values in 'array'

    Returns
    --------
    np.ndarray of booleans of size(m, d)
        with m unique values

    or if return_idx is 1

    np.ndarray of int of size(m, )
        with m unique values

    Note
    ----
    Can be made faster by removing unique_val as numpy array
    and using pure memory allocation.
    '''

    cdef Py_ssize_t x_size = array.shape[0]
    cdef Py_ssize_t y_size = array.shape[1]

    cdef cnp.ndarray[cnp.float64_t, ndim=2] unique_val = np.empty(
        (x_size, y_size), dtype=np.float64)

    cdef cnp.ndarray[cnp.int_t, ndim=1] unique_idx = np.empty(
        (x_size, ), dtype=int)

    cdef double *current_val =  <double *>PyMem_Malloc(
        y_size * sizeof(double))

    cdef Py_ssize_t x, y
    cdef Py_ssize_t num_unique_values = 0

    cdef char repeated
    cdef long long pos

    # x = 0
    for y in range(y_size):
        unique_val[num_unique_values, y] =  array[0, y]
    unique_idx[num_unique_values] = 0
    num_unique_values += 1

    for x in range(1, x_size):

        for y in range(y_size):
            current_val[y] = array[x, y]

        repeated, pos = is_inside(
            current_val, unique_val[:num_unique_values, :],
            tol=tol)

        if repeated == 0:
            for y in range(y_size):
                unique_val[num_unique_values, y] =  current_val[y]
            unique_idx[num_unique_values] = x
            num_unique_values += 1

    PyMem_Free(current_val)

    if return_idx == 1:
        return unique_idx[:num_unique_values]
    else:
        return unique_val[:num_unique_values]


cpdef unique(array, tol=1e-15, return_idx=False):
    ''' Returns array of unique values

    Parameters
    -----------
    array_1: np.ndarray

    tol: float with default 1e-15

    return_idx: boolean
        If True the returned array is the indices of the first elements
        in 'array' equal to the unique values in 'array'

    Returns
    --------
    np.ndarray of booleans of size(m, d)
        with m unique values

    or if return_idx True

    np.ndarray of integers of size(m, )
        with m unique values

    Note
    -----
    Calls cdef function cunique.
    '''

    array = np.asfortranarray(array).astype(float)

    cdef char rtrn_idx = 0

    if return_idx:
        rtrn_idx = 1

    return c_unique(array=array, tol=tol, return_idx=rtrn_idx)


cdef (int *, int *) c_list_repeated_values(
     double [::1, :] test_array,
     double [::1, :] reference_array,
     float tol):
    ''' Returns a map of booleans showing where / if a
    element in test_array matches one in reference_array.

    Parameters
    -----------
    array_1: F-contiguous np.ndarray of floats with shape(m, d)

    array_2: F-contiguous np.ndarray of floats with shape(n d)

    tol: float

    Returns
    -------
    repeated_list: int * with size m * n

        for ref_ele in reference_array[repeated_list[0:m]]:
            assert test_array[0] == ref_ele

    num_repeated_list: int with size m
        number of times an element inside test_array
        was found in reference_array
    '''
    cdef Py_ssize_t test_x_size = test_array.shape[0]
    cdef Py_ssize_t test_y_size = test_array.shape[1]

    cdef Py_ssize_t reference_x_size = reference_array.shape[0]
    cdef Py_ssize_t reference_y_size = reference_array.shape[1]

    cdef int *repeated_list = <int *>PyMem_Malloc(
        test_x_size * reference_x_size * sizeof(int))
    cdef int *num_repeated_list = <int *>PyMem_Malloc(
        test_x_size * sizeof(int))

    cdef Py_ssize_t x_test, y_test, x_ref
    cdef int *repeated
    cdef int num_repeated
    cdef double *current_value = <double *>PyMem_Malloc(
        test_y_size * sizeof(double))

    for x_test in range(test_x_size):

        for y_test in range(test_y_size):
            current_value[y_test] = test_array[x_test, y_test]

        repeated, num_repeated = c_repeated_values(
            array_1=current_value, array_2=reference_array, tol=tol)

        num_repeated_list[x_test] = num_repeated
        for x_ref in range(reference_x_size):
            repeated_list[x_test * reference_x_size + x_ref] = repeated[x_ref]

        PyMem_Free(repeated)

    return repeated_list, num_repeated_list


cpdef remove(array_1, array_2, tol=1e-15):
    ''' Return an array with the elements in array_1 not inside array_2

    Parameters
    -----------
    array_1: np.ndarray with shape(m, d)

    array_2: np.ndarray with shape(n, d)

    tol: float with default 1e-15

    Returns
    -------
    np.ndarray of floats with shape(e, d)
        with e the number of elements in array_1 and not in array_2
        elements of array_1 not in array_2

    np.ndarray of ints with shape(e, )
        indices of the elements in array_1 not in array_2

    Calls function c_remove.
    TODO: change output if it is 0, 0
    '''

    arrays = [array_1, array_2]
    for i, array in enumerate(arrays):
        assert (np.issubdtype(array.dtype, np.floating)
                or np.issubdtype(array.dtype, np.integer)), (
            '{0}.dtype not a np.interger or np.float'.format(array))
        arrays[i] = np.asfortranarray(array).astype(float)

    return c_remove(array_1=arrays[0], array_2=arrays[1], tol=tol)


cpdef c_remove(
     double [::1, :] array_1,
     double [::1, :] array_2,
     float tol):
    ''' Return an array with the elements in array_1 not inside array_2

    Parameters
    -----------
    array_1: F-contiguous np.ndarray of floats with shape(m, d)

    array_2: F-contiguous np.ndarray of floats with shape(n, d)

    tol: float

    Returns
    -------
    np.ndarray of floats with shape(e, d)
        with e the number of elements in array_1 and not in array_2
        elements of array_1 not in array_2

    np.ndarray of ints with shape(e, )
        indices of the elements in array_1 not in array_2
    '''
    cdef Py_ssize_t x_size_1 = array_1.shape[0]
    cdef Py_ssize_t y_size_1 = array_1.shape[1]

    cdef Py_ssize_t y_size_2 = array_2.shape[1]

    if y_size_1 != y_size_2:
        raise ValueError(
            'array_1 is {0}D while array_2 is {1}D'.format(
                y_size_1, y_size_2))

    cdef int *repeated_list
    cdef int *num_repeated_list
    cdef Py_ssize_t x_test, new_array_x, y_test

    repeated_list, num_repeated_list = c_list_repeated_values(
        test_array=array_1, reference_array=array_2, tol=tol)

    cdef int new_array_1_size = 0
    cdef int *indices = <int *>PyMem_Malloc(x_size_1 * sizeof(int))

    for x_test in range(x_size_1):
        if num_repeated_list[x_test] == 0:
            indices[new_array_1_size] = x_test
            new_array_1_size +=1

    # WILL STOP HERE IF EMPTY ARRAY IS TO BE RETURNED
    #TODO: Find more elegant way of doing this.
    if new_array_1_size == 0:

        PyMem_Free(indices)
        PyMem_Free(repeated_list)
        PyMem_Free(num_repeated_list)

        return 0, 0

    cdef cnp.ndarray[cnp.float64_t, ndim=2] unique = np.empty(
        (new_array_1_size, y_size_1), dtype=np.float64)

    cdef cnp.ndarray[cnp.int_t, ndim=1] indices_array_1 = np.empty(
        (new_array_1_size), dtype=int)

    for new_array_x in range(new_array_1_size):

        indices_array_1[new_array_x] = indices[new_array_x]

        for y_test in range(y_size_1):
            unique[new_array_x, y_test] = array_1[
                indices[new_array_x], y_test]

    PyMem_Free(indices)
    PyMem_Free(repeated_list)
    PyMem_Free(num_repeated_list)

    return unique, indices_array_1


cpdef repeated_values(array_1, array_2, tol=1e-15):
    ''' Returns a map of booleans showing where / if a
        element in 'array_1' matches one in 'array_2'.

    Parameters
    -----------
    array_1: np.ndarray with shape(m, d)

    array_2: np.ndarray with shape(n, d)

    tol: float with default 1e-15

    Returns
    -------
    repeated_map: np.ndarray of floats with shape(m, n)

        for ref_ele in array_2[repeated_list[0, :]]:
            assert array_1[0] == ref_ele

    num_repeated_list: Sequence of m intergers
        number of times an element inside array_1
        was found in reference_array

    Calls function c_np_repeated_values.
    '''

    arrays = [array_1, array_2]
    for i, array in enumerate(arrays):
        assert (np.issubdtype(array.dtype, np.floating)
                or np.issubdtype(array.dtype, np.integer)), (
            '{0}.dtype not a np.interger or np.float'.format(array))
        arrays[i] = np.asfortranarray(array).astype(float)

    return c_np_repeated_values(
        test_array=arrays[0],
        reference_array=arrays[1], tol=tol)


cpdef c_np_repeated_values(
     double [::1, :] test_array,
     double [::1, :] reference_array,
     float tol):
    ''' Returns a map of booleans showing where / if a
    element in test_array matches one in reference_array.

    Parameters
    -----------
    test_array: F-contiguous np.ndarray of floats with shape(m, d)

    reference_array: F-contiguous np.ndarray of floats with shape(n d)

    tol: float

    Returns
    -------
    repeated_map: np.ndarray with shape(m, n)

        for ref_ele in reference_array[repeated_list[0, :]]:
            assert test_array[0] == ref_ele

    num_repeated_list: Sequence of m intergers
        number of times an element inside test_array
        was found in reference_array
    '''

    cdef Py_ssize_t test_x_size = test_array.shape[0]
    cdef Py_ssize_t test_y_size = test_array.shape[1]

    cdef Py_ssize_t reference_x_size = reference_array.shape[0]
    cdef Py_ssize_t reference_y_size = reference_array.shape[1]

    if test_y_size != reference_y_size:
        raise ValueError(
            'test_array is {0}D while reference_array is {1}D'.format(
                test_y_size, reference_y_size))

    cdef cnp.ndarray[cnp.uint8_t, ndim=2] repeated_map = np.empty(
        (test_x_size, reference_x_size), dtype=np.uint8)

    cdef cnp.ndarray[cnp.int64_t, ndim=1] repeated_num = np.empty(
        (test_x_size), dtype=np.int64)

    cdef int *repeated_list
    cdef int *num_repeated_list
    cdef Py_ssize_t x_test, x_ref

    repeated_list, num_repeated_list = c_list_repeated_values(
        test_array=test_array, reference_array=reference_array, tol=tol)

    for x_test in range(test_x_size):

        repeated_num[x_test] = num_repeated_list[x_test]

        for x_ref in range(reference_x_size):
            repeated_map[x_test, x_ref] = repeated_list[
                x_test * reference_x_size + x_ref]

    PyMem_Free(repeated_list)
    PyMem_Free(num_repeated_list)

    return repeated_map, repeated_num
