import numpy as np

from pescado.tools.sparse_vector import SparseVector
#from pescado.self_consistent.problem import stupid_iteration

def iteration_error(
    instance_1, instance_2=None, ite1=None, ite2=None):
    ''' Calculates the absolute difference between 'ite1' of 'instance_1'
        and 'ite2' of 'instance_2'

    Parameters
    ----------

        instance_1: instance of self_consistent.naive_solver.NaiveSchrodingerPoisson
            or self_consistent.problem.SchrodingerPoisson

        instance_2: instance of self_consistent.naive_solver.NaiveSchrodingerPoisson
            or self_consistent.problem.SchrodingerPoisson.

            Defaults to "instace_1" if None

        ite1: integer, default None
            Defaults to the last iteration (-1)

        ite2: integer, default None
            Defaults to the 2nd last iteration (-2)

    Returns
    --------
        err_pot: pescado.SparseVector instance
            np.abs(potential_of_ite1 - potential_of_ite2)

        err_char: pescado.SparseVector instance
            np.abs(charge_of_ite1 - charge_of_ite2)
    '''

    if ite1 is None:
        ite1 = -1
    if ite2 is None:
        ite2 = -2

    if instance_2 is None:
        instance_2 = instance_1

    all_sites_1 = instance_1.sites_ildos.indices
    all_sites_2 = instance_2.sites_ildos.indices

    if len(np.setdiff1d(all_sites_1, all_sites_2)) == 0:
        all_sites = all_sites_1
    else:
        raise RuntimeError(
            'Self-consistent sites from "instace_1" do not match those of instance_2"')

    err_pot = SparseVector(
        values=np.zeros(len(all_sites)), indices=all_sites)

    err_char = SparseVector(
        values=np.zeros(len(all_sites)), indices=all_sites)

    for i, (ite, inst) in enumerate(
        zip([ite1, ite2], [instance_1, instance_2])):

        # Potential and charge from inst consistent last iteration
        sc_char = inst.charge(iteration=ite)
        sc_pot = inst.potential(iteration=ite)

        err_pot[all_sites]= err_pot[all_sites] + sc_pot[all_sites] * ((-1) ** (i + 1))
        err_char[all_sites] = err_char[all_sites] + sc_char[all_sites] * ((-1) ** (i + 1))

    err_char[all_sites] = np.abs(err_char[all_sites])
    err_pot[all_sites] = np.abs(err_pot[all_sites])

    return err_pot, err_char
