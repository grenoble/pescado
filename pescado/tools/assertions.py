import copy 

import numpy as np
import pescado


def custom_issubdtype(arg1, list):
    ''' For each element in 'list' checks arg1 dtype

    Parameters
    ----------
    arg1: numpy dtype
        
    list: list of numpy dtype

    Returns
    -------
    bool - True if arg1 dtype corresponds to any of those
    inside 'list' 

    '''

    passed = False
    for dtype_ in list:
        if np.issubdtype(arg1, dtype_):
            passed = True

    return passed


def assert_coord_convention(coord_array, name, max_dim=3):
    """Enforces the array coordinate convention

    Parameters
    ----------
    coord_array: list or np.array of np.interger or np.floating 
    with shape(m, d)
        m number of coordinates \n
        d dimension of coordinates

    name: str
        Name of the object

    max_dim: int, default 3
        Maximum dimension of the coordinates

    Returns
    -------
        None

    Note
    ----

    Raises AssertionError() in case the convention is not respected
    """
    assert isinstance(name, str)

    assert isinstance(coord_array, np.ndarray), (
        '{0} not a np.ndarray'.format(name))

    assert custom_issubdtype(
        coord_array.dtype, 
        (np.float128, np.float64, np.float32, np.float16, 
         np.int8, np.int16, np.int32, np.int64, 
         np.uint8, np.uint16, np.uint32, np.uint64)), (
        '{0}.dtype not a np.interger or np.float'.format(name))

    assert len(coord_array.shape) == 2, (
        '{0}.shape not (m, d)'.format(name))

    assert coord_array.shape[1] <= max_dim, (
        '{0}.shape[1] larger than {1}'.format(name, max_dim))


def assert_is_inside_output(is_inside_output, is_inside_input):
    """Verifies that the output of 'shapes.Shape._is_inside' respects convention

    Parameters
    ----------
        is_inside_output: np.array of booleans with shape(m, )
            Output of shapes.Shape._is_inside

        is_inside_input: np.array of np.interger or np.floating with shape(m, d)
            m number of coordinates \n
            d dimension of coordinates

    Returns:
        None

    Note
    ----

    Raises AssertionError() in case convention not respected
    """

    assert is_inside_output.shape[0] == is_inside_input.shape[0], (
        '_is_inside function object size is wrong'
    )

    assert (np.issubdtype(is_inside_output.dtype, np.bool_)), (
        '_is_inside must return a array of booleans, not {0}'.format(
            is_inside_output.dtype))


def assert_unique(array, tol=1e-14):
    ''' Asserts that array is unique up to tolerace

    Parameters
    -----------
    array: numpy.array of floats

    tol: float

    Raises:
    --------
    AssertionError : 'Array not unique'

    '''

    unique_array = pescado.tools._meshing.unique(
        array, tol=tol)

    assert np.all(unique_array.shape == array.shape), (
         'Array not unique')


def assert_not_unique(array, tol=1e-14):
    ''' Asserts that array is not unique up to tolerace

    Parameters
    -----------
    array: numpy.array of floats

    tol: float

    Raises:
    --------
    AssertionError : 'Array unique'
    '''

    unique_array = pescado.tools._meshing.unique(
        array, tol=tol)

    assert np.any(unique_array.shape != array.shape), (
         'Array unique')
