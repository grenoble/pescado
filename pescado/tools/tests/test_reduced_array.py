''' Tests reduced array
'''
import numpy as np
from numpy.testing import assert_array_equal

from pescado.tools import reduced_array as ra
from pescado.tools import meshing
from pescado import mesher


def test_zip():
    ''' Tests zip
    '''
    # Test 1D arrays
    test = np.repeat(np.arange(0, 20, dtype=int), repeats=5)
    unq_test, test_idx = ra.zip(values=test)

    assert_array_equal(unq_test[test_idx], test)

    test = np.repeat(np.linspace(0, 20, 30), repeats=2)
    unq_test, test_idx = ra.zip(values=test)

    assert_array_equal(unq_test[test_idx], test)

    # Generate typical data with pescado voronoi
    bbox_2d = np.array([[-2., -1],
                 [10., 1]])
    grid_pts_2d = meshing.grid(bbox=bbox_2d, step=[0.3, 1])

    (points_2d, vertices_2d,
     ridge_points_2d, ridge_vertices_2d,
     ridges_2d, region_vertices_2d, region_2d) = mesher.voronoi_diagram_qhull(
     points=grid_pts_2d)
    
    vertices_rep = vertices_2d[ridge_vertices_2d]
    unq_val, val_idx, kdtree = ra.zip(values=vertices_rep, return_kdtree=True)

    ref_idx = meshing.kd_where(vertices_2d, kdtree=kdtree)[0]
    assert_array_equal(unq_val[ref_idx], vertices_2d)

    assert_array_equal(vertices_2d[ridge_vertices_2d], unq_val[val_idx])
    
    print('Passed zip test')    

def test_extract_elements_and_values():
    ''' Tests:
        
        reduced_array.extract_elements()
        reduced_array.element_values()
    '''
    
    values_1, indices_1 = ra.make_rand(
        low=0, high=50, size=200, cnts=[3, 4, 5])

    t1, t2 = len(indices_1) // 4, 3 * (len(indices_1) // 4)

    elements = [
        np.arange(0, len(indices_1) - 1, 3),
        np.arange(t1, t2, dtype=int)]
        
    for ele in elements:

        v1, i1 = ra.extract_elements(
            elements=ele, values=values_1, indices=indices_1)
               
        for ee, e in enumerate(ele):
            assert_array_equal(
                values_1[indices_1[e]:indices_1[e+1]],
                v1[i1[ee]:i1[ee + 1]])

        v1e = ra.elements_value(
            elements=ele, values=values_1, indices=indices_1)

        assert_array_equal(v1, v1e)

        np.random.shuffle(ele)

        v1, i1 = ra.extract_elements(
        elements=ele, values=values_1, indices=indices_1)

        for ee, e in enumerate(ele):
            assert_array_equal(
                values_1[indices_1[e]:indices_1[e+1]], 
                v1[i1[ee]:i1[ee + 1]])

        v1e = ra.elements_value(
            elements=ele, values=values_1, indices=indices_1)
        assert_array_equal(v1, v1e)


    print('Passed extract_elements test')
    print('Passed elements_value test')


def test_shuffle():
    '''  Test shuffle
    '''


    for indices in ([0, 2, 6, 8], [0, 2, 5, 8]):

        indices = np.array(indices)
        position = np.array([2, 0, 1])
        values = np.array([7, 8, 19, 11, 12, 87, 76, 65])

        sv, si, sindex = ra.shuffle(
            indices=indices, values=values, 
            positions=position, return_idx=True)
        
        for av, ap in zip(np.arange(len(indices) -1), position):
            assert_array_equal(
                values[indices[ap]:indices[ap+1]],
                sv[si[av]:si[av+1]])

    values_1, indices_1 = ra.make_rand(
        low=0, high=30, size=80, cnts=[3, 4, 5])
    
    position = np.arange(len(indices_1) -1, dtype=int)
    a = np.array([2, 4, 5, 8, 12])
    b = np.array([3, 1, 0, 10, 11])
    position[a] = b
    position[b] = a

    sv, si, sindex = ra.shuffle(
        indices=indices_1, values=values_1, 
        positions=position, return_idx=True)
    
    for av, ap in zip(np.arange(len(indices_1) -1), position):
        assert_array_equal(
            values_1[indices_1[ap]:indices_1[ap+1]],
            sv[si[av]:si[av+1]])

    print('Passed shuffle test')


def test_concatenate():
    ''' Test concatenate
    '''

    ########### No common values between ra
    val_1 = np.random.random_sample((50, 3)) * np.array([5, 5, 5])
    
    values_1, indices_1 = ra.make_rand(
        low=0, high=len(val_1), size=300, cnts=[3, 4, 5])

    val_2 = (
        np.random.random_sample((40, 3)) 
        * np.array([5, 5, 5]) + np.array([6, 6, 6]))

    values_2, indices_2 = ra.make_rand(
        low=0, high=len(val_2), size=100, cnts=[2, 3, 4])
   
    # Concatenate them
    values, indices = ra.concatenate(
        [(values_1, indices_1), (values_2, indices_2), (values_1, indices_1)])

    # Test it 
    testv, testi = ra.extract_elements(
        elements=np.arange(
            len(indices_1) // 2, 
            len(indices_1) + len(indices_2) // 2, dtype=int),
        values=values, indices=indices)

    refv1, refi1 = ra.extract_elements(elements=np.arange(
            len(indices_1) // 2, len(indices_1) - 1, dtype=int),
        values=values_1, indices=indices_1)
    
    assert_array_equal(testi[:len(indices_1) // 2], refi1)
    assert_array_equal(refv1, testv[:len(refv1)])

    refv2, refi2 = ra.extract_elements(
        elements=np.arange(
            0, len(indices_2) // 2 + 1, dtype=int),
        values=values_2, indices=indices_2)

    assert_array_equal(refv2, testv[len(refv1):])    
    assert_array_equal(
        testi[len(refi1) -1:] - testi[len(refi1) - 1], refi2)

    print('Passed concatenate test')

    # Common values between ra
    val_3 = np.concatenate((val_1[10:30], val_2[10:30]))
    values_3, indices_3 = ra.make_rand(
        low=0, high=len(val_2), size=100, cnts=[2, 3, 4])


test_concatenate()
test_extract_elements_and_values()
test_shuffle()
test_zip()