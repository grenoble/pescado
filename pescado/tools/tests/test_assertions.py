import itertools
import numpy as np

from pescado.tools import assert_coord_convention
from pescado.tools import assert_is_inside_output
from pytest import raises


def test_assert_coord_convention():

    # Check if all float, int and uint types inside numpy are accepted
    for allowed_type in ['float', 'int', 'uint']:
        for dtype in np.sctypes[allowed_type]:
            assert_coord_convention(
                np.ones((10, 2), dtype=dtype),
                name=dtype.__name__)

    for not_allowed_type in ['complex', 'others']:
        for dtype in np.sctypes[not_allowed_type]:
            with raises(
                AssertionError,
                match=('{0}.dtype'.format(dtype.__name__)
                       + ' not a np.interger or np.float')):

                assert_coord_convention(
                    np.ones((10, 2), dtype=dtype),
                    name=dtype.__name__)


def test_assert_is_inside_output():

    all_np_types = list(
        itertools.chain.from_iterable(
            [np.sctypes[key] for key in np.sctypes.keys()]))
    m = 10
    d = 3

    for dtype in all_np_types:
        is_inside_output = np.ones((m, ), dtype=dtype)
        is_inside_input = np.ones((m, d), dtype=float)

        if dtype is bool:
            assert_is_inside_output(
                is_inside_output=is_inside_output,
                is_inside_input=is_inside_input)
        else:
            with raises(
                AssertionError,
                match=('_is_inside must return a array of booleans, not '
                       + '{0}'.format(is_inside_output.dtype))):

                assert_is_inside_output(
                    is_inside_output=is_inside_output,
                    is_inside_input=is_inside_input)
