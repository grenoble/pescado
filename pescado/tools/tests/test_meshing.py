import copy

import numpy as np

from numpy.testing import (
    assert_array_almost_equal, assert_array_equal, assert_allclose)

from pescado.tools import meshing, _meshing, reduced_array
from pescado.mesher import patterns, shapes
from pescado import mesher
from pytest import raises

def test_sequential_against_grid():
    ''' Test if the ordering in grid is correct.
    '''

    bbox = np.array([[-10, -20, -15],
                     [20, 30, 25]])

    grid_indices = [np.arange(bbox[0, d], bbox[1, d] + 1)
                    for d in range(bbox.shape[1])]

    grid = np.dstack(
        [axis_ind.flatten()
        for axis_ind in np.meshgrid(*grid_indices)])[0]

    ordered_grid = meshing.sequencial_sort(
        array=grid, order=[2, 1, 0])

    assert_array_equal(
        ordered_grid,
        meshing.grid(bbox, step=[1, ] * 3, center=[0, ] * 3),
        err_msg=('Sequential_sort did not pass test'
                 + ' compairing it to grid()'))

    # Check return_indices as True
    ordered_idx = meshing.sequencial_sort(
        array=grid, order=[2, 1, 0], return_indices=True)

    assert_array_equal(
        grid[ordered_idx],
        meshing.grid(bbox, step=[1, ] * 3, center=[0, ] * 3),
        err_msg=(
            'squential_sort failed return_indices test '
            + 'when comparing it to grid()'))

def test_sequential():

    bbox = np.array([[-2., -2., -1.5],
                     [2., 3., 2.5]])

    grid_indices = [np.arange(bbox[0, d], bbox[1, d] + 1)
                    for d in range(bbox.shape[1])]

    grid = np.dstack(
        [axis_ind.flatten()
        for axis_ind in np.meshgrid(*grid_indices)])[0]

    # Scramble the array
    seed = 50
    np.random.seed(seed)

    scrambled_grid = copy.deepcopy(grid)
    np.random.shuffle(scrambled_grid)

    # Assert that is scrambled
    with raises(AssertionError):
        assert_array_equal(grid, scrambled_grid)

    t, tt = _meshing.remove(scrambled_grid, grid)

    # Assert that it is notheless equal
    assert t == 0, ('Scrambled grid different from original')

    sequentially_sorted_grid = meshing.sequencial_sort(
        scrambled_grid, order=[1, 0, 2])

    assert_array_equal(
        grid,
        sequentially_sorted_grid,
        err_msg='Sequential sort did not pass 3D test')

    # Test precision in sequential_sorting
    precision = 14
    error = 1e-15
    precision_sequential_test(
        precision=precision, error=error)

    # Check return_indices as True
    sequentially_sorted_idx = meshing.sequencial_sort(
        scrambled_grid, order=[1, 0, 2], return_indices=True)

    assert_array_equal(
        sequentially_sorted_grid,
        scrambled_grid[sequentially_sorted_idx],
        err_msg='squential_sort failed return_indices test')

def precision_sequential_test(precision, error):
    ''' Tests the precision in sequential_sorting

    Parameters
    -----------
    precision: interger
        Decimal value

    error: float
        The magnitude of the random error

    Raises:
    -------
        AssertionError
    '''
    bbox_2d = np.array([[-2., -1],
                        [10., 1]])

    grid_pts_2d = np.around(
        meshing.grid(bbox=bbox_2d, step=[0.3, 1]),
        decimals=precision)

    random_error = np.random.rand(len(grid_pts_2d), 2) * error

    not_ordered = np.around(
        meshing.sequencial_sort(
            grid_pts_2d + random_error, order=[1, 0], decimals=precision + 1),
        decimals=precision)

    ordered = np.around(
        meshing.sequencial_sort(
            grid_pts_2d + random_error, order=[1, 0], decimals=precision),
        decimals=precision)

    assert_array_equal(grid_pts_2d, ordered)

    with raises(AssertionError, match='Arrays are not equal'):
        assert_array_equal(grid_pts_2d, not_ordered)


def test_surface_shoelace():
    ''' Test surface_shoelace method in pescado.tools.meshing.

        Tests :
            i) rectangle shape
            ii) numerical precision rectangle shape
            iii) concave shape
            iv) negative surface test
            v) vectorized surface test
    '''

    vert_rect = np.array([[0, 0], [1, 0], [1, 1], [0, 1]])

    assert_array_almost_equal(
        meshing.surface_shoelace(vertices=vert_rect), [1, ],
        err_msg=('surface_shoelace failed simple rectangle shape test'))

    vert_rect = np.array([[0, 0], [1, 0], [1, 1], [0, 1]]) * 1.937495

    assert_array_almost_equal(
        meshing.surface_shoelace(vertices=vert_rect), [1.937495 * 1.937495, ],
        err_msg=(
            'surface_shoelace failed numerical precision rectangle shape test'))

    vertices_2d_conc = np.array(
        [[0, 0], [2, 0], [2, 1],
        [1.2, 1], [1.2, 2],
        [1.5, 2], [1.5, 5], [1.2, 5], [1.2, 3],
        [-1, 3], [-1, 2],
        [0.8, 2], [0.8, 1],
        [0, 1]])

    assert_array_almost_equal(
        meshing.surface_shoelace(vertices=vertices_2d_conc), [5.5, ],
        err_msg=('surface_shoelace failed concave shape test'))

    vertices_2d_conc_2 = np.array([
        [3, 4], [5, 11], [12, 8], [9, 5], [5, 6]])

    assert_array_almost_equal(
        meshing.surface_shoelace(vertices=vertices_2d_conc_2), [30, ],
        err_msg=('surface_shoelace failed concave shape negative test'))

    ver_pol_list = [vert_rect, vertices_2d_conc, vertices_2d_conc_2]
    vertices_vect = np.concatenate(ver_pol_list)
    indtpr = np.insert(
        np.cumsum([len(verts) for verts in ver_pol_list]),
        obj=0, values=0)

    assert_array_almost_equal(
        meshing.surface_shoelace( vertices=vertices_vect, indtpr=indtpr),
        [1.937495 * 1.937495, 5.5, 30],
        err_msg=('surface_shoelace failed vectorized surface test'))


def test_surface_3D():
    ''' Test surface_3D method in pescado.tools.meshing.

        Tests :
            A) Same plane (z coordinate equal to 0)
                i) rectangle shape
                ii) numerical precision rectangle shape
                iii) concave shape
                iv) negative surface test
                v) vectorized surface test
            B) Different plane
                i) rectangle shape
                ii) concanve shape
                iii) concave + rectangle shape
    '''

    ############### A - same plane

    vert_rect = np.zeros((4, 3))
    vert_rect[:, :-1] = np.array([[0, 0], [1, 0], [1, 1], [0, 1]])

    vert_rect_p = np.zeros((4, 3))
    vert_rect_p[:, :-1] = vert_rect[:, :-1] * 1.937495

    vertices_3d_conc = np.zeros((14, 3))
    vertices_3d_conc[:, :-1] = np.array(
        [[0, 0], [2, 0], [2, 1],
        [1.2, 1], [1.2, 2],
        [1.5, 2], [1.5, 5], [1.2, 5], [1.2, 3],
        [-1, 3], [-1, 2],
        [0.8, 2], [0.8, 1],
        [0, 1]])

    vertices_3d_conc_2 = np.zeros((5, 3))
    vertices_3d_conc_2[:, :-1] = np.array([
        [3, 4], [5, 11], [12, 8], [9, 5], [5, 6]])

    assert_array_almost_equal(
        meshing.surface_3D(vertices=vert_rect), [1, ],
        err_msg=('surface_shoelace failed simple rectangle shape test'))

    assert_array_almost_equal(
        meshing.surface_3D(vertices=vert_rect_p), [1.937495 * 1.937495, ],
        err_msg=(
            'surface_shoelace failed numerical precision rectangle shape test'))

    assert_array_almost_equal(
        meshing.surface_3D(vertices=vertices_3d_conc), [5.5, ],
        err_msg=('surface_shoelace failed concave shape test'))

    assert_array_almost_equal(
        meshing.surface_3D(vertices=vertices_3d_conc_2), [30, ],
        err_msg=('surface_shoelace failed concave shape negative test'))

    ############### B

    vert_rect_3D = np.array([[0, 0, 0], [1.88, 0, 0],
                            [1.88, 5.33, 4.52], [0, 5.33, 4.52]])

    vol = 1.88 * np.sqrt(5.33 ** 2 + 4.52 ** 2)

    assert_array_almost_equal(
        meshing.surface_3D(vertices=vert_rect_3D), vol,
        err_msg=('Surface for rectangle with 3D coordinates'))

    vert_concav_3D = np.array(
        [[0, 0, 0], [1.88, 0, 0], [1.88, 5.33, 4.52],
        [1.88 / 2, 5.33 / 2, 4.52 / 2], [0, 5.33, 4.52]])

    assert_array_almost_equal(
        meshing.surface_3D(vertices=vert_concav_3D), vol * 3 / 4,
        err_msg=('Surface for concave 3D surface'))

    vert_concav_convex_3D = np.array(
        [[0, 0, 0], [1.88, 0, 0], [1.88, 5.33, 4.52],
        [1.88 / 2, 5.33 / 2, 4.52 / 2], [0, 5.33, 4.52],
        [-1.88, 5.33, 4.52], [-1.88, 0, 0]])

    assert_array_almost_equal(
        meshing.surface_3D(vertices=vert_concav_convex_3D), vol * 7 / 4,
        err_msg=('Surface for concave / convex 3D surface'))


def test_polygon_surface():
    ''' Test polygons_surface method for 2D and 3D coordinates

    OBS: Almost redundant with the the test for
    surface_3D and surface_shoelace.
    '''

    vert_rect = np.zeros((4, 3))
    vert_rect[:, :-1] = np.array([[0, 0], [1, 0], [1, 1], [0, 1]])

    vert_rect_p = np.zeros((4, 3))
    vert_rect_p[:, :-1] = vert_rect[:, :-1] * 1.937495

    vertices_3d_conc = np.zeros((14, 3))
    vertices_3d_conc[:, :-1] = np.array(
        [[0, 0], [2, 0], [2, 1],
        [1.2, 1], [1.2, 2],
        [1.5, 2], [1.5, 5], [1.2, 5], [1.2, 3],
        [-1, 3], [-1, 2],
        [0.8, 2], [0.8, 1],
        [0, 1]])

    vertices_3d_conc_2 = np.zeros((5, 3))
    vertices_3d_conc_2[:, :-1] = np.array([
        [3, 4], [5, 11], [12, 8], [9, 5], [5, 6]])

    vert_rect_3D = np.array([[0, 0, 0], [1.88, 0, 0],
                            [1.88, 5.33, 4.52], [0, 5.33, 4.52]])

    vol = 1.88 * np.sqrt(5.33 ** 2 + 4.52 ** 2)

    vert_concav_3D = np.array(
        [[0, 0, 0], [1.88, 0, 0], [1.88, 5.33, 4.52],
        [1.88 / 2, 5.33 / 2, 4.52 / 2], [0, 5.33, 4.52]])

    vert_concav_convex_3D = np.array(
        [[0, 0, 0], [1.88, 0, 0], [1.88, 5.33, 4.52],
        [1.88 / 2, 5.33 / 2, 4.52 / 2], [0, 5.33, 4.52],
        [-1.88, 5.33, 4.52], [-1.88, 0, 0]])

    ver_pol_list_3d = [
        vert_rect, vert_rect_p, vertices_3d_conc, vertices_3d_conc_2,
        vert_concav_3D, vert_rect_3D, vert_concav_convex_3D]

    ver_pol_list_2d = [
        vert_rect[:, :-1], vert_rect_p[:, :-1],
        vertices_3d_conc[:, :-1], vertices_3d_conc_2[:, :-1]]

    vertices_vect_3d = np.concatenate(ver_pol_list_3d)
    vertices_vect_2d = np.concatenate(ver_pol_list_2d)

    indtpr_3d = np.insert(
        np.cumsum([len(verts) for verts in ver_pol_list_3d]), obj=0, values=0)

    indtpr_2d = np.insert(
        np.cumsum([len(verts) for verts in ver_pol_list_2d]), obj=0, values=0)

    assert_array_almost_equal(
        meshing.polygons_surface(vertices=vertices_vect_3d, indtpr=indtpr_3d),
        [1, 1.937495 * 1.937495, 5.5, 30, vol * 3 / 4, vol, vol * 7 / 4],
        err_msg=('polygons_surface failed 3D surface test'))

    assert_array_almost_equal(
        meshing.polygons_surface(vertices=vertices_vect_2d, indtpr=indtpr_2d),
        [1, 1.937495 * 1.937495, 5.5, 30],
        err_msg=('polygons_surface failed 2D surface test'))

    vertices_vect_2d = np.concatenate([ver_pol_list_2d[1], ver_pol_list_2d[2]])

    indtpr_2d = np.insert(
        np.cumsum(
            [len(verts) for verts in [ver_pol_list_2d[1], ver_pol_list_2d[2]]]),
        obj=0, values=0)

    assert_array_almost_equal(
        meshing.polygons_surface(vertices=vertices_vect_2d, indtpr=indtpr_2d),
        [1.937495 * 1.937495, 5.5],
        err_msg=('polygons_surface failed 2 elements vectorized test'))


def test_polygons_volume():
    ''' Tests that the output of polygons_volume matches that of
    convex_hull_volume.

    '''
    seed = 50
    np.random.seed(seed)

    bbox_3d = np.array([[-2., -1, -10.3],
                        [10., 1, 5.21]])

    random_pts_3d = np.random.rand(25, bbox_3d.shape[1])

    coord_3D = (bbox_3d[0, :]
                + (random_pts_3d * (bbox_3d[1,:] - bbox_3d[0,:])))

    (points_3d, vertices_3d,
     ridge_points_3d, ridge_vertices_3d, ridges_3d,
     regions_3d, point_region_3d) = mesher.voronoi_diagram_qhull(
        points=coord_3D)

    bounded, unbounded = mesher.voronoi.region_type(
        region_vertices=regions_3d, regions=point_region_3d)

    bded_vertices, bded_pt_vert = reduced_array.extract_elements(
        elements=bounded, indices=point_region_3d, values=regions_3d)

    volumes = meshing.polygons_volume(
        vertices=vertices_3d[bded_vertices], indtpr=bded_pt_vert)

    vol_qhull = list()
    for i in range(len(bounded)):
        vert = vertices_3d[bded_vertices[
            bded_pt_vert[i]:bded_pt_vert[i+1]]]

        vol_qhull.append(meshing.convex_hull_volume(vertices=vert))

    assert_array_equal(volumes, vol_qhull, err_msg=(
        'polygons_volume does not give the same result as convex_hull_volume'))


def test_grid_unique_2D():
    grid_unique_test(dim=2)


def test_grid_unique_1D():
    grid_unique_test(dim=1)


def test_grid_unique_3D():
    grid_unique_test(dim=3)


def grid_unique_test(dim):
    ############## Generate grid
    step = (0.25, ) * dim

    bbox_min = [-2., ] * (dim - 1)
    bbox_max = [7., ] * (dim - 1)

    grid_1 = meshing.grid(
        bbox=np.array(
            [[-0., ] + bbox_min,
             [5., ] + bbox_max]), step=step)

    grid_2 = meshing.grid(
        bbox=np.array(
            [[4.,] + bbox_min,
             [7.,] + bbox_max]), step=step)

    grid_3 = meshing.grid(
        bbox=np.array(
            [[-2., ] + bbox_min,
             [0.,  ] + bbox_max]), step=step)

    bb = list()
    if dim == 2:
        bb = [-0.25, ]
    elif dim == 3:
        bb = [-0.25, 7.]
    grid_4 = meshing.grid(
        bbox=np.array(
            [[4.,] + bbox_min,
            [7.,] + bb]), step=step)

    bb = list()
    if dim == 2:
        bb = [4., ]
    elif dim == 3:
        bb = [4., -2.]
    grid_5 = meshing.grid(
        bbox=np.array(
            [[-2.,] + bb,
            [-0.25,] + bbox_max ]), step=step)

    grid_all = meshing.grid(
        bbox=np.array(
            [[-2.,] + bbox_min,
            [7., ] + bbox_max]),
        center=(0, ) * dim,  step=step)

    grid_all_rep = np.concatenate(
        [grid_1, grid_2, grid_3, grid_4, grid_5])

    if dim in [2, 3]:
        grid_sep = np.concatenate([grid_4, grid_5])
    else:
        grid_sep = np.concatenate([grid_5, grid_4])

    ############## Testing tools.meshing.unique

    err_msg = ('meshing.unique did not pass {0}D test'.format(dim))

    unique_grid_all_rep, unique_grid_all_rep_idx = meshing.unique(grid_all_rep)

    #sort it
    if dim == 1:
        order = [0, ]
    elif dim == 2:
        order = [1, 0]
    elif dim == 3:
        order = [2, 1, 0]
    else:
        raise NotImplementedError()

    sorted_unique_grid_all_rep = meshing.sequencial_sort(
        unique_grid_all_rep, order=order)
        
    if dim in [2, 3]:
        with raises(AssertionError):

            assert_array_equal(
                grid_all, unique_grid_all_rep, err_msg=err_msg)

        with raises(AssertionError):

            assert_array_equal(
                grid_all, grid_all_rep[unique_grid_all_rep_idx],
                err_msg=err_msg + 'return_indices = True')

    assert_array_equal(
        grid_all, sorted_unique_grid_all_rep, err_msg=err_msg)

    unique_grid_sep, unique_grid_sep_idx = meshing.unique(grid_sep)
    
    assert_array_equal(
        unique_grid_sep, grid_sep[unique_grid_sep_idx],
        err_msg= (err_msg
                  + 'result with return_indices = True not equal to False'))

    sorted_unique_grid_sep = meshing.sequencial_sort(
        unique_grid_sep, order=order)

    grid_sep = meshing.sequencial_sort(grid_sep, order=order)

    assert_array_equal(
        grid_sep, sorted_unique_grid_sep, err_msg=err_msg)


def internal_bd_test(pat, shape):
    ''' Tests if patterns.internal_boundary manages to capture the internal
    boundary of a given 'shape' for a given 'pat'

    Parameters
    -----------

    shape: instance of 'pescado.continuous.shapes.Shapes'

    pat: shape: instance of 'pescado.continuous.patterns.Pattern'

    Retunrs
    -------
        False if all points belong to the internal boudnary of shape
        True if it passed the test
    '''

    internal_tags = pat.inside(shape)
    neig_tag, neighbours, points_neighbour = pat.neighbours(
        points_tag=internal_tags, nth=1)
    neig_coord = pat(neig_tag)

    internal_coord = pat(internal_tags)

    internal_bd, idx = meshing.internal_boundary(
        internal_coord=internal_coord,
        neig_coord=neig_coord,
        neighbours=neighbours,
        points_neighbour=points_neighbour,
        shape=shape, return_idx=True)

    assert np.all(internal_bd == internal_coord[idx]), (
        'internal_boundary did not pass returin_idx test')

    inside_neig = np.arange(len(neig_coord))[shape(neig_coord)]
    pts, index = _meshing.remove(neig_coord[inside_neig], internal_bd)
    if pts is not 0:
        internal_fneig = neig_tag[inside_neig[index]]
        if len(internal_fneig.shape) == 1:
            internal_fneig = internal_fneig[None, :]
    else:
        internal_fneig = np.array([])

    if len(internal_fneig) == 0:
        return False
    else:
        fneig_of_internal_fneig = pat(
            pat.neighbours(points_tag=internal_fneig, nth=1)[0])

        sneig_tag, sneig, point_sneig = pat.neighbours(
                points_tag=internal_fneig, nth=2)

        # Check that all of them are inside
        assert np.all(shape(fneig_of_internal_fneig)), (
            'internal_bd did not pass internal first neighbour test')

        # Check that the second first neig of the internal first neig have
        # at least one neig outside
        for i in range(len(internal_fneig)):
            sneig_i = sneig_tag[sneig[point_sneig[i]:point_sneig[i+1]]]

            outside_sneig = ~shape(pat(sneig_i))

        assert np.any(outside_sneig), (
                'internal_bd did not pass second first neighbour test'
            )

        return True


def external_bd_test(pat, shape):
    ''' Tests if meshing.external_boundary manages to capture the external
    boundary of a given 'shape' for a given 'pat'

    Parameters
    -----------

    shape: instance of 'pescado.continuous.shapes.Shapes'

    pat: shape: instance of 'pescado.continuous.patterns.Pattern'

    '''

    internal_tags = pat.inside(shape)
    # Find real internal boundary
    neig_tag, neighbours, points_neighbour = pat.neighbours(
        points_tag=internal_tags, nth=1)
    neig_coord = pat(neig_tag)

    internal_bd, internal_idx = meshing.internal_boundary(
        internal_coord=pat(internal_tags),
        neig_coord=neig_coord,
        neighbours=neighbours,
        points_neighbour=points_neighbour,
        shape=shape, return_idx=True)

    #### First test, neig comparison
    internal_bdr_neig_tag = pat.neighbours(
        points_tag=internal_tags[internal_idx], nth=1)[0]
    fneigs_coord = pat(internal_bdr_neig_tag)

    external_bd, external_idx = meshing.external_boundary(
        internal_bdr_neig=fneigs_coord,
        shape=shape, return_idx=True)

    external_tags = internal_bdr_neig_tag[external_idx]
    # Check if the first neig neighbours inside the shape
    # form the internal boundary.
    external_tags_neig_coord, idx = meshing.unique(pat(
        pat.neighbours(points_tag=external_tags, nth=1)[0]))

    sorted_internal_bd = meshing.sequencial_sort(
        internal_bd, order=np.arange(external_bd.shape[1]))
    sorted_inside_external_bd_fneigs = meshing.sequencial_sort(
        external_tags_neig_coord[shape(external_tags_neig_coord)],
        order=np.arange(external_tags_neig_coord.shape[1]))

    assert_array_equal(
        sorted_internal_bd,
        sorted_inside_external_bd_fneigs,
        'patterns.external_boundary failed compairison with fneigs test')


def test_boundary_3D(plot=False):
    ''' Test the internal boundary in 3D using a rectangular pattern.
    The external boundary has been tested in 2D. The neighbour
    test in 3D is enough for the external boundary.

    TODO: Add test for random pattern
    '''

    # Pattern
    rect_pattern = patterns.Rectangular.constant(element_size=[0.5, ] * 3)

    # Shapes
    rect_0_A_small_2 = shapes.Box(
        lower_left=[-9.6, -9.6, -9.6], size=[4.2, 4.7, 4.7])
    rect_0_A_small = shapes.Box(
        lower_left=[-10.1, -10.1, -10.1], size=[4.7, 5.2, 5.2])

    ll = np.array([-10.51, -10.51, -10.51])
    size = np.array([5.2, 5.7, 5.7])
    rect_0_A = shapes.Box(lower_left=ll, size=size)

    sphere = shapes.Ellipsoid.hypersphere(center=(ll + (size / 2)), radius=1)

    rect_1 = rect_0_A - rect_0_A_small
    rect_2 = rect_0_A - rect_0_A_small_2
    rect_sphere = rect_0_A - sphere

    assert internal_bd_test(pat=rect_pattern, shape=rect_1) is False

    internal_bd_test(pat=rect_pattern, shape=rect_2)
    internal_bd_test(pat=rect_pattern, shape=rect_0_A)
    internal_bd_test(pat=rect_pattern, shape=rect_sphere)

    external_bd_test(pat=rect_pattern, shape=rect_1)
    external_bd_test(pat=rect_pattern, shape=rect_2)
    external_bd_test(pat=rect_pattern, shape=rect_0_A)
    external_bd_test(pat=rect_pattern, shape=rect_sphere)


def test_boundary_2D(plot=False):
    ''' Test the internal boundary in 3D using a rectangular pattern.
    The external boundary has been tested in 2D. The neighbour
    test in 3D is enough for the external boundary.

    TODO: Add test for random pattern
    '''

    # Pattern
    rect_pattern = patterns.Rectangular.constant(element_size=[0.5, ] * 2)

    # Shapes
    rect_0_A_small_2 = shapes.Box(
        lower_left=[-9.6, -9.6], size=[6.2, 6.7])
    rect_0_A_small = shapes.Box(
        lower_left=[-10.1, -10.1], size=[6.7, 7.2])

    ll = np.array([-10.51, -10.51])
    size =  np.array([7.2, 7.7])
    rect_0_A = shapes.Box(lower_left=ll, size=size)

    sphere = shapes.Ellipsoid.hypersphere(center=(ll + (size / 2)), radius=3)

    rect_1 = rect_0_A - rect_0_A_small
    rect_2 = rect_0_A - rect_0_A_small_2
    rect_sphere = rect_0_A - sphere

    assert internal_bd_test(pat=rect_pattern, shape=rect_1) is False
    internal_bd_test(pat=rect_pattern, shape=rect_2)
    internal_bd_test(pat=rect_pattern, shape=rect_0_A)
    internal_bd_test(pat=rect_pattern, shape=rect_sphere)

    external_bd_test(pat=rect_pattern, shape=rect_1)
    external_bd_test(pat=rect_pattern, shape=rect_2)
    external_bd_test(pat=rect_pattern, shape=rect_0_A)
    external_bd_test(pat=rect_pattern, shape=rect_sphere)


def test_safety_region():
    ''' Tests if the safety region captures the points
    that will change the shape of the Voronoi cell.
    '''

    seed = 132

    np.random.seed(seed)
    pts = np.random.rand(50, 2) * 1000
    pattern_finite = patterns.Finite.points(points=pts)

    def circle_pos(center, radius, n_pt=100):

        rad_sq = radius ** 2

        coord = np.empty((n_pt, 2))
        coord[:int(n_pt / 2), 1] = np.linspace(
            center[1] - radius, center[1] + radius, int(n_pt / 2))
        coord[int(n_pt / 2):, 1] = np.linspace(
            center[1] - radius, center[1] + radius, int(n_pt / 2))

        cosnt = np.sqrt(
            rad_sq - (coord[:int(n_pt / 2), 1] - center[1]) ** 2)

        coord[:, 0] = np.hstack(
            [center[0] + cosnt, center[0] - cosnt])

        return coord[(coord[:, 1] - center[1]) ** 2 <= rad_sq]

    # Tests if the points at the boundary of the Safety region
    # modify the voroinoi cell. They should not.
    pts_idx = pattern_finite.bounded_region
    coord = pattern_finite.coordinates[pts_idx]

    safety_radius_pat = pattern_finite.safety_radius(
        points_tag=pattern_finite.bounded_region) # Calls meshing.safety_radius

    for center, radius in zip(coord, safety_radius_pat):

        new_diag_points = meshing.unique(np.concatenate([
            pts, circle_pos(center=center, radius=radius, n_pt=40)]))[0]
        (points, vertices,
         ridge_points, ridge_vertices, ridges,
         region_vertices, regions) = mesher.voronoi.voronoi_diagram(
            points=new_diag_points)
        
        pt = meshing.kd_where(center, points)[0][0]
        coord_test = points[pt]
        
        assert_allclose(
            center, coord_test,
            err_msg='SR test performed incorrectly. The center point of'
            + 'the reference and test voronoi cell are not the same.')
        
        vert_test = vertices[region_vertices[regions[pt]:regions[pt+1]]]

        vor_ref = pattern_finite.voronoi(point_tag=
                                        pattern_finite.tags([center,]))

        cond = _meshing.remove(vor_ref.vertices, vert_test, tol=1e-10)

        assert cond == (0, 0),(
            'The vertices in the reference voronoi cell do not match'
            + ' that of the test cell.')

    print('Passed safety radius test')        
test_safety_region()