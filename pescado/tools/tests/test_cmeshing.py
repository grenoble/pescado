'''
    TODO: Clean up these tests
'''
import numpy as np
import pescado

from numpy.testing import assert_array_equal
from pytest import raises

def test_remove_unique_repeated_values():

    seed = 50
    np.random.seed(seed)

    array_1 = (np.array([0, 0, 0])[None, :]
            + np.random.rand(10, 3) * np.array([1, 2, .6])[None, :])

    array_2 = (np.array([1, 1, 2])[None, :]
            + np.random.rand(10, 3) * np.array([1, 2, .6])[None, :])

    array_3 = (np.array([.5, 2, 2])[None, :]
            + np.random.rand(10, 3) * np.array([1, 2, .6])[None, :])

    test_all = np.concatenate(
        [array_2, array_1, array_3])
    np.random.shuffle(test_all)

    test_2_3 = np.concatenate([array_2, array_3])

    ########################### Testing remove

    test_1, index = pescado.tools._meshing.remove(test_all, test_2_3)

    err_msg = 'mesher._mesher.remove did not pass basic 3D test'

    # Test the index
    assert np.all(test_1 == test_all[index]), err_msg
    # Test if it found all the values to be removed
    assert_array_equal(
        pescado.tools.meshing.sequencial_sort(test_1, order=[2, 1, 0]),
        pescado.tools.meshing.sequencial_sort(array_1, order=[2, 1, 0]),
        err_msg=err_msg)

    ########################### Testing repeated_value

    # No repeated value
    err_msg = 'mesher._mesher.repeated_values did not pass basic 3D test'
    repeated_map, repeated_num = pescado.tools._meshing.repeated_values(
        test_1, test_2_3)

    assert ~np.all(repeated_map), err_msg
    assert ~np.all(repeated_num), err_msg

    # Find all repeated values
    repeated_map, repeated_num = pescado.tools._meshing.repeated_values(
        test_all, test_1)

    # Test if they are correct
    index_array = np.repeat(
        np.arange(0, test_all.shape[0], 1)[:, None],
        repeats=repeated_map.shape[1], axis=1) [repeated_map.astype(bool)]

    not_repeated = np.setdiff1d(np.arange(len(repeated_num)), index_array)

    assert ~np.all(repeated_num[not_repeated]), err_msg
    assert np.all(repeated_num[index_array] == 1), err_msg
    assert np.all(test_all[index_array] - test_1 < 1e-14), err_msg

    ########################### Testing unique

    err_msg = 'mesher._mesher.unique did not pass basic 3D test'

    unique_test = pescado.tools._meshing.unique(test_all)

    unique_double_test = np.asfortranarray(np.concatenate([test_all, test_all]))

    verify_unique = pescado.tools._meshing.repeated_values(
        unique_test, unique_double_test)

    assert np.all(verify_unique[1] == 2), err_msg

    verify_unique = pescado.tools._meshing.repeated_values(
        unique_double_test, unique_test)

    assert np.all(verify_unique[1] == 1), err_msg

    ########################### Testing unique, index
    # Once unique has passed, verify that return_idx has the same result
    unique_idx = pescado.tools._meshing.unique(test_all, return_idx=True)
    assert_array_equal(
        unique_test, test_all[unique_idx],
        err_msg='Unique idx did not pass test')


def test_grid_remove_unique():

    ############## Generate grid
    dim = 2
    step = (0.25, ) * dim

    grid_1 = pescado.tools.meshing.grid(
        bbox=np.array([[-0., -2.],
                    [5., 7.]]),
        step=step)

    grid_2 = pescado.tools.meshing.grid(
        bbox=np.array([[4., -2.],
                    [7., 7.]]),
        step=step)

    grid_3 = pescado.tools.meshing.grid(
        bbox=np.array([[-2., -2.],
                    [0., 7.]]),
        step=step)

    grid_4 = pescado.tools.meshing.grid(
        bbox=np.array([[4., -2.],
                    [7., -0.25]]),
        step=step)

    grid_5 = pescado.tools.meshing.grid(
        bbox=np.array([[-2., 4.],
                    [-0.25, 7.]]),
        step=step)

    grid_all = pescado.tools.meshing.grid(
        bbox=np.array([[-2., -2.],
                    [7., 7.]]),
        center=(0, ) * dim,  step=step)

    grid_all_rep = np.concatenate(
        [grid_1, grid_2, grid_3, grid_4, grid_5])

    grid_sep = np.concatenate([grid_4, grid_5])

    ############## Testing _mesher.unique

    err_msg = ('mesher._mesher.unique or '
               + 'tools.meshing.grid did not pass basic 2D test')

    unique_grid_all_rep = pescado.tools._meshing.unique(grid_all_rep)

    #sort it
    sorted_unique_grid_all_rep = pescado.tools.meshing.sequencial_sort(
        unique_grid_all_rep, order=[1, 0])

    with raises(AssertionError):
        assert_array_equal(
            grid_all, unique_grid_all_rep, err_msg=err_msg)

    assert_array_equal(
        grid_all, sorted_unique_grid_all_rep, err_msg=err_msg)

    unique_grid_sep = pescado.tools._meshing.unique(grid_sep)

    assert_array_equal(grid_sep, unique_grid_sep, err_msg=err_msg)

    ############## Test remove
    grid_3_, ignore = pescado.tools._meshing.remove(
        np.concatenate([grid_2, grid_3]), grid_2)

    err_msg = ('mesher._mesher.remove or '
               + 'tools.meshing.grid did not pass basic 2D test')

    assert_array_equal(grid_3_, grid_3,
                    err_msg=err_msg)

    grid_4_removed, ignore = pescado.tools._meshing.remove(
        grid_4, grid_3)

    assert_array_equal(grid_4_removed, grid_4, err_msg=err_msg)


    grid_3_removed, ignore = pescado.tools._meshing.remove(
        grid_3, grid_4)

    assert_array_equal(grid_3_removed, grid_3, err_msg=err_msg)


def _test_where(grid_1, grid_2, tol=1e-15):
    ''' Tests the cython function where for grid1 and grid2
    '''

    idx = pescado.tools._meshing.where(grid_1, grid_2)

    idx_ordered = pescado.tools._meshing.where(
        grid_1, grid_2, ordering=np.arange(grid_2.shape[1])[::-1])

    if ~np.all(idx == idx_ordered):
        raise RuntimeError(
            'Cython where function changes when ordering is given')

    in_grid_2 =  np.arange(len(idx_ordered))[idx_ordered != -1]
    zero_diff = grid_1[in_grid_2] - grid_2[idx[in_grid_2]]

    if ~np.all(np.all(np.abs(zero_diff) < tol, axis=1)):
        raise RuntimeError('Cython where function failed 3D test')


def test_where():
    ''' Tests cython where function

        Generates two 1D, 2D, 3D grids. Sends them to the where
        function and verifies that its result is correct.

    '''

    ## 3D mesh

    tol=1e-15

    grid_1 = pescado.tools.meshing.grid(
        bbox=np.array([[-2.5, -2, -3],[2.5, 2, 3]]), step=[0.5, 0.5, 0.5])

    grid_2 =  pescado.tools.meshing.grid(
        bbox=np.array([[-2, -1, -1.5],[2, 1, 1.5]]), step=[0.3, 0.3, 0.3])

    _test_where(grid_1=grid_1, grid_2=grid_2)

    ## 2D mesh
    grid_1 = pescado.tools.meshing.grid(
        bbox=np.array([[-2.5, -2],[2.5, 2]]), step=[0.5, 0.5])

    grid_2 =  pescado.tools.meshing.grid(
        bbox=np.array([[-2, -1],[2, 1]]), step=[0.3, 0.3])

    _test_where(grid_1=grid_1, grid_2=grid_2)

    ## 1D mesh
    grid_1 = pescado.tools.meshing.grid(
        bbox=np.array([[-2.5,],[2.5,]]), step=[0.5,])

    grid_2 =  pescado.tools.meshing.grid(
        bbox=np.array([[-2,],[2,]]), step=[0.3,])

    _test_where(grid_1=grid_1, grid_2=grid_2)

    ## Manual reference test.

    grid_1 = np.array(
            [[1e-14, 1e-15, 1e-15], # zero precision - pos 0
            [0, 0, 0], # zero precision -1
            [1e-15, 1e-15, 1e-15], # zero precision -1
            [0.5, 0.5, 0.5], # 1
            [0.5 + 1e-14, 0.5, 0.5], # -1
            [3460, 1e5, 1e15 + 1e-2], # large number precision 11
            [3460, 1e5, 1e15 + 1e-1], # large number precision -1
            [1, 1.01, 2], # 5
            [0.89, 2, 3], # 4
            [2, 2, 2], # 7
            [0.55, 0.439, 0.0], # 3
            [2.5, 2.5, 2.5], # 9
            [3, 2.5, 2.5], # -1
            [3460, 1e5, 1e15], # 11
            [0, 1e-14, 0]]) #-1

    grid_2 = np.array(
            [[1e-14, 1e-15, 1e-15],
            [0.5, 0.5, 0.5],
            [0.55, 0.439, 1e-13],
            [0.55, 0.439, 1e-15],
            [0.89, 2, 3],
            [1, 1.01, 2],
            [1, 1.01 + 1e-14, 2],
            [2, 2, 2],
            [2.1, 2, 2],
            [2.5, 2.5, 2.5],
            [2.5 + 1e-14, 2.5, 2.5],
            [3460, 1e5, 1e15]])

    idx_ref = np.array([ 0, -1, -1,  1, -1, 11, -1,  5,  4,  7,  3,  9, -1, 11, -1])

    idx_calc = pescado.tools._meshing.where(grid_1, grid_2, ordering=[0, 1, 2])

    if np.any(idx_ref - idx_calc != 0):
        raise RuntimeError('Cython where function failed 3D unit test')


