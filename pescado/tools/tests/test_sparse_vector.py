import random
import copy

import numpy as np

from numpy.testing import (
    assert_array_almost_equal, assert_array_equal, assert_allclose)

from pytest import raises

from pescado.tools import SparseVector

def test_sparse_vector():
    '''
    '''

    np.random.seed(134432)

    indices = np.random.randint(low=0, high=30, size=20)
    values = np.random.random(30) * 50

    # With repeated values in the indices.
    with raises(IndexError, match='"indices" is not unique'):
        s_v = SparseVector(indices=indices, values=values)

    with raises(IndexError, match='"indices" is not unique'):
        s_v_default = SparseVector(
            indices=indices, values=values, default=1.0)

    indices = np.unique(indices)
    values = values[np.arange(0, len(indices), dtype=int)]
    s_v = SparseVector(indices=indices, values=values)
    s_v_default = SparseVector(
        indices=indices, values=values, default=1.0)

    with raises(NotImplementedError):
        s_v()

    test_idx = np.arange(0, 30, dtype=int)
    #s_v.indices is already ordered
    non_default, self_pos, pos = np.intersect1d(
        s_v.indices, test_idx, return_indices=True)
    default = np.setdiff1d(test_idx, non_default)
    ######### ######### #########
    # Test __getitem__
    ######### ######### #########

    with raises(ValueError):
        s_v[test_idx]
    # If default is defined than no error should be raised
    s_v_default[test_idx]

    assert_array_equal(
        s_v[non_default],
        s_v.values[self_pos],
        err_msg='sparse_vector without default failed __getitem__ test')

    assert_array_equal(
        s_v_default[non_default],
        s_v_default.values[self_pos],
        err_msg='sparse_vector with default failed __getitem__ test')

    assert_array_equal(
        s_v_default[default],
        np.ones(len(default)) * s_v_default.default,
        err_msg='sparse_vector with default failed __getitem__ test')

    # Test with a non ordered and repeated array
    
    indices = np.arange(0, 10, dtype=int)
    values = np.linspace(0, 5, 10)
    s_v = SparseVector(indices=indices, values=values)
    s_v_default = SparseVector(
        indices=indices, values=values, default=1.0)

    test_idx = np.array([0, 1, 5, 2, 2, 5, 6])
    assert_array_equal(values[test_idx], s_v[test_idx])
    assert_array_equal(values[test_idx], s_v_default[test_idx])
    
    ######### ######### #########
    # Test __setitem__
    ######### ######### #########

    set_i_nd = np.arange(0, len(non_default), 3, dtype=int)
    set_i_d = np.arange(0, len(default), 3, dtype=int)

    s_v[non_default[set_i_nd]] = np.arange(0, len(set_i_nd), dtype=float)
    s_v[default[set_i_d]] = np.concatenate(
        [np.arange(0, len(set_i_d) // 2, dtype=float),
         np.ones(len(set_i_d) - len(np.arange(0, len(set_i_d) // 2)),
                 dtype=float) * 1.0])

    s_v_default[non_default[set_i_nd]] = np.arange(
         0, len(set_i_nd), dtype=float)
    s_v_default[default[set_i_d]] = np.concatenate(
            [np.arange(0, len(set_i_d) // 2, dtype=float),
            np.ones(len(set_i_d) - len(np.arange(0, len(set_i_d) // 2)),
                    dtype=float) * 1.0])

    assert_array_equal(
        s_v_default[non_default[set_i_nd]],
        np.arange(0, len(set_i_nd), dtype=float),
        err_msg='sparse_vector with default failed __setitem__ test')

    assert_array_equal(
        s_v_default[default[set_i_d]],
        np.concatenate(
        [np.arange(0, len(set_i_d) // 2, dtype=float),
         np.ones(len(set_i_d) - len(np.arange(0, len(set_i_d) // 2)),
                 dtype=float) * 1.0]),
        err_msg='sparse_vector with default failed __setitem__ test')

    assert_array_equal(
        s_v[non_default[set_i_nd]],
        np.arange(0, len(set_i_nd), dtype=float),
        err_msg='sparse_vector without default failed __setitem__ test')

    assert_array_equal(
        s_v[default[set_i_d]],
        np.concatenate(
        [np.arange(0, len(set_i_d) // 2, dtype=float),
         np.ones(len(set_i_d) - len(np.arange(0, len(set_i_d) // 2)),
                 dtype=float) * 1.0]),
        err_msg='sparse_vector without default failed __setitem__ test')

    ######### ######### #########
    # Test extend
    ######### ######### #########

    indices_2 = np.unique(np.random.randint(low=50, high=60, size=20))
    indices_ext = np.concatenate([indices_2, indices])

    values_ext = np.random.random(len(indices_ext)) * 30
    s_v_extend = SparseVector(indices=indices_ext, values=values_ext)

    val_before_ext = copy.deepcopy(s_v.values)
    indices_before_ext = copy.deepcopy(s_v.indices)

    val_before_ext_default = copy.deepcopy(s_v_default.values)
    indices_before_ext_default = copy.deepcopy(s_v_default.indices)

    s_v.extend(s_v_extend)
    s_v_default.extend(s_v_extend)

    assert_array_equal(
        s_v[indices_before_ext],
        val_before_ext,
        err_msg='sparse_vector without default failed extend test')

    assert_array_equal(
        s_v_default[indices_before_ext_default],
        val_before_ext_default,
        err_msg='sparse_vector with default failed extend test')

    assert_array_equal(
        s_v[indices_2],
        values_ext[0:len(indices_2)],
        err_msg='sparse_vector without default failed extend test')

    assert_array_equal(
        s_v_default[indices_2],
        values_ext[0:len(indices_2)],
        err_msg='sparse_vector with default failed extend test')

    ######### ######### #########
    # Test addition
    ######### ######### #########

    indices_add = np.unique(np.random.randint(low=80, high=100, size=20))
    values_add = np.random.random(len(indices_add)) * 60
    s_v_add = SparseVector(indices=indices_add, values=values_add)
    s_v_add_default = SparseVector(
        indices=indices_add, values=values_add, default=3)

    err_msg = ('Addition is only allowed between instances of SparseVector'
               + " however 'other_pv' is {0}".format(type(2)))

    with raises(
        ValueError,
        match=err_msg):
        s_v_default + 2

    with raises(ValueError):
        s_v_t = s_v + s_v_add # diff None + diff None
    with raises(ValueError):
        s_v_t = s_v + s_v_add_default # diff None + diff default
    with raises(ValueError):
        s_v_t = s_v_default + s_v_add # diff default + diff None

    s_v_t_none = s_v + s_v #same None + same None
    s_v_t_def = s_v_default + s_v_default # same default + same default
    s_v_new = s_v_default + s_v_add_default # diff default + diff default

    assert_array_equal(
        s_v_t_none[s_v.indices],
        s_v.values * 2,
        err_msg='sparse_vector with default failed addition test')

    assert_array_equal(
        s_v_t_none[s_v.indices],
        s_v.values * 2,
        err_msg='sparse_vector with default failed addition test')

    assert_array_equal(
        s_v_t_def[s_v_default.indices],
        s_v_default.values * 2,
        err_msg='sparse_vector with default failed addition test')

    assert_array_equal(
        s_v_t_def[indices_add],
        np.ones(len(indices_add)) * s_v_default.default * 2,
        err_msg='sparse_vector with default failed addition test')

    assert_array_equal(
        s_v_new[s_v_default.indices],
        s_v_default.values + s_v_add_default.default,
        err_msg='sparse_vector with default failed addition test')

    assert_array_equal(
        s_v_new[s_v_add_default.indices],
        s_v_add_default.values + s_v_default.default,
        err_msg='sparse_vector with default failed addition test')

    ######### ######### #########
    # Test multiplication by scalar
    ######### ######### #########

    with raises(
        ValueError,
        match='scalar is {0} however float or int was expected'.format(
            type(s_v))):
        s_v_default * s_v

    assert_array_equal(
        (s_v_default * 2)[indices_2],
        values_ext[0:len(indices_2)] * 2,
        err_msg='sparse_vector with default failed multiplication test')

    ######### ######### #########
    # Test substraction
    ######### ######### #########

    idx = np.arange(0, 10)

    ref_1 = SparseVector(
        indices=idx, values = np.arange(0, 100, 10), default=0, dtype=float)
    diff = ref_1 + (ref_1 * -1)
    
    assert_array_equal(diff[idx], np.zeros(10, dtype=float), 
                       err_msg='Failed substraction')

test_sparse_vector()
