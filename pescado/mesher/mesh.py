''' Pescado Mesher
        Contains the Mesh class and usefull methods.
'''
import collections
import copy
import warnings
import pickle
import importlib

import numpy as np


from pescado.mesher import shapes, patterns

from pescado.tools import reduced_array as ra
from pescado.tools.meshing import (    
    kd_where, unique, internal_boundary, external_boundary)
from pescado.tools.plotting import plot_2d_finalized_mesh
from pescado.tools.sparse_vector import SparseVector

from numpy.testing import assert_allclose

def external_boundary_shape(shape, pattern, nth):
    ''' Returns a convex 'pescado.shapes.Delaunay' shape
    using the 'nth' - external boundary for a given
    'shape' and 'pattern'

    Parameter
    ---------
    shape: instance of 'pescado.mesher.shape.Shape'

    pattern: instance of 'pescado.mesher.patterns.Pattern'

    nth: The 'first-neighbour' distance of a point in the external boundary from the internal boundary.

    Return
    -------
        instance of 'pescado.shapes.Delaunay'
    '''

    internal, external = pattern.boundary(shape=shape)

    if nth == 1:
        ext_bd = external
    else:
        internal_neig = pattern.neighbours(
            points_tag=internal, nth=nth)[0]
        ext_bd = internal_neig[~shape(pattern(internal_neig))]

    ext_coord = pattern(ext_bd)

    if shape.bbox.shape[1] == 1:
        new_shape = shapes.Box(
            lower_left=(np.min(ext_coord),),
            size=(np.abs(np.max(ext_coord) - np.min(ext_coord)), ))

    else:
        new_shape = shapes.Delaunay(coordinates=ext_coord)

    return new_shape


class Mesh():

    def __init__(self, simulation_region, pattern):
        ''' Pescado Mesh object

        Parameters
        -----------
        simulation_region: instance of pescado.shapes.Shape

            Defines the total - simulation - region to be discretized

        pattern: instance of pescado.patterns.Pattern

            Defines the discretization step. Hence the mesh points
            coordinates, neighbours and voronoi cells.

        "Public" methods
        -----------
        coordinates()
            Returns real space coordinates
        neighbours()
            Retruns neighbours index
        refine()
            Refines self

        Attributes
        -----------

        The mesh points are divided into complete and incomplete mesh points

        The complete points are those whose number of neighbours is the same
        as the number of its voronoi cell facets

        Only complete points are "real" mesh points; 

        The incomplete points are those whose number of neighbours is smaller
        than then number of voronoi cell facets

        The incomplete points are "virtual" mesh points; 

        --- Quantities referring to incomplete points indices
        
            self._coordinates: complete and incomplete coordinates
            self._tags: complete and incomplete tags

            self._patterns: list of patterns
            self._index2pat: SparseVectors - complete and incomplete
                index to pattern 

            self._neighbours, self._pt_neig : reduced array refering to the 
                neighbours of incomplete and complete mesh points
                neig_idx_i = self._neighbours[
                    self._pt_neig[i]:self._pt_neig[i+1]]

            self._safety_radius
        
        --- Quantites referring to complete mesh points 

            self.npoints: number of mesh points - number of complete points in
                the mesh
        
            self._complete_neig, self._complete_pt_neig : reduced array 
                referring to the neighbours of complete mesh points

        To go from complete mesh points index to incomplete:
            self._internal_index : SparseVector

                incomplete_index = self._internal_index[complete_index]

        --- Other quantities
            
            self._simulation_region: region defined by the mesh
            
        '''
        
        # Properties       
        self._refined = None        
        self.npoints = None
        self._coordinates = None
        self._tags = None

        self._patterns = None
        self._index2pat = None

        self._pt_neig = None
        self._neighbours = None

        
        self._safety_radius = None
        
        ### Add the simulation_region using pattern
        
        # Add external points -> c.f. refinement algorithm
        ext_bdr_region = external_boundary_shape(
            shape=simulation_region, pattern=pattern, nth=2)
        self._add_region(
            region=ext_bdr_region, pattern=pattern)

        self._refined = None
        self._simulation_region = simulation_region
        
        # Extract the - complete - mesh points
        imcomplete_points = np.arange(len(self._coordinates), dtype=int)[
            ~self._simulation_region(self._coordinates)]

        self._complete_updated = False

        self._update_complete_points(
            imcomplete_points=imcomplete_points)
        
        self._complete_updated = True

    def _add_points(
        self, coord, tags=None, neighbours=None, points_neighbours=None,
        safety_radius=None, pattern=None):
        ''' Add points at 'coord' to the mesh. Attention, this function
        only update the mesh attributes, but does not insure that
        mesh continuity is preserved. 

        !!! Notice - After adding points mesh continuity is not guarateed. If
        you want to refine a region with 'coord' then run self.refine(
        region, coordinates) instead. !!!!

        The parameters tags, neighbours, points_neighbours, safety_radius 
        and pattern are optional. Their default value is None. 

        Parameters
        -----------

        coord: np.ndarray of floats with shape(n, d)
            n: number of points
            d: dimension 

        tags: Sequence or np.ndarray of objects (default None)
            tags of the added points
            
            If None, then mesh generates its own tags
                tags = list(np.arange(len(idx), dtype=object))

        neighbours: np.ndarray of integers of shape(m, )
            m: number of neighbours

        points_neighbours: np.ndarray of integers of shape(n+1 )
            neig_of_i = neighbours[
                points_neighbours[i]:points_neighbours[i+1]]

        safety_radius: np.ndarray of floats with shape(n, )
            See documentation for definition

        pattern: instance of pescado.mesher.patterns.Pattern

        Returns
        --------
        np.ndarray of integers with shape(n, ) defining the indices
        of the added points.
        '''

        # We start modifying the mesh properties
        self._refined = False

        # Add coordinates and generate the indexing for the new
        # mesh points.
        if (self._coordinates is None) and (self._tags is None):
            
            idx = np.arange(len(tags), dtype=int)
            self._coordinates = coord

            self._tags = np.empty((len(idx), ), dtype=object)
        elif (self._coordinates is not None) and (self._tags is not None):

            idx = np.arange(
                len(self._coordinates), 
                len(self._coordinates) + len(coord), dtype=int)
            self._coordinates = np.concatenate(
                (self._coordinates, coord))

            self._tags = np.concatenate(
                (self._tags, np.empty((len(idx),), dtype=object)))     
        else:
            raise RuntimeError(
            'self._coordinates is {0} while self._tags is {1}'.format(
                self._coordinates, self._tags))

        # Update the self._tags
        if tags is None:
            tags = list(np.arange(len(idx), dtype=object))
        
        if isinstance(tags, collections.abc.Sequence):
            self._tags[idx] = tags
        elif isinstance(tags, np.ndarray):
            self._tags[idx] = np.split(tags, len(tags), axis=0)
        else:
            raise RuntimeError('Unexpected type for "tags"')
        
        if self._patterns is None:
            self._patterns = [pattern, ]
        else:
            self._patterns.append(pattern)

        # Update self._index2pat
        if self._index2pat is None:
            self._index2pat = SparseVector(default=0, dtype=int)
        else:
            self._index2pat[idx] = np.ones(len(idx)) * (len(self._patterns) - 1)
                
        # Update the neighbours
        if neighbours is None:
            neighbours = np.array([], dtype=int)
            points_neighbours = np.zeros(len(idx) + 1, dtype=int)
        
        if (self._neighbours is None) and (self._pt_neig is None):
            self._neighbours = neighbours
            self._pt_neig = points_neighbours
        elif (self._neighbours is not None) and (self._pt_neig is not None):
            neighbours += idx[0] # Update indexing
            self._neighbours, self._pt_neig = ra.concatenate(
                [[self._neighbours, self._pt_neig], 
                 (neighbours, points_neighbours)])
        else:
            raise RuntimeError(
            'self._neighbours is {0} while self._pt_neig is {1}'.format(
                self._neighbours, self._pt_neig))
        
        # Some integrity tests
        if self._pt_neig[0] != 0:
            raise RuntimeError(
                'self._pt_neig array is corrupted. First element should be 0.')

        if np.any(self._neighbours == -1):
            raise RuntimeError(
                '-1 value found when calculating the pattern neighbours')

        # Update the npoints
        if self.npoints is None:
            self.npoints = len(tags)
        else:
            self.npoints += len(tags)
        
        if self._safety_radius is not None:
            if safety_radius is None:
                safety_radius = np.ones(len(idx)) * np.nan

            self._safety_radius = np.concatenate(
                (self._safety_radius, safety_radius))

        return idx

    def _remove_points(self, points):
        ''' Remove points 

        After this step mesh integrity is broken
        '''        
        npoints = len(self._coordinates)

        old_points = np.setdiff1d(
            np.arange(npoints, dtype=int), points)
        
        npoints -= len(points)

        kept_points = np.arange(npoints, dtype=int)
        self.imcomplete_points = kept_points[
            np.intersect1d(old_points, self.imcomplete_points,
                return_indices=True)[1]]

        if len(points) < 0:
            raise ValueError('Region failed to remove points')
        
        self._refined = False
        self._complete_updated = False

        self._coordinates =  self._coordinates[old_points]
        self._tags = self._tags[old_points]
        if self._safety_radius is not None:
            self._safety_radius = self._safety_radius[old_points]

        # Update the index2pat
        # Find -not default- points
        index2pat_old, com1, index2pat_new = np.intersect1d(
            self._index2pat.indices, old_points, return_indices=True)
        index2pat_val = self._index2pat[index2pat_old] # Recup their values
        self._index2pat = SparseVector(default=0, dtype=int)
        self._index2pat[index2pat_new] = index2pat_val

        # Update neighbours        
        # Recover the points the remaining points that lost a neighbour
        (self._neighbours,
         self._pt_neig, kept_points_modified) = ra.remove_element(
            elements=points, 
            values=self._neighbours, indices=self._pt_neig)

        return kept_points, kept_points_modified

    def _update_points(self, points, pattern):
        ''' Update the properties of 'points' using the 
        'pattern'. 
        
        Updates the 'self._neighbours', 'self._pt_neig', 'self._tags' using
        'pattern'.

        Then adds 'pattern' to 'self._patterns'. 
        '''
        
        mnt, mnti, mni = pattern.neighbours(points)
        
        self._neighbours, self._pt_neig = ra.update_element(
            elements=points,
            new_values=mnt[mnti], new_indices=mni,
            values=self._neighbours, indices=self._pt_neig)
        
        self._tags[points] = points
        self._index2pat[points] = np.ones(
            len(points), dtype=int) * len(self._patterns)

        self._patterns.append(copy.deepcopy(pattern))

        if len(points) > 0:
            self._safety_radius[points] = pattern.safety_radius(
                points_tag=points)

    def _update_complete_points(self, imcomplete_points):
        ''' Updates the class properties concerning
        the complete mesh points.

        - clomplete mesh points :  Those whose number of
        voronoi ridges is the same as mesh neighbours.

        Parameters
        -----------
            imcomplete_points: sequence of integers
                Indices of the points in the mesh that
                are imcomplete
        '''
        self.imcomplete_points = copy.deepcopy(imcomplete_points)
        
        self.npoints -= len(self.imcomplete_points)

        self._internal_index = SparseVector(
            indices=np.arange(0, self.npoints, dtype=int),
            values=np.setdiff1d(
                np.arange(len(self._coordinates), dtype=int), 
                self.imcomplete_points),
            default=None,
            dtype=int)

        (self._complete_neig,
         self._complete_pt_neig, ignore) = ra.remove_element(
                elements=self.imcomplete_points,
                values=copy.deepcopy(self._neighbours),
                indices=copy.deepcopy(self._pt_neig))

        self._complete_updated = True

    def _add_region(self, region, pattern=None, coordinates=None):
        ''' Adds the coordinates inside a given 'region' following 'pattern'.
    
        Parameters
        -----------
        region: instance of pescado.shapes.Shape

            Defines the region to be discretized

        pattern: instance of pescado.patterns.Pattern

            Defines the discretization step. Hence the mesh points
            coordinates, neighbours and voronoi cells.

        Notice
        --------

            The added points are disconnected from the current points. 
            Hence after this step self._refined is set to False.

            To fix this, run self._refine(). 

        '''
        
        if pattern is not None:
            tags = pattern.inside(region)
            coordinates = pattern(tags)

            (neig_tags, 
            neighbours, points_neighbours) = pattern.neighbours(tags)
                    
            neig_coord = pattern(neig_tags)

            # Remove the neighbours outside of region
            outside_tags_idx = np.arange(
                len(neig_tags), dtype=int)[~region(neig_coord)]

            # Filter out the tags outside from 'neig_tags'
            inside_tags = np.arange(len(neig_tags), dtype=int)[
                region(neig_coord)]
            
            # Tags to be added to the Mesh
            neig_coord = neig_coord[inside_tags]        
            new_values = np.empty(len(neig_tags), dtype=int)

            idx, isin = kd_where(array=neig_coord, ref_array=coordinates)
            out = np.arange(len(neig_coord))[isin == False]

            new_values[inside_tags] = kd_where(
                array=neig_coord, ref_array=coordinates)[0]
            # Remove points from neighbours and Update points_neighbours
            neighbours, points_neighbours, upd_ele = ra.remove_values(
                val2remove=outside_tags_idx, values=neighbours, 
                indices=points_neighbours)
            neighbours = new_values[neighbours]
            
            if len(neighbours) == 0:
                warnings.warn((
                    "No neighbours inside the 'region' when adding "
                    + 'coordinates {0}'.format(coordinates)))
            
            if self._safety_radius is not None:
                safety_radius = pattern.safety_radius(tags)
            else:
                safety_radius = None
        elif coordinates is not None:
            
            if np.any(np.logical_not(region(coordinates))):
                raise ValueError('Not all coordinates inside region')
            
            (tags, neighbours, 
             points_neighbours, safety_radius, pattern) = [None, ] * 5
        else:
            raise ValueError(('pattern and coordinates are not defined'))

        idx = self._add_points(
            coordinates, tags=tags, neighbours=neighbours, 
            points_neighbours=points_neighbours,
            safety_radius=safety_radius, pattern=pattern)

        return tags, idx

    def _merging_pattern(self, modified_points):
        ''' Creates a finite pattern from 'modified_points'
        and their neighbours according to 'self._neighbours'. Use this 
        finite pattern ('merging_pattern') to update the 
        'modified_points' mesh information to ensure self.mesh is continuous : 
            
            self._update_points(modified_points, merging_pattern))

        Parameters
        -----------

        modified_points: np.ndarray of integers with shape(n, )
            Indices of the mesh points

        Returns
        --------
        instance of pescado.mesher.patterns.Pattern

        '''
                
        neig_idx_merge = ra.elements_value(
            elements=modified_points, 
            values=copy.deepcopy(self._neighbours),
            indices=copy.deepcopy(self._pt_neig),
            assume_sorted=True)

        finite_points = unique(
            np.concatenate((modified_points, neig_idx_merge)))[0]
        
        merging_pattern = patterns.Finite(
            {i:self._coordinates[i] for i in finite_points})

        ################### Test the Merging pattern
        test, com1, com2 = np.intersect1d(
            merging_pattern.index(modified_points),
            merging_pattern.unbounded_region, return_indices=True)        
        idx = np.setdiff1d(modified_points[com1], self.imcomplete_points)

        if len(idx) > 0:
            #print(self._simulation_region(self._coordinates[idx]))
            raise RuntimeError(
                'mesher failed to refine. {0} do not form a close cell'.format(
                    self._coordinates[idx]))

        return merging_pattern

    def refine(self, region, pattern=None, coordinates=None, nth=None):
        ''' Refines the 'region' inside the mesh. Removes the 
        'self._coordinates' inside 'region' and replaces by either:
            
            i) pattern(pattern.inside(region))
            
            ii) coordinates

                Notice if there are coordinates outside 
                of self._simulation_region then they are not added. 
        
        If 'nth' is not None, it enlarges the 'region'.

        Parameters:
        -----------
        
        region: pescado.mesher.shapes.Shape instance

        pattern: instance of pescado.mesher.pattern.Pattern

        coordinates: np.ndarray of floats with shape(n, d)

        nth: integer (default None)

        '''

        ##### Define the refinment region
        if nth is not None:
           region = external_boundary_shape(
               shape=region, pattern=pattern, nth=nth)

        region = (self._simulation_region & region)

        ##### Remove the points inside the refinment region
        points_to_remove = np.arange(len(self._coordinates), dtype=int)[
            region(self._coordinates)]
        if np.any(points_to_remove):
            kept_points, kept_points_modified = self._remove_points(
                points=points_to_remove)
        else:
            kept_points_modified = np.array([], dtype=int)
            kept_points = np.arange(len(self._coordinates), dtype=int)
        
        ##### Update the safety radius for the current mesh points
        if (self._safety_radius is None) and (len(self._patterns) == 1):
            self._safety_radius = self._patterns[0].safety_radius(
                np.concatenate(self._tags))
        elif self._safety_radius is None:
            raise RuntimeError(
                'Error recovering safety radius for remaining mesh points')

        ##### Add the points inside the refinment region
        if pattern is not None:
            
            if np.any(pattern.inside(region)):
                added_tags, added_points = self._add_region(
                    region=region, pattern=pattern)
            else:
                raise RuntimeError(
                    'No points inside refined region.')

            if len(added_tags.shape) == 1:
                added_points_modified = added_points[np.searchsorted(
                    added_tags, pattern.boundary(region)[0], side='right') -1]
            else: 
                added_points_modified = added_points[kd_where(
                    array=pattern.boundary(region)[0], 
                    ref_array=added_tags)[0]]

        elif coordinates is not None:
            
            coordinates = coordinates[region(coordinates)]

            added_tags, added_points = self._add_region(
                region=region, coordinates=coordinates)
            added_points_modified = added_points
        else:
            raise ValueError(('Pattern nor coordinates were given'))

        ##### Find the modified points
        ############ Find those inside the safety region
        kept_points_test = np.setdiff1d(kept_points, kept_points_modified)
        if len(kept_points_test) > 0:
            kept_points_modified = np.concatenate(
                [kept_points_modified,
                 kept_points_test[kd_where(
                    array=self._coordinates[kept_points_test],
                    dist=self._safety_radius[kept_points_test],
                    ref_array=self._coordinates[added_points])[1]]])
        
        added_points_test = np.setdiff1d(added_points, added_points_modified)
        if len(added_points_test) > 0:
            added_points_modified = np.concatenate(
                [added_points_modified,
                 added_points_test[kd_where(
                    array=self._coordinates[added_points_test],
                    dist=self._safety_radius[added_points_test],
                    ref_array=self._coordinates[kept_points])[1]]])
        
        modified_points = np.sort(np.concatenate(
            [kept_points_modified, added_points_modified]))

        ##### Make the merging pattern
                
        merging_pattern = self._merging_pattern(modified_points=modified_points)
            
        ################### Update N and tags and index2pat
        self._update_points(points=modified_points, pattern=merging_pattern)

        #######################################################################
        ################### Update Mesh properties

        self.npoints = len(self._coordinates)
        self._update_complete_points(imcomplete_points=self.imcomplete_points)
        
        self._refined = True
        
        del merging_pattern

    def neighbours(self, points, reduced_array=False):
        ''' Return the neighbours for a given set of points

        Parameters
        -----------
            points: sequence of integers

            reduced_array: bool (default is False)
        Returns
        --------
            If reduced_array is False:
                np.array or sequence of np.arrays containing
                the neighbours for 'points'
            else:
                neighbours: np.ndarray of integers shape(m, )
                    m: number of total neighbours                
                neig_idx: np.ndarray of integers shape(n+1, )
                    n: number of 'points'

                To acess neighbours i 
                    neighbours[neig_idx[i]:neig_idx[i+1]]

                Check tools.reduced_array.py for useful
                methods to work with (neighbours, neig_idx).
        '''

        if (self._refined is False) and (self._refined is not None):
            raise RuntimeError(
                'Mesh integrity failed. Please run self._refine')
        if self._complete_updated is False:
            raise RuntimeError(
                'Mesh integrity failed.'
                'Please run self._update_complete_points')

        if np.any(points >= self.npoints):
            i_e = points[points >= self.npoints]
            msg = 'You"ve requested the neighbour of  {0}'.format(i_e)
            msg += (
                ' Its value is larger than the number of complete mesh'
                + ' points (self.npoints). If you were looking for '
                + 'the neighbours of an imcomplete point, '
                + 'please use self._neighbours and self.neig_idx'
                +' instead.')
            raise RuntimeError(msg)

        if self._pt_neig[0] != 0:
            raise RuntimeError(
                '_pt_neig array is corrupted. First element should be 0.')

        c_neighbours = self._complete_neig
        c_neig_idx = self._complete_pt_neig

        if isinstance(points, (int, np.integer)):
            neig = c_neighbours[c_neig_idx[points]:c_neig_idx[points + 1]]
        else:
            
            neighbours, neig_idx = ra.extract_elements(
                elements=np.asanyarray(points), values=c_neighbours, 
                indices=c_neig_idx)
            
            if reduced_array:
                return neighbours, neig_idx
            else:
                neig = np.split(neighbours, neig_idx[1:-1])

                if len(neig) != len(points ):
                    raise RuntimeError(
                        'Problem when extracting neig'
                        + 'neig has {0} points  instead of {1}'.format(
                            len(neig), len(points )))

        return neig

    def coordinates(self, points=None):
        ''' Return the real space coordinates for a given set of points 

        Parameters
        -----------
            points : integer or sequence of integers

        Returns
        --------
            np.array containing the coordinated of 'points '
        '''

        if (self._refined is False) and (self._refined is not None):
            raise RuntimeError(
                'Mesh integrity failed. Please run self._refine')
        if self._complete_updated is False:
            raise RuntimeError(
                'Mesh integrity failed.'
                'Please run self._update_complete_points')

        if points  is None:
            points  = self._internal_index.indices
        if isinstance(points , collections.abc.Sequence):
            if not isinstance(points [0], (int, np.integer)):
                raise Exception(
                    'points  has to be a sequence of integer or an integer.'
                    + ' points [0] was {0}'.format(type(points [0])))
        
        return self._coordinates[self._internal_index[points]]

    def points(self, coordinates=None, tol=1e-12):
        ''' Return the points  for a given set of coordinates

        Parameters
        -----------
            coordinates: np.array of floats (m, d)
                m: number of coordinates, d: dimension

            tol: float

        Returns
        --------
            points : np.array of integers
        '''

        if (self._refined is False) and (self._refined is not None):
            raise RuntimeError(
                'Mesh integrity failed. Please run self._refine')
        if self._complete_updated is False:
            raise RuntimeError(
                'Mesh integrity failed.'
                'Please run self._update_complete_points')

        if len(coordinates.shape) != 2:
            raise ValueError('Wrong number of dimensions in "coordinates"')

        
        points = np.ones(len(coordinates), dtype=int) * -1
        points_, isin  = kd_where(coordinates, self.coordinates())
        points[isin] = points_

        return points 

    def _ridges_and_vertices(self, points):
        ''' Recovers the ridges and vertices for 'points'. 
        Uses each points parent pattern 'ridges_and_vertices' method.
        
        Returns
        --------
        vertices: np.ndarray of floats shape(v, dim)
            Real space coordinates of dimension dim
            for the vertices forming the ridges of 
            the voronoi cells of 'points_tag'               
            
        neighbours: np.ndarray of integers with shape(n, )
            Mesh points index of the neighbours of 'points'.
            
            -1 indicates a neighbour is outside of the Mesh region (
                self._simulation_region)


        ridge_vertices: np.ndarray of integers shape(m, )
            'vertices' indices of the vertices coordinates of
            each element in ridges
        
        ridges: np.ndarray of integers shape(m+1)
            Interval in 'ridge_vertices' containing the 'vertices' indices
            for each ridge
            
            s.t. the indices of 'vertices' forming the ridge 'm'
            is obtained with :
                ridge_vertices[ridges[m]:ridges[m+1]]

        region_vertices: np.ndarray of integers shape(v)
            indices of the 'vertices' defining each element in regions

        regions: np.ndarray of integers shape(n+1)
            Interval in 'region_vertices' containing the 'vertices' indices
            for each region in 'points_tag'

        point_ridges: np.ndarray of integers shape(r, )
            The ridges indices for each point in points_tag.
            Follows the same ordering as point_neighbours
            Hence for point i do :
                r = point_ridges[
                    point_neighbours[i]:point_neighbours[i+1]]
        
        point_neighbours: np.ndarray of integers shape(p+1, )
            The interval in 'neighbours' for each point in 'points'
            neig_i = neighbours[
                point_neighbours[i]:point_neighbours[i+1]]]

        Notice
        ------

            Neighbours outside of the Mesh are tagged with -1 in 'neighbours'
        '''

        if (self._refined is False) and (self._refined is not None):
            raise RuntimeError(
                'Mesh integrity failed. Please run self._refine')
        if self._complete_updated is False:
            raise RuntimeError(
                'Mesh integrity failed.'
                'Please run self._update_complete_points')

        internal_idx = self._internal_index[points]
        pat_num = self._index2pat[internal_idx]

        positions = np.ones(len(internal_idx), dtype=int) * -1
        num_added_ele = 0

        vertices = None
        
        ridges = list()
        regions = list()
        point_ridges = list()
        neighbours = list()

        for i, pat in enumerate(self._patterns):            
            in_idx = np.arange(len(internal_idx), dtype=int)[pat_num == i]
            in_pat_idx = internal_idx[in_idx]
            
            if len(in_pat_idx) > 0:

                positions[in_idx] = np.arange(
                    num_added_ele, num_added_ele+len(in_idx), dtype=int)

                num_added_ele += len(in_idx)

                point_tags = self._tags[in_pat_idx]

                if isinstance(point_tags[0], np.ndarray):
                    point_tags = np.concatenate(self._tags[in_pat_idx])
                else:
                    point_tags = np.asarray(point_tags)

                neig_tags, neighbours_i, points_neighbour_i = pat.neighbours(
                    points_tag=point_tags)

                (vertices_i, ridge_vertices_i, ridges_i,
                point_ridges_i, 
                region_vertices_i, regions_i) = pat.ridges_and_vertices(
                    points_tag=point_tags, 
                    neig_tags=neig_tags, neighbours=neighbours_i, 
                    point_neighbours=points_neighbour_i)
                
                if vertices is None:
                    vertices = vertices_i
                else:                    
                    ridge_vertices_i += len(vertices)
                    region_vertices_i += len(vertices)

                    vertices = np.concatenate((vertices, vertices_i))

                point_ridges_i += np.sum(
                    [len(r[1]) - 1 for r in ridges], dtype=int)
                
                ridges.append((ridge_vertices_i, ridges_i))
                regions.append((region_vertices_i, regions_i))

                neighbours_c = self.points(pat(neig_tags))[neighbours_i]
    
                assert_allclose(
                    pat(neig_tags)[neighbours_i][neighbours_c != -1],
                    self.coordinates(neighbours_c[neighbours_c != -1]))

                point_ridges.append((point_ridges_i, points_neighbour_i))
                neighbours.append(neighbours_c)

        # Concatenate them
        ridge_vertices, ridges = ra.concatenate(ridges)
        region_vertices, regions = ra.concatenate(regions)
        point_ridges, point_neighbours = ra.concatenate(point_ridges)
        neighbours = np.concatenate(neighbours)

        # Zip the vertices
        vertices, vertex_idx = ra.zip(vertices)
        ridge_vertices = vertex_idx[ridge_vertices]
        region_vertices = vertex_idx[region_vertices]

        # Shuffle the reduced arrays to match the mesh index
        if np.any(positions == -1):
            raise RuntimeError(
                'Failed to extract ridge and vertices for {0} points'.format(
                sum(positions == -1)))
        
        region_vertices, regions = ra.shuffle(
            indices=regions, values=region_vertices, 
            positions=positions)
        
        point_ridges, ignore = ra.shuffle(
            indices=point_neighbours, values=point_ridges, 
            positions=positions)
        
        neighbours, point_neighbours = ra.shuffle(
            indices=point_neighbours, values=neighbours, 
            positions=positions)            
        
        return (
            vertices, neighbours,
            ridge_vertices, ridges, 
            region_vertices, regions,
            point_ridges, point_neighbours)

    def _geometrical_property(self, points, _property):
        ''' Calculates the - geometrical properties - for 'points'. 
        By geometrical properties I mean -> distances, surfaces and volumes. 

        Parameters
        -----------
        points: np.ndarray of integers with shape(n, )
            Index of mesh points

        _property: Sequence of strings
            'surface', 'volume', 'distance'
                
                Name of the properties to be calculated. It is more
                efficient to calculate all at once. 

        Returns
        --------
        property_dict: dictionary
                key:_property
                value: np.ndarray of floats 
            
        point_properties: np.ndarray of integers of shape(n+1, )
            In case a surface or distance is requested, interval 
            in 'value' that corresponds to the properties of a element 
            in point.  

            surf_i = dict['surface'][point_properties[i]:point_properties[i+1]]
            dist_i = dict['distance'][point_properties[i]:point_properties[i+1]]
            volume_i = dict['distance'][i]
        
        neighbours: np.ndarray of integers with shape(m, )
            Mesh points index of the neighbours of 'points' whom they share a 
            'surface' or 'distance'. 

        Notice -> If neighbours = -1, then the point is outside the Mesh region.
        '''
        
        if (self._refined is False) and (self._refined is not None):
            raise RuntimeError(
                'Mesh integrity failed. Please run self._refine')
        if self._complete_updated is False:
            raise RuntimeError(
                'Mesh integrity failed.'
                'Please run self._update_complete_points')

        internal_idx = self._internal_index[points]
        pat_num = self._index2pat[internal_idx]

        positions = np.ones(len(internal_idx), dtype=int) * -1
        num_added_ele = 0
        
        neighbours = list()
        property_dict = {i:None for i in _property}
        
        for i, pat in enumerate(self._patterns):            
            in_idx = np.arange(len(internal_idx), dtype=int)[pat_num == i]
            in_pat_idx = internal_idx[in_idx]
            
            if len(in_pat_idx) > 0:

                positions[in_idx] = np.arange(
                    num_added_ele, num_added_ele+len(in_idx), dtype=int)
                num_added_ele += len(in_idx)                

                point_tags = self._tags[in_pat_idx]
                if isinstance(point_tags[0], np.ndarray):
                    point_tags = np.concatenate(self._tags[in_pat_idx])
                else:
                    point_tags = np.asarray(point_tags)                
                
                (pat_property_dict, pat_neig_tags, pat_neighbours, 
                 pat_point_neighbours) = pat.geometrical_properties(
                    points_tag=point_tags, _property=_property)

                neighbours.append((
                    self.points(
                        coordinates=pat(pat_neig_tags))[pat_neighbours],
                    pat_point_neighbours))
                
                for key in _property:
                    if property_dict[key] is None:
                        property_dict[key] = pat_property_dict[key]
                    else:
                        property_dict[key] = np.concatenate(
                            (property_dict[key], pat_property_dict[key]))

        neighbours, point_neighbours = ra.concatenate(neighbours)
        
        # Shuffle the data to match the 'points' ordering.
        if np.any(positions == -1):
            raise RuntimeError(
                'Failed to extract ridge and vertices for {0} points'.format(
                sum(positions == -1)))
        
        for key in _property:
            if key in ['surface', 'distance']:
                property_dict[key], ignore = ra.shuffle(
                    indices=point_neighbours, values=property_dict[key], 
                    positions=positions)
            elif key in ['volume', 'safety_radius']:
                property_dict[key] = property_dict[key][positions]
            else:
                raise RuntimeError(
                    'Unexpected key found in pattern.geometrical_properties'
                    + ' output')
        
        neighbours, point_neighbours = ra.shuffle(
            indices=point_neighbours, values=neighbours,
            positions=positions)

        return property_dict, point_neighbours, neighbours

    def distance(self, points, neigs=None):
        ''' Returns the distance betwen 'point' and 'neig'

        Parameters
        -----------

        points: interger or sequence n of interger

            Index of the mesh point from which to find the distance

        neigs: list of interger or list of n lists of interger - default None

            Default behavior is to find all separating distances. The distance
            indexing uses the same order as the one in 'self.neighbours'

        Returns
        -------
        distance: np.ndarray of floats with shape (m, )
            Distances between 'points' and 'neighbours'
                            
        point_properties: np.ndarray of integers of shape(n+1, )
            In case a surface or distance is requested, interval 
            in 'value' that corresponds to the properties of a element 
            in point.  

            surf_i = dict['surface'][point_properties[i]:point_properties[i+1]]
            dist_i = dict['distance'][point_properties[i]:point_properties[i+1]]
            volume_i = dict['distance'][i]
        
        neighbours: np.ndarray of itnegers with shape(m, )
            Mesh points index of the neighbours of 'points' whom they share a 
            'surface' or 'distance'. 

        '''

        if neigs is not None:
            raise NotImplementedError('Feature not available yet.')
        
        (property_dict,
         points_properties, neighbours) = self._geometrical_property(
            points, _property=['distance'])

        _distance = property_dict['distance']

        
        return _distance, points_properties, neighbours

    def surface(self, points, neigs=None):
        ''' Returns the surface betwen 'point' and 'neig'

        Parameters
        -----------

        points: interger or sequence n of interger

            Index of the mesh point from which to find the surface

        neigs: list of interger or list of n lists of interger - default None

            Index of the fneig of 'points' from which to find
            the separating surface.

            If points is a int -> neig is a list of integers
            If points is a list -> neig is a list of list of integers

            Default behavior is to find all separating surfaces. The surface
            indexing uses the same order as the one in 'self.neighbours'

        Returns
        -------
        surface: np.ndarray of floats with shape (m, )
            Surface between 'points' and 'neighbours'

        point_properties: np.ndarray of integers of shape(n+1, )
            In case a surface or distance is requested, interval 
            in 'value' that corresponds to the properties of a element 
            in point.  

            surf_i = dict['surface'][point_properties[i]:point_properties[i+1]]
            dist_i = dict['distance'][point_properties[i]:point_properties[i+1]]
            volume_i = dict['distance'][i]
        
        neighbours: np.ndarray of itnegers with shape(m, )
            Mesh points index of the neighbours of 'points' whom they share a 
            'surface' or 'distance'. 

        '''

        if neigs is not None:
            raise NotImplementedError('Feature not available yet.')
        
        (property_dict,
         points_properties, neighbours) = self._geometrical_property(
            points, _property=['surface'])

        _surface = property_dict['surface']
        
        return _surface, points_properties, neighbours

    def volume(self, points):
        ''' Returns the volume betwen 'point' and 'neig'

        Parameters
        -----------

        points: interger or sequence n of interger

            Index of the mesh point from which to find the volume

        Returns
        -------
        np.ndarray of floats with shape (m, )
            Volume of 'points'. 
        '''

        (property_dict, ignore, ignore) = self._geometrical_property(
            points, _property=['volume'])

        return property_dict['volume']
    
    def voronoi(self, points):
        ''' Return the 'voronoi cell' object for 'points'

        Parameters
        -----------
            points: int or sequence of n integer

        Returns
        --------
            vor_cel: instance or list of instances
                of pescado.mesher.VoronoiCell for each element in 'points'

        Notice 
        -------
            VoronoiCell is the - local - voronoi cell of 'points'. Hence
            the quantities are defined locally. 

            The vor_cel[i].ridges refer to the vor_cel[i].vertices index. 
            
            Also, the 'oth' vor_cel[i].ridges is associated with the '0th'
            vor_cel[i].neighbours. 

            vor_cel[i].neighbours_tags and vor_cel[i].point_tag refers 
            to the tag used by the pattern instance from which 
            'vor_cel[i]' was generated. This can be obtained using:

                pat = self._patterns[self._index2pat[self._tags[
                    self._internal_index[i]]]]
            
        '''

        ip_int = False
        if isinstance(points, (int, np.integer)):
            points = [points, ]
            ip_int = True

        points = np.asanyarray(points)
        vor = list()
        for ele in points:
            
            pat_num = self._index2pat[self._internal_index[ele]]
            tag = self._tags[self._internal_index[ele]]
            
            if isinstance(tag, (int, np.integer)):
                tag = np.array([tag, ])
            
            vor.append(
                self._patterns[pat_num].voronoi(tag[0]))

        if ip_int:
            vor = vor[0]

        return vor
    
    def inside(self, region):
        ''' Return the points of the points inside 'shape'

        Parameters
        -----------

        region: instance of 'pescado.mesher.shapes.Shap'

        Returns
        -------
        sorted numpy.ndarray of integers containing the site points

        '''
        return np.arange(0, self.npoints, dtype=int)[
            region(self.coordinates())]

    def boundary(self, region):
        ''' Returns the internal and external boundary of a given
        'region'.

        Parameters
        -----------

        region: instance of 'pescado.mesher.shapes.Shape'

        Returns
        --------

        internal: numpy.array of integers with shape(m, )
            contains the shape internal boundary points

        external: numpy.array of integers with shape(m, )
            contains the shape external boundary points.
            The external boundary is defined by the first neighbours of the
            internal boundary.
        '''

        assert isinstance(region, shapes.Shape), (
            '{0} not a pescado.mesher.shapes.Shape'.format(region))

        err_msg = ('No internal boundary found for mesh '
                   + ': {0}, region : {1}'.format(self, region))
        try:
            internal_tags = self.inside(region)
        except ValueError as e:
            if str(e)[:75] == (
                'Points with unbounded ra cells were found inside region.'
                + ' Their index is'):
                raise ValueError(err_msg + '. \n' + str(e))
            else:
                raise e

        if len(internal_tags) == 0:
            raise ValueError(err_msg)

        neig_mesh_idx, pt_neig = self.neighbours(
            internal_tags, reduced_array=True)
        unq_neig_idx, neighbours = ra.zip(neig_mesh_idx)

        ignore, internal_bdr_idx = internal_boundary(
            internal_coord=self.coordinates(internal_tags),
            neig_coord=self.coordinates(unq_neig_idx),
            neighbours=neighbours, 
            points_neighbour=pt_neig,
            shape=region,
            return_idx=True)

        internal = internal_tags[internal_bdr_idx]

        #External boundary
        neigs = self.neighbours(internal, reduced_array=True)[0]

        # Get only external boundary and remove redundant values.
        ignore, external_idx = external_boundary(
            internal_bdr_neig=self.coordinates(neigs),
            shape=region, return_idx=True)

        external = neigs[external_idx]

        if len(external) == 0:
            raise ValueError(
                'No external boundary found for Mesh '
                + ': {0}, region : {1}'.format(self, region))

        return internal, external

    def plot(self, ax=None,interval=(None, None),
             ratio=3, regions=None, c=None, **kwargs):
        ''' Plots the mesh

        Parameters
        -----------
        ax: matplotlib.axes.Axes instace

        regions: instances of pescado.shape.Shapes to be plotted
            If None it uses self._simulated_region.

        interval: Sequence of d tuples of 2 floats
            Defines the interval along each dimension 'd'
            from which to selects the points of the mesh
            to plot.

        ratio: integer
            The mesh point size is 'ratio' times larger
            than the vertex point.

        kwargs: passed to ax.plot()
            Notice, for the vertex points the ms is
            divided by 'ratio'.

        c: color(s) of the cell central point
        '''
        if self._coordinates.shape[1] == 2:

            if regions is None:
                regions = [self._simulation_region, ]

            plot_2d_finalized_mesh(
                mesh_inst=self,
                ax=ax,
                x_lim=interval[0],
                y_lim=interval[1],
                regions=regions,
                ratio=ratio, c=c, **kwargs)
        else:
            raise NotImplementedError(
                'Plotting only implemented for 2D')
    
    def save(self, name):
        ''' Saves current instance of Mesh. Uses pickle. 

        Parameters
        -----------

        name: string
            Path and file name where to save the current Mesh instance
        '''

        data = copy.deepcopy(self.__dict__)
        for key, val in data.items():
            if isinstance(val, SparseVector):
                
                att_data = val.__dict__
                att_module = val.__module__
                att_name = val.__class__.__name__
                
                data[key] = {
                    'dict':att_data, 
                    'module':att_module, 
                    'name':att_name}

        with open(name, 'wb') as f:
            pickle.dump(data, file=f)

    @classmethod   
    def load(cls, name):
        ''' Load saved Mesh instance.

        Note: the file in 'name' contains instances of Shape and Pattern

        Parameters
        -----------
        
        name: string
            Path and file name of the saved Mesh instance

        Returns
        --------
        instance of pescado.mesher.Mesh
        '''

        with open(name, 'rb') as f:
            data = pickle.load(f)

        new_mesh = cls.__new__(cls)
        new_mesh.__dict__ = copy.deepcopy(data)

        for key in ['_index2pat', '_internal_index']:
            att = new_mesh.__dict__[key]
            module_sv = importlib.import_module(att['module'])
            new_mesh.__dict__[key] = getattr(
                module_sv, att['name'])(**att['dict'])

        return new_mesh  
