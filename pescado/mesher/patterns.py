# TODO: Replace asserts by raise when it should deal with a user
#       input or a user related error.
# TODO: Add abstract function to go from real to index value.
# TODO: 1D boundary test ??
# TODO: Check TODO's in Finite

from abc import ABC, abstractmethod
import time
import copy
import collections
import warnings
import functools
from distutils.log import warn
import numpy as np
import numpy.lib.arraysetops as arr_ops

from scipy.optimize import newton, brentq
from scipy import linalg

from pescado.mesher import shapes, voronoi
from pescado.tools import assert_coord_convention, custom_issubdtype
from pescado.tools.meshing import (kd_where,
    grid, external_boundary, internal_boundary)
from pescado.tools.meshing import unique

from pescado.tools import meshing, reduced_array

class Pattern(ABC):
    ''' Generates a pattern
    '''
    @abstractmethod
    def __call__(self, tags):
        ''' Returns the real space coordinates for the given pattern tags

        Parameters
        -----------
        tags: numpy.array of immutable object with shape(m, ) or shape(m, d)
            Tags of the pattern points.
            m number of points
            d number of dimensions for the index

        Returns
        -------
        Real space coordinates of the pattern 'tags' with shape(m, d)

        Note
        -----
        The dimension of the tag does not need to be the same
        as the dimension of the real space coordinate to which it is
        associated.
        '''

        pass

    @abstractmethod
    def inside(self, shape):
         '''Returns the tags inside shape for the given pattern

        Parameters
        -----------
        shape: instance of 'pescado.continuous.shapes.Shape'

        Returns
        --------
        numpy.array of immutable object with shape(m, ) or shape(m, d)
            Tags of the pattern points inside 'shape'.

            m number of points
            d number of dimensions for the index

         '''

         pass

    def around(self, center, radius):
        ''' Return a list of indices around 'pt' within a distance 'radius'

        Parameters
        -----------
        center: sequence of d floats
            real space coordinates of the center

        radius: sequence of d floats
            radius around 'pt' where we will look for points
            inside the pattern in 'self'

        Returns
        -------
        numpy.array of immutable object with shape(m, ) or shape(m, d)
            Tags of the pattern points.

            m number of points
            d number of dimensions for the index

        '''

        if not isinstance(radius, collections.abc.Sequence):
            radius = [radius, ] * len(center)

        hyper_shpere = shapes.Ellipsoid(center=center, radius=radius)
        pts_tags = self.inside(hyper_shpere)

        # Sort according to the distance from center
        min_dis_sort = np.argsort(
            np.sum(np.abs(self(pts_tags) - center) ** 2, axis=1))

        return pts_tags[min_dis_sort]

    @abstractmethod
    def first_neighbours(self, points_tag, reduce_array=True):
        '''Return the first neighbours of points

        Parameters
        ----------
        points_tag: np.ndarray of immutable objects with shape(m, d)
        or shape(m, )
            Tags of the points to find the neighbours
            m number of pts
            d their dimension

        reduced_array: boolean (default False)
            If True then 'first_neighbours' returns a 
            'tags', 'neighbours' and 'point_neighbours'

        Returns
        -------
        If reduced_array = False
            neighbours: list of m np.ndarray of immutable objects 
                with shape(n, d) or shape(n, )
                
                The tags of the first neighbours of 'points_tag'

        If reduced_array = True
            tags: np.ndarray of immutable objects with shape (n, d)
            or shape (n, )
                
                Tags of the first neighbours of 'points_tag'

            neighbours: np.ndarray of integers with shape(n, )
                tags indices of the first neighbours for each element in 
                'points_tag'

            point_neighbours: np.ndarray of integer with shape(m+1)
                Interval in 'neighbours' containing the first 
                neighbours for each element in 'points_tag'

                To obtain the neighbour tags of point_tag[i]:
                    
                    tags[neighbours[point_neighbours[i]:point_neighbours[i+1]]]

        '''

        pass
        
    def neighbours(self, points_tag, nth=1, reduce_array=True):
        '''Return the 'nth'-first neighbours of pt

        Parameters
        ----------
        points_tag: np.ndarray of immutable objects with shape(m, d)
        or shape(m, )
            Tags of the points to find the neighbours
            m number of pts
            d their dimension

        nth: float

        reduced_array: boolean (default False)


        Returns
        -------
        list of np.ndarray of immutable objects with shape(n, d) or shape(n, )
            containing the nth-first neighbours

        Note
        -----
        This implementation profits from first_neighbours being a
            vectorized function.
        '''

        if nth < 1:
            raise ValueError(
                "'nth' must be greater or equal to 1"
                + ", however {0} was given".format(nth))
        
        # Test if the tag has the correct shape by calling the Pattern instance
        try:
            self(points_tag[0])
        except Exception as e:
            raise ValueError(
                'Wrong format for "points_tag" in self.neighbours \n'
                + '"_call_" function error message is : {0}'.format(e))
        
        if len(points_tag) > 0:

            tags, neighbours, points_neighbour = self.first_neighbours(
                points_tag=points_tag, reduce_array=True)

        else:
            raise ValueError('points_tag is empty')

        old_points_tag = points_tag
        if nth > 1:
            for j in range(nth - 1):
                current_points_tag = tags

                tags, neighbours, points_neighbour = self._second_neig(
                    points_tag=old_points_tag, 
                    neigs_tag=tags, neigs=neighbours,
                    points_neig=points_neighbour)

                old_points_tag = current_points_tag
        elif nth == 0:
            neighbours = old_points_tag

        if reduce_array:
            return tags, neighbours, points_neighbour
        else:
            return np.split(tags[neighbours], points_neighbour[1:-1])

    def _second_neig(self, 
        points_tag, neigs_tag, neigs, points_neig):
        ''' Return the voronoi second neighbours for 'points_tag'

        Parameters
        -----------

        points_tag: np.ndarray of immutable objects with shape (n, d)
        or shape (n, )           

        neigs_tag: np.ndarray of immutable objects with shape (n, d)
        or shape (n, )
            
            Tags of the first neighbours of 'points_tag'

        neigs: np.ndarray of integers with shape(n, )
            Indices of the first neighbours for each element in 
            'points_tag'

        points_neig: np.ndarray of integer with shape(m+1)
            Interval in 'neighbours' containing the first 
            neighbours for each element in 'points_tag'

        Returns
        --------
        sneig_tags: np.ndarray of immutable objects with shape (n, d)
        or shape (n, )
            Tags containing all second neighbours of 'points_tag'
        
        sneigs: np.ndarray of integers with shape(n, )
            'sneig_tags' indices of the second neighbours for
            each element in 'points_tag'
        
        points_sneig: np.ndarray of integer with shape(m+1)
            Interval in 'sneigs' containing the first 
            neighbours for each element in 'points_tag'

            The tags of the second neighbours for point_tag[i] : 
                
                sneig_tags[sneigs[points_sneig[i]:points_sneig[i+1]]]

        '''        

        sneig_tags, sneigs, neig_sneig = self.first_neighbours(
             points_tag=neigs_tag, reduce_array=True)
        
        point_index = np.empty(len(points_tag), dtype=int)
        point_idx_, pts_inside, kdtree = kd_where(
            self(points_tag), self(sneig_tags), return_tree=True)
        point_index[pts_inside] = point_idx_

        # Search for neigs_tag in sneig_tags
        neig_idx = np.empty(len(neigs_tag), dtype=int)
        neig_idx_, neig_inside = kd_where(
            self(neigs_tag), kdtree=kdtree)
        neig_idx[neig_inside] = neig_idx_
        
        sneig_list = [[] for i in range(len(points_neig) -1)]
        for i in np.arange(len(points_neig) - 1, dtype=int):

            pt_neig_idx = neigs[points_neig[i]:points_neig[i+1]]
            sneig_list[i] = reduced_array.elements_value(
                elements=pt_neig_idx, indices=neig_sneig, values=sneigs, 
                assume_sorted=True)
                       
            rm = neig_idx[pt_neig_idx[neig_inside[pt_neig_idx]]]
            if pts_inside[i]:
                rm = np.concatenate([[point_index[i],], rm])

            sneig_list[i] = np.setdiff1d(sneig_list[i], rm)
            
        sneigs, points_sneig = reduced_array.make_from(sneig_list)
        
        return sneig_tags, sneigs, points_sneig

    @abstractmethod
    def ridges_and_vertices(self, 
        points_tag, neig_tags, neighbours, point_neighbours):
        '''Return the 'ridges' and 'vertices' of points

        Parameters
        ----------
        points_tag: np.ndarray of immutable objects with shape(n, d)
        or shape(n, )
            Tags of the points to find the neighbours
            n number of pts
            d their dimension

        (neig_tags, neighbours, 
         point_neighbours): output of self.first_neighbours(
            self, point_tags, reduce_array=True)

        Returns
        -------

        vertices: np.ndarray of floats shape(v, dim)
            Real space coordinates of dimension dim
            for the vertices forming the ridges of 
            the voronoi cells of 'points_tag'               
                
        ridge_vertices: np.ndarray of integers shape(m, )
            'vertices' indices of the vertices coordinates of
            each element in ridges
        
        ridges: np.ndarray of integers shape(m+1)
            Interval in 'ridge_vertices' containing the 'vertices' indices
            for each ridge
            
            s.t. the indices of 'vertices' forming the ridge 'm'
            is obtained with :
                ridge_vertices[ridges[m]:ridges[m+1]]
        
        point_ridges: np.ndarray of integers shape(r, )
            The ridges indices for each point in points_tag.
            Follows the same ordering as point_neighbours
            Hence for point i do :
                r = point_ridges[
                    point_neighbours[i]:point_neighbours[i+1]]
            
                
        region_vertices: np.ndarray of integers shape(v)
            indices of the 'vertices' defining each element in regions

        regions: np.ndarray of integers shape(n+1)
            Interval in 'region_vertices' containing the 'vertices' indices
            for each region in 'points_tag'
        '''

    pass

    def geometrical_properties(self, points_tag, _property):
        ''' Calculates the - geometrical properties - for 'points_tag'. 
        By geometrical properties I mean -> distances, surfaces and volumes. 

        Parameters
        -----------
        points_tag: np.ndarray of immutable objects with shape(n, d)
        or shape(n, )
            Tags of the points to find the neighbours
            n number of pts
            d their dimension

        _property: Sequence of strings
            'surface', 'volume', 'distance', 'safety_radius'
                
                Name of the properties to be calculated. It is more
                efficient to calculate all at once. 

        Returns
        --------
        property_dict: dictionary
                key:_property
                value: np.ndarray of floats 
            
        neig_tag: np.ndarray of immutable objects with shape (n, d)
            or shape (n, )
                
                Tags of the first neighbours of 'points_tag'

        neighbours: np.ndarray of integers with shape(m, )
            Mesh points index of the neighbours of 'points' whom they share a 
            'surface' or 'distance'. 

        point_properties: np.ndarray of integers of shape(n+1, )
            In case a surface or distance is requested, interval 
            in 'value' that corresponds to the properties of a element 
            in point.  

            surf_i = dict['surface'][point_properties[i]:point_properties[i+1]]
            dist_i = dict['distance'][point_properties[i]:point_properties[i+1]]
            volume_i = dict['distance'][i]
            safety_radius_I = dict['safety_radius'][i]

        '''

        if isinstance(self, Regular):
            return self._geometrical_properties(
                points_tag=points_tag, _property=_property)
        else:

            neig_tags, neighbours, point_neighbours = self.first_neighbours(
                points_tag=points_tag, reduce_array=True)

            (vertices, ridge_vertices, ridges,
             point_ridges, region_vertices, regions) = self.ridges_and_vertices(
                points_tag=points_tag, neig_tags=neig_tags,
                neighbours=neighbours, point_neighbours=point_neighbours)
            
            property_dict = {i:None for i in _property}

            if 'surface' in _property:
                surfaces = meshing.surfaces(vertices, ridge_vertices, ridges)
                property_dict['surface'] = surfaces[point_ridges]

            if 'distance' in _property:
                
                neigs_coord = self(neig_tags)
                points_coord = self(points_tag)

                property_dict['distance'] = meshing.distances(
                    points_coord=points_coord,
                    neig_coord=neigs_coord,
                    neig=neighbours,
                    points_neig=point_neighbours)
            
            if 'volume' in _property:
                property_dict['volume'] = meshing.volumes(
                    vertices=vertices, region_vertices=region_vertices,
                    regions=regions)
            
            if 'safety_radius' in _property:
                property_dict['safety_radius'] = meshing.safety_radius(
                    points=self(points_tag), vertices=vertices,
                    region_vertices=region_vertices, regions=regions)

        return property_dict, neig_tags, neighbours, point_neighbours

    def safety_radius(self, points_tag):
        '''Returns the surfaces betwen 'points_tag'
        and self.neighbours(points_tag)

        Notice - If the pattern does not heritate from 'Regular', then 
        if you already have calculated the ridges or the neighbours
        it is faster to use 'pescado.tools.meshing.safety_radius'. 

        Parameters
        -----------
        points_tag: np.ndarray of immutable objects with shape(n, d)
        or shape(n, )
            Tags of the points to find the neighbours
            n number of pts
            d their dimension

        Returns
        --------
        safety_radius: np.array of floats with shape(m, )

        ''' 

        property_dict = self.geometrical_properties(
            points_tag=points_tag, _property=['safety_radius'])[0]

        return property_dict['safety_radius']

    def voronoi(self, point_tag):
        '''Return the voronoi cell for point

        Parameters
        ----------
        point_tag: Tag of a point generated by self. 
            Tag of the points to generate local voronoi cell. 

        Returns
        -------
        Instance of VoronoiCell

        '''   

        tags, neighbours, point_neighbours = self.first_neighbours(
            points_tag=point_tag, reduce_array=True)

        (vertices, ridge_vertices, 
         ridges, point_ridges, 
         region_vertices, regions) = self.ridges_and_vertices(
            points_tag=point_tag, 
            neig_tags=tags, neighbours=neighbours, 
            point_neighbours=point_neighbours)
        
        points = self(point_tag)
        
        safety_radius = meshing.safety_radius(
            points=points, vertices=vertices, 
            region_vertices=region_vertices, regions=regions)       
               
        vor_vertices = vertices[region_vertices]            
                
        arg_sr_ridges = np.argsort(point_ridges)
        neighbours = neighbours[arg_sr_ridges]

        ridge_v, itrv = reduced_array.extract_elements(
            elements=point_ridges[arg_sr_ridges], values=ridge_vertices,
            indices=ridges)
        
        ridge_v = kd_where(
            array=vertices[ridge_v], ref_array=vor_vertices)[0]

        voronoi_arr = voronoi.VoronoiCell(
            point=self(point_tag), point_tag=point_tag,
            neighbours=self(tags[neighbours]),
            neighbours_tags=tags[neighbours],
            vertices=vor_vertices,
            ridges=np.split(ridge_v, itrv[1:-1]), 
            safety_radius=safety_radius)
        
        return voronoi_arr

    def boundary(self, shape):
        '''Return the boundary of the shape for the pattern in 'self'

        Parameters
        -----------
        shape: instance of 'pescado.continuous.shapes.Shape'

        Returns
        -------
        internal: numpy.array of immutable objects with shape(m, d)
            contains the shape internal boundary indices

        external: numpy.array of immutable objects with shape(m, d)
            contains the shape external boundary indices.
            The external boundary is defined by the first neighbours of the
            internal boundary.
        '''

        assert isinstance(shape, shapes.Shape), (
            '{0} not a pescado.mesher.shapes.Shape'.format(shape))

        err_msg = ('No internal boundary found for pattern '
                   + ': {0}, shape : {1}'.format(self, shape))
        try:
            internal_tags = self.inside(shape)
        except ValueError as e:
            if str(e)[:75] == (
                'Points with unbounded voronoi cells were found inside shape.'
                + ' Their index is'):
                raise ValueError(err_msg + '. \n' + str(e))
            else:
                raise e

        if len(internal_tags) == 0:
            raise ValueError(err_msg)

        tags, neighbours, points_neighbour = self.neighbours(
            points_tag=internal_tags, nth=1)

        ignore, internal_bdr_idx = internal_boundary(
            internal_coord=self(internal_tags),
            neig_coord=self(tags), neighbours=neighbours, 
            points_neighbour=points_neighbour,
            shape=shape, return_idx=True)

        internal = internal_tags[internal_bdr_idx]
        #External boundary
        tags, neighbours, points_neighbour = self.neighbours(
            points_tag=internal, nth=1)

        # Get only external boundary and remove redundant values.
        ignore, external_idx = external_boundary(
            internal_bdr_neig=self(tags), shape=shape,
            return_idx=True)

        external = tags[external_idx]

        if len(external) == 0:
            raise ValueError(
                'No external boundary found for pattern '
                + ': {0}, shape : {1}'.format(self, shape))

        return internal, external

    def save(self, shape, name_file='mesh_to_vtk', only_boundary=False):
        ''' Write coordinates of 3D voronoi cells into a .vtu file.
            The data could be later visualized using Paraview or Mayavi
            for example

        Parameters
        ----------
        shape: instance of 'pescado.continuous.shapes.Shape'

        name_file: string
            The default is 'mesh_to_vtk'.

        only_boundary: boolean, optional
            The default is False.
            If True, save only the coordinates at the boundary of
            "shape"

        Returns
        -------
        None.

        '''
        try:
            import vtk

            points = vtk.vtkPoints()
            ugrid = vtk.vtkUnstructuredGrid()

            if only_boundary:
                idx = self.boundary(shape)[0]
            else:
                idx = self.inside(shape)
            n = 0

            for i in range(idx.shape[0]):
                voronoi = self.voronoi(idx[i])
                vertices = voronoi.vertices

                for j in range(vertices.shape[0]):
                    points.InsertNextPoint(vertices[j])

                ridges = np.asarray(voronoi.ridges) + n
                ridgesId = vtk.vtkIdList()
                ridgesId.InsertNextId(len(ridges))
                for ridge in ridges:
                    ridgesId.InsertNextId(ridge.size)
                    for k in ridge:
                        ridgesId.InsertNextId(k)

                ugrid.InsertNextCell(vtk.VTK_POLYHEDRON, ridgesId)
                n += len(vertices)

            ugrid.SetPoints(points)

            writer = vtk.vtkXMLUnstructuredGridWriter()
            writer.SetInputData(ugrid)
            writer.SetFileName(name_file + '.vtu')
            writer.SetDataModeToAscii()
            writer.Update()

        except ModuleNotFoundError as e:
            warn('Vtk module not found, can"t use save function : {0}'.format(
                e))

class Finite(Pattern):
    ''' Finite pattern

        TODO: Coordinate precision for dictionary inv_elements.
            Discuss with CG.

        Notes
        ------

        The points in finite are divided into unbounded points (with
        unbounded voronoi cells) and bounded points (with bounded
        voronoi cells). For a shape to be used with an instance
        of Finite, it can only contain bounded points, otherwise the
        inside method raises a ValueError().

        Bounded points can have unbounded points as first neighbours.
        TODO: Decide how we are going to deal with this once mesher.py is done.
        TODO: Once this is decided, add test to verify the behaviour.

        This behavior allows a Shape that contains all bounded points
        inside an instance of Finite to have an external boundary (which
        would be composed of the unbounded points).

    '''

    def __init__(self, elements):
        ''' Returns a finite pattern defined for a given 'elements' dictionary

        Parameters
        -----------

        elements: dict
            key : immutable object, element index
            el : sequence of d floats
                real space coordinate associated with the index
                d real space dimension of the system

            e.g. elements = {-4:[0., -0.5, 0.3],
                             -5:[0.3, -1., 0.5],
                             ...}

            e.g. elements = {'test':[0., -0.5, 0.3],
                             'test2':[0.3, -1., 0.5],
                             ...}
        Returns
        --------
        Instance of pescado.mesher.patterns.Finite

        '''

        self.elements = elements
        
        self.keys = np.empty((len(self.elements.keys()), ), dtype=object)
        self.coordinates = np.empty(
            (len(self.elements.keys()),
             len(list(self.elements.values())[0])), dtype=float)

        self.inv_elements = dict()

        for i, (tag, coord) in enumerate(self.elements.items()):

            key = np.asanyarray(coord)
            self.keys[i] = tag
            self.inv_elements.update({tuple(key): (tag, i)})
            self.coordinates[i, :] = key

        assert_coord_convention(
            coord_array=self.coordinates,
            name='self.values in pescado.mesher.patterns.Finite')

        self.dim = self.coordinates.shape[1]
        
        with warnings.catch_warnings(record=True) as warning_list:

            (values, self.vertices,
             self.ridge_points, self.ridge_vertices, self.ridges,
             self.region_vertices, self.regions) = voronoi.voronoi_diagram(
                 points=self.coordinates)
        
        (self.bounded_region,
         self.unbounded_region) = voronoi.region_type(
            region_vertices=self.region_vertices, 
            regions=self.regions)
        
        # vor_neighbours / vor_ridges / points_neighbours
        # are correlated to internal point indexing. 
        (self.vor_neighbours, self.vor_ridges,
         self.points_neighbours) = voronoi.first_neighbours_qhull(
            ridge_points=self.ridge_points, npoints=len(self.coordinates))

        for warning in warning_list:
            err_msg = ('\n Some points were ignored when calculating the ridges'
                       + ' in the voronoi diagram.')

            if ((str(warning.message)[:78] == err_msg[:78])
                and warning.category is RuntimeWarning):

                unique_index = unique(np.round(
                    np.sort(self.ridge_points.ravel()), decimals=14))[0]
                missing_closed_point = [
                    pt for pt in np.setdiff1d(
                        np.arange(len(self.coordinates)), unique_index)
                    if pt in self.bounded_region]

                assert len(missing_closed_point) == 0, (
                    '\n Some of the points ignored by Qhull when calculating'
                    + ' the ridges in the voronoi diagram are in the'
                    + ' bounded point list. This is not allowed. '
                    + ' The problematic points are : \n'
                    + ' val : {0} \n index: {1} \n'.format(
                        self.coordinates[missing_closed_point],
                        missing_closed_point))
            else:
                warnings.warn(warning.message, warning.category)

    def tags(self, coords):
        ''' Return the tags associated with 'coord'

        Parameters
        ----------

        coords: np.array of floats with shape(m, dim)

        Returns
        --------
        np.array of immutable values
        '''

        return np.array([
            self.inv_elements[tuple(coord)][0] for coord in coords], 
            dtype=object)

    def inside(self, shape):

        if np.any(shape(self.coordinates[self.unbounded_region])):
            raise ValueError(
                ('Points with unbounded voronoi cells were found'
                 + ' inside shape. Their index is : \n'
                 + '{0}'.format(self.unbounded_region[
                     shape(self.coordinates[self.unbounded_region])])))

        points_inside = self.bounded_region[
            shape(self.coordinates[self.bounded_region])]

        return self.keys[points_inside]

    def __call__(self, tags):
                
        if isinstance(tags, np.ndarray):
            coordinates = np.array([self.elements[tag] for tag in tags])
        else:
            coordinates = np.array(self.elements[tags])

        if len(coordinates.shape) == 1:
            coordinates = coordinates[None, :]

        return coordinates
    
    def index(self, points_tag):
        ''' Returns the 'points_indx' for the 'points_tag'. 
        Internal indexing for the vornoi Qhull diagram

        Parameters
        ----------

        points_tag: np.array of immutable objects with shape(m, d)

        Returns
        --------
        np.array of integers objects with shape(m, )
        '''
        
        if isinstance(points_tag, (collections.abc.Sequence, np.ndarray)):
            points_index = np.array(
                [self.inv_elements[tuple(self.elements[tag])][1]
                 for tag in points_tag])
        else:
            points_index = np.array(
                [self.inv_elements[tuple(self.elements[points_tag])][1],])
            
        return points_index

    def first_neighbours(self, points_tag, reduce_array=True):

        points_index = self.index(points_tag=points_tag)

        neighbours, points_neighbours = reduced_array.extract_elements(
            elements=points_index, values=self.vor_neighbours,
            indices=self.points_neighbours)
        
        if not reduce_array:          
            return np.split(
                self.keys[neighbours], points_neighbours[1:-1])
        else:

            unq_neig, neighbours = reduced_array.zip(neighbours)

            return self.keys[unq_neig], neighbours, points_neighbours
    
    def ridges_and_vertices(self,
     points_tag, neig_tags, neighbours, point_neighbours):

        points_index = self.index(points_tag=points_tag)
        # Update the values in neighbours to correspond
        # to the internal indexing of the pattern. 
        neighbours_index = self.index(neig_tags)

        neig_point_ridges = reduced_array.elements_value(
            elements=points_index, values=self.vor_neighbours, 
            indices=self.points_neighbours)        
        
        if not np.all((neig_point_ridges - neighbours_index[neighbours]) == 0):
            raise NotImplementedError(
                "Finite ridges_and_vertices "
                " can't handle neighbours unless directly provided by"
                " self.first_neighbours()")

        ref_counts = reduced_array.counts(
            elements=points_index, indices=self.points_neighbours)        
        counts = reduced_array.counts(indices=point_neighbours)
        
        if np.any((ref_counts - counts) < 0):
            idx = np.arange(len(ref_counts))[(ref_counts - counts) < 0]
            raise RuntimeError(
                'Wrong number of neighbours found in point_neighbours '
                + '{0} - {1} exists and {2} were requested'.format(
                idx, ref_counts[idx], counts[idx]))

        # Get the ridges
        point_ridges = reduced_array.elements_value(
            elements=points_index, values=self.vor_ridges, 
            indices=self.points_neighbours)

        unq_ridges, point_ridges = reduced_array.zip(
            point_ridges)                       
       
        ridge_vertices, ridges = reduced_array.extract_elements(
            elements=unq_ridges, values=self.ridge_vertices,
            indices=self.ridges)
        
        # Do regions and regions_vertices
        region_vertices, regions = reduced_array.extract_elements(
            elements=points_index, values=self.region_vertices,
            indices=self.regions)
    
        # Get the vertices
        vertices, region_vertices, kdtree = reduced_array.zip(
            values=self.vertices[region_vertices], return_kdtree=True)
                
        ridge_vertices, inside = kd_where(
            array=self.vertices[ridge_vertices], kdtree=kdtree)
        
        if not np.all(inside):
            raise RuntimeError(
                'Not all ridge vertices coordinates'
                + 'found among those fo region.')

        if len(np.setdiff1d(region_vertices, ridge_vertices)) > 0:
            raise RuntimeError(
                'region and ridges do not have the same number of vertices')
        
        return (vertices, 
                ridge_vertices, ridges, point_ridges, 
                region_vertices, regions)

    @classmethod
    def points(cls, points):
        ''' Returns a pescado.mesher.patterns.Finite instance
        from a array of poitns

        Parameters
        -----------

        points: np.ndarray of floats with shape(m, d)
            Real space coordinates of the points in the pattern
            m number of points
            d dimension of real space coordinates
        '''

        assert_coord_convention(
            coord_array=points,
            name='points in pescado.mesher.patterns.Finite.points')

        elements_dict = {key:val for key, val in enumerate(points)}

        return cls(elements=elements_dict)
    
    @classmethod
    def lattice_vector(cls, lv_matrix, region):
        ''' Returns a pescado.mesher.patterns.Finite instance by filling
        up a 'region' with points following the lattice vectors defined 
        in 'lv_matrix'.

        Parameters
        -----------
        lv_matrix: np.ndarray of floats with shape(n, d)
            n: number of lattice vectors
            d: number of real space dimensions

            vect_i = lv_matrix[i, :]

        region: instance of pescado.mesher.Shape
            Define the region to be filled with points given a 'lv_matrix'

        Returns
        --------
        Instance of pescado.mesher.patterns.Finite

        Notice
        -------
        If a specific volume cell is required for a large mesh, it is advised
        to make your own Pattern child class heritating from "Regular'. 

        Give the reciprocal lattice vectors, e.g. for hexagonal cells: 

            a = 1
            coef = 2 * np.pi / (3 * a)
            vec_mat = coef * np.array([[1, np.sqrt(3)], [1, -1 *  np.sqrt(3)]])

        Only tested for 1D lattice, 2D and 3D hexagonal lattice.

        '''

        inv_lv_mat = linalg.inv(lv_matrix)
        
        rec_bbox = np.round(
            np.matmul(shapes.bbox2box(region.bbox), inv_lv_mat), 0).astype(int)
        rec_bbox = np.array([np.min(rec_bbox, axis=0),
                             np.max(rec_bbox, axis=0)])
        
        coord_pat = np.matmul(
            meshing.grid(bbox=rec_bbox, step=np.ones(lv_matrix.shape[1])),
            lv_matrix)

        return cls.points(coord_pat[region(coord_pat)])


class Regular(Pattern):
    ''' Defines a Regular Pattern
    '''
    
    @abstractmethod
    def _geometrical_properties(self, points_tag, _property):
        ''' Returns the geometrical properties for point_tags.
        By geometrical properties I mean 
            -> distances, surfaces, volumes and safety_radius

        Parameters
        -----------

        points_tag: np.ndarray of immutable objects with shape(m, d)
        or shape(m, )
            Tags of the points to find the surfaces
            m number of pts
            d their dimension

        _property: Sequence of strings
            'surface', 'volume', 'distance', 'safety_radius'
                
                Name of the properties to be calculated. It is more
                efficient to calculate all at once. 

        Returns
        --------
        property_dict: dictionary
                key:_property
                value: np.ndarray of floats 
            
        point_properties: np.ndarray of integers of shape(n+1, )
            In case a surface or distance is requested, interval 
            in 'value' that corresponds to the properties of a element 
            in point.  

            surf_i = dict['surface'][point_properties[i]:point_properties[i+1]]
            dist_i = dict['distance'][point_properties[i]:point_properties[i+1]]
            volume_i = dict['distance'][i]
            safety_radius_i = dict['safety_radius'][i]

        neig_tag: np.ndarray of immutable objects with shape(n, d)
        or shape(n, )
            Tags of the points to find the surfaces
            n number of unique neighbour tags
            d their dimension
        
        neighbours: np.ndarray of integers with shape(nn, )
            Mesh points index of the neighbours of 'points' whom they share a 
            'surface' or 'distance'. 
            nn >= n
        
        '''
        pass


class Rectangular(Regular):
    '''Returns a rectangular pattern
    '''
        
    def __init__(
        self, ticks, tag2ticks, center=None, assume_constant=False):
        ''' Defines a 1D, 2D or 3D rectangular pattern

        TODO: Improve Docstring

        Parameters
        -----------
        ticks: sequence of d monotonically increasing functions

            Each element 'j' of ticks defines the generating
            function of a sequence.

            Each element 'i' of the sequence 'j' defines
            the size along 'j' of the 'i'th rectangle in the pattern.

            ex: ticks = [gen_fun1, gen_fun2]

                gen_fun1(i) = a
                    i: float
                    the 'i'th element of the sequence defined by gen_fun1
                    a: float
                    'a' the value of the 'i'th element

                gen_fun2(i) = b
                    i: float
                    the 'i'th element of the sequence defined by gen_fun2
                    b: float
                    'b' the value of the 'i'th element

                Pattern element (i, i) is a rectangle with size aXb

        tag2ticks: Inverse function of ticks
            Sequence of d monotonically increasing functions

            Each element 'j' of ticks defines the inverse function
            of the generation function in ticks s.t:

                tag2ticks[i](ticks[i](val)) = val

        center: sequence of d floats, default is (0, ) * d
            The center of the pattern

        assume_constant: boolean (default False)
            Whether Rectangular assumes the point spacing to be
            constant. Considerably speeds up _geometrical_properties.

        Returns
        --------
        An instance of 'pescado.continuous.patterns.Pattern'

        '''

        self.assume_constant = assume_constant
        self.dim = len(ticks)
        self.ticks = ticks

        self.tag2ticks = tag2ticks

        if center is None:
            center = [0., ] * self.dim

        self.center = np.asanyarray(center, dtype=float)[None, :]
        assert self.center.shape[1] == self.dim

        shapes.assert_coord_convention(
            self.center, name='center for Rectangular')

    def tags(self, coords, closest=True, tol=1e-13):
        ''' Return the tags associated with the closest value to 'coord'

        Parameters
        ----------

        coords: np.array of floats with shape(m, dim)

        closest: bool, default True
            If True returns the tag corresponding to the mesh
            point closest to the coordinate.

        tol: float, default 1e-13
            Tolerance when comparing two floats

        Returns
        --------
        np.array of immutable values
        '''
        res = np.around(np.array(
            [self.tag2ticks[d](coords[:, d] - self.center[0, d])
             for d in range(self.dim)]).T).astype(int)
        
        if not closest:
            error = np.abs(coords - self(res))
            if np.any(error) > tol:
                err_msg = ('The coordinates {0} '.format(
                                coords[np.any(error > tol, axis=1)])
                           + ' were not found in the pattern.'
                           + ' The error is {0}'.format(
                               error[np.any(error > tol, axis=1)]))

                raise ValueError(err_msg)

        return res

    def inside(self, shape, n_max=1e6):
        ''' Fills up a 'shape' using the current rectangular pattern

            Parameters
            ----------
            shape: instance of pescado.mesher.shapes

            n_max: integer - default 1e6
                Generates the rectangular sites inside shape.bbox by
                groups of 1e6 elements. Optmization to reduce memory
                consumption.


        '''
        assert shape.bbox.shape[1] == self.dim, (
            'Shape must have the same dimension as pattern')

        bbox_tags = self.tags(shape.bbox)
        # Add one element in case the shape.bbox is found
        # between two Pattern points
        fact = np.ones(bbox_tags.shape, dtype=int)
        fact[0, :] = fact[0, :] * -1
        bbox_tags += fact
        
        if (np.any((shape.bbox[0, :] - self(bbox_tags[0, :])) < 0)
            or np.any((shape.bbox[1, :] - self(bbox_tags[1, :])) > 0)):
            err_msg = (
                'Pattern'
                '{0} underestimates the bounding box for shape {1}. \n'.format(
                    self, shape))
            err_msg_1 = (
                'The bounding_box value is: \n'
                + '{0} \nwhile pescado found: \n {1}.'.format(
                    shape.bbox, self(bbox_tags)))
            err_msg2 = (
                '\n It might be related to rounding of floating point numbers'
                + ' in the user defined "tag2ticks" method. If the floating'
                + ' point coordinate is rounded to its lowest integer value for'
                + ' tag then it might cause the bounding box tag to be'
                + ' underestimated. We advise the use of np.round() before'
                + ' converting the float to an integer value.')

            raise RuntimeError(err_msg + err_msg_1 + err_msg2)

        if n_max is None:
            tags = self.inside_bounding_box(shape=shape, bbox_tags=bbox_tags)
        else:
            tags = self.inside_bounding_box_multiple(
                shape=shape, n_max=n_max, bbox_tags=bbox_tags)

        return tags

    def inside_bounding_box(self, shape, bbox_tags, tag_constant=None):
        ''' Fiils up the 'bbox_tags' with sites of 'self' using a single
        call to tools.meshing.grid.

        Parameters
        ----------

        shape: instance of pescado.mesher.shapes

        bbox_tags: np.array of shape(2, d)
            The (min, max) values delimiting the region to be
            filled with the sites from 'self'

        tag_constant: sequence of n floats, with self.dim = d + n

            The region defined by 'bbox_tags' doesn't need to have the same
            dimensions as the current pattern ('self').

            However, when d < self.dim, then one must give a value for the
            tags along the missing dimensions.

            This is the value in tag_constant.

        Returns
        --------

        tags: np.array(m, n + d = self.dim)
            Tags inside shape.

        '''
        center_id = np.average(bbox_tags, axis=0).astype(np.int64)

        tags = grid(
            bbox=bbox_tags, 
            step=np.array([1, ] * bbox_tags.shape[1], dtype=np.int64),
            center=center_id).astype(int)

        if tag_constant is not None:
            tags = np.hstack((
                    tags,
                    np.repeat(np.array(tag_constant)[None, :],
                              repeats=tags.shape[0], axis=0)))

        tags = tags[shape(self(tags))]

        return tags

    def inside_bounding_box_multiple(
        self, shape, bbox_tags, n_max=1e6, tag_constant=None):
        ''' Fills up bbox_tags with points inside 
            'shape' using multiple calls to tools.meshing.grid()

            Parameters:
            -----------
            shape: instance of pescado.shapes

            n_max_: integer,
                Maximum number of points generated by each call to tools.meshing.grid()

            bbox_tags: np.array of shape(2, d)
                The (min, max) values delimiting the region to be
                filled with the sites from 'self'

            tag_constant: sequence of n floats, with self.dim = d + n

                The region defined by 'bbox_tags' doesn't need to have the same
                dimensions as the current pattern ('self').

                However, when d < self.dim, then one must give a value for the
                tags along the missing dimensions.

                This is the value in tag_constant.

            Returns
            --------
            tags: np.array with shape(m, n + d)

                The sites contained in the bbox_tags that are inside 'shape'
            '''

        n_elements = np.diff(bbox_tags, axis=0)[0]
        ite = np.prod(n_elements) / n_max

        tags_list = list()

        # If the number of elements is smaller than n_max
        if ite <= 1. :
            tags_list.append(self.inside_bounding_box(
                shape=shape, bbox_tags=bbox_tags, tag_constant=tag_constant))

        # If the number of elements along the last dimension is larger
        # then the number of calls to tools.meshing.grid -> # of iterations.
        # Split the bbox_tags along the last dimension
        elif int(ite) < (n_elements[-1] - 1):

            intervals = np.linspace(
                *bbox_tags[:, -1], num=int(ite) + 1, dtype=int)

            bb = copy.deepcopy(bbox_tags)

            for i in range(len(intervals) - 1):

                bb[0, -1] = intervals[i] + (1 - (i == 0))
                bb[1, -1] = intervals[i + 1]

                tags_list.append(
                    self.inside_bounding_box(
                        shape=shape, bbox_tags=bb, tag_constant=tag_constant))

        # If the number of elements along the last dimension is smaller than
        # the number of iterations.
        else:

            for tag in range(bbox_tags[0, -1], bbox_tags[1, -1] + 1):

                if tag_constant is None:
                    tag_constant_ = [tag, ]
                else:
                    tag_constant_ = [tag, ] + tag_constant

                tags_list.append(
                    self.inside_bounding_box_multiple(
                        shape=shape, bbox_tags=bbox_tags[:, :-1],
                        n_max=n_max, tag_constant=tag_constant_))


        return np.concatenate(tags_list)

    def first_neighbours(self, points_tag, reduce_array=True):

        if len(points_tag.shape) == 1:
            points_tag = points_tag[None, :]
        elif len(points_tag.shape) > 2:
            raise ValueError(
                'Wrong format for points_tag array: \n {0}'.format(points_tag))

        assert_coord_convention(
            points_tag, name='Points in Rectangular.first_neighbours')
        
        idx = np.empty(self.dim * 2, dtype=int)
        idx[np.arange(0, self.dim * 2, 2)] = np.arange(0, self.dim)
        idx[np.arange(1, self.dim * 2, 2)] = np.arange(self.dim, self.dim * 2)
        neig_motif = np.concatenate(
            (np.identity(self.dim, dtype=int),
             -1 * np.identity(self.dim, dtype=int)))[idx]

        shift_neig = np.tile(neig_motif, reps=(len(points_tag), 1))

        neig_idx = np.repeat(
            np.arange(len(points_tag)), repeats=self.dim * 2, axis=0)

        neighbour_tags = points_tag[neig_idx] + shift_neig

        if not reduce_array:          
            return np.split(neighbour_tags, len(points_tag))
        else:
            tags, neighbours = reduced_array.zip(neighbour_tags)            
            point_neighbours = np.arange(0, len(neighbours) + 1, self.dim * 2)
            
            return tags, neighbours, point_neighbours

    def _vertices_and_ridges(self, 
        points_coord, neig_coord, point_neighbours):
        ''' Calculates the 'vertices' and 'ridges' for 'points_coord'
            and 'neigs_coord'. 

        Parameters
        -----------
        points_coord: np.ndarray of floats with shape(m, d)
            Tags of the points to calculate the ridges and vertices
            m number of pts
            d their dimension

        neig_coord: np.ndarray of floats with shape(m, d)
            Neighbours of points_coord
            m number of pts
            d their dimension       
        Returns
        --------        
        ridges: np.ndarray of integers with shape(n * dim * 2, c)
            
            Each point has dim * 2 ridges
            c = 1 (1d), 2 (2D), 3 (3D)
            
            The element index in 'vertices' for the vertices
                forming the ridge. 

        vertices: np.ndarray of floats with shape(v, dim)
            The coordinates for the vertices

        '''

        repeats = reduced_array.counts(point_neighbours)
        
        if self.dim == 1:
            rp_pts = np.repeat(points_coord, repeats=repeats, axis=0)    
            vertices = (rp_pts + neig_coord) / 2
            ridges = np.arange(0, len(neig_coord), dtype=int)[:, None]

        elif self.dim in [2, 3]:
            
            if self.dim == 2:
                perm_vert = [0, 2, 1, 3]
                perm_vert = np.vstack(
                    (perm_vert, np.roll(perm_vert, shift=-1, axis=0))).T
                perm_ridges = [[0, 3], [1, 2], [0, 1], [2, 3]]

            elif self.dim == 3:
                perm_3D = np.array(
                    [[0, 2, 4], [0, 3, 4], [0, 3, 5], [0, 2, 5]])
                ones = np.repeat(
                    np.array([[1, 0, 0]]), repeats=len(perm_3D), axis=0)
                perm_vert = np.concatenate([perm_3D, perm_3D + ones])

                perm_ridges = np.array(
                    [[0, 1, 2, 3], [4, 5, 6, 7], [0, 4, 7, 3],
                    [1, 5, 6, 2], [0, 4, 5, 1], [2, 6, 7, 3]])
            
            ridges = (
                np.tile(perm_ridges, reps=(len(points_coord), 1))
                + np.repeat(
                    np.arange(
                        0, len(points_coord) * len(perm_vert), len(perm_vert)),
                    repeats=len(perm_ridges))[:, None])

            vertex_motif = (
                np.tile(perm_vert, reps=(len(points_coord), 1))
                + np.repeat(
                    point_neighbours[:-1], 
                    repeats=len(perm_vert))[:, None])

            rp_pts = np.repeat(points_coord, repeats=len(perm_vert), axis=0)
            
            diff = np.sum(
                [(neig_coord[vertex_motif[:, i]] - rp_pts) / 2
                 for i in range(self.dim)], axis=0)

            vertices = (rp_pts + diff)
        else:
            raise RuntimeError('Dimensions too large')
        
        return vertices, ridges        

    def ridges_and_vertices(
        self, points_tag, neig_tags, neighbours, point_neighbours):
        
        all_vertices, ridges_arr = self._vertices_and_ridges( 
            points_coord=self(points_tag), 
            neig_coord=self(neig_tags)[neighbours], 
            point_neighbours=point_neighbours)

        ridges_vertices, ridges = reduced_array.make_from(ridges_arr)

        vertices, region_vertices = reduced_array.zip(all_vertices)
        
        ridge_vertices = region_vertices[ridges_vertices]
        region = np.arange(0, len(all_vertices) + 1, 2 ** self.dim, dtype=int)
        return (vertices, ridge_vertices, ridges, 
                np.arange(len(neighbours), dtype=int), region_vertices, region)
    
    def _geometrical_properties(self, points_tag, _property):
        
        neigs_tag, neighbours, points_neighbour = self.first_neighbours(
            points_tag=points_tag, reduce_array=True)

        if self.assume_constant:
            # Calculate it for only one point
            i = 0
            i_neig = neighbours[points_neighbour[i]:points_neighbour[i+1]]
            i_neigs_tag = neigs_tag[i_neig]
            property_dict = self._geometrical_properties_internal(
                points_tag=points_tag[i][None, :], _property=_property, 
                neigs_tag=i_neigs_tag, 
                neighbours=np.arange(len(i_neigs_tag), dtype=int),
                points_neighbour=np.array([0, len(i_neig)], dtype=int))
            
            for key in property_dict.keys():
                if key in ['distance', 'surface']:
                    property_dict[key] = (
                        np.tile(property_dict[key], reps=len(points_tag)))
                elif key in ['volume', 'safety_radius']:
                    property_dict[key] = (
                        np.ones(len(points_tag)) * property_dict[key])
        else:
            property_dict = self._geometrical_properties_internal(
                points_tag=points_tag, _property=_property, 
                neigs_tag=neigs_tag, neighbours=neighbours,
                points_neighbour=points_neighbour)
        
        return property_dict, neigs_tag, neighbours, points_neighbour
    
    def _geometrical_properties_internal(
            self, points_tag, _property, 
            neigs_tag, neighbours, points_neighbour):
        
        property_dict = {i:None for i in _property}
        
        points_coord = self(points_tag)
        neig_coord = self(neigs_tag)

        distances = np.ones(
            (max(points_neighbour) * 2, points_coord.shape[1]), dtype=float)

        distances[np.arange(1, max(points_neighbour) * 2, 2), :] = np.repeat(
            points_coord, 
            repeats=reduced_array.counts(points_neighbour), axis=0)
    
        distances[np.arange(0, max(points_neighbour) * 2, 2), :] = neig_coord[
            neighbours]

        distances = (
            distances[np.arange(0, len(distances), 2), :]
            - distances[np.arange(1, len(distances), 2), :])

        # Find the length of the sides of the rectangular polygon
        tol = 1e-12
        idx_perp = [np.arange(len(distances), dtype=int)[
            np.abs(distances[:, d]) > tol] for d in range(self.dim)]
        
        sides = [meshing.points_distance(distances[idx, :]) / 2
                 for idx in idx_perp]
        
        if 'distance' in _property:
            property_dict['distance'] = np.sqrt(np.sum(
                distances * distances, axis=1))
        
        if 'surface' in _property:
            _surfaces = np.ones(len(distances), dtype=float)
            if self.dim == 2:
                for i in range(self.dim):
                    _surfaces[idx_perp[(i + 1) % self.dim]] = np.repeat(
                        sides[i], repeats=2)
            elif self.dim == 3:
                for i in range(self.dim):
                    _surfaces[idx_perp[(i + 2) % self.dim]] = np.repeat(
                        sides[i] * sides[(i + 1) % self.dim], repeats=2)       
            
            property_dict['surface'] = _surfaces
       
        if 'volume' in _property:

            _volumes = np.ones(len(points_tag), dtype=float)
            for i in range(self.dim):
                _volumes = _volumes * sides[i]            
            
            property_dict['volume'] = _volumes
        
        if 'safety_radius' in _property:
            
            furthest_vertex = np.zeros((len(points_tag), self.dim))
            for i in range(self.dim):
                furthest_vertex[:, i] = np.max(np.vstack(
                    np.split(np.abs(distances[idx_perp[i], i]) * 1 / 2, 
                             len(points_tag))), axis=1)

            property_dict['safety_radius'] = meshing.vertex_safety_radius(
                centered_vertex=furthest_vertex)

        return property_dict

    def __call__(self, tags):
        
        if isinstance(tags, list):
            dtp = type(tags[0])
        elif isinstance(tags, np.ndarray):
            dtp = tags.dtype
        elif not isinstance(tags, collections.abc.Sequence):
            dtp = type(tags)
        else:
            raise ValueError('{0} can"t be called with type {1}'.format(
                self, type(tags)))

        tags = np.asarray(tags, dtype=dtp)
        
        if len(tags.shape) == 1:
            tags = tags[None, :]
        elif len(tags.shape) != 2:
            raise ValueError('tags do not have the correct format')

        assert_coord_convention(tags, name='Pattern indices in __call__')

        if np.any((tags - tags.astype(int)) != 0.0):
            err_msg = ('The tags in a Rectangular pattern'
                    + 'can be of float type, but must '
                    + 'belong to the Integer ensemble.')
            raise ValueError(err_msg)

        real_coordinates = np.array(
            [self.ticks[d](tags[:, d])
             for d in range(self.dim)]).T + self.center

        if len(real_coordinates.shape) == 1:
            real_coordinates = real_coordinates[None, :]

        return real_coordinates

    @classmethod
    def constant(cls, element_size, center=None):
        """ Returns an instance of pescado.mesher.patterns.Rectangular with
        a rectangular pattern of constant size

        Parameters
        -----------
        element_size : sequence of d floats
            rectangular pattern element size

        center : sequence of d floats
            center of the pattern

        Returns
        -------
        'pescado.mesher.patterns.Rectangular'

        Note
        ----
        This is the future syntax in __init__(). Here until a permanent solution
        to remove tag2ticks is found.

        """
        ticks= [functools.partial(constant_rectangle, step=s)
                for s in element_size]

        tag2ticks = [functools.partial(constant_rectangle, step=(1 / s))
                for s in element_size]

        return cls(
            ticks=ticks, tag2ticks=tag2ticks, center=center,
            assume_constant=True)


def constant_rectangle(tags=None, step=None):
    ''' Returns tags * step

    Parameters
    -----------

    tags: np.ndarray of floats

    step: float

    Returns
    --------
    np.ndarray of floats
    '''

    return tags * step


def find_interval(fun, max_index=1e9):
    ''' Finds interval for scipy.optmize.brentq

    Parameters
    ----------
    fun: function
        fun is a injective monotone increasing function defined s.t
            fun(float) -> float

    max_index: interger
        maximum possible index that can be used as input for fun

    Returns:
    a, b: max and min interval respectively

    Note:
    -----
    Works for monotone function only
    '''

    assert fun(2) > fun(1), ('The function is not "increasing"')

    ite = 0.
    bound = 0.
    sign = fun(bound) * -1 #If > 0 decrease, if < 0 increase

    while ((sign * fun(bound)) <= 0.0
            and np.abs(bound) <= max_index):

        ite += 1 * sign
        old_bound = bound
        bound = (ite ** 2) * np.sign(ite)

    return min((bound, old_bound)), max((bound, old_bound))


def with_brentq(fun, max_index=1e9):
    '''Returns the root of fun using scipt.optmize.brentq

    Parameters
    ----------
    fun: function
        fun is a injective monotone increasing function defined s.t
            fun(float) -> float

    max_index: interger
        maximum possible index that can be used as input for fun

    Returns
    -------
    Index: float
        Returns a float i s.t. fun(i) is closest to zero.

    Note
    -----
    Works for monotone function.

    '''

    min_int, max_int = find_interval(fun=fun, max_index=max_index)

    if (max([np.abs(max_int), np.abs(max_int)]) > max_index):
        raise ValueError('Can"t find root, {1} too larger, {1} > {0}'.format(
            max_index,
            (np.sign(max(np.abs(max_int), np.abs(max_int)))
             * max(np.abs(max_int), np.abs(max_int)))))
    else:
        index = brentq(
            fun, a=min_int, b=max_int, xtol=1)

    return index


def find_root(
    fun, test_fun, initial_guess=0,
    max_ite_newton=50, max_value=1e9):
    '''Returns the 'value' such that fun('value') is closest to zero.

    Parameters
    ----------
    initial_guess: integer with default 0

    fun: function
        fun is a injective monotone increasing function defined s.t
            fun(float) -> float

    max_ite_newton: interger
        argument passed to scipy.optmize.newton as maxiter=max_ite_newton

    max_index: interger
        maximum possible index that can be used as input for fun

    test_fun: function taking as input 'value' and 'fun'
        Raises RuntimeError('Failed to find inverse function') if
        'value' is not root for 'fun'.

    Returns
    -------
    value: float
        Returns a float 'value' s.t. fun('value') is closest to zero.

    Notes
    ------

    First attempt using scipy.optmize.newton.

    Second attempt using pescado.mesher.patterns.with_brentq.
    '''

    try:
        value = newton(fun, x0=initial_guess, maxiter=max_ite_newton)

        if not test_fun(value, fun):
            raise RuntimeError(
                'Failed to find root function using scipy.optmize.newton')

    except RuntimeError as e:
        print(
            'Failed to find inverse using scipy.optmize.newton method : \n')

        if (str(e) ==  ('Failed to find inverse function')
            or str(e) == (
                'Failed to find root function using scipy.optmize.newton')):
            print('The value {0} found is not the correct one'.format(value))

        elif (str(e)[:str(e).find(',')]
              == 'Failed to converge after {0} iterations'.format(
                  max_ite_newton)):
            print('   Scipy error msg -> RuntimeError : {0} \n '.format(str(e)))

        elif (str(e)[str(e).find('F'):str(e).find('after') + 5]
              == 'Failed to converge after'):
            print('   Scipy error msg 2 -> RuntimeError : {0} \n '.format(str(e)))
        else:
            raise RuntimeError(str(e))

        print('Will try with brentq')

        value = with_brentq(fun, max_value)
        if not test_fun(value, fun):
            raise RuntimeError(
                'Failed to find inverse function using brentq')

    return value


def remove(array_1, array_2, tol=1e-15):
    ''' Remove elements in array_1 that are present in array_2

    If array_1 is a numpy.array of floats / int or doubles, then
    the function _mesher.unique is used.

    Parameters
    -----------
    array_1: np.ndarray of immutable objects with shape(m, ) or shape(m, d)

    array_2: np.ndarray of immutable objects with shape(m, ) or shape(m, d)

    tol: float with default 1e-15

    Returns
    -------
    np.ndarray of immutable objects with shape(e, d) or shape(e, )
        with e the number of elements in array_1 and not in array_2
        elements of array_1 not in array_2

    '''

    if len(array_1.shape) != len(array_2.shape):
        raise ValueError('array_1 elements not of the same type as array_2')

    # Avoid problems when a value is an instance of Sequence
    if len(array_1.shape) == 1:
        reshaped_array_1 = array_1[:, None]
        reshaped_array_2 = array_2[:, None]
    elif len(array_1.shape) == 2:
        reshaped_array_1 = array_1
        reshaped_array_2 = array_2
    elif len(array_1.shape) > 2:
        raise NotImplementedError(
    'patterns.remove not implemented for arrays with > 2 axis')

    if (custom_issubdtype(
        array_1.dtype,  (np.float64, np.float32, np.int32, np.int64))
        and custom_issubdtype(
        array_2.dtype, (np.float64, np.float32, np.int32, np.int64))):

        inside = kd_where(array=reshaped_array_1, 
                          ref_array=reshaped_array_2, dist=tol)[1]
        
        result = reshaped_array_1[np.logical_not(inside)]
    else:

        result = list()
        for val in reshaped_array_1:
            if np.all(~np.all(val == reshaped_array_2, axis=1)):
                result.append(val)
        result = np.array(result)

    if len(array_1.shape) == 1:
        result = np.squeeze(result)

    return result
