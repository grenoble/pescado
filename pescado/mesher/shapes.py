'''
    ABC class for shape definition.
'''

__all__ = ['Shape',
           'General',
           'Ellipsoid',
           'Box',
           'Delaunay']


from abc import ABC, abstractmethod
import collections

import numpy as np
from scipy.spatial import Delaunay as DelaunayScipySpatial
from scipy.spatial import ConvexHull

import copy

from pescado.tools import assert_coord_convention, assert_is_inside_output

# TODO: Check with Xavier / Christoph on how to deal with
# intersection between non-intersecting shapes.

def bbox2box(bbox):
    '''Constructs the coordinates for a rectangle using a bounding box.

    Parameters
    ----------

    bbox: np.array, shape(2, d), floats
        d dimension of the coordinates \n
        [[xmin, xmax], [ymin, ymax], [zmin, zmax]]

    Returns
    --------

    np.array, shape(2 ** d, d), floats \n
    coordinates of the rectangle defined by the bouding box.
    '''

    d = bbox.shape[1]
    mat = np.zeros((2 ** d, d))

    if d > 0:
        mat[-1, :] = np.ones(d)
    if d > 1:
        mat[1:d + 1, :] = np.diag(np.ones(d))
    if d == 3:
        mat[d + 1:2 ** d - 1, :] = np.array([
            [1 , 1, 0],
            [0, 1, 1],
            [1, 0, 1]])

    size = bbox[1, :] - bbox[0, :]
    lower_left = bbox[0, :]

    corner_mat = np.vstack([lower_left,] * (2 ** d))
    size_mat = np.vstack([size,] * (2 ** d)) * mat
    return corner_mat + size_mat


def and_bbox(bbox_seq):
    '''Returns the bbox covering the intersection of all bbox
    Works for and operation.

    Parameters
    ----------
    bbox_seq: sequence of 'pescado.continuous.shapes.Shape.bbox'
        Sequence of 'pescado.continuous.shapes.Shape'
        attribute bbox. The latter is a numpy array of shape
        (2, d), with d the number of dimensions in shape.

    Returns
    -------
    numpy.array, shape(2, d), floats \n
        The bounding box containing all the bounding boxes in
        in 'bbox_seq'. \n
        d is the dimension of the system \n
        [:, 0] -> min, max values for dimension 0.
    '''

    d = bbox_seq[0].shape[1]

    assert all([bbox.shape[1] == d for bbox in bbox_seq])

    # Finds max of min
    global_min = np.max(np.vstack(
        [bbox[0, :] for bbox in bbox_seq]), axis=0)

    # Finds min of max.
    global_max = np.min(np.vstack(
        [bbox[1, :] for bbox in bbox_seq]), axis=0)

    global_bbox = np.vstack([global_min, global_max])
    return global_bbox


def add_bbox(bbox_seq):
    '''Returns the bbox covering the union of all bbox in 'bbox_seq'.
    Works for Union operation.

    Parameters
    ----------
    bbox_seq: sequence of 'pescado.continuous.shapes.Shape.bbox'
        Sequence of 'pescado.continuous.shapes.Shape'
        attribute bbox. The latter is a numpy array of shape
        (2, d), with d the number of dimensions in shape.

    Returns
    -------
    numpy.array, shape(2, d), floats \n
        The bounding box containing all the bounding boxes in
        in 'bbox_seq'. \n
        d is the dimension of the system \n
        [:, 0] -> min, max values for dimension 0.
    '''

    d = bbox_seq[0].shape[1]
    
    assert all([bbox.shape[1] == d for bbox in bbox_seq])

    global_min = np.min(np.vstack(bbox_seq), axis=0)

    global_max = np.max(np.vstack(bbox_seq), axis=0)

    global_bbox = np.vstack([global_min, global_max])

    return global_bbox


def subtract_bbox(bbox1, bbox2):
    '''Return a bbox that contains bbox1 - bbox2
    There is no reason why this should work for a subtaction operation
    in Shapes !! This since there is no reason for the bbox to encompass
    perfectly any shape, in fact, it can't.

    Parameters
    ----------
    bbox1: numpy array of floats with shape (2, d)
        bbox attribute of 'pescado.continuous.shapes.Shape'
        d number of dimensions

    bbox2: numpy array of floats with shape (2, d)
        bbox attribute of 'pescado.continuous.shapes.Shape'
        d number of dimensions

    Returns
    -------
    numpy.array, shape(2, d), floats \n
        The bounding box containing all the bounding boxes of the shapes
        in shapes_list. \n
        d is the dimension of the system \n
        [:, 0] -> min, max values for dimension 0.
    '''

    assert bbox1.shape == bbox2.shape
    concatenated_bbox = np.zeros(bbox1.shape)

    d = bbox1.shape[1]

    bbox_comp = (np.repeat(np.array([1, -1])[:, None], repeats=d, axis=1)
                 == np.sign(bbox1 - bbox2))

    if (np.all(np.sum(bbox_comp, axis=1) == d)
        or np.all(~np.sum(bbox_comp, axis=1) == d)):

        concatenated_bbox = bbox1
    elif (np.any(np.sum(bbox_comp, axis=1) == d)
          and np.any(np.sum(bbox_comp, axis=1) != d)):

        concatenated_bbox[bbox_comp] = bbox2[~bbox_comp]
        concatenated_bbox[~bbox_comp] = bbox1[~bbox_comp]
    else:
        raise ValueError('Can"t concatenate bbox')

    return concatenated_bbox


def rotate(shape, angle, axis, origin=None):
    """Returns a rotated shape object (General shape instance).

    Parameters
    ----------
    shape: `pescado.continous.Shape`
        Input shape instance which should be rotated.

    angle: float
        Counter-clockwise rotation angle.

    axis: sequence of d real numbers
        Axis around which the rotation is performed. \n
        ``axis`` must contain d real numbers, \n
        where d is the dimension of ``shape``.
        The rotation is performed in the plane perpendicular to axis. \n
        e.g. if axis = (1, 0, 0), the rotation is performed in the y-z plane.

    origin: sequence of d real numbers, optional
        Origin of the rotation axis. \n
        ``origin`` must contain d real numbers, where d is the dimension of ``shape``.

    Returns
    --------
    `pescado.continous.General`
        Rotated shape instance.
    """

    return Rotated(
        initial_shape=shape, axis=axis, angle=angle, origin=origin)


def dilatate(shape, dilatation, center):
    """Returns a dilatated shape (General shape instance)

    Parameters
    ----------
    shape: `pescado.continous.Shape`
        Input shape instance which should be translated.

    dilatation: sequence of d real numbers
        Dilatation coefficients \n
        ''dilatation'' must contain d real numbers, where d is the dimension
        of ''shape'''
        e.g (1, 1, 1) equivalent to no dilatation \n
            (2, 1, 1) doubles the dimension along axis 0 \n
            (0.5, 1, 1) decreases by half the dimension along axis 0 \n

    center: sequence of d real numbers
        Center of dilatation

    Returns
    --------
    `pescado.continous.General`
        Dilatated shape instance.

    NOTE - Here for compatibility with old code.
    """

    return Dilated(initial_shape=shape, alpha=dilatation, center=center)


def translate(shape, translation):
    """Returns a translated shape object.

    Parameters
    ----------
    shape: `pescado.continous.Shape`
        Input shape instance which should be translated.

    translation: sequence of d real numbers
        Translation vector. \n
        ``translation`` must contain d real numbers,
        where d is the dimension of ``shape``.

    Returns
    --------
    `pescado.continous.Shape`
        Translated shape instance.
    """

    return Translated(initial_shape=shape, translation=translation)


def reflect(shape, origin, normal):
    """Reflection of a Shape instance across a given line (2D) or plane (3D)

    Parameters
    ----------

    shape: Shape instance

    origin: np.array, shape(1, d), floats
        Origin of the plane / line

    normal: np.array, shape(1, d), floats
        Vector normal to the plane / line \n

    Returns
    --------
        Shape instance
            An instance of the reflected shape
    """

    return Reflected(
        initial_shape=shape, origin=origin, normal=normal)


def extrude(shape, axis, bounds):
    ''' Extrudes a 'shape' along the direction defined
    by 'axis', from bounds[0] to bounds[1]. 

    Parameters
    -----------

    shape: instance of Shape

    axis: sequence of n integers
        n directions to extrude

    bounds: np.array of floats with shape(2, n)
        Bounds of the new Shape along the n 'axis'

    Returns
    --------
    instance of pescado.mesher.shapes.Extruded
    '''

    return Extruded(shape=shape, axis=axis, bounds=bounds)


class Shape(ABC):
    """ABC class overloading +, -, &, ^ and | operators.
    A Subclass of Shape can be called using a 1D, 2D or 3D numpy array and
    will return a 1D numpy array of booleans.

    Abstract Methods
    -----------------

    _is_inside(): Returns True if the point is within the
        shape defined by the function or False otherwise.

    bbox(): Returns the bbox of the Shape instance

    """
    #TODO: when assembling two subclasses of
    #   type WrapperShape check for intersections  ?


    def __init__(self):
        '''

        Note
        -----
        Whenever the class instance is called, the parameters are send to
        its _is_inside() method.
        '''
        self._bbox = None

    @abstractmethod
    def _is_inside(self, x):
        """Returns True if the point is within the shape defined
        by the function or False otherwise.

        Parameters
        -----------
        x: numpy.array, shape(n, d)
            Set of coordinates (shape(1, m)) \n
            n is the number of coordinates \n
            d the dimension

        Returns
        -----------
        numpy.array of booleans, shape(n, ) \n
            True if the point is inside the region defined by _is_inside

        Note
        -----
        Whenever the class instance is called, the parameters are send to
        its _is_inside() method.
        """
        pass

    @property
    @abstractmethod
    def bbox(self):
        '''
        Returns
        --------
        numpy.array, shape(2, d), floats \n
            The bounding box for the shape instance. \n
            d is the dimension of the system \n
            [:, 0] -> min, max values for dimension 0.
        '''

        pass

    def __call__(self, x=None):
        '''
        Parameters
        -----------
        x: numpy.array, shape(n, d)
            Set of coordinates (shape(1, m)) \n
            n is the number of coordinates \n
            d the dimension

        Returns
        --------
        numpy.array of booleans, shape(n, ) \n
            True if the point is inside the region defined by _is_inside

        Note
        -----
        The input x is sent to the self._is_inside(x) method.
        '''
        #TODO Test bbox as well ?

        assert_coord_convention(self.bbox, name='bbox')
        if x is not None:
            assert_coord_convention(
                coord_array=x, name='x')

            bool_array = np.asanyarray(self._is_inside(x))

            assert_is_inside_output(
                is_inside_output=bool_array,
                is_inside_input=x)

            return bool_array

    def __or__(self, other_shape):
        '''
            Returns a Union class made of self and other_shape

            Param:
            ------
            other_shape: subclass of Shapes

            Returns:
            -------
            `pescado.continous.Union`
       '''

        if not isinstance(other_shape, Shape):
            raise ValueError(
        ('Union operation only allowed between subclasses'
          + 'of Shape. '))

        add_shapes_list = []

        for shape in [self, other_shape]:
            if isinstance(shape, Union):
                add_shapes_list += shape.add_shapes_list
            else:
                add_shapes_list.append(shape)

        return Union(
            add_shapes_seq=add_shapes_list)

    def __and__(self, other_shape):
        '''
            Returns a, Intersection of self and other_shape

            Param:
            ------
            other_shape: subclass of Shapes

            Returns:
            -------
            shape.General

            Notes:
            ------
            Performs the and operation by multiplying
            array of booleans. Not done with numpy.logical_and().
        '''

        if not isinstance(other_shape, Shape):
            raise ValueError(
        ('Intersection operation only allowed between subclasses'
          + 'of Shape. '))

        and_shapes_list = []

        for shape in [self, other_shape]:
            if isinstance(shape, Intersection):
                and_shapes_list += shape.and_shapes_list
            else:
                and_shapes_list.append(shape)

        return Intersection(
            and_shapes_seq=and_shapes_list)

    def __sub__(self, other_shape):
        '''
            Difference of two shapes together.

            Param:
            ------
            other_shape: subclass of `pescado.continous.Shape`

            Returns:
            -------
            `pescado.continous.General`
        '''

        if not isinstance(other_shape, Shape):
            raise ValueError('Subtraction operation only allowed between \
                             subclasses of Shape. ')

        return Sub(current_shape=self, other_shape=other_shape)

    def __xor__(self, other_shape):
        '''
            Logical xor operation betwen self and other_shape

            Param:
            ------
            other_shape: subclass of Shapes

            Returns:
            -------
            shape.General
        '''
        if not isinstance(other_shape, Shape):
            raise ValueError('XOR operation only allowed between \
                             subclasses of Shape. ')

        return Xor(current_shape=self, other_shape=other_shape)

    @staticmethod
    def concatenate_bbox(shapes_list):
        '''Concatenates the bbox of a list of shapes into a single bbox that
            contains all other bboxes.

        Parameters
        ----------
            shapes_list: iterable, shape(n,), Shape instances

        Returns
        --------
            numpy.array, shape(2, d), floats \n
                The bounding box containing all the bounding boxes of the shapes
                in shapes_list. \n
                d is the dimension of the system \n
                [:, 0] -> min, max values for dimension 0.
        '''
        if not np.all(
            [isinstance(shape, Shape) for shape in shapes_list]):
            raise ValueError('Difference operation only allowed between \
                             subclasses of Shape. ')

        global_min = np.min(
            np.vstack([
                shape.bbox[0, :] for shape in shapes_list]),
            axis=0)

        global_max = np.max(
            np.vstack([
                shape.bbox[1, :] for shape in shapes_list]),
            axis=0)

        global_bbox = np.vstack([global_min, global_max])

        return global_bbox


class Sub(Shape):
    ''' Defines the shape resulting of the difference between shapes.
    '''

    def __init__(self, current_shape, other_shape):
        ''' Returns a Sub object

        Parameters
        -----------

        current_shape: pescado.continuous.Shape isntance.

        other_shape: pescado.continuous.Shape isntance.
            To be subtracted from current_shape.

        Returns
        --------
            'pescado.continuous.Shape'
        '''

        super().__init__()
        if not all(isinstance(shape, Shape)
        for shape in [current_shape, other_shape]):
            raise RuntimeError(
                'Can"t only initialize Sub class with two instances of Shapoe')

        self.current_shape = current_shape
        self.other_shape = other_shape

    @property
    def bbox(self):

        self._bbox = self.current_shape.bbox
        return self._bbox

    def _is_inside(self, x):
        ''' Defines a subtraction operation between self.current_shape
        and self.other_shape.

        '''
        return self.current_shape(x) * np.invert(self.other_shape(x))


class Xor(Shape):
    ''' Defines the shape resulting from a XOR operation between two shapes
    '''

    def __init__(self, current_shape, other_shape):
        ''' Returns a Xor object

        Parameters
        -----------

        current_shape: pescado.continuous.Shape isntance.

        other_shape: pescado.continuous.Shape isntance.
            To be subtracted from current_shape.

        Returns
        --------
            'pescado.continuous.Shape'
        '''

        super().__init__()
        if not all(isinstance(shape, Shape)
        for shape in [current_shape, other_shape]):
            raise RuntimeError(
                'Can"t only initialize Sub class with two instances of Shapoe')

        self.current_shape = current_shape
        self.other_shape = other_shape

    @property
    def bbox(self):

        self._bbox =  add_bbox(bbox_seq=(
            self.current_shape.bbox,
            self.other_shape.bbox))

        return self._bbox

    def _is_inside(self, x):
        ''' Defines a XOR operation between self.current_shape
        and self.other_shape.

        '''
        return np.logical_xor(
            self.current_shape(x), self.other_shape(x))


class Union(Shape):
    '''Defines the shape resulting of multiple Union operations

    '''

    def __init__(self, add_shapes_seq):
        """Returns an Union object

        Parameters
        ----------
        add_shapes_seq: sequence of `pescado.continous.Shape`
            Sequence of `pescado.continous.Shape` to be added

        Returns
        -------
          `pescado.continous.Shape`
        """

        super().__init__()
        assert all(isinstance(shape, Shape) for shape in add_shapes_seq), (
            'add_shapes_seq must be a list of shapes')

        assert not any(
            isinstance(shape, Union)
            for shape in add_shapes_seq), (
            'no Union instance can be inside add_shapes_seq')

        self.add_shapes_list = list(add_shapes_seq)

    @property
    def bbox(self):
        
        bbox_seq = [
            shape.bbox for shape in self.add_shapes_list]

        self._bbox = add_bbox(bbox_seq=bbox_seq)

        return self._bbox

    def _is_inside(self, x):

        if len(self.add_shapes_list) < 2:
            raise ValueError('Not enough elements')

        isinside = np.any(
            [shape(x) for shape in self.add_shapes_list], axis=0)
        # I also could do a np.logical_or()

        return isinside


class Intersection(Shape):
    '''Defines the shape resulting of multiple and operations

    '''

    def __init__(self, and_shapes_seq):
        """Returns an and object

        Parameters
        ----------
        and_shapes_seq: sequence of `pescado.continous.Shape`
            Sequence of `pescado.continous.Shape` to find the intersection of.

        Returns
        -------
          `pescado.continous.Shape`
        """

        super().__init__()
        assert all(isinstance(shape, Shape) for shape in and_shapes_seq), (
            'and_shapes_seq must be a list of shapes')

        assert not any(
            isinstance(shape, Intersection)
            for shape in and_shapes_seq), (
            'no Union instance can be inside and_shapes_seq')

        self.and_shapes_list = list(and_shapes_seq)

        global_bbox = self.bbox
        if np.any((global_bbox[0, :] - global_bbox[1, :]) > 0):
            raise NotImplementedError(
                'Intersection between'
                + ' non-intersecting shapes is not implemented')

    @property
    def bbox(self):

        bbox_seq = [
            shape.bbox for shape in self.and_shapes_list]

        self._bbox = and_bbox(bbox_seq=bbox_seq)

        return self._bbox

    def _is_inside(self, x):

        if len(self.and_shapes_list) < 2:
            raise ValueError('Not enough elements')

        # I need to specify bool as dtype in prod otherwise
        # numpy converts it to int.
        isinside = np.prod(
            np.array([shape(x) for shape in self.and_shapes_list], dtype=bool),
            axis=0, dtype=bool)

        assert isinside.shape[0] == x.shape[0]

        return isinside


class Reflected(Shape):
    ''' Defines a reflected Shape
    '''

    def __init__(self, initial_shape, origin, normal):
        ''' Returns an instance of Reflected.  Reflects a given shape across a given
        line (2D) or plane(3D)

        Parameters
        -----------
        initial_shape: 'instance of pescado.continuous.Shape'*

        origin: np.array, shape(1, d), floats
            Origin of the plane / line

        normal: np.array, shape(1, d), floats
            Vector normal to the plane / line \n

        '''

        super().__init__()

        origin = np.asanyarray(origin, dtype=np.float64)[None, :]
        normal = np.asanyarray(normal, dtype=np.float64)[None, :]

        assert_coord_convention(origin, name='Reflection origin')
        assert_coord_convention(normal, name='Reflection plane normal')

        self.origin = origin
        self.normal = normal
        self.initial_shape = initial_shape

    def _is_inside(self, x):

        inverse_reflected_coord = self._inverse_reflect(
            x=x, origin=self.origin, normal=self.normal)

        return self.initial_shape(inverse_reflected_coord)

    @property
    def bbox(self):

        transformed_box = self._reflect(
            x=bbox2box(bbox=self.initial_shape.bbox),
            origin=self.origin, normal=self.normal)

        self._bbox = np.zeros(self.initial_shape.bbox.shape)
        self._bbox[0, :] = np.min(transformed_box, axis=0)
        self._bbox[1, :] = np.max(transformed_box, axis=0)

        return self._bbox

    def _reflect(self, x, origin=None, normal=None):
        '''Reflection across a given line (2D) or plane (3D)

        Parameters
        ----------
        origin: np.array, shape(1, d), floats
            Origin of the plane / line

        normal: np.array, shape(1, d), floats
            Vector normal to the plane / line \n

        Returns
        -------
            Reflected coordinates \n
                m number of coordinates \n
                d their dimension \n

            Returning the "unreflected" coordinates \n
                m number of coordinates \n
                d their dimension \n

        Notes
        ------

        We use the following equation to obtain the rotated coordinates:

            fun(r) = r(id - 2 * P) + 2 * O * P = r' \n

        with id the identity matrix, O the origin and P the projection matrix, i.e.

            P = (n / sqrt(n @ n)).T @ (n / sqrt(n @ n))

        Finally (id - 2 * P)^{-1} is calculated with np.linalg.inv
        '''

        normalized_normal = normal / (normal @ normal.T) ** (1 / 2)

        project_mat = normalized_normal.T @ normalized_normal

        dim = x.shape[1]

        assert np.all(np.array(project_mat.shape) - dim == 0), (
            'Projection matrix dimensions larger than coordinate dimensions')

        reflected_coord =  (x @ (np.identity(dim) - 2 * project_mat)
                            + 2 * origin @ project_mat)

        return reflected_coord

    def _inverse_reflect(self, x, origin=None, normal=None):
        '''Reflection across a given line (2D) or plane (3D)

        Parameters
        ----------
        x: np.array containing the real space coordinates

        origin: np.array, shape(1, d), floats
            Origin of the plane / line

        normal: np.array, shape(1, d), floats
            Vector normal to the plane / line \n

        Returns
        -------
            Inverse_reflected_coord

        Notes
        ------
        We use the following equation :

            fun^{-1}(r') =
            r'(id - 2 * P)^{-1} - 2 * O * P * (id - 2 * P)^{-1} = r \n

        with id the identity matrix, O the origin and P the projection matrix, i.e.

            P = (n / sqrt(n @ n)).T @ (n / sqrt(n @ n))

        Finally (id - 2 * P)^{-1} is calculated with np.linalg.inv
        '''

        normalized_normal = normal / (normal @ normal.T) ** (1 / 2)

        project_mat = normalized_normal.T @ normalized_normal

        dim = x.shape[1]

        assert np.all(np.array(project_mat.shape) - dim == 0), (
            'Projection matrix dimensions larger than coordinate dimensions')

        inv_proj = np.linalg.inv(np.identity(dim) - 2 * project_mat)
        orig_mat = project_mat @ inv_proj
        inverse_reflected_coord =  (x @ inv_proj  - 2 * origin @ orig_mat)

        return inverse_reflected_coord


class Dilated(Shape):
    ''' Defines a dilated Shape
    '''

    def __init__(self, initial_shape, alpha, center):
        ''' Returns an instance of Dilated.  Dilatates a given shape by a factor 'alpha'
            given a central position 'center'.

        Parameters
        -----------
        initial_shape: 'instance of pescado.continuous.Shape'*

        alpha: np.array, shape(1, d), floats
            [[1, 1, 1]] equivalent to no dilatation \n
            [[2, 1, 1]] doubles the dimension along axis 0 \n
            [[0.5, 1, 1]] decreases by half the dimension along axis 0 \n

        center: np.array, shape(d,)
            The center of rotation

        '''

        super().__init__()

        dim  = initial_shape.bbox.shape[1]

        if not isinstance(alpha, (collections.abc.Sequence, str)):
            alpha = [alpha, ] * dim

        alpha = np.asanyarray(alpha, dtype=np.float64)[None, :]
        center = np.asanyarray(center, dtype=np.float64)[None, :]

        assert_coord_convention(center, name='Dilatation center')
        assert_coord_convention(alpha, name='Dilatation coefficients')

        self.alpha = alpha
        self.center = center
        self.initial_shape = initial_shape

    def _is_inside(self, x):

        assert x.shape[1] == self.alpha.shape[1], (
            'alpha.shape[1] must be equal to x.shape[1]')

        return self.initial_shape(self._dilatate(x))

    def _dilatate(self, x, alpha=None, center=None):
        ''' Dilatates 'x' by a factor 'alpha' given a central position 'center'
        '''

        if alpha is None:
            alpha = copy.deepcopy(self.alpha)
        if center is None:
            center = copy.deepcopy(self.center)

        new_coord = (np.repeat(center, x.shape[0], axis=0)
                     + (x - center) / alpha)

        return new_coord

    @property
    def bbox(self):

        transformed_box = self._dilatate(
            x=bbox2box(bbox=self.initial_shape.bbox),
            alpha=1 / self.alpha)

        self._bbox = np.zeros(self.initial_shape.bbox.shape, dtype=float)
        self._bbox[0, :] = np.min(transformed_box, axis=0)
        self._bbox[1, :] = np.max(transformed_box, axis=0)

        return self._bbox


class Translated(Shape):
    ''' Defines a translated Shape
    '''

    def __init__(self, initial_shape, translation):
        ''' Returns an instance of Translated.  Translates a given
        shape by 'translate'

        Parameters
        -----------
        initial_shape: 'instance of pescado.continuous.Shape'*

        translation: sequence of d real numbers
            Translation vector. \n
            ``translation`` must contain d real numbers,
            where d is the dimension of ``shape``.

        '''

        super().__init__()

        translation = np.asanyarray(translation, dtype=np.float64)[None, :]
        assert_coord_convention(translation, name='Translation vector')

        self.translation = translation
        self.initial_shape = initial_shape

    def _is_inside(self, x):
        return self.initial_shape(x - self.translation)

    @property
    def bbox(self):
        self._bbox = self.initial_shape.bbox + self.translation
        return self._bbox


class Rotated(Shape):
    ''' Defines a rotated Shape
    '''

    def __init__(self, initial_shape, axis, angle, origin=None):
        ''' Returns an instance of Rotated.  Rotates a given shape around 'axis'
            by 'angle'.

        Parameters
        -----------
        axis: np.ndarray, shape(d, ), floats
            Defines the axis along which the rotation will be performed \n
            [1, 0, 0] -> along axis 0, [0, 1, 0] -> along axis 1 etc ...

        origin: sequence of d real numbers, optional
            Origin of the rotation axis. \n
            ``origin`` must contain d real numbers, where d is the dimension of ``shape``.

        angle: float, int
            Defines the counter-clockwise angle
            by which the rotation will be performed

        '''

        super().__init__()

        if origin is None:
            origin = (0, ) * initial_shape.bbox.shape[1]

        origin = np.asanyarray(origin, dtype=np.float64)[None, :]
        assert_coord_convention(origin, name='Rotation axis origin')

        axis = np.asanyarray(axis, dtype=np.float64)[None, :]
        assert_coord_convention(axis, name='Rotation axis')

        assert isinstance(angle, (float, int)), (
            'Second element of rotation must be the angle')

        self.origin = origin
        self.axis = axis
        self.angle=angle
        self.initial_shape = initial_shape

        self.rot_matrix = self._rotation_matrix()
        self.bbox_rot_matrix = self._rotation_matrix(angle=-1 * self.angle)

    def _is_inside(self, x):

        assert x.shape[1] == self.bbox.shape[1], (
            ' x.shape[1] must be equal to self.bbox.shape[1]')

        return self.initial_shape(self._rotate(x))

    def _rotate(self, x, rot_matrix=None, origin=None):
        ''' Rotates 'x' for a given 'rotation_marix' centered at 'center'

        Parameters
        -----------
        rot_matrix:  np.array, shape(3, 3),
            The 3x3 rotation matrix
            Given by self._rot_matrix

        origin: np.array, shape(1, d)
            The origin of rotation
        '''

        if rot_matrix is None:
            rot_matrix = copy.deepcopy(self.rot_matrix)
        if origin is None:
            origin = copy.deepcopy(self.origin)

        new_x = (np.repeat(origin, x.shape[0], axis=0)
                 + (x - origin) @ rot_matrix[0:x.shape[1], 0:x.shape[1]])

        return new_x

    @property
    def bbox(self):

        transformed_box = self._rotate(
            x=bbox2box(bbox=self.initial_shape.bbox),
            rot_matrix=self.bbox_rot_matrix)

        self._bbox = np.zeros(self.initial_shape.bbox.shape)
        self._bbox[0, :] = np.min(transformed_box, axis=0)
        self._bbox[1, :] = np.max(transformed_box, axis=0)

        return self._bbox

    def _rotation_matrix(self, axis=None, angle=None):
        """Generates the rotation matrix for a rotation around a given :axis: and
        a counter-clockwise :angle:

        Returns
        --------
        rot_mat: np.array, shape(3, 3),
            The 3x3 rotation matrix
        """

        if axis is None:
            axis = copy.deepcopy(self.axis)
        if angle is None:
            angle = copy.deepcopy(self.angle)

        assert np.all([isinstance(self.angle, (int, float)),
                    isinstance(self.axis, (list, tuple, np.ndarray))]), (
            'Angle must be a float or int. Axis must be a sequence')

        assert self.axis.shape[1] == 3, (
            'Must specify the three components of the axis')

        cost = np.cos(self.angle)
        sint = np.sin(self.angle)
        ux, uy, uz = self.axis[0, :]

        rot_mat = np.array(
            [[cost + ux ** 2 * (1 - cost),
            ux * uy * (1 - cost) - uz * sint,
            ux * uz * (1 - cost) + uy * sint],
            [uy * ux * (1 - cost) + uz * sint,
            cost + uy ** 2 * (1 - cost),
            uy * uz * (1 - cost) - ux * sint],
            [uz * ux * (1 - cost) - uy * sint,
            uz * uy * (1 - cost) + ux * sint,
            cost + uz ** 2 * (1 - cost)]])

        return rot_mat


class Extruded(Shape):
    ''' Defines a extruded shape
    '''

    def __init__(self, shape, axis, bounds):
        ''' Extrudes a 'shape' along the direction defined
        by 'axis', from bounds[0] to bounds[1]. 

        Parameters
        -----------

        shape: instance of Shape

        axis: sequence of n integers
            n directions to extrude

        bounds: np.array of floats with shape(2, n) or shape(2, )
            Bounds of the new Shape along the new 'axis'

            If bounds has shape(2, ) then axis defines one extruding direction

            If bounds has shape(2, n) then axis defines 'n' extruding direction
            
            
        '''
        
        self.shape = shape

        if isinstance(axis, (np.integer, int)):
            axis = [axis,]
        
        if len(bounds.shape) == 1:
            bounds = bounds[:, None]
        
        self.axis = axis

        shape_axis = np.arange(
            self.shape.bbox.shape[1] + len(self.axis), dtype=int)

        self.shape_axis = shape_axis[
            np.logical_not(np.isin(shape_axis, self.axis))]

        self._bbox = np.zeros((2, len(self.axis) + len(self.shape_axis)))
        
        if self._bbox.shape[1] > 3:
            raise ValueError(
                'Too many values given to axis. shape is a '
                + '{0} hence axis can have at most {1} elements'.format(
                    self.shape_axis, 3 - self.shape_axis))
        
        self._bbox[:, self.shape_axis] = self.shape.bbox
        self._bbox[:, self.axis] = bounds

    def _is_inside(self, x):
        #  Filter out the coordinates outside of self.bounds
        x_inside = np.zeros(len(x), dtype=int)
        for _ax in self.axis:
            _map = ((x[:, _ax] >= self._bbox[0, _ax])
                    & (x[:, _ax] <= self._bbox[1, _ax]))
            x_inside[_map] += 1
        
        inside_bool = np.zeros(len(x), dtype=bool)

        if len(self.shape_axis) == 1:
            shape_coord = x[
                x_inside == len(self.axis), self.shape_axis][:, None]
        else:
            shape_coord = x[x_inside == len(self.axis)][:, self.shape_axis]

        if shape_coord.shape[0] != 0:
            inside_bool[x_inside == len(self.axis)] = self.shape(shape_coord)

        return inside_bool

    @property
    def bbox(self):
        return self._bbox


class General(Shape):
    '''Defines a Shape from an input function

        Notes
        ------
        This is called directly by Shape abc.
    '''

    def __init__(self, func, bbox):
        """Returns an instance of General

        Parameters
        -----------
        func: function(x) -> y
            Function that defines a region in space. \n
            x: numpy.array, shape(n,m), floats \n
            y: numpy array, shape(n, ), booleans \n
            n is the number of elements (site coordinates) \n
            m is their dimension

            ! Can't be a lambda function !

        bbox: numpy.array, shape(2, s), floats
            Bounding box that contains the region in space defined by func \n
            d dimension of the coordinates

        """

        super().__init__()
        
        lambda_name = (lambda x:None).__name__
        
        if func.__name__ == lambda_name:
            raise ValueError(
                "General can't be initialized with a lambda function. However"
                + ' func is {0}'.format(func))
        
        self.func = func
        self._bbox = bbox.astype(float)

    def _is_inside(self, x):
        return self.func(x)

    @property
    def bbox(self):
        return self._bbox


class Box(Shape):
    """Defines a Box in space.

        Can be called using a numpy.array of floats with shape(n, d)\n
            n the number of coordinates \n
            d the dimension of the system. \n
        When called it returns an numpy.array of bool with shape(n,). \n
        True if the coordinate n is inside the box. False otherwise

    """

    def __init__(self, lower_left, size):
        '''
        Parameters
        -----------
        lower_left: sequence, shape(d, ), floats
            (x0, y0, z0) - Lower left of the box. \n
            d the dimension of the box (1D, 2D or 3D)

        size: sequence, shape(d, ), floats
            (Lx, Ly, Lz) - Length of the box \n
            d the dimension of the box (1D, 2D or 3D)

        Returns
        --------
            class, instance of Box
        '''

        super().__init__()

        self.size = np.asanyarray(size, dtype=float)
        if len(self.size.shape) == 1:
            self.size = self.size[None, :] # Transform it to (1, d)
        else:
            raise ValueError('size has wrong shape')

        assert np.all(self.size > 0), 'Size must be bigger than zero. '

        self.lower_left = np.asanyarray(lower_left, dtype=float)
        if len(self.lower_left.shape) == 1:
            self.lower_left = self.lower_left[None, :]
        else:
            raise ValueError('size has wrong shape')

        assert_coord_convention(coord_array=self.size, name='size')
        assert_coord_convention(coord_array=self.lower_left, name='lower_left')

        assert self.lower_left.shape == self.lower_left.shape, (
            'lower_left and size do not have the same dimensions')

    def _is_inside(self, x):

        x = np.asarray(x)
        if len(x.shape) == 1:
            x = x[None, :]

        assert x.shape[1] == self.size.shape[1], (
            'Box shape is defined in {0}D'.format(self.size.shape[1]))
        isin = (np.all(x <= self.lower_left + self.size, axis=1)
                * np.all(x >= self.lower_left, axis=1))

        return isin

    @property
    def bbox(self):

        _bbox = np.concatenate([
            self.lower_left,
            self.lower_left + self.size])

        self._bbox = _bbox    
        return self._bbox

    @classmethod
    def centered(cls, center, size):
        '''
        Parameters
        -----------
        center: sequence, shape(d, ), floats
            (x0, y0, z0) - Center of the box. \n
            d the dimension of the box (1D, 2D or 3D)

        size: sequence, shape(d, ), floats
            (Lx, Ly, Lz) - Length of the box \n
            d the dimension of the box (1D, 2D or 3D)

        Returns
        --------
            class, instance of Box
        '''
        center = np.asanyarray(center)
        size = np.asanyarray(size)
        box_cls = cls(lower_left=center - size / 2, size=size)

        return box_cls


class Ellipsoid(Shape):
    """Defines a ellipsoind

    """

    def __init__(self, center, radius, tol=1e-12):
        """Defines a ellipsoid shape

        Parameters
        ----------

        center: sequence, shape(d,), floats
            The center of the ellipsoid \n
            d dimension of the system

        radius: sequence, shape(d,), floats
            The radius along each direction.

        tol: float,
            numerical accuracy when applying _is_inside \n
            default is 1e-12

        Returns
        -------
            instance of Shape

        Note
        -----

        _is_inside() is the function:

            (np.sum(((x - center) / radius) ** 2, axis=1) - 1) < tol

        """

        super().__init__()
        # Transform it to (1, d)
        self.radius = np.asanyarray(radius, dtype=float)[None, :] 
        self.center = np.asanyarray(center, dtype=float)[None, :] 
        self.tol = tol

        if np.any(self.radius <= 0):
            raise ValueError(
                'Radius must be positive and non zero along all directions,'
                'its current value is {0}'.format(self.radius))

        assert self.radius.shape == self.center.shape, (
            'radius and center do not have the same dimensions')

        assert_coord_convention(self.radius, name='Radius')
        assert_coord_convention(self.center, name='Center')

    def _is_inside(self, x):

        x = np.asanyarray(x)
        if len(x.shape) <= 1:
            x = x[:, None] #Transform it to (len(shape), 1)

        assert x.shape[1] == self.radius.shape[1], AttributeError(
            ('Dimension for the coordinates do not match that'
             + 'of the Shape instance'))

        return (np.sum(((x - self.center) / self.radius) ** 2, axis=1)
                - 1) < self.tol

    @property
    def bbox(self):

        _bbox = np.concatenate([
            self.center - self.radius,
            self.center + self.radius])

        self._bbox = _bbox
        return self._bbox

    @classmethod
    def hypersphere(cls, center, radius):
        """Defines a hypersphere - sphere if 3d or circle if 2d

        Parameters
        ----------

        center: sequence, shape(d,), floats
            The center of the ellipsoid \n
            d dimension of the system

        radius: float or int
            Radious of the circle

        Returns
        -------
            Instance of Shape

        """
        return cls(center=center, radius=[radius,] * len(center))


class Delaunay(Shape):
    '''Generate a shape from a Delaunay triangulation.
    (scipy.spatial.Delaunay instance). The latter is used to define
    is_instance().
    '''

    def __init__(self, coordinates, rtol=1e-10):
        """Generates a Delaunay instance from a np.array of coordinates

        Parameters
        ----------
        coordinates: np.array, shape(m, d), floats
            coordinates of the vertex passed to scipy.spatial.Delaunay \n
            m number of vertexes \n
            d their dimension \n

        rel_tol : float - default is 1e-10
            Relative tolerance when defining the delaunay bounding box

        Returns
        --------
        Delaunay instance (subclass of Shape and InHull)
        """
        super().__init__()

        assert coordinates.shape[1] > 1, (
            'Qhull cannot be defined in 1D')

        self.rtol = rtol
        self.delaunay = None
        self.hull = None
        self.points_coordinates = np.asarray(coordinates)

    @property
    def bbox(self):
        '''
        Notes
        ------

        Uses a convex hull to get the bounding box of the hull made by
        list of points.
        '''
        
        import matplotlib.pyplot as plt

        if self._bbox is None:

            if self.hull is None:
                self.hull = ConvexHull(self.points_coordinates)

            self._bbox = np.concatenate([
                self.hull.min_bound[None, :],
                self.hull.max_bound[None, :]]).astype(float)
            self._bbox += self._bbox * self.rtol

        return self._bbox

    def _is_inside(self, x):

        if self.delaunay is None:
            self._prepare_param()

        if self.delaunay is not None:
            return self.delaunay.find_simplex(x) >= 0
        else:
            raise ValueError('InHull.hull has not been defined')

    def _make_delaunay(self):
        '''Make Delaunay tessalatioon.
        '''

        if self.points_coordinates is not None:
            # Find the boundary of the shape
            self.hull = ConvexHull(self.points_coordinates)
            # take the boundary points
            vertices_hull = self.points_coordinates[self.hull.vertices]

            # Make the delaunay object using only the surface points.
            self.delaunay = DelaunayScipySpatial(vertices_hull)

    def _prepare_param(self):
        '''Calculate the Deulaynay triangulation if a set of points is given.
        '''
        if self.delaunay is None:
            if self.points_coordinates is None:
                raise ValueError('points_coordinates can"t be None')
            self._make_delaunay()

    @classmethod
    def import_instance(cls, delaunay_instance):
        """Generates a Delaunay instance from a scipy.spatial.Delaunay instance

        Parameters
        ----------
        delaunay_instance: scipy.spatial.Delaunay instance

        Returns
        --------
        shapes.Delaunay instance (subclass of Shape and InHull)
        """

        delaunay_shape = cls(coordinates=delaunay_instance.points)
        delaunay_shape.delaunay = delaunay_instance

        return delaunay_shape
