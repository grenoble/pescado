# Only do "sanity" check. Can be done by using a voronoi diagram as example.
# Create a regular voronoi diagram and check if the result corresponds to what
# One would expect. Do it for a rectangular voronoi diagram in 1d, 2d and 3d.

import warnings
import time
from scipy.spatial import Voronoi

import numpy as np
import numpy.lib.arraysetops as arr_ops

from pescado.tools import (
    assert_coord_convention,
    polygons_surface, points_distance, polygons_volume)
from pescado.tools import reduced_array, meshing
from matplotlib import pyplot as plt


class VoronoiCell():
    ''' Local voronoi cell centered around a point.
    Note -> Testd by compare_local() at test_mesh.py Move it
    to test_voronoi.py
    '''

    def __init__(
        self, point, point_tag, neighbours, neighbours_tags,
        vertices, ridges, safety_radius, 
        distances=None, surfaces=None, volume=None):
        ''' Returns a VoronoiCell instance defining the voronoi cell for 'point'

        Parameters
        ----------

        point: numpy.array of floats with shape(n, d)
            Real space coordinates of the point

        point_tag: immutable object
            Indices for the point used by the pattern

        neighbours: numpy.array of floats with shape(n, d)
            Real space coordinates of first neighbours of point

        neighbours_tag: numpy.array of immutable objects with shame(m)
            Indices for the first neighbours of point used by pattern

        vertices: numpy.array of floats with shape(v, d)
            Real space coordinates of the vertices forming the voronoi cell
            around 'point'

        ridges: Sequence of n numpy.arrays of intergers with shape(r, )
            The indices of the vertices forming the ridges between 'point'
            and 'neighbours'.

            r depends on dimension -> 1D - 1; 2D - 2; 3D - 4

            example
            --------

            The indices for the vertices forming the ridge in betwen 'point'
            and neighbour i is:

                vert_id = ridges[i]
        
        safety_radius: float

        distances: sequence of n floats, default is None
            Distances betwen 'point' and its first neighbours

            If None, then it is calculated using self.distance() method

        surfaces: sequence of n floats, default is None
            Surfaces betwen 'point' and its first neighbours

            If None, then it is calculated using self.surface() method

        volume: sequence of n floats, default is None
            Volume for the voronoi cell.

            If None, then it is calculated using self.volume() method

        safety_radius: float
            If a point is added to the mesh within the 'safety radius' of
            the center of the current voronoi cell, then the current
            voronoi cell - might - change.

        Returns
        -------

        Instance of VoronoiCell for 'point'.

        Notes
        ------
        In a 1D, 2D or 3D euclidean space the voronoi cell must be convex

        self.ridges must provide with an ordered set of vertices forming
        a line in 2D or region in 3D. In 1D it is the point in between the
        center of the cell and its neighbourg

        '''
        
        self.point = point
        self.point_tag = point_tag
        self.dim = point.shape[1]

        if self.dim not in [1, 2, 3]:
            raise NotImplementedError(
                'Voronoi not implemented for d = {0}'.format(self.dim))

        self.neighbours = neighbours
        self.neighbours_tags = neighbours_tags

        self.safety_radius = safety_radius

        self.vertices = vertices
        self.ridges = ridges

        self._distances = distances
        self._surfaces = surfaces
        self._volume = volume

    def distance(self, neig_idx):
        '''Returns the distance betwen 'self.point'
        and self.neighbours[neig_idx]

        Parameters
        -----------
        neig_idx: sequence of n intergers
            Index of neighbours in self.neighbours from which to
            get the distance from 'self.point'.

        Returns
        --------
        Sequence of n floats.

        Notes
        ------
        Change this so it calculates all distances
        if self._distances is None ?
        '''

        if self._distances is None:

            pts = np.ones(
                (len(neig_idx) * 2, self.dim), dtype=float) * self.point
            pts[np.arange(1, len(neig_idx) * 2, 2), :] = self.neighbours[
                neig_idx, :]

            distances = points_distance(points=pts)
        else:
            distances = self._distances[neig_idx]

        return distances

    def surface(self, neig_idx):
        '''Returns the surface of the ridge betwen 'self.point' and
        self.neighbours[neig_idx]

        Parameters
        -----------
        neig_idx: sequence of n intergers
            Index of neighbours in self.neighbours from which to
            get the surface separating it from 'self.point'.

        Returns:
        --------
        Sequence of n floats.

        Notes
        ------
        Change this so it calculates all surfaces
        if self._surfaces is None ?
        '''

        if self._surfaces is None:
            vertices_index = [[] for i in range(len(neig_idx) + 1)]
            for i, neig in enumerate(neig_idx):
                vertices_index[i + 1] = self.ridges[neig]
            if self.dim == 1:
                surface = np.ones((len(neig_idx)))
            elif self.dim == 2:        
                surface = points_distance(
                    points=self.vertices[
                        np.concatenate(vertices_index).astype(int)])

            elif self.dim == 3:

                indtpr = np.cumsum(
                    [len(vert_poly) for vert_poly in vertices_index])

                surface = polygons_surface(
                    vertices=self.vertices[
                        np.concatenate(vertices_index).astype(int)],
                    indtpr=indtpr)
            else:
                raise NotImplementedError(
                    'Surface can"t be calculated for d = {0}'.format(self.dim))

        else:
            surface = self._surfaces[neig_idx]

        return surface

    @property
    def volume(self):
        '''Returns the volume of the voronoi cell sourrounding 'self.point'

        Returns
        --------
        Float

        Notes
        ------
        It only calculates the volume if self._volume is not None.
        Then it saves the information to self._volume.
        '''
        if self._volume is None:

            if self.dim == 1:
                self._volume = points_distance(points=self.vertices)[0]
            elif self.dim == 2:
                self._volume = polygons_surface(vertices=self.vertices)[0]
            elif self.dim == 3:
                self._volume = polygons_volume(vertices=self.vertices)[0]
            else:
                raise NotImplementedError(
                    'Volume can"t be calculated for d = {0}'.format(self.dim))

        return self._volume


def voronoi_diagram_1d(points):
    ''' Returns a 1D voronoi "diagram"

    Parameters
    -----------
    points: numpy.array of floats with shape(m, 1)
        The coordinates of the points making the voronoi diagram
        m the number of points

    Returns
    -------

    points: np.ndarray of floats, shape(m, 1)
        Coordinates of input points
        m the number of points

    vertices: np.ndarray of floats, shape(m, 1)
        Coordinates of the voronoi vertices
        m the number of points

    ridge_points: np.ndarray of intergers, shape(n, 2)
        Indices of the points in between two voronoi ridges
        n the number of ridges

    ridge_vertices: np.ndarray of intergers, shape(n, 1)
        Indices of the vertices forming the ridges
        n the number of ridges

    ridges: np.ndarray of integers with shape (m+1, )
        Interval in 'ridge_vertices' containing the indices
        for all 'vertices' forming a ridge. 

        The vertices coordinates for ridges 'i' are : 
        vertices[ridge_vertices[ridges[i]]:
                 ridge_vertices[ridges[i] + 1]]

    region_vertices: np.ndarray of integers with shape(p,)
        Indices of the vertices forming the regions of
        the voronoi diagram. -1 indicates a vertex outside 
        of the voronoi diagram
       
    regions: np.ndarray of integers with shape (m+1, )
        Interval in 'region_vertices' containing the indices
        for all 'vertices' forming a region. 

        The vertices coordinates for region 'i' are : 
        vertices[region_vertices[regions[i]]:
                 region_vertices[regions[i] + 1]]

    '''

    assert_coord_convention(
        points, name='points in pescado.mesher.voronoi.voronoi_1d',
        max_dim=1)

    assert points.shape[0] >= 2
    # Sort them
    sorted_index = np.argsort(points[:, 0])
    unsort_index = np.argsort(sorted_index)

    sorted_points = points[sorted_index, 0]
    point_region = np.arange(len(sorted_index))[sorted_index]

    distances = np.abs(sorted_points[:-1] - sorted_points[1:])[:, None]

    point_region = np.empty((len(sorted_index)), dtype=int)
    point_region[sorted_index] = np.arange(len(sorted_index))

    vertices = (sorted_points[:-1, None] + 0.5 * distances)

    ridge_points = np.array(
        list(zip(sorted_index[:-1], sorted_index[1:])))

    ridge_vertices = np.arange(len(vertices), dtype=np.int64)
    ridges =  np.insert(
        np.cumsum(
            np.ones(len(ridge_vertices)), dtype=int), obj=0, values=[0, ])

    # The last two points are open voronoi cells
    region_vertices = np.array(
        ([[-1, 0]]
         + list(zip(np.arange(0, len(vertices) - 1),
                   np.arange(1, len(vertices))))
         + [[len(vertices) - 1, -1]]))

    region_vertices, regions = reduced_array.make_from(
        region_vertices, order=unsort_index)

    return (points, vertices,
            ridge_points, ridge_vertices, ridges, 
            region_vertices, regions)


def voronoi_diagram_qhull(points, qhull_options=None):
    ''' Returns a 2D or 3D, Voronoi diagram. Uses scipy.spatial.qhull

    Parameters
    -----------
    points: numpy.array of floats with shape(m, d)
        The coordinates of the points making the voronoi diagram
        m the number of points
        d the real space dimension

    qhull_options: str, default "Qbb Qc Qz"
        Options passed  to Qhull. See Qhull documentation for
        more information.

    Returns
    -------

    points: np.ndarray of floats, shape(m, d)
        Coordinates of input points
        m the number of points

    vertices: np.ndarray of floats, shape(m, d)
        Coordinates of the voronoi vertices
        m the number of points

    ridge_points: np.ndarray of intergers, shape(n, 2)
        Indices of the points in between two voronoi ridges
        n the number of ridges

    ridge_vertices: np.ndarray of intergers, shape(n, )
        Indices of the vertices forming the ridges        

    ridges: np.ndarray of integers with shape (m+1, )
        Interval in 'ridge_vertices' containing the indices
        for all 'vertices' forming a ridge. 

        The vertices coordinates for ridges 'i' are : 
        vertices[ridge_vertices[ridges[i]]:
                 ridge_vertices[ridges[i] + 1]]

    region_vertices: np.ndarray of integers with shape(p,)
        Indices of the vertices forming the regions of
        the voronoi diagram. -1 indicates a vertex outside 
        of the voronoi diagram
       
    regions: np.ndarray of integers with shape (m+1, )
        Interval in 'region_vertices' containing the indices
        for all 'vertices' forming a region. 

        The vertices coordinates for region 'i' are : 
        vertices[region_vertices[regions[i]]:
                 region_vertices[regions[i] + 1]]

    Note
    -----

    Generates an instance of scipy.spatial.Voronoi
    Then recovers the voronoi diagram information
    Then deletes the instance of scipy.spatial.Voronoi

    '''

    assert_coord_convention(
        coord_array=points,
        name='points in mesher.voronoi.voronoi_nd',
        max_dim=3)

    if qhull_options is None:
        qhull_options = "Qbb Qc Qz"

    t0 = time.time()    
    vor_diag = Voronoi(
        points=np.ascontiguousarray(points, dtype=np.double),
        incremental=False, furthest_site=False,
        qhull_options=qhull_options)

    qhull_points = vor_diag.points

    # Check if any point was added in Qhull
    assert len(qhull_points) == len(points), (
        'Initial points sent to _Qhull do not match _Qhull.get_points()')

    check_ordering(points=vor_diag.points, input_points=points)
    
    test_point_region(
        point_region=np.array(vor_diag.point_region), 
        regions=vor_diag.regions, 
        input_points=points, 
        points=vor_diag.points)
           
    t0 = time.time()
    # Make the voronoi diagram
    vertices = vor_diag.vertices
    ridge_points = vor_diag.ridge_points
    ridge_vertices, ridges = reduced_array.make_from(vor_diag.ridge_vertices)
    region_vertices, regions = reduced_array.make_from(
        vor_diag.regions, order=vor_diag.point_region)
           
    del vor_diag

    # Check if all points were captured by qhull in ridge_points
    _warning_all_points_in_ridges(ridge_points=ridge_points, points=points)
            
    return (points, vertices,
            ridge_points, ridge_vertices, ridges,
            region_vertices, regions)

def check_ordering(points, input_points):
    '''Verifies that the point indexing is preserved by comparing 'points' to
    'input_points'.
    
    Parameters
    -----------
    points: np.ndarray of floats with shape(r, )
        p: the number of points

    input_points: np.ndarray of floats with shape(r, )
        p: the number of points
        
    Raises
    -------
        RuntimeError if any of the listed conditions are not respected
    '''
    
    map2input, inside = meshing.kd_where(
        array=points, ref_array=input_points)
    
    if not np.all(inside):
        idx = np.arange(len(points), dtype=int)[
            np.logical_not(inside)]
        raise RuntimeError(
            'Voronoi.points has added coordinates {0}'.format(
            points[idx]))
    
    map2vor, inside = meshing.kd_where(
        array=input_points, ref_array=points) 
    
    if not np.all(inside):
        idx = np.arange(len(input_points), dtype=int)[
            np.logical_not(inside)]
        raise RuntimeError(
            'Voronoi.points has deleted coordinates {0}'.format(
            input_points[idx]))
    
    if np.any((map2input - map2vor) != 0):
        raise RuntimeError(
            'Voronoi messed up points ordering')
    

def test_point_region(point_region, input_points, regions, points):
    ''' Tests 'point_region' for '-1' or if it has the same size as 'regions'. 

    Parameters
    -----------
    point_region: np.ndarray of integers with shape(p, )
        p: the number of points

    points: np.ndarray of floats with shape(p, )
        p: the number of points

    regions: list of r lists 
        r: number of ridges


    Raises
    -------
        RuntimeError if any of the listed conditions are not respected

        For more information check 'scipy.spatial.Voronoi' documentation    
    '''
    
    if np.any(point_region == -1):
        idx = np.array(len(point_region))[point_region == -1]
        raise RuntimeError(
            'Coordinates {0} have no voronoi region'.format(
            points[idx]))
    
    if len(point_region) == (len(regions) + 1):
        raise RuntimeError(
            'An extra point at infinity was added to aid calculation')


def _warning_all_points_in_ridges(ridge_points, points, decimals=14):
    ''' Sends a RuntimeWarning if not all points are inside ridge_points.

    Parameters
    -----------
    points: np.ndarray of floats, shape(m, d)
        Coordinates of input points
        m the number of points

    ridge_points: np.ndarray of intergers, shape(n, 2)
        Indices of the points in between two voronoi ridges
        n the number of ridges

    decimals: int, default 14
        Parameter passed to :
            arr_ops.unique(
                np.round(raveled_rdige_points[argsort], decimals=decimals))

    Notes
    -----
    NOT TESTED.
    '''

    inx = arr_ops.unique(
        np.round(np.sort(ridge_points.ravel()), decimals=decimals))
    val = np.setdiff1d(np.arange(len(points)), inx)

    if len(val) != 0:
        warn_msg = ('\n Some points were ignored when calculating the ridges'
                    + ' in the voronoi diagram. The points \n'
                    + ' val : {0} \n index : {1} \n'.format(
                        points[val], val)
                    + 'were not found in any ridge inside the voronoi diagram.'
                    + ' One reason might be because the ridges containing those'
                    + ' points require more than one midpoint. Option "Fv"'
                    + '(the default) in Qhull does not list such ridges.'
                    + ' One way to fix this is to set \n'
                    + ' qhull_options = b"Qbb Qc Qz QJ" \n'
                    + ' For more information check: \n'
                    + 'https://stackoverflow.com/questions/25754145/'
                    + 'scipy-voronoi-3d-not-all-ridge-points-are-shown')
        warnings.warn(warn_msg, RuntimeWarning)


def voronoi_diagram(points, qhull_options=None):
    ''' Returns a 1D, 2D or 3D voronoi "diagram"

    Parameters
    -----------
    points: numpy.array of floats with shape(m, d)
        The coordinates of the points making the voronoi diagram
        m the number of points
        d the real space dimension

    qhull_options:  str, default "Qbb Qc Qz"
        Options passed  to Qhull. See Qhull documentation for
        more information.

    Returns
    -------

    points: np.ndarray of floats, shape(m, d)
        Coordinates of input points
        m the number of points

    vertices: np.ndarray of floats, shape(m, d)
        Coordinates of the voronoi vertices
        m the number of points

    ridge_points: np.ndarray of intergers, shape(n, 2)
        Indices of the points in between two voronoi ridges
        n the number of ridges

    ridge_vertices: np.ndarray of intergers, shape(n, )
        Indices of the vertices forming the ridges        

    ridges: np.ndarray of integers with shape (m+1, )
        Interval in 'ridge_vertices' containing the indices
        for all 'vertices' forming a ridge. 

        The vertices coordinates for ridges 'i' are : 
        vertices[ridge_vertices[ridges[i]]:
                 ridge_vertices[ridges[i] + 1]]

    region_vertices: np.ndarray of integers with shape(p,)
        Indices of the vertices forming the regions of
        the voronoi diagram. -1 indicates a vertex outside 
        of the voronoi diagram
       
    regions: np.ndarray of integers with shape (m+1, )
        Interval in 'region_vertices' containing the indices
        for all 'vertices' forming a region. 

        The vertices coordinates for region 'i' are : 
        vertices[region_vertices[regions[i]]:
                 region_vertices[regions[i] + 1]]

    Notes
    ------
    If 1D diagram see voronoi_1d

    If 2D or 3D diagram see voronoi_nd

    NOT TESTED.
    '''

    assert_coord_convention(
        coord_array=points, name='points in pescado.mesher.voronoi')

    if points.shape[1] == 1:
        return voronoi_diagram_1d(points=points)
    elif points.shape[1] <= 3:
        return voronoi_diagram_qhull(points=points, qhull_options=qhull_options)
    else:
        raise NotImplementedError(
    'Coordinates in points are {0}D. The can only be 1D, 2D or 3D'.format(
        points.shape[1]))


def region_type(region_vertices, regions):
    ''' Returns the index of points with a closed region
    and the ones with an opened region.

    Parameters
    -----------
    region_vertices: np.ndarray of integers with shape(p,)
        Indices of the vertices forming the regions of
        the voronoi diagram. -1 indicates a vertex outside 
        of the voronoi diagram
       
    regions: np.ndarray of integers with shape (m+1, )
        Interval in 'region_vertices' containing the indices
        for all 'vertices' forming a region. 

    Returns
    --------
    bounded_region : np.ndarray of intergers with shape(mc, )
        Index of the bounded regions

    unbounded_region : np.ndarray of intergers with shape(m - mc, )
        Index of the unbounded regions
    '''
   
    negative_vertices = np.arange(0, regions[-1])[region_vertices == -1]

    unbounded_region = np.searchsorted(
        regions, negative_vertices, side='right') - 1  

    bounded_region = np.setdiff1d(
        np.arange(len(regions) -1, dtype=int), unbounded_region)

    return bounded_region, unbounded_region


def first_neighbours_qhull(ridge_points, npoints):
    ''' Returns the first neighbours of points.

    Parameters
    -----------
    ridge_points: np.ndarray of intergers, shape(n, )
        Indices of the points in between two voronoi ridges
        n the number of ridges
    
    npoints: floats
        Number of points in the qhull voronoi diagram
    
    Returns
    --------
    neighbours: np.ndarray of integers, shape(n, )
        Indices of the voronoi neighbours

    ridges: np.ndarray of integers, shape(n, )
        Indices of the ridges separating neighbours and 
        its corresponding 'points_neighbour'

    points_neighbour:
         Interval in 'neighbours' and 'ridges' containing 
         respectively the first neighbours and the ridges
         separating them.


    Method
    -------

        The points sharing the same ridge are by definition
    neighbours of each other. Hence:
        
        neig[ridge_points[odd]] = ridge_points[even]
        neig = [[] for i in range(npoints)]
        
        for i in np.arange(0, len(ridge_points)):
            neig[ridge_points[i]].append(ridge_points[i +1])
            neig[ridge_points[i] + 1].append(ridge_points[i])
        
        However this is slowwwwwwwwwwwwwwww ---------------

        Hence we make a reduced array of size 2 * npoints. 
        
        The even elements contain the neighbours for the 
        mesh points in the even ridge_points elements

        The odd elements contain the neighbours for the 
        mesh points in the even ridge_points elements

        Then we reduce the size of the reduced array to npoints.
    '''

    
    nele_ra = npoints * 2    
    points_neighbour = np.zeros(nele_ra + 1, dtype=int)
    neighbours = np.empty(len(ridge_points) * 2, dtype=int)
    ridges = np.empty(len(ridge_points) * 2, dtype=int)

    # How many neighbours each one of them have
    ele_arg_sort = [[] for i in range(2)]
    for i in range(2):
        
        ele_ridges = ridge_points[:, i]            
        
        ele_arg_sort[i] = np.argsort(ele_ridges)                      
        ele_idx = ele_ridges[ele_arg_sort[i]]
        
        # bincount returns an array of size max(ele_idx)
        ele_count = np.bincount(ele_idx).astype(int)
        points_neighbour[
            np.arange(i, nele_ra, 2)[:len(ele_count)] + 1] = ele_count

    points_neighbour = np.cumsum(points_neighbour, dtype=int)
    
    # Fill up their values
    for i in range(2):            
        _map = reduced_array.map2elements(
            elements=np.arange(i, nele_ra, 2), indices=points_neighbour)
        neighbours[_map == 0] = ridge_points[:, (i + 1) % 2][ele_arg_sort[i]]
        ridges[_map == 0] = ele_arg_sort[i]

    points_neighbour = points_neighbour[np.arange(0, nele_ra + 1, 2)]

    return neighbours, ridges, points_neighbour
