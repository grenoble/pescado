''' external_boundary_shape tested indirectly in test_mesh.
'''
import copy

import numpy as np
from numpy.testing import (
    assert_array_almost_equal, assert_array_equal, assert_allclose)

from matplotlib import pyplot as plt

from pescado.mesher import shapes, patterns
from pescado.mesher import voronoi as voronoi_m
from pescado.tools.meshing import (kd_where,
    grid, sequencial_sort, unique, external_boundary, internal_boundary)
from pescado.tools import _meshing, meshing
from pescado.tools import reduced_array as ra

from pytest import raises

from pescado.mesher.patterns import Rectangular

def compare_voronoi(
    qhull_pts, pattern, shape, test_points=None):
    ''' Asserts that the voronoi inside shape is the same as the one in pattern

    Parameters
    ----------
    qhull_pts: np.ndarray of floats with shape(m, d)
        Coordinates of the points used to make the reference Qhull
        voronoi diagram
        m number of points
        d dimension
        d dimension
    
    test_points: np.ndarray of integers (default None)
        Index of the coodinates in qhull_pts whose vornoi
        diagram should match that obtained by pattern.

        Default it takes all bounded points. 
    
    pattern: instance of 'pescado.mesher.patterns.Pattern'

    shape: instance of 'pescado.mesher.shapes.Shape'

    
    Note:
    -----

    The basic example is with

        qhull_pts = np.concatenate([points, ext_bd])

    This assures that the tested points are the ones with a
    bounded voronoi cell. Hence, the ext_bd is used to make sure the neighbours
    in the voronoi diagram constructed by Qhull using [points + ext_bd] will
    match those obtained by pattern.voronoi.neighbours. If ext_bd is not used,
    some of the neighbours will not match, since some pattern.voronoi.
    neighbours migth not belong to the points array. If [points + ext_bd] is
    used such problem is not found as long as only the points forming a
    bounded_cell in the corresponding voronoi diagram built using Qhull are
    tested. If unbounded points are tested, then the error reappears.

    Tests de following :

        That the values of first_neighbours_qhull() is the same as the one
        obtained with pescado.mesher.pattern.first_neighbours()

        Test the values pescado.mesher.pattern.ridges_and_vertices()

        Uses pattern.inside and pattern.__call__()
    '''

    pat_tags = pattern.inside(shape)
    pat_coord = pattern(pat_tags)

    (values, vertices,
     ridge_points, ridge_vertices, ridges,
     region_vertices, regions) = voronoi_m.voronoi_diagram(
        points=qhull_pts)
    
    bounded_pts, unbounded_pts = voronoi_m.region_type(
        region_vertices=region_vertices, regions=regions)
    
    if test_points is not None:
        bounded_pts = np.intersect1d(test_points, bounded_pts)
        assert len(bounded_pts) == len(test_points), (
            'Some unbounded points were given as test_points')

    assert_array_equal(
        pat_coord, qhull_pts[bounded_pts], 
        err_msg='Pattern coordinates differ from testing reference')
    

    (neighbours, 
     ridges_neig, points_neighbour) = voronoi_m.first_neighbours_qhull(
        npoints=qhull_pts.shape[0], ridge_points=ridge_points)
    
    bnd_neigs, bnd_point_neig  = ra.extract_elements(
        elements=bounded_pts, values=neighbours, indices=points_neighbour)

    bnd_region_vertices, bnd_regions = ra.extract_elements(
        elements=bounded_pts, values=region_vertices, indices=regions)

    ### Generate reference voronoi geometrical data
    _property = ['surface', 'distance', 'volume', 'safety_radius']
    property_dict = {i:None for i in _property}

    if 'surface' in _property:
        property_dict['surface'] = meshing.surfaces(
            vertices, ridge_vertices, ridges)

    if 'distance' in _property:
        
        property_dict['distance'] = meshing.distances(
            points_coord=qhull_pts,
            neig_coord=qhull_pts[neighbours],
            neig=np.arange(len(neighbours), dtype=int),
            points_neig=points_neighbour)
    
    if 'volume' in _property:
        property_dict['volume'] = meshing.volumes(
            vertices=vertices, 
            region_vertices=bnd_region_vertices,
            regions=bnd_regions)
    
    if 'safety_radius' in _property:
        property_dict['safety_radius'] = meshing.safety_radius(
            points=qhull_pts[bounded_pts], vertices=vertices,
            region_vertices=bnd_region_vertices, regions=bnd_regions)


    #############"" Test first neighbours
    (pat_neig_tag, 
     pat_neighbours, pat_points_neighbour) = pattern.first_neighbours(        
        points_tag=pat_tags)

    map2bnd_n, inside = kd_where(
        array=pattern(pat_neig_tag), 
        ref_array=qhull_pts)
    
    print(np.bincount(map2bnd_n))
    assert np.all(np.bincount(map2bnd_n) < 2), (
        'pat_neig_tag is not unique')
    assert np.all(inside), (
        'Not all neighbours where found in Qhull neighbours')

    assert_array_equal(
        bnd_point_neig, pat_points_neighbour, 
        err_msg='pattern.first_neighbours failed interval testing')
    
    assert_array_equal(
        map2bnd_n[pat_neighbours][
            ra.argsort(pat_points_neighbour, map2bnd_n[pat_neighbours])],
        bnd_neigs[
            ra.argsort(bnd_point_neig, bnd_neigs)],
        err_msg='pattern.first_neighbours failed index testing')
    
    ############# Test ridges_and_vertices
    (pat_vertices, pat_ridge_vertices, 
     pat_ridges, point_ridges, pat_region_vertices, 
     pat_regions) = pattern.ridges_and_vertices(
        points_tag=pat_tags, neig_tags=pat_neig_tag,
        neighbours=pat_neighbours, point_neighbours=pat_points_neighbour)

    ## Generate test geometrical properties
    (pat_property_dict, geo_neig_tags, geo_neighbours,
     geo_point_neighbours) = pattern.geometrical_properties(
        points_tag=pat_tags, _property=_property)

    map2bnd_v, inside = kd_where(
        array=pat_vertices, ref_array=vertices)    
    
    assert np.all(np.bincount(map2bnd_v) < 2), (
        'pat_vertices is not unique')
    assert np.all(inside), (
        'Not all vertices where found in Qhull vertices')

    # vertices and vertex indexing     
    assert_array_almost_equal(
        vertices[map2bnd_v], pat_vertices,
        err_msg='pattern.ridges_and_vertices failed ridge index test')
    
    # # regions - since the points have the same indexing
    assert_array_equal(
        bnd_regions, pat_regions,
        err_msg='pattern.ridges_and_vertices failed ridge interval testing')

    assert_array_equal(
        map2bnd_v[pat_region_vertices][
            ra.argsort(
                indices=pat_regions, values=map2bnd_v[pat_region_vertices])],
        bnd_region_vertices[
            ra.argsort(indices=bnd_regions, values=bnd_region_vertices)],
        err_msg='pattern.first_neighbours failed index testing')
    
    ## Volume and safety_radius
    assert_allclose(
        pat_property_dict['volume'], property_dict['volume'],
        err_msg='pattern volume failed reference testing')
    
    assert_allclose(
        pat_property_dict['safety_radius'],
        property_dict['safety_radius'],
        err_msg='pattern safety_radius failed reference testing')
    
    ## Ridges vertex test
    for ref_pt, pat_pt in zip(bounded_pts, np.arange(len(pat_coord))):
                
        # Get the neighbours and sort them for test pattern
        idx = np.arange(pat_points_neighbour[pat_pt], 
                        pat_points_neighbour[pat_pt+1])
        
        neig_idx = pat_neig_tag[pat_neighbours[idx]]
        a_sort = np.argsort(pattern(neig_idx)[:, 0])
        
        # Get the neighbour index for the geometrical properties
        pt_surface = pat_property_dict['surface'][
            geo_point_neighbours[pat_pt]:geo_point_neighbours[pat_pt+1]]
        pt_distance = pat_property_dict['distance'][
            geo_point_neighbours[pat_pt]:geo_point_neighbours[pat_pt+1]]
        
        map_geo_neig, inside = kd_where(
            array=pattern(neig_idx), 
            ref_array=pattern(geo_neig_tags[geo_neighbours
                [geo_point_neighbours[pat_pt]:geo_point_neighbours[pat_pt+1]]]))
        
        assert np.all(inside)
        pt_surface = pt_surface[map_geo_neig]
        pt_distance = pt_distance[map_geo_neig]

        # Get the neighbours and sort them for ref pattern
        ref_idx = np.arange(points_neighbour[ref_pt], 
                            points_neighbour[ref_pt+1])
        ref_neig_idx = neighbours[ref_idx]
        refa_sort = np.argsort(qhull_pts[ref_neig_idx][:, 0])        

        map_, inside = kd_where(array=pattern(neig_idx),
                                ref_array=qhull_pts[ref_neig_idx])
        assert np.all(inside)
        
        for i, (pat_i, ref_i) in enumerate(zip(idx, ref_idx[map_])):
            
            assert_allclose(
                pt_distance[i],
                property_dict['distance'][ref_i],
                err_msg='Failed individual neighbour distance test')
                        
            rdg_idx = point_ridges[pat_i]
            ref_rdg_idx = ridges_neig[ref_i]
            assert_allclose(
                pt_surface[i], property_dict['surface'][ref_rdg_idx],
                err_msg='Failed individual ridge surface test')
            
            vertex_coord = pat_vertices[pat_ridge_vertices[
                pat_ridges[rdg_idx]:pat_ridges[rdg_idx +1]]]
            ref_vertex_coord = vertices[ridge_vertices[
                ridges[ref_rdg_idx]:ridges[ref_rdg_idx +1]]]

            assert np.all(_meshing.remove(
                vertex_coord, ref_vertex_coord, tol=1e-12) == (0, 0)), (
                'Failed individual ridge coordinate test')


def inside_rectangular_test(plot):

    vertices = (np.array([[-10, -20, -10]])
                + (np.random.rand(50, 3)
                * np.array([[5, 5, 5]])))

    delaunay = shapes.Delaunay(coordinates=vertices)
    box = shapes.Box(lower_left=[-11, -18.5, -8.5], size=[2.5, 2, 2])

    step = [1, 0.8, 0.7]
    center = (-12.34, -10.432, -4.53)

    ticks = [
        lambda i: i * step[0],
        lambda j: j * step[1],
        lambda k: k * step[2]]

    tag2ticks = [
        lambda i: i / step[0],
        lambda j: j / step[1],
        lambda k: k / step[2]]

    rect_pattern = patterns.Rectangular(
        ticks=ticks,
        tag2ticks=tag2ticks,
        center=copy.deepcopy(center))

    constant_rect_pattern = patterns.Rectangular.constant(
        element_size=step,
        center=copy.deepcopy(center))

    coordinates_reference = grid(
        bbox=np.array([[-15, -20, -12], [0, -10, 0]]),
        step=step,
        center=copy.deepcopy(center))

    for shape_ in [delaunay, box]:
        for pat in [rect_pattern, constant_rect_pattern]:

            coordinates_reference_shape = coordinates_reference[
                shape_(coordinates_reference)]

            indices = pat.inside(shape_)

            coordinates = pat(indices)

            with raises(ValueError):

                wrong_coordinates = copy.deepcopy(coordinates)
                wrong_coordinates[0, 0] += 1e-5
                wrong_coordinates[1, 1] += 1e-2
                wrong_coordinates[2, 2] -= 1e-2
                pat.tags(coords=wrong_coordinates, closest=False)

            assert_allclose(
                coordinates,
                coordinates_reference_shape,
                rtol=1e-10,
                atol=1e-16,
                err_msg=(
                    'inside method for Rectangular pattern did not pass test.'))

            if plot:

                plt.plot(coordinates[:, 0],
                        coordinates[:, 1], 'g.', alpha=1)

                plt.plot(coordinates_reference_shape[:, 0],
                        coordinates_reference_shape[:, 1], 'r.', alpha=0.3)

                plt.show()

    print('Rectangular passed all inside method tests')


def voronoi_first_neig_rectangular_test(plot=False):
    ''' Verifies that the local voronoi cell obtained from a voronoi diagram
    calculated with Qhull is the same as the one with Pattern.Rectangle.
    The same for first_neighbours method.
    '''

    seed = 50
    # 2D Test
    np.random.seed(seed=seed)

    vertices = (np.array([[-3, -5]])
                + (np.random.rand(20, 2)
                   * np.array([[3, 5]])))

    delaunay = shapes.Delaunay(coordinates=vertices)

    step = [1, 3.54]
    center = (15.34, 23.432)
    #center = (0.0, 0.0)

    ticks = (
        lambda i: i * step[0],
        lambda j: j * step[1])

    tag2ticks = (
        lambda i: i / step[0],
        lambda j: j / step[1])

    rect_pattern = patterns.Rectangular(
        ticks=ticks, tag2ticks=tag2ticks, center=center)

    delaunay_indices = rect_pattern.inside(delaunay)
    delaunay_coordinates = rect_pattern(delaunay_indices)

    int_bd, ext_bd = rect_pattern.boundary(delaunay)
    qhull_pts = np.concatenate([delaunay_coordinates, rect_pattern(ext_bd)])

    compare_voronoi(
        qhull_pts=qhull_pts,        
        pattern=rect_pattern, shape=delaunay)

    # 3D test
    vertices_3D = (np.array([[-3, -5, -4]])
                + (np.random.rand(10, 3)
                   * np.array([[3, 5, 4]])))

    delaunay_3D = shapes.Delaunay(coordinates=vertices_3D)

    step_3D = [1, 3.54, 2.3]
    center_3D = (15.34, 23.432, 39)

    generating_functions_3D = (
        lambda i: i * step_3D[0],
        lambda j: j * step_3D[1],
        lambda K: K * step_3D[2])

    inverse_generating_functions_3D = (
        lambda x: x / step_3D[0],
        lambda y: y / step_3D[1],
        lambda z: z / step_3D[2])

    rect_pattern_3D = patterns.Rectangular(
        ticks=generating_functions_3D,
        tag2ticks=inverse_generating_functions_3D,
        center=center_3D)

    delaunay_indices_3D = rect_pattern_3D.inside(delaunay_3D)
    delaunay_coordinates_3D = rect_pattern_3D(delaunay_indices_3D)

    int_bd_3D, ext_bd_3D = rect_pattern_3D.boundary(delaunay_3D)

    qhull_pts_3D = np.concatenate([
        delaunay_coordinates_3D,
        rect_pattern_3D(ext_bd_3D)])

    compare_voronoi(
        qhull_pts=qhull_pts_3D,
        pattern=rect_pattern_3D, shape=delaunay_3D)
    
    # 3D constant test
    vertices_3D = (np.array([[-3, -5, -4]])
                + (np.random.rand(10, 3)
                   * np.array([[3, 5, 4]])))

    delaunay_3D = shapes.Delaunay(coordinates=vertices_3D)

    rect_pattern_3D_c = patterns.Rectangular.constant(
        element_size=(1, 3, 2))

    delaunay_indices_3D = rect_pattern_3D_c.inside(delaunay_3D)
    delaunay_coordinates_3D = rect_pattern_3D_c(delaunay_indices_3D)

    int_bd_3D, ext_bd_3D = rect_pattern_3D_c.boundary(delaunay_3D)

    qhull_pts_3D = np.concatenate([
        delaunay_coordinates_3D,
        rect_pattern_3D_c(ext_bd_3D)])

    compare_voronoi(
        qhull_pts=qhull_pts_3D,
        pattern=rect_pattern_3D_c, shape=delaunay_3D)    
    
    # 1D test
    shape_1D = shapes.Box(lower_left=[-10, ], size=[10, ])

    step_1D = [1, ]
    center_1D = (15.34,)

    generating_functions_1D = (
        lambda i: i * step_1D[0],)

    inverse_generating_functions_1D = (
        lambda x: x / step_1D[0], )

    rect_pattern_1D = patterns.Rectangular(
        ticks=generating_functions_1D,
        tag2ticks=inverse_generating_functions_1D,
        center=center_1D)

    indices_1D = rect_pattern_1D.inside(shape_1D)
    coordinates_1D = rect_pattern_1D(indices_1D)

    int_bd_1D, ext_bd_1D = rect_pattern_1D.boundary(shape_1D)

    qhull_pts_1D = np.concatenate([
        coordinates_1D, rect_pattern_1D(ext_bd_1D)])

    compare_voronoi(
        qhull_pts=qhull_pts_1D,
        pattern=rect_pattern_1D, shape=shape_1D)


def test_Pattern():
    '''
        Test Pattern functions that are not abstract

        internal_boundary_fneig and boundary already covered

        around depends on inside and on shapes.Ellipsoid(). The former abstract
        and the latter already tested. The only thing would be the sorted aspect
        of the indices that are returned, but it is a trivial thing.

        Need to test neighbours for nth > 1.
    '''

    vertices = (np.array([[-10, -20, -10]])
                + (np.random.rand(20, 3)
                * np.array([[30, 50, 20]])))

    delaunay = shapes.Delaunay(coordinates=vertices)

    center = (15.34, 43.432, -4.53)

    ticks = [
        lambda i: i ** 2 * np.sign(i),
        lambda j: j ** 3,
        lambda k: k * 0.5]

    tag2ticks = [
        lambda i: np.sqrt(np.abs(i)) * np.sign(i),
        lambda j: j ** (1 / 3),
        lambda k: k / 0.5]

    rect_pattern = patterns.Rectangular(
        ticks=ticks,
        tag2ticks=tag2ticks,
        center=center)

    # Test for nth = 1
    points_tag = np.array([[2, 2, 2], [2, 3, 5]])
    neig_tag, neighbours, point_neighbours = rect_pattern.neighbours(
        points_tag=points_tag, nth=1)

    first_neig_ref = np.array(
        [[3, 2, 2], [1, 2, 2], [2, 3, 2],
         [2, 1, 2], [2, 2, 3], [2, 2, 1],
         [3, 3, 5], [1, 3, 5], [2, 4, 5],
         [2, 2, 5], [2, 3, 6], [2, 3, 4]])

    assert_array_equal(
        neig_tag[neighbours], first_neig_ref,
        err_msg='Pattern neighbour failed Rectangular nth=1 test')

    # Test for nth = 2
    # Since for nth = 1 it passed the test
    sneig_tag_ref, sneig_ref, neig_sneig_ref = rect_pattern.neighbours(
            first_neig_ref, nth=1)
        
    sneig_tag, sneig, neig_sneig = rect_pattern.neighbours(
        points_tag=points_tag, nth=2)

    for i in range(len(points_tag)):
        sneig_ref_ele = unique(sneig_tag_ref[
            sneig_ref[neig_sneig_ref[6 * i]:neig_sneig_ref[6 * (i+1)]]])[0]
        
        sneig_ref_ele = _meshing.remove(
            sneig_ref_ele, points_tag)[0]
        err_msg = ('Pattern neighbour failed Rectangular nth=2 test')
        
        sneig_tag_ele = sneig_tag[
            sneig[neig_sneig[i]:neig_sneig[i+1]]]

        assert np.all(_meshing.remove(
            sneig_ref_ele, sneig_tag_ele) == (0, 0)), err_msg

        assert np.all(_meshing.remove(
            sneig_tag_ele, sneig_ref_ele) == (0, 0)), err_msg


def call_rectangular_test(plot=False):
    ''' Test __call__ method in patterns.Rectangular.
    '''

    vertices = (np.array([[-10, -20, -10]])
                + (np.random.rand(20, 3)
                * np.array([[30, 50, 20]])))

    delaunay = shapes.Delaunay(coordinates=vertices)

    center = (15.34, 43.432, -4.53)

    ticks = [
        lambda i: i ** 2 * np.sign(i),
        lambda j: j ** 2 * np.sign(j),
        lambda k: k * 0.5]

    tag2ticks = [
        lambda i: np.sqrt(np.abs(i)) * np.sign(i),
        lambda j: np.sqrt(np.abs(j)) * np.sign(j),
        lambda k: k / 0.5]

    rect_pattern = patterns.Rectangular(
        ticks=ticks,
        tag2ticks=tag2ticks,
        center=center)

    # Generate a grid using inside
    index_grid = rect_pattern.inside(shape=delaunay)
    real_coord_grid = np.array(
        [ticks[d](index_grid[:, d])
         for d in range(len(center))]).T + center

    # Check if all real_coord_grid belongs to delaunay. Sanity check for inside.
    assert np.all(delaunay(real_coord_grid))

    # Check that if a pass it thorugh the inverse I get the same thing
    inverse_real_coord = np.array(
        [rect_pattern.tag2ticks[d](real_coord_grid[:, d] - center[d])
         for d in range(len(center))]).T

    # For some reason I need to add almost equal with at most decimal=14
    # Before commit 1f6a390c028aa26a91dc6748ce63cb39128df56e
    # I didn't need to TODO: Figure this out !!!
    assert_array_almost_equal(
        index_grid, inverse_real_coord, decimal=14,
        err_msg='call_rectangular_test did not pass 3D test.')

    # Check the __call__ function.
    assert_array_equal(
        real_coord_grid, rect_pattern(index_grid),
        err_msg='__call__ in Rectangular did not pass 3D test.')



def test_Rectangular(plot=False):

    # I don't test if the distance between the points
    # respect the step directly, I do that
    # inderectly using a np.arange with the given
    # step to generate the reference coordinates.
    inside_rectangular_test(plot=plot)

    # Test voronoi cell
    voronoi_first_neig_rectangular_test(plot=plot)

    # Test __call__
    call_rectangular_test(plot=plot)


def voronoi_first_neig_finite_test(tol=1e-14):
    ''' Verifies that the local voronoi cell obtained from a voronoi diagram
    calculated with Qhull is the same as the one with Pattern.Finite.
    The same for first_neighbours method.
    '''

    seed = 50
    # 2D Test
    np.random.seed(seed=seed)

    # Random list of coordinates
    coordinates = np.concatenate([
        (np.array([[-12, -22]])
         + (np.random.rand(20, 2)
            * np.array([[4, 4]]))),
        (np.array([[-8, -18]])
         + (np.random.rand(40, 2)
            * np.array([[10, 20]]))),
        (np.array([[2, 2]])
         + (np.random.rand(20, 2)
            * np.array([[4, 4]])))])

    finite_pattern = patterns.Finite.points(points=coordinates)

    box = shapes.Box(lower_left=[-6, -10], size=[8, 10])
    box_indices = finite_pattern.inside(box)
    box_coordinates = finite_pattern(box_indices)

    int_bd, ext_bd = finite_pattern.boundary(box)
    qhull_pts = np.concatenate([box_coordinates, finite_pattern(ext_bd)])

    compare_voronoi(
        qhull_pts=qhull_pts, 
        test_points=np.arange(len(box_coordinates), dtype=int),
        pattern=finite_pattern, shape=box)

    # # Test if no unbounded value is found in first neig
    # first_neig_unbounded = finite_pattern.neighbours(
    #     finite_pattern.keys[finite_pattern.unbounded_region], nth=1)
    # err_msg = (
    #         'Unbounded points found in finite_pattern.neighbours()'
    #         + 'output for 1D Finite test')
    # # print(first_neig_unbounded)
    # # print(finite_pattern.keys[finite_pattern.unbounded_region])
    # # assert (len(patterns.remove(
    # #     np.concatenate(first_neig_unbounded),
    # #     finite_pattern.keys[finite_pattern.unbounded_region],
    # #     tol=tol)) == 0), err_msg

    # first_neig_bounded = finite_pattern.neighbours(
    #     np.concatenate(first_neig_unbounded), nth=1)

    # print(first_neig_bounded)
    # print(finite_pattern.keys[finite_pattern.unbounded_region])
    # assert (len(patterns.remove(
    #     np.concatenate(first_neig_bounded),
    #     finite_pattern.keys[finite_pattern.unbounded_region],
    #     tol=tol)) == 0), err_msg

    # 3D test
    coordinates = np.concatenate([
        (np.array([[-12, -22, -7]])
         + (np.random.rand(20, 3)
            * np.array([[4, 4, 4]]))),
        (np.array([[-8, -18, -3]])
         + (np.random.rand(80, 3)
            * np.array([[10, 20, 10]]))),
        (np.array([[2, 2, 7]])
         + (np.random.rand(20, 3)
            * np.array([[4, 4, 4]])))])

    finite_pattern = patterns.Finite.points(points=coordinates)

    box = shapes.Box(lower_left=[-5, -8, -1], size=[8, 10, 4])
    box_indices = finite_pattern.inside(box)
    box_coordinates = finite_pattern(box_indices)

    int_bd, ext_bd = finite_pattern.boundary(box)
    qhull_pts = np.concatenate([box_coordinates, finite_pattern(ext_bd)])

    compare_voronoi(
        qhull_pts=qhull_pts,
        test_points=np.arange(len(box_coordinates), dtype=int),
        pattern=finite_pattern, shape=box)
    
    # 1D test
    coordinates = np.concatenate([
        (np.array([[-10,]]) + (np.random.rand(20, 1) * np.array([[2, ]]))),
        (np.array([[-8, ]]) + (np.random.rand(20, 1) * np.array([[10, ]]))),
        (np.array([[2, ]]) + (np.random.rand(20, 1) * np.array([[2, ]])))])

    finite_pattern = patterns.Finite.points(points=coordinates)

    box = shapes.Box(lower_left=[-8, ], size=[10, ])
    box_indices = finite_pattern.inside(box)
    box_coordinates = finite_pattern(box_indices)

    int_bd, ext_bd = finite_pattern.boundary(box)
    qhull_pts = np.concatenate([box_coordinates, finite_pattern(ext_bd)])

    compare_voronoi(
        qhull_pts=qhull_pts,
        test_points=np.arange(len(box_coordinates), dtype=int),
        pattern=finite_pattern, shape=box)

def inside_finite_test():
    '''
        Tests inside function for Finite.
        Verifies as well if an error is raised when the shape contains
        unbounded points.
    '''

    coordinates_reference = grid(
        bbox=np.array([[-10, -15, -12], [8, 4, 6]]),
        step=[4, 4, 4],
        center=[0, 0, 0])

    n_coord = coordinates_reference.shape[0]
    elements = {
        idx:coordinates_reference[idx, :] for idx in range(n_coord // 3)}
    elements.update(
        {'test_{0}'.format(idx):coordinates_reference[idx, :]
         for idx in np.arange(n_coord // 3, (n_coord // 3) * 2)})
    elements.update(
        {(idx, idx):coordinates_reference[idx, :]
         for idx in np.arange((n_coord // 3) * 2, n_coord)})

    finite_test = patterns.Finite(elements=elements)

    box = shapes.Box(lower_left=[-7, -10, -8], size=[10, 12, 10])

    assert_array_equal(
        finite_test(finite_test.inside(box)),
        coordinates_reference[box(coordinates_reference)],
        err_msg='Finite failed inside test')

    bounded_tag = finite_test.keys[finite_test.bounded_region]
    unbounded_tag = finite_test.keys[finite_test.unbounded_region]

    shape_only_unbounded = lambda coord: np.array([
        True if (x in list(unbounded_tag)) else False
        for x in finite_test.tags(coord)])

    # Test shape_only_unbounded
    assert ~np.any(shape_only_unbounded(finite_test(bounded_tag)))
    assert np.all(shape_only_unbounded(finite_test(unbounded_tag)))

    shape_unbounded = shapes.Delaunay(coordinates=finite_test(unbounded_tag))
    #shape_unbounded = shapes.Box(lower_left=[-22, -27, -14], size=[60, 90, 40])

    msg_captured = ('Points with unbounded voronoi cells were'
                    + ' found inside shape. Their index is : \n*')
    with raises(ValueError, match=msg_captured) as err_info:
        finite_test.inside(shape_unbounded)

    msg_captured = ('Points with unbounded voronoi cells were'
                    + ' found inside shape. Their index is : \n*')
    with raises(ValueError, match=msg_captured) as err_info:
        finite_test.inside(shape_only_unbounded)

    print('Finite passed all inside method tests')


def call_finite_test():
    ''' Test __call__ method in patterns.Finite.

        Test initializing Finite with a dictinoary and with the points method.
        Then verifies that the __call__ method is implemented correctly.

        Also tests self.tags
    '''

    coordinates_reference = grid(
        bbox=np.array([[-10, -15, -12], [8, 4, 6]]),
        step=[4, 4, 4],
        center=[0, 0, 0])

    n_coord = coordinates_reference.shape[0]
    elements = {'test_{0}'.format(idx):coordinates_reference[idx, :]
                for idx in np.arange(n_coord // 3, (n_coord // 3) * 2)}
    elements.update(
        {idx:coordinates_reference[idx, :] for idx in range(n_coord // 3)})
    elements.update(
        {(idx, idx):coordinates_reference[idx, :]
         for idx in np.arange((n_coord // 3) * 2, n_coord)})

    finite_test = patterns.Finite(elements=elements)

    bounded_tag = finite_test.keys[finite_test.bounded_region]
    unbounded_tag = finite_test.keys[finite_test.unbounded_region]

    assert_array_equal(
        finite_test(bounded_tag),
        np.array([elements[tag] for tag in bounded_tag]),
        err_msg='Finite failed __call__ test')

    assert_array_equal(
        finite_test(unbounded_tag),
        np.array([elements[tag] for tag in unbounded_tag]),
        err_msg='Finite failed __call__ test')

    # Test finite_test.tags
    shape_only_unbounded = lambda coord: np.array([
        True if (x in list(unbounded_tag)) else False
        for x in finite_test.tags(coord)])

    assert ~np.any(shape_only_unbounded(finite_test(bounded_tag)))
    assert np.all(shape_only_unbounded(finite_test(unbounded_tag)))

    finite_from_pts = patterns.Finite.points(points=coordinates_reference)

    bounded_tag = finite_from_pts.keys[finite_from_pts.bounded_region]
    unbounded_tag = finite_from_pts.keys[finite_from_pts.unbounded_region]

    assert_array_equal(
        finite_from_pts(bounded_tag),
        coordinates_reference[finite_from_pts.bounded_region],
        err_msg='Finite failed __call__ test')

    assert_array_equal(
        finite_from_pts(unbounded_tag),
        coordinates_reference[finite_from_pts.unbounded_region],
        err_msg='Finite failed __call__ test')

    # Test finite_from_pts.tags
    shape_only_unbounded = lambda coord: np.array([
        True if (x in list(unbounded_tag)) else False
        for x in finite_from_pts.tags(coord)])

    assert ~np.any(shape_only_unbounded(
        coordinates_reference[finite_from_pts.bounded_region]))
    assert np.all(shape_only_unbounded(
        coordinates_reference[finite_from_pts.unbounded_region]))


def lattice_vector_finite():
    ''' Compares finite.lattice_vector points to reference points
    '''

    # 1D
    a = 1
    coef = 2 * np.pi / (3 * a)
    vec_mat = coef * np.array([np.sqrt(3)])[:, None]

    pts = meshing.grid(bbox=np.array([[-11,], [11,]]), step=(1,))

    coord = np.matmul(pts, vec_mat)
    
    reg1 = shapes.Box(lower_left=np.array([-30, ]), size=(60,))
    reg2 = (reg1 
            - shapes.Box(lower_left=np.array([-10, ]), size=(20,)))
    
    for reg in [reg1, reg2]:
        ref_coord = coord[reg(coord)]
        hex_finite = patterns.Finite.lattice_vector(
            lv_matrix=vec_mat, region=reg)

        assert_allclose(ref_coord, hex_finite.coordinates)

    # 2D
    a = 1
    coef = 2 * np.pi / (3 * a)
    vec_mat = coef * np.array([[1, np.sqrt(3)], 
                               [1, -1 *  np.sqrt(3)]])

    pts = meshing.grid(bbox=np.array([[-11, -11], [11, 11]]), step=(1, 1))

    coord = np.matmul(pts, vec_mat)
    
    reg1 = shapes.Box(lower_left=np.array([-30, -30]), size=(60, 60))
    reg2 = (reg1 
            - shapes.Box(lower_left=np.array([-10, -10]), size=(20, 20)))
    
    for reg in [reg1, reg2]:
        ref_coord = coord[reg(coord)]

        hex_finite = patterns.Finite.lattice_vector(
            lv_matrix=vec_mat, region=reg)

        assert_allclose(ref_coord, hex_finite.coordinates)

    # 3D
    a = 1
    coef = 2 * np.pi / (3 * a)
    vec_mat = coef * np.array([[1, np.sqrt(3), 0], 
                               [1, -1 *  np.sqrt(3), 0], 
                               [0, 0, 1 / coef]])

    pts = meshing.grid(
        bbox=np.array([[-11, -11, -5], [11, 11, 5]]), step=(1, 1, 1))

    coord = np.matmul(pts, vec_mat)
    
    reg1 = shapes.Box(lower_left=np.array([-30, -30, -5]), size=(60, 60, 10))
    reg2 = (reg1 
            - shapes.Box(lower_left=np.array([-10, -10, -2]), size=(20, 20, 5)))
    
    for reg in [reg1, reg2]:
        ref_coord = coord[reg(coord)]

        hex_finite = patterns.Finite.lattice_vector(
            lv_matrix=vec_mat, region=reg)

        assert_allclose(ref_coord, hex_finite.coordinates)


def test_Finite():

    call_finite_test()
    inside_finite_test()
    # Missing bounded / unbounded thing test
    voronoi_first_neig_finite_test()
    lattice_vector_finite()


def test_inside_rect():
    ''' Tests inside using multiple 'n_max' values.
    Compares the result of Rectangular.inside with 
    Rectangular.inside_bounding_box.

    3D test -> Covers 3D and 2D cases.
    1D test.

    Ensures that inside / inside_bounding_box_multiple and 
    inside_bounding_box give the same result.
    '''
    ## 3D
    pattern = patterns.Rectangular.constant(element_size=(0.4, 0.2, 0.5))

    shape = shapes.Box(lower_left=[-15, -10, -5], size=(10, 5, 5))

    bbox_tags = pattern.tags(shape.bbox)
    # Add one element in case the shape.bbox is found
    # between two Pattern points
    fact = np.ones(bbox_tags.shape, dtype=int)
    fact[0, :] = fact[0, :] * -1
    bbox_tags += fact
    
    # Number of elements -> 26, 25, 10
    n_max_list = [26 * 5, 26 * 25, 26 * 30, 26 * 25 * 10, 26 * 25 * 11]
    for n_max in n_max_list:
        assert_array_equal(
            pattern.inside_bounding_box(shape=shape, bbox_tags=bbox_tags),
            pattern.inside(shape=shape, n_max=n_max),
            err_msg=(
                'Inside in patterns fails when splitting'
                + ' the region to be filled into sections of {0} sites'.format(n_max)))

    ## 1D
    pattern = patterns.Rectangular.constant(element_size=(0.4, ))

    shape = shapes.Box(lower_left=[-15, ], size=(10, ))
    
    bbox_tags = pattern.tags(shape.bbox)
    # Add one element in case the shape.bbox is found
    # between two Pattern points
    fact = np.ones(bbox_tags.shape, dtype=int)
    fact[0, :] = fact[0, :] * -1
    bbox_tags += fact

    # Number of elements 26
    n_max_list = [23, 26]
    for n_max in n_max_list:
        #print(n_max)
        assert_array_equal(
            pattern.inside_bounding_box(shape=shape, bbox_tags=bbox_tags),
            pattern.inside(shape=shape, n_max=n_max),
            err_msg=(
                'Inside in patterns fails when splitting'
                + ' the region to be filled into sections of {0} sites'.format(n_max)))


# def test_InverseSeries(plot=False):
#     ''' Test the InverseSeries class
#     '''

#     seed = 50
#     vertices = (np.array([[-10, -20, -10]])
#                 + (np.random.rand(20, 3)
#                 * np.array([[30, 50, 20]])))

#     delaunay = shapes.Delaunay(coordinates=vertices)

#     delaunay = shapes.Box(
#         lower_left=[-80, -80, -80], size=[160, 160, 160])

#     center = (15.34, 43.432, -4.53)

#     ticks = [
#         lambda i: i ** 2 * np.sign(i),
#         lambda j: j ** 3,
#         lambda k: k * 0.5]

#     rect_pattern = patterns.Rectangular(
#         ticks=ticks,
#         center=center)

#     # Generate a grid using inside
#     index_grid = rect_pattern.inside(shape=delaunay)

#     inverse_sequence = [
#         patterns.InverseSequence(generating_function=fun)
#         for fun in ticks]

#     for d, generating in enumerate(ticks):

#         assert_array_equal(
#             rect_pattern.tag2ticks[d](
#                 generating(index_grid[:, d])),
#             rect_pattern.tag2ticks[d](
#                 generating(index_grid[:, d])),
#             err_msg='InverseSequence failed manual test'
#         )

#     # Test _search_known_elements
#     inverse_sequence = patterns.InverseSequence(
#         generating_function=ticks[0])

#     for val in [3.2, -0.5, 8.3, 14.0]:

#         element = np.rint(rect_pattern.tag2ticks[0](val))

#         assert np.all(
#             inverse_sequence._search_known_elements(val) == (None, None)), (
#              '_search_known_elements did not pass test')

#         # Call the inverse with the val
#         el = inverse_sequence(val)

#         assert el == element
#         assert inverse_sequence._search_known_elements(val)[0] == element, (
#              '_search_known_elements did not pass test')


def boundary_test(pat, shape):
    ''' Tests if patterns.external_boundary manages to capture the external
    boundary of a given 'shape' for a given 'pat'

    Parameters
    -----------

    shape: instance of 'pescado.continuous.shapes.Shapes'

    pat: shape: instance of 'pescado.continuous.patterns.Pattern'

    '''

    internal_tags = pat.inside(shape)
    neigs_tag, neighbours, point_neighbours = pat.neighbours(
        points_tag=internal_tags, nth=1)

    # Find real internal boundary
    internal_bd, internal_idx = internal_boundary(
        internal_coord=pat(internal_tags),
        neig_coord=pat(neigs_tag),
        neighbours=neighbours,
        points_neighbour=point_neighbours,
        shape=shape, return_idx=True)

    internal_bdr_neig_tag = pat.neighbours(
        points_tag=internal_tags[internal_idx], nth=1)[0]
    
    external_bd, external_idx = external_boundary(
        internal_bdr_neig=pat(internal_bdr_neig_tag),
        shape=shape, return_idx=True)

    external_tags = internal_bdr_neig_tag[external_idx]

    int_bd, ext_bd = pat.boundary(shape)
    assert_array_equal(
        ext_bd,
        external_tags,
        ('patterns.boundary failed compairison with'
         + 'pescado.tools.meshing.external_boundary'))

    assert_array_equal(
        int_bd,
        internal_tags[internal_idx],
        ('patterns.boundary failed compairison with'
         + ' pescado.tools.meshing.internal_boundary'))


def test_boundary_3D(plot=False):
    ''' Test the internal boundary in 3D using a rectangular pattern.
    The external boundary has been tested in 2D. The neighbour
    test in 3D is enough for the external boundary.

    TODO: Add test for random patter
    '''

    # Pattern
    rect_pattern = patterns.Rectangular.constant(element_size=[0.5, ] * 3)

    # Shapes
    rect_0_A_small_2 = shapes.Box(
        lower_left=[-9.6, -9.6, -9.6], size=[4.2, 4.7, 4.7])
    rect_0_A_small = shapes.Box(
        lower_left=[-10.1, -10.1, -10.1], size=[4.7, 5.2, 5.2])

    ll = np.array([-10.51, -10.51, -10.51])
    size = np.array([5.2, 5.7, 5.7])
    rect_0_A = shapes.Box(lower_left=ll, size=size)

    sphere = shapes.Ellipsoid.hypersphere(center=(ll + (size / 2)), radius=1)

    rect_1 = rect_0_A - rect_0_A_small
    rect_2 = rect_0_A - rect_0_A_small_2
    rect_sphere = rect_0_A - sphere

    boundary_test(pat=rect_pattern, shape=rect_1)
    boundary_test(pat=rect_pattern, shape=rect_2)
    boundary_test(pat=rect_pattern, shape=rect_0_A)
    boundary_test(pat=rect_pattern, shape=rect_sphere)


def test_boundary_2D(plot=False):
    ''' Test the internal boundary in 3D using a rectangular pattern.
    The external boundary has been tested in 2D. The neighbour
    test in 3D is enough for the external boundary.

    TODO: Add test for random pattern
    '''

    # Pattern
    rect_pattern = patterns.Rectangular.constant(element_size=[0.5, ] * 2)

    # Shapes
    rect_0_A_small_2 = shapes.Box(
        lower_left=[-9.6, -9.6], size=[6.2, 6.7])
    rect_0_A_small = shapes.Box(
        lower_left=[-10.1, -10.1], size=[6.7, 7.2])

    ll = np.array([-10.51, -10.51])
    size =  np.array([7.2, 7.7])
    rect_0_A = shapes.Box(lower_left=ll, size=size)

    sphere = shapes.Ellipsoid.hypersphere(center=(ll + (size / 2)), radius=3)

    rect_1 = rect_0_A - rect_0_A_small
    rect_2 = rect_0_A - rect_0_A_small_2
    rect_sphere = rect_0_A - sphere

    boundary_test(pat=rect_pattern, shape=rect_1)
    boundary_test(pat=rect_pattern, shape=rect_2)
    boundary_test(pat=rect_pattern, shape=rect_0_A)
    boundary_test(pat=rect_pattern, shape=rect_sphere)


def neighbours_test(pat, point_test, nth=4, tol=1e-12):
    ''' Tests the pattern.neighbours function

    Parameters:
    ------------
        pat: 'pescado.mesher.patterns.Pattern' instance

        point_test: point_tag

        nth: float, default 4
            Number of nt-neighbour to be tested

        tol: float

    TODO: Rethink this test. Not good enough.
    '''

    neigs = pat.neighbours(points_tag=point_test, nth=nth)[0]

    all_neigs = pat(pat.neighbours(neigs, nth=1)[0])

    if nth >= 2:
        ref_all_neigs = unique(pat(np.concatenate(
            (pat.neighbours(points_tag=point_test, nth=nth - 1)[0],
             pat.neighbours(points_tag=point_test, nth=nth + 1)[0]))))[0]
    else:
        ref_all_neigs = unique(pat(np.concatenate(
            (point_test,
             pat.neighbours(points_tag=point_test, nth=nth + 1)[0]))))[0]

    sorted_all_neigs = sequencial_sort(
        all_neigs, order=np.arange(all_neigs.shape[1]))

    sorted_ref_all_neigs = sequencial_sort(
        ref_all_neigs, order=np.arange(ref_all_neigs.shape[1]))

    assert_array_equal(
        sorted_all_neigs,
        sorted_ref_all_neigs,
        'pattern.neighbours failed nth={0} test'.format(nth))


def test_neig_2D():
    ''' Test patterns.neighbours function for a simple Rectangular pattern

    TODO: Do it for different patterns
    '''

    rect_pattern = patterns.Rectangular.constant(element_size=[0.5, ] * 2)

    for i in np.arange(1, 4):
        neighbours_test(
            pat=rect_pattern, point_test=np.array([[0, 0]]), nth=i)


def test_neig_3D():
    ''' Test patterns.neighbours function for a simple Rectangular pattern

    TODO: Do it for different patterns
    '''

    rect_pattern = patterns.Rectangular.constant(element_size=[0.5, ] * 3)

    for i in np.arange(1, 4):
        neighbours_test(
            pat=rect_pattern, point_test=np.array([[0, 0, 0]]), nth=i)


def test_boundary_2D_old(plot=False):
    ''' Test the internal and external boundary in 2D using a rectangular
    pattern. Old test before "internal_bd_test" function.
    '''

    ############ Pattern
    rect_pattern = patterns.Rectangular.constant(element_size=[0.5, ] * 2)
    ############ Shapes

    # To increase the bounding box
    rect_0_A = shapes.Box(lower_left=[-10, -10], size=[2.5, 50])
    rect_0_B = shapes.Box(lower_left=[50, -10], size=[2.5, 50])
    rect_0  = rect_0_A | rect_0_B

    rect_1 = shapes.Box(lower_left=[0, 0], size=[10, 20])
    rect_2 = shapes.Box(lower_left=[20, 0], size=[10, 20])

    rect_disjoint = rect_0 | rect_1 | rect_2

    rect_3 = shapes.Box(lower_left=[-5, 9.6], size=[40, 0.95])

    rect_diag = shapes.rotate(
        rect_3, axis=(0, 0, 1), angle=-1 * np.pi / 4, origin=[15, 10])

    # To test if a single point outside is detected
    rect_single = shapes.Box(
        lower_left=[1, -5], size=[0.4, 25.6])
    rect_single_brd = shapes.Box(
        lower_left=[0.4, -5.6], size=[1.5, 26.6]) - rect_single

    sum_of_all_shapes = rect_disjoint | rect_3 | rect_diag | rect_single

    ############ Boundary along x
    boundary_x = list()
    ext_boundary_x = list()

    for x_min, x_max in [[0, 20], [40, 60]]:
        for y in [0, 40]:
            boundary_x.append(
                np.hstack([np.arange(x_min, x_max + 1)[:, None],
                        (np.ones(x_max - x_min + 1) * y)[:, None]]))
        for y in [-1, 41]:
            ext_boundary_x.append(
                np.hstack([np.arange(x_min, x_max + 1)[:, None],
                        (np.ones(x_max - x_min + 1) * y)[:, None]]))

    for x_min, x_max in [[-20, -15], [100, 105]]:
        for y in [-20, 80]:
            boundary_x.append(
                np.hstack([np.arange(x_min, x_max + 1)[:, None],
                        (np.ones(x_max - x_min + 1) * y)[:, None]]))
        for y in [-21, 81]:
            ext_boundary_x.append(
                np.hstack([np.arange(x_min, x_max + 1)[:, None],
                        (np.ones(x_max - x_min + 1) * y)[:, None]]))

    boundary_x_mid = list()
    ext_boundary_x_mid = list()

    for x_min, x_max in [[-10, 70],]:
        for y in [20, 21]:
            boundary_x_mid.append(
                np.hstack([np.arange(x_min, x_max + 1)[:, None],
                        (np.ones(x_max - x_min + 1) * y)[:, None]]))

        for y in [19, 22]:
            ext_boundary_x_mid.append(
                np.hstack([np.arange(x_min, x_max + 1)[:, None],
                        (np.ones(x_max - x_min + 1) * y)[:, None]]))

    ############ Boundary along y
    boundary_y = list()
    ext_boundary_y = list()
    y = 40
    for x in [0, 20, 40, 60]:
        boundary_y.append(np.hstack(
            [(np.ones(y + 1) * x)[:, None], np.arange(0, y + 1)[:, None]]))
    for x in [-1, 21, 39, 61]:
        ext_boundary_y.append(
            np.hstack([(np.ones(y + 1) * x)[:, None],
                    np.arange(0, y + 1)[:, None]]))

    y_max = 80
    y_min = -20
    for x in [-20, -15, 100, 105]:
        boundary_y.append(np.hstack(
            [(np.ones(y_max - y_min + 1) * x)[:, None],
             np.arange(y_min, y_max + 1)[:, None]]))
    for x in [-21, -14, 99, 106]:
        ext_boundary_y.append(
            np.hstack([(np.ones(y_max - y_min + 1) * x)[:, None],
                        np.arange(y_min, y_max + 1)[:, None]]))

    y_max = 21
    y_min = 20
    boundary_y_mid = list()
    ext_boundary_y_mid = list()
    for x in [-10, 70]:
        boundary_y_mid.append(np.hstack(
            [(np.ones(y_max - y_min + 1) * x)[:, None],
             np.arange(y_min, y_max + 1)[:, None]]))
    for x in [-11, 71]:
        ext_boundary_y_mid.append(np.hstack(
            [(np.ones(y_max - y_min + 1) * x)[:, None],
             np.arange(y_min, y_max + 1)[:, None]]))

    ############ Diagonal boundary
    boundary_diag = list()
    ext_boundary_diag = list()

    y_max, y_min, x_max, x_min = (47, -8, 57, 2)
    boundary_diag.append(np.hstack(
        [np.arange(x_min, x_max + 1)[::-1, None],
         np.arange(y_min, y_max + 1)[:, None]]))

    y_max, y_min, x_max, x_min = (48, -7, 58, 3)
    boundary_diag.append(np.hstack(
        [np.arange(x_min, x_max + 1)[::-1, None],
         np.arange(y_min, y_max + 1)[:, None]]))

    boundary_diag.append(np.array([[2, 48],
                                [58, -8]]))

    y_max, y_min, x_max, x_min = (46, -9, 57, 2)
    ext_boundary_diag.append(np.hstack(
        [np.arange(x_min, x_max + 1)[::-1, None],
         np.arange(y_min, y_max + 1)[:, None]]))
    y_max, y_min, x_max, x_min = (49, -6, 58, 3)
    ext_boundary_diag.append(np.hstack(
        [np.arange(x_min, x_max + 1)[::-1, None],
         np.arange(y_min, y_max + 1)[:, None]]))

    ext_boundary_diag.append(
        np.array([[1, 47], [1, 48], [3, 49], [2, 49],
                 [59, -7], [58, -9], [59, -8]]))

    boundary_x_mid = np.concatenate(boundary_x_mid)
    boundary_y_mid = np.concatenate(boundary_y_mid)
    boundary_y = np.concatenate(boundary_y)
    boundary_x = np.concatenate(boundary_x)
    boundary_diag = np.concatenate(boundary_diag)

    ext_boundary_x_mid = np.concatenate(ext_boundary_x_mid)
    ext_boundary_y_mid = np.concatenate(ext_boundary_y_mid)
    ext_boundary_y = np.concatenate(ext_boundary_y)
    ext_boundary_x = np.concatenate(ext_boundary_x)
    ext_boundary_diag = np.concatenate(ext_boundary_diag)

    ############ Boundary for single line
    inside_line = rect_pattern.inside(rect_single)
    boundary_single_line = inside_line[~rect_1(rect_pattern(inside_line))]

    ext_boundary_single_line = rect_pattern.inside(rect_single_brd)

    ############ Test that internal boundary find
    ############   all values when the shape is a single line

    err_msg = 'patterns.boundary did not pass single line 2D test'
    inside_line = rect_pattern.inside(rect_single)
    internal_bd, ignore = rect_pattern.boundary(shape=rect_single)

    assert (len(internal_bd)
            == len(unique(internal_bd)[0])), (
        'patterns.boundary did not pass unique test.')

    assert np.all(
        _meshing.remove(inside_line, internal_bd) == (0, 0)), err_msg
    assert np.all(
        _meshing.remove(internal_bd, inside_line) == (0, 0)), err_msg

    ############ Test if it finds all boundaries
    err_msg = 'patterns.boundary did not pass 2D test'
    all_boundaries, ignore = rect_pattern.boundary(shape=sum_of_all_shapes)

    assert (len(all_boundaries) == len(unique(all_boundaries)[0])), (
        'patterns.boundary did not pass unique test.')

    boundary_val = np.concatenate(
        [boundary_x, boundary_x_mid,
         boundary_y, boundary_y_mid,
         boundary_diag, boundary_single_line])

    assert np.all(
        _meshing.remove(all_boundaries, boundary_val) == (0, 0)), err_msg

    not_bdr_all, index = _meshing.remove(boundary_val, all_boundaries)

    # If not in all_boundaries, then its first neig should all be inside:
    neig_tag, neighbours, point_neighbours = rect_pattern.neighbours(
        points_tag=not_bdr_all, nth=1)

    assert np.all(sum_of_all_shapes(rect_pattern(neig_tag))), err_msg

    ############ Test all disjoint bondaries
    bdr_disjoint, ignore = rect_pattern.boundary(shape=rect_disjoint)

    assert (len(bdr_disjoint) == len(unique(bdr_disjoint)[0])), (
        'patterns.boundary did not pass unique test.')

    assert np.all(
        _meshing.remove(np.concatenate([boundary_x, boundary_y]), bdr_disjoint)
        == (0, 0)), err_msg

    ############ Test output of pattenr.Boundary
    inside_all, external_all = rect_pattern.boundary(
        shape=sum_of_all_shapes)

    err_msg = 'Test Pattern.boundary() inside failed 2D test'
    assert np.all(_meshing.remove(
            all_boundaries, inside_all) == (0, 0)), err_msg
    assert np.all(_meshing.remove(
            inside_all, all_boundaries) == (0, 0)), err_msg

    err_msg = 'Test Pattern.boundary() external failed 2D test'
    ext_boundary_val = np.concatenate(
        [ext_boundary_x, ext_boundary_x_mid,
        ext_boundary_y, ext_boundary_y_mid,
        ext_boundary_diag, ext_boundary_single_line])

    assert np.all(
        _meshing.remove(external_all, ext_boundary_val)
        == (0, 0)), err_msg

    inside_disjoint, external_disjoint = rect_pattern.boundary(
        shape=rect_disjoint)

    err_msg = 'Test Pattern.boundary() inside failed 2D test'
    assert np.all(_meshing.remove(
            bdr_disjoint, inside_disjoint) == (0, 0)), err_msg
    assert np.all(_meshing.remove(
            inside_disjoint, bdr_disjoint) == (0, 0)), err_msg

    err_msg = 'Test Pattern.boundary() external failed 2D test'

    assert np.all(
        _meshing.remove(
            np.concatenate([ext_boundary_x, ext_boundary_y]),
            external_disjoint)
            == (0, 0)), err_msg

    assert np.all(
        _meshing.remove(
            external_disjoint,
            np.concatenate([ext_boundary_x, ext_boundary_y]))
        == (0, 0)), err_msg

    ## Check if it has any repeated values (it shouldn't)

    box_1 = shapes.Box(lower_left=[-10, -15], size=[5, 5])
    step = [0.5, 0.8]
    center = (15.34, 43.432)

    ticks = [
        lambda i: i * step[0],
        lambda j: j * step[1]]

    tag2ticks = [
        lambda i: i / step[0],
        lambda j: j / step[1]]

    rect_pattern = patterns.Rectangular(
        ticks=ticks,
        tag2ticks=tag2ticks,
        center=center)

    internal_bd, ignore = rect_pattern.boundary(shape=box_1)
    assert len(internal_bd) == len(unique(internal_bd)[0]), (
        'patterns.boundary did not pass repeated values test')


def test_unique():

    test_arr = np.array(
        [(4, 3), (4, 3, 3), (4, 3, 3)], dtype=tuple)

    assert_array_equal(
        unique(array=test_arr)[0],
        np.array([(4, 3), (4, 3, 3)], dtype=tuple),
        'unique failed dtype.object test')

    test_arr = np.array(
        ['test1', 'test2', 'test2'], dtype=str)
    
    assert_array_equal(
        unique(array=test_arr)[0],
        np.array(['test1', 'test2'], dtype=str),
        'unique failed dtype.object test')

    test_arr = np.array([[5, 5], [4, 5], [5, 5]])

    assert_array_equal(
        unique(array=test_arr)[0],
        np.array([[4, 5], [5, 5]]),
        'unique failed dtype.interger test')

    test_arr = np.array([[5, 5], [4, 5]])

    assert_array_equal(
        unique(array=test_arr)[0],
        np.array([[4, 5], [5, 5]]),
        'unique failed no repeated value test')


def test_remove():

    test_arr = np.array(
        ['test1', 'test2', 'test2', 2, 3, 3, (4, 3), (4, 5)], dtype=object)
    assert_array_equal(
        patterns.remove(
            array_1=test_arr[:, None],
            array_2=np.array(['test2', 3, (4, 3)], dtype=object)[:, None]),
        np.array(['test1', 2, (4, 5)], dtype=object)[:, None],
        'patterns.remove failed dtype.object test with 2 axis')

    assert_array_equal(
        patterns.remove(array_1=test_arr,
                        array_2=np.array(['test2', 3, (4, 3)], dtype=object)),
        np.array(['test1', 2, (4, 5)], dtype=object),
        'patterns.remove failed dtype.object test with 1 axis')

    test_arr = np.array([[5, 5], [4, 5], [5, 5]])
    assert_array_equal(
        patterns.remove(array_1=test_arr, array_2=np.array([[4, 5], [10, 5]])),
        np.array([[5, 5], [5, 5]]),
        'patterns.remove failed dtype.interger test')

    test_arr = np.array([[5, 5], [4, 5], [5, 5]])
    assert_array_equal(
        patterns.remove(array_1=test_arr, array_2=np.array([[3, 5], [10, 5]])),
        test_arr,
        'patterns.remove failed nothing to remove test')
