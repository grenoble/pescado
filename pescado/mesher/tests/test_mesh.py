''' Tests for mesh_.py
'''
import time
import numpy as np
import copy


from scipy.spatial import Voronoi

from pescado.mesher import shapes
from pescado.mesher import patterns

from pescado.mesher import mesh as mesh_

from matplotlib import pyplot as plt

from pescado.tools import _meshing as c_tool
from pescado.tools import reduced_array as ra


from pescado.tools._meshing import remove as c_remove
from pescado.tools._meshing import where as c_where
from pescado.tools._meshing import unique as c_unique

from pescado.tools.meshing import sequencial_sort

from pescado.tools.meshing import (unique,
    kd_where, internal_boundary, external_boundary)

from pescado.mesher.voronoi import (
    voronoi_diagram_qhull, voronoi_diagram_1d, 
    first_neighbours_qhull, region_type)

from pescado.mesher import voronoi as voronoi_m
from numpy.testing import assert_array_equal, assert_allclose

from pytest import raises


def compare_local(
    mesh_inst, mesh_idx, ref_neig_idx, ref_neig_coord, 
    ref_vertices, ref_reg_vertices, ref_vor_ridges, 
    ref_ridges, ref_ridge_vertices):
    ''' Compare mesh properties with that of the local voronoi
    cell defined for 'mesh_idx'. 
    '''

    neig_ = mesh_inst.neighbours(points=mesh_idx)
    vor_cell = mesh_inst.voronoi(points=mesh_idx)
    
    # Some local voronoi neighbours migth not exist in the mesh.
    neig_coord = mesh_inst.coordinates(neig_)
    vor_neig_coord = vor_cell.neighbours
    
    mesh2vor_neig_idx = kd_where(
        array=neig_coord, ref_array=vor_neig_coord)[0]

    assert_array_equal(
        neig_coord, vor_neig_coord[mesh2vor_neig_idx], 
        'Voronoi neighbour not equal to mesh neighbour')

    ref_idx_neig, isin = kd_where(
        array=neig_coord, ref_array=ref_neig_coord)

    assert np.all(isin), (
        'Reference region does not have the same neighbours'
        + ' as tested region')
    
    # Compare with the reference voronoi
    reg_vertices = vor_cell.vertices
    
    idx, isin = kd_where(
        array=reg_vertices, ref_array=ref_reg_vertices)

    assert np.all(isin), (
        'Reference region does not have the same vertex'
        + ' as tested region')
         
    for n_idx in np.arange(len(neig_coord)):
        
        r_v = vor_cell.vertices[
            vor_cell.ridges[mesh2vor_neig_idx[n_idx]]]
        
        n_ref_idx = ref_idx_neig[n_idx]
        ref_r_v = ref_vertices[ref_ridge_vertices[ref_ridges[
            ref_vor_ridges[n_ref_idx]]:ref_ridges[ref_vor_ridges[n_ref_idx]+1]]]
        
        idx, isin = kd_where(
            array=r_v, ref_array=ref_r_v)

        assert np.all(isin), (
            'Reference ridge does not have the same vertices'
            + ' as tested ridge')


def compare_geometrical_properties(mesh_inst, test_idx, tol=1e-13):
    ''' Compares mesh._geometrical_property() to those in each
    point VoronoiCell. 
    '''
    
    (property_dict, 
     points_property, neighbours) = mesh_inst._geometrical_property(
        points=test_idx,
        _property=['surface', 'volume', 'distance', 'safety_radius'])
    
    for i, mesh_idx in enumerate(test_idx):

        idx_prop = np.arange(
            points_property[i], points_property[i+1], dtype=int)
        
        prop_neig = neighbours[idx_prop]

        inside_prop_neig = np.arange(len(prop_neig))[prop_neig != -1]
        prop_neig_coord = mesh_inst.coordinates(prop_neig[inside_prop_neig])
        
        neig_ = mesh_inst.neighbours(points=mesh_idx)
        vor_cell = mesh_inst.voronoi(points=mesh_idx)
        
        # Some local voronoi neighbours migth not exist in the mesh.
        neig_coord = mesh_inst.coordinates(neig_)
        vor_neig_coord = vor_cell.neighbours
        # print(mesh_inst.coordinates(mesh_idx))
        # print(neig_coord)
        # print(vor_neig_coord)
        mesh2vor_neig_idx = kd_where(
            array=neig_coord, ref_array=vor_neig_coord)[0]

        assert_array_equal(
            neig_coord, vor_neig_coord[mesh2vor_neig_idx], 
            'Voronoi neighbour not equal to mesh neighbour')

        ref_idx_neig, isin = kd_where(
            array=neig_coord, ref_array=prop_neig_coord)

        assert np.all(isin), (
            'Reference region does not have the same neighbours'
            + ' as tested region')
        
        vor_volume = vor_cell.volume
        vor_safety_radius = vor_cell.safety_radius
        vor_surface = vor_cell.surface(neig_idx=mesh2vor_neig_idx)
        vor_distance = vor_cell.distance(neig_idx=mesh2vor_neig_idx)

        test_volume = property_dict['volume'][i]
        test_safety_radius = property_dict['safety_radius'][i]
        test_surface = property_dict['surface'][idx_prop[inside_prop_neig]][
            ref_idx_neig]
        test_distance = property_dict['distance'][idx_prop[
            inside_prop_neig]][ref_idx_neig]
        
        # print(test_surface)
        # print(vor_surface)
        # print(vor_cell.surface(neig_idx=np.arange(len(vor_cell.neighbours))))
# 
        assert_allclose(
            vor_surface, test_surface, 
            err_msg='geometrical_property did not pass surface test')
        assert_allclose(
            vor_distance, test_distance, 
            err_msg='geometrical_property did not pass distance test')
        assert_allclose(
            vor_volume, test_volume,
            err_msg='geometrical_property did not pass volume test')
        assert_allclose(
            vor_safety_radius, test_safety_radius,
            err_msg='geometrical_property did not pass safety radius test')
        

def mesh_boundary_test(mesh_inst, shape):
    ''' Compares that the result from 'mesh_inst.boundary(shape)'
    is the same as the one obtained from
    'pescado.tools.meshing.internal' and
    'pescado.tools.meshing.external'
    '''

    int_bd, ext_bd = mesh_inst.boundary(shape)

    internal_idx = mesh_inst.inside(shape)
    
    neig_mesh_idx, pt_neig  = mesh_inst.neighbours(
        internal_idx, reduced_array=True)

    unq_neig_idx, neighbours = ra.zip(neig_mesh_idx)
    
    # Find real internal boundary
    ignore, internal_bdr_idx = internal_boundary(
        internal_coord=mesh_inst.coordinates(internal_idx),
        neig_coord=mesh_inst.coordinates(unq_neig_idx),
        neighbours=neighbours, 
        points_neighbour=pt_neig,
        shape=shape,
        return_idx=True)

    internal_bdr_neig_tag = mesh_inst.neighbours(
        internal_idx[internal_bdr_idx], reduced_array=True)[0]
        
    fneigs_coord = mesh_inst.coordinates(internal_bdr_neig_tag)

    external_bd, external_bd_idx = external_boundary(
        internal_bdr_neig=fneigs_coord,
        shape=shape, return_idx=True)

    external_tags = internal_bdr_neig_tag[external_bd_idx]

    assert_array_equal(
        ext_bd,
        external_tags,
        ('Mesh.boundary failed compairison with'
         + 'pescado.tools.meshing.external_boundary'))

    assert_array_equal(
        int_bd,
        internal_idx[internal_bdr_idx],
        ('Mesh.boundary failed compairison with'
         + ' pescado.tools.meshing.internal_boundary'))
    

def assert_mesh_coherence(
    mesh_inst, test_idx, tol=1e-13):
    '''   Cross tests internal methods within Mesh

    Parameters
    -----------
        mesh_inst: instance of pescado.mesher.mesh.Mesh

        test_idx: sequence of integers
            Index of the points in 'mesh_inst' and in 'coordinates' to
            test

        tol: float
            Error tolerance

    Notes
    -----
        Tests :

            ridges_and_vertices gives the same result as 
            voronoi(test_idx)
                - Compares neighbours
                - region vertices
                - ridges vertices
    '''

    # Compares mesh_inst.ridges_and_vertices with 
    # its local voronoi values. 
    # Compare with vectorized values
    (vertices, neighbours,
     ridge_vertices, ridges, 
     region_vertices, regions,
     point_ridges, point_neighbours) = mesh_inst._ridges_and_vertices(
        points=test_idx)  
    
    # Verify that the ridges vertices contain the region vertices
    # It verifies the indexing !
    for pt in range(len(test_idx)):
    
        idx_n = np.arange(point_neighbours[pt], point_neighbours[pt+1])
        pt_r = np.sort(point_ridges[idx_n])

        pt_ridg_vertices = ra.elements_value(
            elements=pt_r, values=ridge_vertices, indices=ridges)

        pt_reg_vert = region_vertices[regions[pt]:regions[pt+1]]
        
        assert len(np.setdiff1d(pt_reg_vert, pt_ridg_vertices)) == 0


    # Check -1 indeed refers to outside neighbours
    idx_outside = neighbours[neighbours == -1]
    idx_point = unique(np.searchsorted(point_neighbours, idx_outside))[0]

    neig_of_outside = ra.elements_value(
        elements=idx_point, values=neighbours, indices=point_neighbours)
    
    neig_of_outside = mesh_inst._internal_index[
        neig_of_outside[neig_of_outside != -1]]

    all_neig_of_outside = ra.elements_value(
        elements=mesh_inst._internal_index[test_idx[idx_point]], 
        values=mesh_inst._neighbours, indices=mesh_inst._pt_neig)
    
    outside_pts = np.setdiff1d(all_neig_of_outside, neig_of_outside)

    assert len(np.setdiff1d(outside_pts, mesh_inst.imcomplete_points)) == 0

    # Compare the vertices and neighbours
    # Use local voronoi cell to verify vectorized data        
    for vect_idx, local_index in enumerate(test_idx):   

        vect_neig = neighbours[
            point_neighbours[vect_idx]:point_neighbours[vect_idx+1]]

        vect_reg_vertices = vertices[region_vertices[
                    regions[vect_idx]:regions[vect_idx +1]]]
        
        vect_r = point_ridges[
            point_neighbours[vect_idx]:point_neighbours[vect_idx+1]]

        compare_local(
            mesh_inst=mesh_inst, mesh_idx=local_index, 
            ref_neig_idx=vect_neig, 
            ref_neig_coord=mesh_inst.coordinates((
                vect_neig[vect_neig != -1])),
            ref_vertices=vertices, 
            ref_reg_vertices=vect_reg_vertices,
            ref_vor_ridges=vect_r[vect_neig != -1],
            ref_ridges=ridges, ref_ridge_vertices=ridge_vertices)


def compare_voronoi_with_reference(
    mesh_inst, ref_coordinates, test_mesh_idx, test_ref_idx, tol=1e-13):
    ''' Verifies the voronoi diagram main properties: point coordinates, 
    neighbours, regions vertices and ridge vertices. Asserts that they
    matche those in the 'ref_voronoi' for the 'test_points'.

    Parameters
    -----------
    mesh_inst: instance of pescado.mesher.Mesh

    ref_coordinates: np.array of floats
        Coordinates used to generate the voronoi diagram using

            scipy.spatial.Voronoi if 2D or 3D
            pescado.mesher.voronoi.voronoi_diagram

    test_mesh_idx: np.ndarray of intergers with shape(d, )

    tol: float
        tolerance when compairing vertex  / neighbours coordinates

    Returns
    --------

    None

    Notes
    ------
    References from : 
        voronoi_diagram_qhull() method
        voronoi_diagram_1d() method
        first_neighbours_qhull() method
        region_type() method

    Tested values recovered from:
        mesh_inst.coordinates()
        mesh_inst.neighbours()
        mesh_inst.voronoi() -> generates a vor :  VoronoiCell()
            vertices -> from vor.vertices
            ridges -> from vor.ridges

    Quite slow, tests every point in test_points with a python for loop.
    
    TODO: Test vectorized functions as well once I have finished with PESCADO.
        Notice -> Coherence between the vectorized ridges_and_vertices and the
        mesh_inst.voronoi() method is tested in asser_mesh_coherence(). 

    '''

    # Points that should be ignored when testing
    ignore_idx = np.setdiff1d(
        np.arange(len(ref_coordinates), dtype=int), test_ref_idx)

    coordinates = mesh_inst.coordinates()

    assert np.all(
        c_unique(coordinates).shape == coordinates.shape), (
            ('Repeated coordinates were found in the mesh'
             + '. Failed compare_voronoi_with_reference test'))

    if ref_coordinates.shape[1] in [2, 3]:

        (values, ref_vertices,
         ref_ridge_points, ref_ridge_vertices, ref_ridges,
         ref_region_vertices, ref_regions) = voronoi_diagram_qhull(
            ref_coordinates)
        
    elif ref_coordinates.shape[1] == 1:
        (values, ref_vertices,
         ref_ridge_points, ref_ridge_vertices, ref_ridges,
         ref_region_vertices, ref_regions) = voronoi_diagram_1d(ref_coordinates)

    ref_bounded, ref_unbounded = region_type(
        region_vertices=ref_region_vertices, regions=ref_regions)

    # Compare the neighbours
    (ref_neighbours, 
     ref_vor_ridges, ref_points_neighbour) = first_neighbours_qhull(
        npoints=len(ref_coordinates), ridge_points=ref_ridge_points)
    
    # Compare the vertices and neighbours
    for ref_idx, mesh_idx in zip(test_ref_idx, test_mesh_idx):
        if ref_idx in ref_bounded:

            point = mesh_inst.coordinates(points=mesh_idx)
            ref_point = ref_coordinates[ref_idx]
            ref_neig = ref_neighbours[
                ref_points_neighbour[ref_idx]:ref_points_neighbour[ref_idx+1]]
            ref_neig_coord = ref_coordinates[ref_neig]
            
            assert_array_equal(
                point, ref_point[:], 'Points not equal')

            ref_reg_vertices = ref_vertices[ref_region_vertices[
                        ref_regions[ref_idx]:ref_regions[ref_idx +1]]]
            
            ref_r = ref_vor_ridges[
                ref_points_neighbour[ref_idx]:ref_points_neighbour[ref_idx+1]]

            compare_local(
                mesh_inst=mesh_inst, mesh_idx=mesh_idx, 
                ref_neig_coord=ref_neig_coord, 
                ref_neig_idx=ref_neig, 
                ref_vertices=ref_vertices, 
                ref_reg_vertices=ref_reg_vertices,
                ref_vor_ridges=ref_r,
                ref_ridges=ref_ridges, ref_ridge_vertices=ref_ridge_vertices)

        elif ref_idx in ref_unbounded:

            vor_cell = mesh_inst.voronoi(points=mesh_idx)
            assert np.all(
                mesh_inst._coordinates[ref_idx]
                == ref_coordinates[ref_idx])

        else:
            raise ValueError(
                '{0} neither unbounded nor bounded.'
                + 'Uncompatible voronoi diagram reference'.format(mesh_idx))
    

def compare_w_spatial(mesh_inst, tol=1e-10):
    ''' Compares the assembling pattern to the voronoi diagram
    generated with scipy.spatial.

    Parameters
    ----------
    
    tol: float
        tolerance passed to compare_voronoi_with_reference

    Notes
    -----
    If the assembling pattern passes the 'compare_voronoi_with_reference' test,
    then it is assumed to be correct.
    '''   
    
    compare_voronoi_with_reference(
        mesh_inst=mesh_inst, ref_coordinates=mesh_inst._coordinates,
        test_mesh_idx=np.arange(mesh_inst.npoints, dtype=int),
        test_ref_idx=mesh_inst._internal_index[
            np.arange(mesh_inst.npoints, dtype=int)],
        tol=tol)
    

def mesh_continuity_test(
    coordinates, tags, tags2pat, neighbours, neig_idx,
    _internal_index, patterns_list, meshing_region):
    ''' Mesh continuity is defined for the first neighbours
    and for the voronoi cell vertices and ridges.

    Ensures that the the coordinates of the first neighbours in
    'neig_list' matches that of the pattern first neighbours for
    every points in the coordinates.

    Ensures that the the voronoi cell vertices of the
    first neighbours is 'neig_list' matches that of
    point in question.

    Parameters
    -----------
        coordinates:
        tags2pat:
        neighbours:
        neig_idx:
        meshing_region:

    Raises assertion error
    '''

    ndim = coordinates.shape[1]
    
    for ele in np.arange(len(coordinates)):
                
        pat_num = tags2pat[_internal_index[ele]]
        tag = tags[_internal_index[ele]]

        mesh_neig = neighbours[neig_idx[ele]:neig_idx[ele + 1]]        
        mesh_neig_coord = coordinates[mesh_neig]
        
        if isinstance(tag, (int, np.integer)):
            tag = np.array([tag, ])

        pat_neig_coord =  patterns_list[pat_num](
            patterns_list[pat_num].neighbours(tag)[0])

        # The neighbour must be inside the pattern
        pat_pt_neig = np.arange(len(pat_neig_coord), dtype=int)[
            meshing_region(pat_neig_coord)]

        map_ = c_tool.where(mesh_neig_coord, pat_neig_coord[pat_pt_neig])

        assert isinstance(map_, np.ndarray)
        pat_pt_neig = pat_pt_neig[map_]

        #pat_pt_neig = np.arange(len(pat_neig_coord))[pat_pt_neig[[map_]]]
        # Check that the neighbours have all the same coordinate

        assert_array_equal(
                mesh_neig_coord, pat_neig_coord[pat_pt_neig],
                err_msg='Mesh not continuous')

        ele_vor = patterns_list[pat_num].voronoi(tag[0])

        # Check that they share the same ridge
        for i, neig in zip(pat_pt_neig, mesh_neig):

            neig_pat_num = tags2pat[_internal_index[neig]]           
            neig_vor = patterns_list[neig_pat_num].voronoi(tags[
                _internal_index[neig]])
                        
            neig_ele_idx = c_tool.where(
                coordinates[ele][None, :], neig_vor.neighbours)
            ele_idx = c_tool.where(
                coordinates[neig][None, :], ele_vor.neighbours)

            assert len(neig_ele_idx) == 1
            neig_ele_idx = neig_ele_idx[0]
            assert len(ele_idx) == 1
            ele_idx = ele_idx[0]

            neig_ridge = neig_vor.vertices[neig_vor.ridges[neig_ele_idx]]
            
            ele_ridge = ele_vor.vertices[ele_vor.ridges[ele_idx]]
            res = c_tool.remove(neig_ridge, ele_ridge, tol=1e-12)
            
            if isinstance(res[0], np.ndarray):
                raise RuntimeError(
                    'Failed ridge comparison test')

##############################################################################
################################ Refine tests ################################
##############################################################################

def test_indexing_and_boundary_mesh():
    ''' Test indexing related methods in pescado.mesher.mesh.Mesh
    and test Mesh.boundary method.
    '''
    ## Generate the 2D patterns
    step = [1, 1]
    center = (15.34, 43.432)

    ticks = [
        lambda i: i * step[0],
        lambda j: j * step[1]]

    tag2ticks = [
        lambda i: i / step[0],
        lambda j: j / step[1]]

    rect_pattern_1 = patterns.Rectangular(
        ticks=ticks,
        tag2ticks=tag2ticks,
        center=center)

    step2 = [0.5, 0.5]
    center = (15, 41.432)

    ticks = [
        lambda i: i * step2[0],
        lambda j: j * step2[1]]

    tag2ticks = [
        lambda i: i / step2[0],
        lambda j: j / step2[1]]

    rect_pattern_2 = patterns.Rectangular(
        ticks=ticks,
        tag2ticks=tag2ticks,
        center=center)

    # step2 = [0.12, 0.26]
    # center = (15, 41.432)

    # ticks = [
    #     lambda i: i * step2[0],
    #     lambda j: j * step2[1]]

    # rect_pattern_3 = patterns.Rectangular(
    #     ticks=ticks,
    #     center=center)

    #####print(test_idx)[-5, -10], size=[5, 5])
    space = shapes.Box(lower_left=[-13, -16], size=[25, 20])
    pattern_def = patterns.Rectangular.constant(element_size=(2, 2))


    box_1 = shapes.Box(lower_left=[-10, -15], size=[5, 5])
    box_1_in = shapes.Box(lower_left=[-9, -14], size=[1, 1])
    box_1_inB = shapes.Box(lower_left=[-7, -12], size=[1, 1])
    box_2 = shapes.Box(lower_left=[-5, -10], size=[5, 5])
    box_3 = shapes.Box(lower_left=[-0, -5], size=[5, 5])
    box_4 = shapes.Box(lower_left=[-4.9, -15], size=[5, 5])
    box_5 = shapes.Box(lower_left=[-5.1, -10], size=[5, 5])
    box_6 = shapes.Box(lower_left=[5, -10], size=[5, 5])
    box_7 = shapes.Box(lower_left=[5, -12], size=[5, 5])
    box_not_inter = shapes.Box(lower_left=[-12, -12], size=[1, 1])

    meshes = [(box_not_inter, rect_pattern_2), #rect_pattern_3
              (box_1_inB, rect_pattern_2),
              (box_2, rect_pattern_1),
              (box_3, rect_pattern_2), #rect_pattern_3
              (box_4, rect_pattern_2),
              (box_5, rect_pattern_2),
              (box_6, rect_pattern_1),
              (box_7, rect_pattern_2)] #rect_pattern_3
    
    mesh = mesh_.Mesh(simulation_region=space, pattern=pattern_def)
    for reg, pat in meshes:
        mesh.refine(region=reg, pattern=pat)   

    meshes_2 = [(box_1, rect_pattern_1, 1),
                (box_1_in, rect_pattern_2, 2)]
    
    for reg, pat, nth in meshes_2:
        mesh.refine(region=reg, pattern=pat, nth=nth)

    patterns_ref = mesh._patterns

    ## Test the mesh_inst.boundary() method.

    # Test a shape entirely inside the mesh
    mesh_boundary_test(
        mesh_inst=mesh,
        shape=shapes.Box(lower_left=[-8, -12], size=[2, 2]))

    # Test a shape partially inside the mesh
    mesh_boundary_test(
        mesh_inst=mesh,
        shape=shapes.Box(lower_left=[-14, -18], size=[6, 4]))

    # Test a shape outside inside the mesh
    with raises(ValueError):
        mesh_boundary_test(
            mesh_inst=mesh,
            shape=shapes.Box(lower_left=[-30, -30], size=[6, 4]))

    print('Passed single 2d mesh - boundary test')

    coordinates = mesh.coordinates()

    assert np.all(
        c_unique(coordinates).shape == coordinates.shape), (
            ('Repeated coordinates were found in the mesh'
             + '. Failed indexing test'))

    test_indices = np.concatenate(
        [np.array([0, mesh.npoints - 1]).astype(int),
         np.arange(0, mesh.npoints, 4, dtype=int),
         np.arange(0, mesh.npoints, 3, dtype=int)])
    test_coordinates = mesh.coordinates(test_indices)

    assert_array_equal(
        test_indices,
        mesh.points(
            coordinates=test_coordinates),
        '_indices_from_c oordinates did not pass regression test')

    for i in range(len(patterns_ref)):
        
        ele_idx = mesh._internal_index.indices[
            mesh._index2pat[mesh._internal_index.values] == i]
        internal_idx = mesh._internal_index[ele_idx]
        tags = mesh._tags[internal_idx]

        if len(tags) > 0:
            if isinstance(tags[0], np.ndarray):
                tags = np.vstack(tags)

            assert_array_equal(
                patterns_ref[i](tags), mesh.coordinates(ele_idx),
                err_msg='Mesh._tags did not pass 2D indexing test')

            index = mesh.points(mesh.coordinates(ele_idx))

            assert_array_equal(
                index, ele_idx,
                err_msg='Mesh.points did not pass 2D indexing test'
            )

            if len(tags) > 0:

                assert_array_equal(
                    mesh._patterns[i](tags),
                    mesh.coordinates(points=index),
                    err_msg=('Mesh.coordinates did not pass 2d'
                            + '(sequence input) indexing test'))

                assert_array_equal(
                    mesh._patterns[i](tags[0])[0],
                    mesh.coordinates(points=index[0]),
                    err_msg=('Mesh.coordinates did not pass 2d'
                            + '(int input) indexing test'))


def mesh_1d_regression_test():

    ## Generate the 2D patterns
    step = [1, ]
    center = (15.34, )

    ticks = [
        lambda i: i * step[0],]

    tag2ticks = [
        lambda i: i / step[0],]

    rect_pattern_1 = patterns.Rectangular(
        ticks=ticks,
        tag2ticks=tag2ticks,
        center=center)

    step2 = [0.5, ]
    center = (15, )

    ticks = [
        lambda i: i * step2[0],]

    tag2ticks = [
        lambda i: i / step2[0],]

    rect_pattern_2 = patterns.Rectangular(
        ticks=ticks,
        tag2ticks=tag2ticks,
        center=center)

    ############################################
    # Test comp with scipy.spatial.Voronoi

    ## Test all points ########################################################
    space = shapes.Box(lower_left=[-16,], size=[30, ])
    pat_coarse = patterns.Rectangular.constant(element_size=(4, ))

    ### Test single builder_inst (no assembling needed)
    box_1 = shapes.Box(lower_left=[-11,], size=[6,])
    
    mesh = mesh_.Mesh(simulation_region=box_1, pattern=rect_pattern_1)
        
    coordinates = mesh.coordinates()

    space_shape = mesh._simulation_region
    ext_bd = rect_pattern_1(rect_pattern_1.boundary(space_shape)[1])
    pts = c_unique(np.concatenate([coordinates, ext_bd]))

    assert np.all(
        c_unique(coordinates).shape == coordinates.shape), (
            ('Repeated coordinates were found in the mesh'
             + '. Failed 1D regression test'))

    compare_voronoi_with_reference(
        mesh_inst=mesh, ref_coordinates=pts,
        test_mesh_idx=np.arange(mesh.npoints, dtype=int),
        test_ref_idx=np.arange(mesh.npoints, dtype=int),
        tol=1e-12)

    print('Passed single 1D mesh - full mesh - reference compairision')

    ### Test non-intersecting mesh (# pattern)
    box_3 = shapes.Box(lower_left=[-0,], size=[5,])

    with raises(
        NotImplementedError,
        match=('Intersection between non-intersecting'
               + ' shapes is not implemented')):
        test = box_1 & box_3

    meshes = [(box_1, rect_pattern_1),
              (box_3, rect_pattern_1)]
    
    mesh = mesh_.Mesh(simulation_region=space, pattern=pat_coarse)
    for reg, pat in meshes:
        mesh.refine(region=reg, pattern=pat)
    compare_w_spatial(mesh_inst=mesh)

    ### Test mesh side-by-side (# pattern)
    box_4 = shapes.Box(lower_left=[-4.9, ], size=[5, ])

    #### Tests if the shapes are not intersecting
    with raises(
        NotImplementedError,
        match=('Intersection between non-intersecting'
               + ' shapes is not implemented')):
        test = box_1 & box_4

    meshes = [(box_1, rect_pattern_1),
              (box_4, rect_pattern_2)]

    mesh = mesh_.Mesh(simulation_region=space, pattern=pat_coarse)
    for reg, pat in meshes:
        mesh.refine(region=reg, pattern=pat)
    compare_w_spatial(mesh_inst=mesh)

    ### Test intersecting mesh only one layer
    box_5 = shapes.Box(lower_left=[-5.1,], size=[5,])

    #### Tests if the intersection is only one layer thick
    test = box_1 & box_5

    assert np.all(
        rect_pattern_2(rect_pattern_2.inside(test))[:, 0] == -5.)
    assert len(rect_pattern_1(rect_pattern_1.inside(test))) == 0

    meshes = [(box_1, rect_pattern_1),
            (box_5, rect_pattern_2)]

    mesh = mesh_.Mesh(simulation_region=space, pattern=pat_coarse)
    for reg, pat in meshes:
        mesh.refine(region=reg, pattern=pat)
    compare_w_spatial(mesh_inst=mesh)

    ### Test intersecting mesh next to border of Builder.shape
    box_6 = shapes.Box(lower_left=[5,], size=[5,])
    box_7 = shapes.Box(lower_left=[5,], size=[5,])

    meshes = [(box_6, rect_pattern_1),
            (box_7, rect_pattern_2)]

    mesh = mesh_.Mesh(simulation_region=space, pattern=pat_coarse)
    for reg, pat in meshes:
        mesh.refine(region=reg, pattern=pat)
    compare_w_spatial(mesh_inst=mesh)

    ### Test finer mesh inside coarser mesh
    box_1_inB = shapes.Box(lower_left=[-7, ], size=[1, ])

    meshes = [(box_1, rect_pattern_1),
            (box_1_inB, rect_pattern_2)]

    mesh = mesh_.Mesh(simulation_region=space, pattern=pat_coarse)
    for reg, pat in meshes:
        mesh.refine(region=reg, pattern=pat)
    compare_w_spatial(mesh_inst=mesh)

    ### test coarser mesh inside finer mesh
    box_1_inB = shapes.Box(lower_left=[-7,], size=[1,])

    meshes = [(box_1_inB, rect_pattern_1),
            (box_1, rect_pattern_2)]

    mesh = mesh_.Mesh(simulation_region=space, pattern=pat_coarse)
    for reg, pat in meshes:
        mesh.refine(region=reg, pattern=pat)
    compare_w_spatial(mesh_inst=mesh)

    ### Test all together
    box_1 = shapes.Box(lower_left=[-11,], size=[6,])
    box_1_in = shapes.Box(lower_left=[-9,], size=[1,])
    box_1_inB = shapes.Box(lower_left=[-7,], size=[1,])
    box_2 = shapes.Box(lower_left=[-5,], size=[5,])
    box_3 = shapes.Box(lower_left=[-0,], size=[5,])
    box_4 = shapes.Box(lower_left=[-4.9,], size=[5,])
    box_5 = shapes.Box(lower_left=[-5.1,], size=[5,])
    box_6 = shapes.Box(lower_left=[5,], size=[5,])
    box_7 = shapes.Box(lower_left=[5,], size=[5,])
    box_not_inter = shapes.Box(lower_left=[-13,], size=[1,])

    meshes = [(box_not_inter, rect_pattern_2),
              (box_1_inB, rect_pattern_2),
              (box_2, rect_pattern_1),
              (box_3, rect_pattern_1),
              (box_4, rect_pattern_2),
              (box_5, rect_pattern_2),
              (box_6, rect_pattern_1),
              (box_7, rect_pattern_2), 
              (box_1, rect_pattern_1)]
    
    mesh = mesh_.Mesh(simulation_region=space, pattern=pat_coarse)
    for reg, pat in meshes:
        mesh.refine(region=reg, pattern=pat)
    mesh.refine(region=box_1_in, pattern=rect_pattern_2, nth=1)
    
    # Remove the points at the extremeties, at the border of the system. 
    test_idx = np.setdiff1d(
        mesh._internal_index.indices,
        np.intersect1d(mesh._internal_index.values,
        ra.remove_element(
            elements=mesh.imcomplete_points, 
            values=copy.deepcopy(mesh._neighbours), 
            indices=copy.deepcopy(mesh._pt_neig))[2]))
    
    assert_mesh_coherence(
        mesh_inst=mesh, test_idx=test_idx, tol=1e-13)
    compare_geometrical_properties(
        mesh_inst=mesh, test_idx=test_idx, tol=1e-13)

    print('Passed single 1D mesh - full mesh - reference voronoi compairison')


def mesh_2d_regression_test():

    ## Generate the 2D patterns
    step = [1, 1]
    center = (15.34, 43.432)

    ticks = [
        lambda i: i * step[0],
        lambda j: j * step[1]]

    tag2ticks = [
        lambda i: i / step[0],
        lambda j: j / step[1]]

    rect_pattern_1 = patterns.Rectangular(
        ticks=ticks,
        tag2ticks=tag2ticks,
        center=center)

    step2 = [0.5, 0.75]
    center = (15, 41.432)

    ticks = [
        lambda i: i * step2[0],
        lambda j: j * step2[1]]

    tag2ticks = [
        lambda i: i / step2[0],
        lambda j: j / step2[1]]

    rect_pattern_2 = patterns.Rectangular(
        ticks=ticks,
        tag2ticks=tag2ticks,
        center=center)

    ############################################
    # Test comp with scipy.spatial.Voronoi

    ## Test all points ########################################################

    space = shapes.Box(lower_left=[-16, -18], size=[30, 20])
    pat_coarse = patterns.Rectangular.constant(
        element_size=(4, 4))
    ### Test single mesh (no assembling needed)
    box_1 = shapes.Box(lower_left=[-10, -15], size=[5, 5])

    mesh = mesh_.Mesh(simulation_region=box_1, pattern=rect_pattern_1)
    coordinates = mesh.coordinates()

    assert np.all(
        c_unique(coordinates).shape == coordinates.shape), (
            ('Repeated coordinates were found in the mesh'
             + '. Failed 2D regression test'))

    compare_w_spatial(mesh_inst=mesh)

    print('Passed single 2D mesh - full mesh - reference compairision')

    ## Compare only the assembling pattern with voronoi #######################
    ### Test non-intersecting mesh (# pattern)
    box_3 = shapes.Box(lower_left=[-0, -5], size=[5, 5])

    with raises(
        NotImplementedError,
        match=('Intersection between non-intersecting'
               + ' shapes is not implemented')):
        test = box_1 & box_3

    meshes = [(box_1, rect_pattern_1),
              (box_3, rect_pattern_1)]

    mesh = mesh_.Mesh(simulation_region=space, pattern=pat_coarse)
    for reg, pat in meshes:
        mesh.refine(region=reg, pattern=pat)
    compare_w_spatial(mesh_inst=mesh)

    print(('Passed non-intersecting 2D mesh'
           + ' -reference compairision'))

    ### Test mesh side-by-side (# pattern)
    box_4 = shapes.Box(lower_left=[-4.9, -15], size=[5, 5])

    #### Tests if the shapes are not intersecting
    with raises(
        NotImplementedError,
        match=('Intersection between non-intersecting'
               + ' shapes is not implemented')):
        test = box_1 & box_4

    meshes = [(box_1, rect_pattern_1),
              (box_4, rect_pattern_2)]

    mesh = mesh_.Mesh(simulation_region=space, pattern=pat_coarse)
    for reg, pat in meshes:
        mesh.refine(region=reg, pattern=pat)
    compare_w_spatial(mesh_inst=mesh)

    print(('Passed 2D  mesh side-by-side'
           + ' -reference compairision'))
    ### Test intersecting mesh only one layer
    box_5 = shapes.Box(lower_left=[-5.1, -15], size=[5, 5])

    #### Tests if the intersection is only one layer thick
    test = box_1 & box_5
    assert np.all(
        rect_pattern_2(rect_pattern_2.inside(test))[:, 0] == -5.)
    assert len(rect_pattern_1(rect_pattern_1.inside(test))) == 0
    meshes = [(box_1, rect_pattern_1),
            (box_5, rect_pattern_2)]

    mesh = mesh_.Mesh(simulation_region=space, pattern=pat_coarse)
    for reg, pat in meshes:
        mesh.refine(region=reg, pattern=pat)
    compare_w_spatial(mesh_inst=mesh)

    print(('Passed intersecting 2D mesh only one layer'
           + ' -reference compairision'))
    ### Test intersecting mesh next to border of Builder.shape
    box_6 = shapes.Box(lower_left=[5, -10], size=[5, 5])
    box_7 = shapes.Box(lower_left=[5, -12], size=[5, 5])

    meshes = [(box_6, rect_pattern_1),
            (box_7, rect_pattern_2)]

    mesh = mesh_.Mesh(simulation_region=space, pattern=pat_coarse)
    for reg, pat in meshes:
        mesh.refine(region=reg, pattern=pat)
    compare_w_spatial(mesh_inst=mesh)

    print(('Passed intersecting 2D mesh next to border of Builder.shape'
           + ' -reference compairision'))
    ### Test finer mesh inside coarser mesh
    box_1_inB = shapes.Box(lower_left=[-7, -12], size=[1, 1])
    meshes = [(box_1, rect_pattern_1),
            (box_1_inB, rect_pattern_2)]

    mesh = mesh_.Mesh(simulation_region=space, pattern=pat_coarse)
    for reg, pat in meshes:
        mesh.refine(region=reg, pattern=pat)
    compare_w_spatial(mesh_inst=mesh)

    print(('Passed finer 2D mesh inside coarser 2D mesh'
           + ' -reference compairision'))
    ### test coarser mesh inside finer mesh
    box_1_inB = shapes.Box(lower_left=[-7, -12], size=[1.1, 1.1])

    meshes = [(box_1_inB, rect_pattern_1),
            (box_1, rect_pattern_2)]

    mesh = mesh_.Mesh(simulation_region=space, pattern=pat_coarse)
    for reg, pat in meshes:
        mesh.refine(region=reg, pattern=pat)
    compare_w_spatial(mesh_inst=mesh)

    print(('Passed coarser 2D mesh inside finer 2D mesh'
           + ' -reference compairision'))
    ### Test all together
    box_1 = shapes.Box(lower_left=[-10, -15], size=[5, 5])
    box_1_in = shapes.Box(lower_left=[-9, -14], size=[1, 1])
    box_1_inB = shapes.Box(lower_left=[-7, -12], size=[1, 1])
    box_2 = shapes.Box(lower_left=[-5, -10], size=[5, 5])
    box_3 = shapes.Box(lower_left=[-0, -5], size=[5, 5])
    box_4 = shapes.Box(lower_left=[-4.9, -15], size=[5, 5])
    box_5 = shapes.Box(lower_left=[-5.1, -10], size=[5, 5])
    box_6 = shapes.Box(lower_left=[5, -10], size=[5, 5])
    box_7 = shapes.Box(lower_left=[5, -12], size=[5, 5])
    box_not_inter = shapes.Box(lower_left=[-12, -12], size=[1, 1])

    sh = mesh_.external_boundary_shape(box_1_in, rect_pattern_2, nth=1)

    # meshes = [(box_1, rect_pattern_1),
    #           (sh, rect_pattern_2)]

    meshes = [(box_not_inter, rect_pattern_2),
            (box_1_inB, rect_pattern_2),
            (box_2, rect_pattern_1),
            (box_3, rect_pattern_1),
            (box_4, rect_pattern_2),
            (box_5, rect_pattern_2),
            (box_6, rect_pattern_1),
            (box_7, rect_pattern_2),
            (box_1, rect_pattern_1),
            (sh, rect_pattern_2), 
            (box_1, rect_pattern_1)]

    mesh = mesh_.Mesh(simulation_region=space, pattern=pat_coarse)
    for reg, pat in meshes:
        mesh.refine(region=reg, pattern=pat)
    
    mesh.refine(region=box_1, pattern=rect_pattern_1, nth=2)
    compare_w_spatial(mesh_inst=mesh)

    print(('Passed all together 2D mesh'
           + ' -reference compairision'))

    assert_mesh_coherence(
        mesh_inst=mesh,
        test_idx=np.arange(0, mesh.npoints), tol=1e-13)
    
    compare_geometrical_properties(
        mesh_inst=mesh,
        test_idx=np.arange(0, mesh.npoints), tol=1e-13)
    
    print('Passed single 2D mesh - full mesh - reference voronoi compairison')


def mesh_3d_regression_test():

    ## Generate the 3D patterns
    step = [1.1, 0.8, 1.5]
    center = (15.34, 43.432, 30.0)

    ticks = [
        lambda i: i * step[0],
        lambda j: j * step[1],
        lambda k: k * step[2]]

    tag2ticks = [
        lambda i: i / step[0],
        lambda j: j / step[1],
        lambda k: k / step[2]]

    rect_pattern_1 = patterns.Rectangular(
        ticks=ticks,
        tag2ticks=tag2ticks,
        center=center)

    #step2 = [0.5, 0.6, 0.4] -> fails to find inverse
    step2 = [0.5, 0.6, 0.41]
    center = (15, 41.432, 31)

    ticks = [
        lambda i: i * step2[0],
        lambda j: j * step2[1],
        lambda k: k * step2[2]]

    tag2ticks = [
        lambda i: i / step2[0],
        lambda j: j / step2[1],
        lambda k: k / step2[2]]

    rect_pattern_2 = patterns.Rectangular(
        ticks=ticks,
        tag2ticks=tag2ticks,
        center=center)

    ############################################
    # Test comp with scipy.spatial.Voronoi

    space = shapes.Box(lower_left=[-15, -20, -15], size=[30, 25, 30])
    pat_coarse = patterns.Rectangular.constant(
        element_size=(5, 5, 5))

    ## Test all points ########################################################

    ### Test single mesh (no assembling needed)
    box_1 = shapes.Box(lower_left=[-10, -15, -10], size=[5, 5, 5])

    mesh = mesh_.Mesh(simulation_region=box_1, pattern=rect_pattern_1)
    
    coordinates = mesh.coordinates()

    assert np.all(
        c_unique(coordinates).shape == coordinates.shape), (
            ('Repeated coordinates were found in the mesh'
             + '. Failed 3D regression test'))

    compare_w_spatial(mesh_inst=mesh)

    print('Passed single 3D mesh - full mesh - reference compairision')

    ## Compare only the assembling pattern with voronoi #######################

    ### Test non-intersecting mesh (# pattern)
    box_3 = shapes.Box(lower_left=[-0, -5, -0], size=[5, 5, 5])

    with raises(
        NotImplementedError,
        match=('Intersection between non-intersecting'
               + ' shapes is not implemented')):
        test = box_1 & box_3

    meshes = [(box_1, rect_pattern_1),
              (box_3, rect_pattern_1)]

    mesh = mesh_.Mesh(simulation_region=space, pattern=pat_coarse)
    for reg, pat in meshes:
        mesh.refine(region=reg, pattern=pat)
    compare_w_spatial(mesh_inst=mesh)

    print(('Passed non-intersecting 3D mesh'
            + ' -reference compairision'))

    ### Test mesh side-by-side (# pattern)
    box_4 = shapes.Box(lower_left=[-4.9, -15, -10], size=[5, 5, 5])

    #### Tests if the shapes are not intersecting
    with raises(
        NotImplementedError,
        match=('Intersection between non-intersecting'
               + ' shapes is not implemented')):
        test = box_1 & box_4

    meshes = [(box_1, rect_pattern_1),
              (box_4, rect_pattern_2)]

    mesh = mesh_.Mesh(simulation_region=space, pattern=pat_coarse)
    for reg, pat in meshes:
        mesh.refine(region=reg, pattern=pat)
    compare_w_spatial(mesh_inst=mesh)

    print(('Passed 3D  mesh side-by-side'
            + ' -reference compairision'))

    ### Test intersecting mesh only one layer
    box_5 = shapes.Box(lower_left=[-5.2, -15, -10], size=[5, 5, 5])

    #### Tests if the intersection is only one layer thick
    test = box_1 & box_5
    assert np.all(
        rect_pattern_2(rect_pattern_2.inside(test))[:, 0] == -5.)
    assert len(rect_pattern_1(rect_pattern_1.inside(test))) == 0

    meshes = [(box_1, rect_pattern_1),
              (box_5, rect_pattern_2)]

    mesh = mesh_.Mesh(simulation_region=space, pattern=pat_coarse)
    for reg, pat in meshes:
        mesh.refine(region=reg, pattern=pat)
    compare_w_spatial(mesh_inst=mesh)

    print(('Passed intersecting 3D mesh only one layer'
                + ' -reference compairision'))

    ### Test intersecting mesh next to border of Builder.shape
    box_6 = shapes.Box(lower_left=[5, -10, 5], size=[5, 5, 5])
    box_7 = shapes.Box(lower_left=[5, -12, 5], size=[5, 5, 5])

    #### Tests if they are intersecting
    test = box_6 & box_7

    meshes = [(box_6, rect_pattern_1),
              (box_7, rect_pattern_2)]

    mesh = mesh_.Mesh(simulation_region=space, pattern=pat_coarse)
    for reg, pat in meshes:
        mesh.refine(region=reg, pattern=pat)
    compare_w_spatial(mesh_inst=mesh)

    print(('Passed intersecting 3D mesh next to border of Builder.shape'
            + ' -reference compairision'))

    ### Test finer mesh inside coarser mesh
    box_1_inB = shapes.Box(lower_left=[-7, -12, -7], size=[1, 1, 1])

    meshes = [(box_1, rect_pattern_1),
              (box_1_inB, rect_pattern_2)]

    mesh = mesh_.Mesh(simulation_region=space, pattern=pat_coarse)
    for reg, pat in meshes:
        mesh.refine(region=reg, pattern=pat)
    compare_w_spatial(mesh_inst=mesh)

    print(('Passed finer 3D mesh inside coarser 2D mesh'
            + ' -reference compairision'))

    ### test coarser mesh inside finer mesh
    box_1_inB = shapes.Box(lower_left=[-7, -12, -7], size=[1, 1, 1])

    meshes = [(box_1_inB, rect_pattern_1),
              (box_1, rect_pattern_2)]

    mesh = mesh_.Mesh(simulation_region=space, pattern=pat_coarse)
    for reg, pat in meshes:
        mesh.refine(region=reg, pattern=pat)
    compare_w_spatial(mesh_inst=mesh)

    print(('Passed coarser 3D mesh inside finer 2D mesh'
            + ' -reference compairision'))

    ### Test all together
    box_1 = shapes.Box(lower_left=[-10, -15, -10], size=[5, 5, 5])
    box_1_in = shapes.Box(lower_left=[-9, -14, -9], size=[1, 1, 1])
    box_1_inB = shapes.Box(lower_left=[-7, -12, -7], size=[1, 1, 1])
    box_2 = shapes.Box(lower_left=[-5, -10, -5], size=[5, 5, 5])
    box_3 = shapes.Box(lower_left=[-0, -5, -0], size=[5, 5, 5])
    box_4 = shapes.Box(lower_left=[-4.9, -15, -10.], size=[5, 5, 5])
    box_5 = shapes.Box(lower_left=[-5.2, -10, -10], size=[5, 5, 5])
    box_6 = shapes.Box(lower_left=[5, -10, -5], size=[5, 5, 5])
    box_7 = shapes.Box(lower_left=[5, -12, -5], size=[5, 5, 5])
    box_not_inter = shapes.Box(lower_left=[-12, -12, -12], size=[1, 1, 1])

    meshes = [(box_not_inter, rect_pattern_2),
            (box_1_inB, rect_pattern_2),
            (box_2, rect_pattern_1),
            (box_3, rect_pattern_1),
            (box_4, rect_pattern_2),
            (box_5, rect_pattern_2),
            (box_6, rect_pattern_1),
            (box_7, rect_pattern_2),
            (box_1, rect_pattern_1)]

    mesh = mesh_.Mesh(simulation_region=space, pattern=pat_coarse)
    for reg, pat in meshes:
        mesh.refine(region=reg, pattern=pat)
    mesh.refine(region=box_1_in, pattern=rect_pattern_2, nth=4)

    compare_w_spatial(mesh_inst=mesh, tol=2e-12)

    print(('Passed all together 3D mesh'
            + ' -reference compairision'))

    assert_mesh_coherence(
        mesh_inst=mesh,
        test_idx=np.arange(0, mesh.npoints), tol=1e-13)

    compare_geometrical_properties(
        mesh_inst=mesh,
        test_idx=np.arange(0, mesh.npoints), tol=1e-13)
    
    print('Passed single 3D mesh - full mesh - reference voronoi compairison')


def test_Builder_1D():

    ############################################
    # Test comp with scipy.spatial.Voronoi

    mesh_1d_regression_test()
    pass
    ############################################
        # Test _neighbours


def test_Builder_2D():

    ############################################
    # Test comp with scipy.spatial.Voronoi

    mesh_2d_regression_test()
    pass
    ############################################
        # Test _neighbours


def test_Builder_3D():

    ############################################
    # Test comp with scipy.spatial.Voronoi

    mesh_3d_regression_test()
    pass
    ############################################
        # Test _neighbours

##############################################################################
################################ Refine tests ################################
##############################################################################


def test_continuity_2D_m0():
    '''  Test mesh continuity for 2D m0 mesh.
    '''

    space = shapes.Box(lower_left=(-200, 0), size=(400, 300))
    pattern_space = patterns.Rectangular.constant(element_size=(30, 30))

    refine_0 = ((shapes.Box(lower_left=(-100, 200), size=(100, 100))
                - shapes.Box(lower_left=(-100, 280), size=(50, 30))),
                patterns.Rectangular.constant(element_size=(10, 10)))

    refine_1 = (shapes.Box(lower_left=(0, 200), size=(100, 100)),
                patterns.Rectangular.constant(element_size=(5, 5)))
    
    t1 = time.time()
    mesh = mesh_.Mesh(simulation_region=space, pattern=pattern_space)
    for reg, pat in [refine_0, refine_1]:
        mesh.refine(region=reg, pattern=pat)
    
   
    t2 = time.time()
    print('Total mesh construction time is {0}'.format(t2 - t1))           
    mesh_continuity_test(
        coordinates=mesh.coordinates(), 
        tags2pat=mesh._index2pat,
        tags=mesh._tags,
        neighbours=mesh._complete_neig, 
        neig_idx=mesh._complete_pt_neig,
        _internal_index=mesh._internal_index,
        patterns_list=mesh._patterns,
        meshing_region=mesh._simulation_region)
    

def test_continuity_2D_m1():
    '''  Test mesh continuity for 2D m1 mesh.
    '''

    space = shapes.Box(lower_left=(-300, -300), size=(600, 600))
    pattern_space = patterns.Rectangular.constant(element_size=(30, 30))

    refine_0 = (shapes.Box(lower_left=(-100, -100), size=(100, 100)),
                patterns.Rectangular.constant(element_size=(10, 10)))

    refine_1 = (shapes.Box(lower_left=(-50, -100), size=(100, 100)),
                patterns.Rectangular.constant(element_size=(5, 5)))

    refine_2 = (shapes.Box(lower_left=(0, -50), size=(50, 50)),
                patterns.Rectangular.constant(element_size=(2, 2)))

    t1 = time.time()
    mesh = mesh_.Mesh(simulation_region=space, pattern=pattern_space)    
    for reg, pat in [refine_0, refine_1, refine_2]:
        mesh.refine(region=reg, pattern=pat)
    t2 = time.time()
    print('Total mesh construction time is {0}'.format(t2 - t1))    
    mesh_continuity_test(
        coordinates=mesh.coordinates(), 
        tags2pat=mesh._index2pat,
        tags=mesh._tags,
        neighbours=mesh._complete_neig, 
        neig_idx=mesh._complete_pt_neig,
        _internal_index=mesh._internal_index,
        patterns_list=mesh._patterns,
        meshing_region=mesh._simulation_region)

def test_continuity_2D_m2():
    '''  Test mesh continuity for 2D m2 mesh.
    '''

    space = shapes.Box(lower_left=(-300, -300), size=(600, 600))
    pattern_space = patterns.Rectangular.constant(element_size=(30, 30))

    refine_0 = ((shapes.Box(lower_left=(-100, 0), size=(100, 200))),
                patterns.Rectangular.constant(element_size=(10, 10)))

    refine_1 = (shapes.Box(lower_left=(-25, 100), size=(100, 4)),
                patterns.Rectangular.constant(element_size=(5, 5)))

    refine_2 = (shapes.Box(lower_left=(-75, 100), size=(25, 4)),
                patterns.Rectangular.constant(element_size=(5, 5)))

    t1 = time.time()
    mesh = mesh_.Mesh(simulation_region=space, pattern=pattern_space)
    for reg, pat in [refine_0, refine_1, refine_2]:
        mesh.refine(region=reg, pattern=pat)
    t2 = time.time()
    print('Total mesh construction time is {0}'.format(t2 - t1))           
    mesh_continuity_test(
        coordinates=mesh.coordinates(), 
        tags2pat=mesh._index2pat,
        tags=mesh._tags,
        neighbours=mesh._complete_neig, 
        neig_idx=mesh._complete_pt_neig,
        _internal_index=mesh._internal_index,
        patterns_list=mesh._patterns,
        meshing_region=mesh._simulation_region)


def test_continuity_2D_m3():
    '''  Test mesh continuity for 2D m3 mesh.
    '''

    space = shapes.Box(lower_left=(-300, -300), size=(600, 600))
    pattern_space = patterns.Rectangular.constant(element_size=(30, 30))

    refine_1 = (shapes.Box(lower_left=(0, 200), size=(100, 100)),
                patterns.Rectangular.constant(element_size=(5, 5)))

    refine_2 = (shapes.Box(lower_left=(-5, 295), size=(20, 5)),
                patterns.Rectangular.constant(element_size=(5, 5)))

    refine_3 = (shapes.Box(lower_left=(60, 295), size=(4, 4)),
                patterns.Rectangular.constant(element_size=(5, 5)))

    refine_4 = (shapes.Box(lower_left=(60, 270), size=(4, 4)),
                patterns.Rectangular.constant(element_size=(5, 5)))

    t1 = time.time()
    mesh = mesh_.Mesh(simulation_region=space, pattern=pattern_space)
    for reg, pat in [refine_1, refine_2, refine_3, refine_4]:
        mesh.refine(region=reg, pattern=pat)
    t2 = time.time()
    print('Total mes construction time is {0}'.format(t2 - t1))
    mesh_continuity_test(
        coordinates=mesh.coordinates(), 
        tags2pat=mesh._index2pat,
        tags=mesh._tags,
        neighbours=mesh._complete_neig, 
        _internal_index=mesh._internal_index,
        neig_idx=mesh._complete_pt_neig,
        patterns_list=mesh._patterns,
        meshing_region=mesh._simulation_region)


def test_continuity_1D():
    '''  Test mesh continuity for 1D mesh.
    '''

    space = shapes.Box(lower_left=(-300,), size=(600, ))

    pattern_space = patterns.Rectangular.constant(element_size=(30,))

    refine_0 = (shapes.Box(lower_left=(-100, ), size=(100,)),
                patterns.Rectangular.constant(element_size=(10,)))

    refine_1 = (shapes.Box(lower_left=(-50,), size=(100,)),
                patterns.Rectangular.constant(element_size=(5,)))

    refine_2 = (shapes.Box(lower_left=(0,), size=(50,)),
                patterns.Rectangular.constant(element_size=(2,)))

    t1 = time.time()
    mesh = mesh_.Mesh(simulation_region=space, pattern=pattern_space)
    for reg, pat in [refine_1, refine_2]:
        mesh.refine(region=reg, pattern=pat)
    t2 = time.time()
    print('Total mesh construction time is {0}'.format(t2 - t1))           
    mesh_continuity_test(
        coordinates=mesh.coordinates(), 
        tags2pat=mesh._index2pat,
        tags=mesh._tags,
        neighbours=mesh._complete_neig, 
        neig_idx=mesh._complete_pt_neig,
        _internal_index=mesh._internal_index,
        patterns_list=mesh._patterns,
        meshing_region=mesh._simulation_region)


def test_refine_coord_continuity_1D():
    '''  Test mesh continuity for 1D mesh refined with coordinates
    '''

    space = shapes.Box(lower_left=(-300,), size=(600, ))

    pattern_space = patterns.Rectangular.constant(element_size=(30,))

    refine_0 = (shapes.Box(lower_left=(-100, ), size=(100,)),
                patterns.Rectangular.constant(element_size=(10,)))

    refine_1 = (shapes.Box(lower_left=(-50,), size=(50,)),
                patterns.Rectangular.constant(element_size=(5,)))

    refine_2 = (shapes.Box(lower_left=(0,), size=(50,)),
                patterns.Rectangular.constant(element_size=(2,)))

    t1 = time.time()
    
    mesh = mesh_.Mesh(simulation_region=space, pattern=pattern_space)
    mesh.refine(
        region=refine_1[0], 
        coordinates=refine_1[1](refine_1[1].inside(refine_1[0])))
    mesh.refine(region=refine_2[0], pattern=refine_2[1])
    
    t2 = time.time()
    
    print('Total mesh construction time is {0}'.format(t2 - t1))           
    
    mesh_continuity_test(
        coordinates=mesh.coordinates(), 
        tags2pat=mesh._index2pat,
        tags=mesh._tags,
        neighbours=mesh._complete_neig, 
        neig_idx=mesh._complete_pt_neig,
        _internal_index=mesh._internal_index,
        patterns_list=mesh._patterns,
        meshing_region=mesh._simulation_region)


def test_refine_coord_continuity_2d_m3():
    '''  Test mesh continuity for 2D m3 mesh.
    '''

    space = shapes.Box(lower_left=(-300, -300), size=(600, 600))
    pattern_space = patterns.Rectangular.constant(element_size=(30, 30))

    refine_0 = (shapes.Box(lower_left=(0, 200), size=(100, 100)),
                patterns.Rectangular.constant(element_size=(5, 5)))

    refine_1 = (shapes.Box(lower_left=(0, 200), size=(100, 10)),
                patterns.Rectangular.constant(element_size=(5, 5)))

    refine_2 = (shapes.Box(lower_left=(-5, 295), size=(20, 5)),
                patterns.Rectangular.constant(element_size=(5, 5)))

    refine_3 = (shapes.Box(lower_left=(60, 295), size=(4, 4)),
                patterns.Rectangular.constant(element_size=(5, 5)))

    refine_4 = (shapes.Box(lower_left=(60, 270), size=(4, 4)),
                patterns.Rectangular.constant(element_size=(5, 5)))

    t1 = time.time()
    mesh = mesh_.Mesh(simulation_region=space, pattern=pattern_space)
    mesh.refine(region=refine_0[0], pattern=refine_0[1])
    for reg, pat in [refine_1, refine_2, refine_3, refine_4]:
        mesh.refine(
            region=reg, 
            coordinates=pat(pat.inside(reg)))
        
    t2 = time.time()
    print('Total mes construction time is {0}'.format(t2 - t1))
    mesh_continuity_test(
        coordinates=mesh.coordinates(), 
        tags2pat=mesh._index2pat,
        tags=mesh._tags,
        neighbours=mesh._complete_neig, 
        _internal_index=mesh._internal_index,
        neig_idx=mesh._complete_pt_neig,
        patterns_list=mesh._patterns,
        meshing_region=mesh._simulation_region)
    
# def test_continuity_3D():
#     '''  Test mesh continuity for 3D mesh.
#     '''

#     space = shapes.Box(lower_left=(-300, -300, -200),
#                     size=(600, 600, 400))

#     pattern_space = patterns.Rectangular.constant(element_size=(10, 10, 5))

#     refine_0 = (shapes.Box(lower_left=(-100, -100, -100),
#                         size=(100, 100, 100)),
#                 patterns.Rectangular.constant(element_size=(10, 10, 10)))

#     refine_1 = (shapes.Box(lower_left=(-50, -100, -50), size=(100, 100, 75)),
#                 patterns.Rectangular.constant(element_size=(5, 5, 5)))

#     refine_2 = (shapes.Box(lower_left=(0, -50, 0), size=(50, 50, 50)),
#                 patterns.Rectangular.constant(element_size=(2, 2, 5)))
    
#     t0 = time.time()
#     mesh = mesh_.Mesh(simulation_region=space, pattern=pattern_space)
#     t1 = time.time()
#     print('Done intialization in {0}'.format(t1 - t0))
#     print('-----------')
#     for reg, pat in [refine_1, refine_2]:
#         ta = time.time()
#         mesh.refine(region=reg, pattern=pat)
#         tb = time.time()
#         print('Done ref in {0}'.format(tb - ta))
#         print('-----------')
#     t3 = time.time()
    

#     print('Done 3D mesh with {0} points'.format(mesh.npoints))
#     print('Total mesh construction time is {0}'.format(t3 - t0))
#     mesh_continuity_test(
#         coordinates=mesh.coordinates(), 
#         tags2pat=mesh._index2pat,
#         tags=mesh._tags,
#         neighbours=mesh._complete_neig, 
#         neig_idx=mesh._complete_pt_neig,
#         _internal_index=mesh._internal_index,
#         patterns_list=mesh._patterns,
#         meshing_region=mesh._simulation_region)

# def test_refine_coord_continuity_3D():
#     '''  Test mesh continuity for 3D mesh.
#     '''

#     space = shapes.Box(lower_left=(-300, -300, -200),
#                     size=(600, 600, 400))

#     pattern_space = patterns.Rectangular.constant(element_size=(10, 10, 5))

#     refine_0 = (shapes.Box(lower_left=(-100, -100, -100),
#                         size=(100, 100, 100)),
#                 patterns.Rectangular.constant(element_size=(10, 10, 10)))

#     refine_1 = (shapes.Box(lower_left=(-50, -100, -50), size=(100, 100, 10)),
#                 patterns.Rectangular.constant(element_size=(5, 5, 5)))

#     refine_2 = (shapes.Box(lower_left=(0, -50, 0), size=(50, 50, 50)),
#                 patterns.Rectangular.constant(element_size=(2, 2, 5)))

#     t0 = time.time()
#     mesh = mesh_.Mesh(simulation_region=space, pattern=pattern_space)
#     t1 = time.time()
#     print('Done intialization in {0}'.format(t1 - t0))
#     print('-----------')
#     for reg, pat in [refine_0, refine_2]:
#         ta = time.time()
#         mesh.refine(region=reg, pattern=pat)
#         tb = time.time()
#         print('Done ref in {0}'.format(tb - ta))
#         print('-----------')

#     mesh.refine(
#         region=refine_1[0], 
#         coordinates=refine_1[1](refine_1[1].inside(refine_1[0])))    
    
#     t3 = time.time()
    
#     raise 
#     print('Done 3D mesh with {0} points'.format(mesh.npoints))
#     print('Total mesh construction time is {0}'.format(t3 - t0))
#     mesh_continuity_test(
#         coordinates=mesh.coordinates(), 
#         tags2pat=mesh._index2pat,
#         tags=mesh._tags,
#         neighbours=mesh._complete_neig, 
#         neig_idx=mesh._complete_pt_neig,
#         _internal_index=mesh._internal_index,
#         patterns_list=mesh._patterns,
#         meshing_region=mesh._simulation_region)


def test_continuity_w_ext_bd_2d():
    ''' Tests if the assembling algorithm works when adding a refinement
    using the external boundary (and the nth boundary) of a given shape
    for a given pattern.
    '''

    ## Generate the 2D patterns
    step = [1, 1]
    center = (15.34, 43.432)

    ticks = [
        lambda i: i * step[0],
        lambda j: j * step[1]]

    tag2ticks = [
        lambda i: i / step[0],
        lambda j: j / step[1]]

    rect_pattern_1 = patterns.Rectangular(
        ticks=ticks,
        tag2ticks=tag2ticks,
        center=center)

    step2 = [0.5, 0.75]
    center = (15, 41.432)

    ticks = [
        lambda i: i * step2[0],
        lambda j: j * step2[1]]

    tag2ticks = [
        lambda i: i / step2[0],
        lambda j: j / step2[1]]

    rect_pattern_2 = patterns.Rectangular(
        ticks=ticks,
        tag2ticks=tag2ticks,
        center=center)

    # Generate space and coarse pattern
    space = shapes.Box(lower_left=[-16, -18], size=[30, 20])
    pat_coarse = patterns.Rectangular.constant(element_size=(4, 4))

    box_1 = shapes.Box(lower_left=[-10, -15], size=[5, 5])
    box_1_in = shapes.Box(lower_left=[-9, -14], size=[1, 1])


    internal, external = rect_pattern_2.boundary(shape=box_1_in)

    sh = shapes.Delaunay(coordinates=rect_pattern_2(external))

    refinment_mesh = [(box_1, rect_pattern_1),
                      (sh, rect_pattern_2)]

    t1 = time.time()
    mesh = mesh_.Mesh(simulation_region=space, pattern=pat_coarse)
    for reg, pat in refinment_mesh:
        mesh.refine(region=reg, pattern=pat)
    t2 = time.time()
    print('Total mesh construction time is {0}'.format(t2 - t1))


def fun_(x):
    return np.abs(x) ** 1.2 * np.sign(x)


def inv_fun_(x):
    return np.abs(x) ** ( 1 / 1.2) * np.sign(x)


def test_SR_capturing_space_boundary():
    ''' In this test one of the refinements has interface points
    that correspond to the boundary of the space. Hence it caused
    open voronoi cells if not dealt with.
    '''

    ########################
    # Kwant part
    ########################
    test = shapes.Box(lower_left=(-50, -5), size=(99, 9))
    pat = patterns.Rectangular.constant(
        element_size=(1, 1))

    ########################
    # Define the patterns and Shapes
    ########################

    pattern_fine = patterns.Rectangular.constant(
        element_size=(1, 2))

    pattern_continuous = patterns.Rectangular(
        ticks=(fun_, ) * 2, tag2ticks=(inv_fun_, ) * 2)

    pattern_coarse = patterns.Rectangular.constant(
        element_size=(10, 20))

    shape_fine = shapes.Box(
        lower_left=(-5, -10), size=(10, 20))

    shape_coarse = shapes.Box(
        lower_left=(-50, -100), size=(100, 200))

    shape_continuous = shapes.Box(
        lower_left=(-25, -50), size=(50, 100))

    ########################
    # Build the mesh using an instance of poisson.ProblemBuilder
    ########################

    t1 = time.time()
    mesh = mesh_.Mesh(simulation_region=shape_coarse, pattern=pattern_coarse)
    for reg, pat in [(test, pat),
                     (shape_continuous, pattern_continuous), 
                     (shape_fine, pattern_fine)]:
        mesh.refine(region=reg, pattern=pat)
    t2 = time.time()
    
    mesh_continuity_test(
        coordinates=mesh.coordinates(), 
        tags2pat=mesh._index2pat,
        tags=mesh._tags,
        neighbours=mesh._complete_neig, 
        _internal_index=mesh._internal_index,
        neig_idx=mesh._complete_pt_neig,
        patterns_list=mesh._patterns,
        meshing_region=mesh._simulation_region)


def test_external_boundary_shape():
    ''' Simple minimceal testing in case the function
    source code changes
    '''

    sh = shapes.Box(lower_left=(0, 0), size=(4, 8))
    sh_ext_r = (shapes.Box(lower_left=(-1, 0), size=(6, 8))
                | shapes.Box(lower_left=(0, -1), size=(4, 10)))
    sh_2ext_r = (shapes.Box(lower_left=(-2, -1), size=(8, 10))
                 | shapes.Box(lower_left=(0, -2), size=(4, 12)))

    pat = patterns.Rectangular.constant(element_size=(1, 1))

    sh_ext = mesh_.external_boundary_shape(
        shape=sh, pattern=pat, nth=1)
    sh_2ext = mesh_.external_boundary_shape(
        shape=sh, pattern=pat, nth=2)

    assert len(pat.inside(sh_ext - sh_ext_r)) == 0, (
        'Failed ext bd shape nth=1 2D test'
    )
    assert len(pat.inside(sh_2ext - sh_2ext_r)) == 0, (
        'Failed ext bd shape nth=2 2D test'
    )


    sh = shapes.Box(lower_left=(0,), size=(4, ))
    sh_ext_r = shapes.Box(lower_left=(-1, ), size=(6,))
    sh_2ext_r = shapes.Box(lower_left=(-2,), size=(8,))

    pat = patterns.Rectangular.constant(element_size=(1, ))

    sh_ext = mesh_.external_boundary_shape(
        shape=sh, pattern=pat, nth=1)
    sh_2ext = mesh_.external_boundary_shape(
        shape=sh, pattern=pat, nth=2)

    assert len(pat.inside(sh_ext - sh_ext_r)) == 0, (
        'Failed ext bd shape nth=1 1D test'
    )
    assert len(pat.inside(sh_2ext - sh_2ext_r)) == 0, (
        'Failed ext bd shape nth=2 1D test'
    )    


def test_save_and_load():
    ''' Tests save and load mesh method. 
    '''


    coarse = patterns.Rectangular.constant(element_size=(47.5,) * 2)
    sim_region = shapes.Box(lower_left=(-500, -500), size=(1001, 1001))

    ref_mesh = mesh_.Mesh(simulation_region=sim_region, pattern=coarse)

    average = patterns.Rectangular.constant(element_size=(25, 25))
    device_region = shapes.Box(lower_left=(-500, -100), size=(1000, 300))

    ref_mesh.refine(region=device_region, pattern=average)

    ref_mesh.save('test_mesh')
    loaded_mesh = mesh_.Mesh.load('test_mesh')

    for key, val in ref_mesh.__dict__.items():

        if key in ['_index2pat', '_internal_index']:
            ref = val.__dict__
            loaded = loaded_mesh.__dict__[key].__dict__
            for key_sc, val_sc in ref.items():
                assert_array_equal(val_sc, loaded[key_sc])
        elif key == '_simulation_region':
            
            ref_coord = ref_mesh.__dict__['_coordinates']
            loaded_coord = loaded_mesh.__dict__['_coordinates']

            assert_array_equal(
                ref_coord[val(ref_coord)],
                loaded_coord[
                        loaded_mesh.__dict__[key](loaded_coord)])
            
        elif key == '_patterns':
            print('Patterns not tested')
        elif key == '_tags':
            
            index2pat = ref_mesh.__dict__['_index2pat'][
                np.arange(len(ref_mesh.__dict__['_coordinates']))]
            _patterns = ref_mesh.__dict__['_patterns']
            for pat_num in range(len(_patterns)):
                idx = np.arange(len(index2pat), dtype=int)[
                    index2pat == pat_num]
                if isinstance(val[idx][0], np.ndarray):
                    assert_array_equal(
                        np.concatenate(val[idx]), 
                        np.concatenate(loaded_mesh.__dict__[key][idx]))
                else:
                    assert_array_equal(
                        val[idx], loaded_mesh.__dict__[key][idx])
        else:
            assert_array_equal(
                val, loaded_mesh.__dict__[key])

##############################################################################
############################ Mesh properties tests ###########################
##############################################################################

# test_indexing_and_boundary_mesh()

# test_Builder_1D()
# test_Builder_2D()
# test_Builder_3D()
# test_external_boundary_shape()

# test_continuity_2D_m0()
# test_continuity_2D_m1()
# test_continuity_2D_m2()
# test_continuity_2D_m3()
# test_continuity_3D()
# test_continuity_1D
# test_continuity_w_ext_bd_2d
# test_SR_capturing_space_boundary()
