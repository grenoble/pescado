import sys

import numpy as np
from matplotlib import pyplot as plt

from scipy.spatial import Delaunay

from numpy.testing import assert_array_equal
from pytest import raises
import pescado.mesher.shapes as shapes
from pescado.mesher import patterns

def check_for_bounded_instance(instance):
    ''' Check that 'instance' is not bounded to another initialized instance'''

    # I don't know exactly in which version they changed the message
    if sys.version_info >= (3,12):
        err_msg = ("cannot access local variable 'instance' "
                   + "where it is not associated with a value")
    else: # Works in Python 3.9
        err_msg = ("local variable instance' "
                    + "referenced before assignment")

    del instance
    with raises(UnboundLocalError, match=err_msg):
        instance()



def gen_info_test_ellipsoid(center, radius, tol):
    """Generates reference function and coordinates used in test_Ellipsoid.

    Parameters
    ----------
    center: np.array, shape(1, d), float
        Center of the ellipsoid

    radius: np.array, shape(1, d), float
        Radius of the ellipsoid

    tol: float
        Tolerance to define the ellipsoid reference function

    Returns
    -------
    coord: np.array, shape(900, d), float
        Testing coordinates

    coord_in: np.array, shape(300, d), float
        Testing coordinates among coord
        that should be inside the defined center and radius

    coord_shpere:np.array, shape(1000, d), float
        Coordinates within the sphere defined by the smallest element
        in radius.

    ell_fun: function
        Function defining a ellipse

    sphere_fun: function
        Function defining a sphere

    """
    # Generate random points inside and outside the region
    min_rad = np.min(radius)
    rad = np.array([[min_rad, min_rad, min_rad]])
    
    def sphere_fun(x):
        return (np.sum(((x - center) / rad) ** 2, axis=1) - 1) < tol

    random_pts = np.random.rand(1000, rad.shape[0])
    coord_sphere_ = (center - rad + (random_pts * (rad * 2)))
    coord_sphere = coord_sphere_[sphere_fun(coord_sphere_)]

    # Inside points -> along the x, y and z axis
    n = 100
    coord_in = np.zeros((n * 3, center.shape[1]))
    for i in range(center.shape[1]):
        coord_in[n * i:n * (i + 1), i] = np.linspace(
            center[0, i] - radius[0, i],
            center[0, i] + radius[0, i], n)

        coord_in[n * i:n * (i + 1), 0:i] = center[0, 0:i]
        coord_in[n * i:n * (i + 1), i + 1:] = center[0, i + 1:]

    coord_out = np.zeros((n * 3, center.shape[1]))
    for i in range(center.shape[1]):
        coord_out[n * i:n * (i + 1), i] = np.linspace(
            center[0, i] + (radius[0, i] * 1.0001),
            center[0, i] + (radius[0, i] * 1.5), n)

        coord_out[n * i:n * (i + 1), 0:i] = center[0, 0:i]
        coord_out[n * i:n * (i + 1), i + 1:] = center[0, i + 1:]

    coord_out2 = np.zeros((n * 3, center.shape[1]))
    for i in range(center.shape[1]):
        coord_out2[n * i:n * (i + 1), i] = np.linspace(
            center[0, i] - (radius[0, i] * 1.5),
            center[0, i] - (radius[0, i] * 1.0001), n)

        coord_out2[n * i:n * (i + 1), 0:i] = center[0, 0:i]
        coord_out2[n * i:n * (i + 1), i + 1:] = center[0, i + 1:]

    coord = np.concatenate([coord_in, coord_out, coord_out2], axis=0)

    def ell_fun(x):
        return (
            (np.sum(((x - center) / radius) ** 2, axis=1) - 1) < tol)

    # Test ellipsoid function
        # Test if the inside coordinates are the same as predicted
    assert_array_equal(
        coord_in, coord[ell_fun(coord)],
        err_msg=('Reference function for Ellipsoid'
                 + 'didnt pass individual coordinate test'))

    assert_array_equal(
        coord_sphere, coord_sphere[ell_fun(coord_sphere)],
        err_msg=('Reference function for Ellipsoid'
                 + 'didnt pass individual coordinate test'))

    return coord, coord_in, coord_sphere, ell_fun, sphere_fun


def generate_rectangle(lower_left, size):
    """Generates a function defining a rectangle

    Parameters
    -----------
    lower_left : sequence, shape(d, ), floats
        (x0, y0, z0) - Lower left lower_left of the box. \n
        d the dimension of the box (1D, 2D or 3D)

    size : sequence, shape(d, ), floats
        (Lx, Ly, Lz) - Length of the box \n
        d the dimension of the box (1D, 2D or 3D)

    Returns
    -------
    rect: function,
        Returns True if inside the rectangle

    bbox: np.array, shape(2, d), float
        Defines the bounding box for the rectangle
    """

    def rect(vec):

        return (np.all(vec <= (lower_left + size), axis=1)
                * np.all(vec >= lower_left, axis=1))

    d = size.shape[0]
    bbox = np.zeros((d, d))
    bbox[0, :] = lower_left
    bbox[1, :] = lower_left + size

    return rect, bbox


def standard_Shape_instance_test(
    shape, reference, name, plot=False, verbose=False):
    """Standard tests applied to a Shapes instance. \n
    The following tests are performed: \n

        Tests if the is_inside() function behaves the same way as reference \n

        Tests if bbox sourrounds the entire region defined by shape \n

    Parameters
    ----------
    shape: Shape instance

    reference: function
        Returns True if coordinate within region

    name: str
        Name of the shape

    plot: bool

    verbose: bool

    Returns
    -------
    None
    """
    random_pts = np.random.rand(10000, 3)

    bbox = shape.bbox

    start_larger = bbox[0, :] - np.abs(bbox[0, :]) * 0.2
    end_larger = bbox[1, :] + np.abs(bbox[1, :]) * 0.2
    coord_larger_ = (start_larger
                    + (random_pts * (end_larger - start_larger) * 1.2))

    if plot:
        plt.plot(coord_larger_[:, 0], coord_larger_[:, 1], 'b.')
        plt.plot(coord_larger_[reference(coord_larger_), 0],
                 coord_larger_[reference(coord_larger_), 1], 'r.', alpha=0.5)

        plt.plot(coord_larger_[shape(coord_larger_), 0],
                 coord_larger_[shape(coord_larger_), 1], 'g.', alpha=0.5)
        plt.show()

    ###########################################################################
    ############### Test if the is_inside method is well defined
    ###########################################################################
    assert_array_equal(
        shape(coord_larger_), reference(coord_larger_),
        err_msg=('{0} shape instance did not pass test_Shape_instance'.format(name)
                 + 'as it does not match the reference is_inside function'))

    ###########################################################################
    ############### Test if the bounding_box is well defined
    ###########################################################################
    standard_bbox_test(shape=shape, name=name, verbose=verbose)

    if verbose:
        print('{0} passed the standart Shapes instance test'.format(name))


def standard_bbox_test(shape, name, test_test=True, verbose=False):
    """Standard tests applied to bbox. \n

    The tests applied are: \n
        Tests if bbox is large enough to capture all
        coordinates inside shape

    Parameters
    ----------
    shape: Shape instance

    name: str
        Name of the shape

    plot: bool

    verbose: bool

    test_test: bool
        default is True

    Returns
    -------
    None
    """
    #Testing test
    if test_test:
        test_standard_bbox_test(verbose=verbose)

    random_pts = np.random.rand(10000, shape.bbox.shape[1])
    coord = (shape.bbox[0, :]
             + (random_pts * (shape.bbox[1,:] - shape.bbox[0,:])))

    # Generate a set of coordinates encompasing the bbox.
    start_larger = shape.bbox[0, :] - np.abs(shape.bbox[0, :]) * 0.2
    end_larger = shape.bbox[1, :] + np.abs(shape.bbox[1, :]) * 0.2
    size_larger = (end_larger - start_larger) * 1.2

    coord_larger_ = (start_larger + (random_pts * size_larger))

    ###########################################################################
    ############### Check if the bbox captures all True values
    ###########################################################################

    ##########################################################################
    # Check that larger_box_shape captures coordinates outside of the region
    # defined by shape. That is, that it is large enough.

    larger_box_shape = shapes.Box(lower_left=start_larger, size=size_larger)
    outer_box_shape = larger_box_shape - shape # Removing the shape.

    # There gotta be at least one coord inside outer_box_shape
    # OR all coord must be alredy inside shape. It can't be both though

    condition = (np.any(outer_box_shape(coord)) # Test if the box is too small
                 ^ np.all(shape(coord))) # logical XOR operation
                 # Test if the box is exact the correct size

    assert condition, (
        'The bounding box does not capture all coordinates inside {0}'.format(name))

    ##########################################################################
    # Test if the box is not misplaced

    # Start by testing if at least one coord exists inside the shape
    assert np.any(shape(coord)), (
        'The bouding box of '
         +'{0} does not contain any point inside the region defined by {0}'.format(
             name))

    # Remove all points inside the bbox
    bbox_shape = shapes.Box(
        lower_left=shape.bbox[0, :], size=shape.bbox[1, :] - shape.bbox[0, :])

    # None of these points can be inside shape
    diff_larger_coord = coord_larger_[~bbox_shape(coord_larger_), :]

    assert ~np.any(shape(diff_larger_coord)), (
        'The bounding box of {0} is misplaced'.format(name))

    ##########################################################################
    # Second test - redundant with earlier two tests

    def outside_bbox(vec):
        return np.logical_or(
            np.any(vec < shape.bbox[0, :], axis=1),
            np.any(vec > shape.bbox[1, :], axis=1))

    # Check that no True value is outside of the BBOX
    # Assures that bbox is not "partially" misplaced and that it emcompases
    # all True values
    assert np.all(~shape(coord_larger_[outside_bbox(coord_larger_)])), (
        'The bbox for {0} is not larger enough or is misplaced'.format(name))

    coord_larger = np.concatenate(
        [coord_larger_[outside_bbox(coord_larger_), :],
         coord])

    bbox_res = coord[shape(coord), :]
    larger_bbox_res = coord_larger[shape(coord_larger), :]
    # Check that some values in coord_later are outside of shape
    assert np.any(~shape(coord_larger)), (
        'The bounding box does not capture all coordinates inside {0}'.format(name))

    # Check that the values inside shape are the same for
    # coord_larger or coord.
    assert bbox_res.shape[0] == larger_bbox_res.shape[0], (
        '{0}.bbox is not large enough to capture all coordinates inside {0}'.format(name))

    assert_array_equal(bbox_res, larger_bbox_res,
                       err_msg='{0} bounding box is not large enough'.format(name))

    if verbose:
        print('{0} passed bbox test'.format(name))


def test_standard_bbox_test(verbose=False):

    size = np.array([250.32, 250.534, 100.2])
    lower_left = np.array([500, -200, 56])
    rect, bbox = generate_rectangle(
        lower_left=lower_left, size=size)

    smaller_size = np.array([150.32, 150.534, 100.2])
    smaller_rect, smaller_bbox = generate_rectangle(
        lower_left=lower_left, size=smaller_size)

    test_wrong_bbox_shape = shapes.General(func=rect, bbox=smaller_bbox)

    with raises(AssertionError):
        standard_bbox_test(
            shape=test_wrong_bbox_shape,
            name='test_wrong_bbox_shape',
            test_test=False)

    test_correct_bbox_shape = shapes.General(func=rect, bbox=bbox)
    standard_bbox_test(
        shape=test_correct_bbox_shape,
        name='test_correct_bbox_shape',
        test_test=False)

    if verbose:
        print('test_bbox passed it''s own test')


def test_General(plot=False, verbose=False):

    size = np.array([250.32, 250.534, 100.2])
    lower_left = np.array([500, -200, 56])
    rect, bbox = generate_rectangle(lower_left=lower_left, size=size)

    general_ins = shapes.General(func=rect, bbox=bbox)

    standard_Shape_instance_test(
        shape=general_ins, reference=rect, name='General',
        plot=plot, verbose=verbose)

    if verbose:
        print('General passed all tests')


def test_Box(plot=False, verbose=False):

    size = np.array([250.32, 250.534, 100.2])
    lower_left = np.array([500, -200, 56])
    rect, bbox = generate_rectangle(lower_left=lower_left, size=size)

    box = shapes.Box(lower_left=lower_left, size=size)
    standard_Shape_instance_test(
        shape=box, reference=rect, name='box',
        plot=plot, verbose=verbose)

    centered_box = shapes.Box.centered(center=lower_left + size / 2, size=size)
    standard_Shape_instance_test(
        shape=centered_box, reference=rect, name='centered_box',
        plot=plot, verbose=verbose)

    if verbose:
        print('Box passed all tests')


def test_Ellipsoid(plot=False, verbose=False):

    # Input tests
    with raises(ValueError):
        shapes.Ellipsoid(
            center=np.array([0, 0, 0]), radius=np.array([1, 0, 5]))
        shapes.Ellipsoid(
            center=np.array([0, 0, 0]), radius=np.array([-1, 0, 5]))

    center = np.array([[434.43, -100.13, 234]])
    radius = np.array([[300, 50, 80]])
    tol = 1e-12

    (coord, coord_in, coord_sphere,
     ell_fun, sphere_fun) = gen_info_test_ellipsoid(
        center=center, radius=radius, tol=tol)

    # Create Ellipsoid shape
    ell_shape = shapes.Ellipsoid(center=center[0, :], radius=radius[0, :])

    # Test ellipsoid shape
        # Test if the inside coordinates are the same as predicted
    assert_array_equal(
        coord_in, coord[ell_shape(coord)],
        err_msg='Ellipsoid shape didnt pass individual coordinate test')

    assert_array_equal(
        coord_sphere, coord_sphere[ell_shape(coord_sphere)],
        err_msg='Ellipsoid shape didnt pass individual coordinate test')

    standard_Shape_instance_test(
        shape=ell_shape, reference=ell_fun,
        name='Ellipsoid', plot=plot, verbose=verbose)

    if verbose:
        print('Ellipsoid passed all tests')

    center = np.array([[434.43, -100.13, 234]])
    radius_sp = np.array([[50.43, 50.43, 50.43]])
    tol = 1e-12

    (coord, coord_in, coord_sphere,
     sphere_fun, sphere_fun_) = gen_info_test_ellipsoid(
        center=center, radius=radius_sp, tol=tol)

    # Create sphere shape
    sphere_shape = shapes.Ellipsoid.hypersphere(
        center=center[0, :], radius=radius_sp[0, 0])

    # Test ellipsoid shape
        # Test if the inside coordinates are the same as predicted
    assert_array_equal(
        coord_in, coord[sphere_shape(coord)],
        err_msg='Sphere shape didnt pass individual coordinate test')

    assert_array_equal(
        coord_sphere, coord_sphere[sphere_shape(coord_sphere)],
        err_msg='Sphere shape didnt pass individual coordinate test')

    standard_Shape_instance_test(
        shape=sphere_shape, reference=sphere_fun,
        name='Sphere', plot=plot, verbose=verbose)

    if verbose:
        print('Sphere passed all tests')


def test_Delaunay(plot=False, verbose=False):
    ##########################################
    ## Test Square shape
    sizes = (np.array([250.32, 250.534, 100.2]),
    np.array([250.32, 250.534, 250.534]),
    np.array([250.32, 0.0001, 20]))

    corners = (np.array([500, 200, 200]),
    np.array([-500, 0.001, 10]),
    np.array([-342, 0.001, -10]))

    for size, lower_left in zip(sizes, corners):

        # Generate points inside and outside the region
        random_pts = np.random.rand(10000, size.shape[0])
        coord_inside = (lower_left + (random_pts * size))
        coord_outside = (lower_left + (size * 1.00001) + (random_pts * size))
        coord_outside_2 = (lower_left * 0.9999 - (random_pts * size))
        coord = np.concatenate([
        coord_inside, coord_outside, coord_outside_2], axis=0)

        # Generates general function
        rect, bbox = generate_rectangle(lower_left=lower_left, size=size)
        # Test the general function
        assert_array_equal(
            coord_inside, coord[rect(coord)],
            err_msg='Reference function didnt pass individual coordinate test')

        bounding_box_vertex = shapes.bbox2box(bbox)

        # Generate Delaunay
        delaunay_box = shapes.Delaunay(coordinates=bounding_box_vertex)

        # Compare with general function
        standard_Shape_instance_test(
            shape=delaunay_box,
            reference=rect, name='Delaunay',
            plot=plot, verbose=verbose)

        # Test individual points
        assert_array_equal(
            coord_inside, coord[delaunay_box(coord)],
            err_msg='Delaunay shape didnt pass individual coordinate test')

        # Generate from Delaunay
        delaunay_box = shapes.Delaunay.import_instance(
            delaunay_instance=Delaunay(points=bounding_box_vertex))

        # Compare with general function
        standard_Shape_instance_test(
            shape=delaunay_box,
            reference=rect, name='Delaunay.import_instance',
            plot=plot, verbose=verbose)

        # Test individual points
        assert_array_equal(
            coord_inside, coord[delaunay_box(coord)],
            err_msg=('Delaunay.import_instance didnt pass'
                     + 'individual coordinate test'))

    ##########################################
    ## Test random shape with Delaunay
    # Not as redundant as the previous test, but ok

    vertices = (np.array([[-100, -20, 0]])
                + (np.random.rand(20, 3)
                * np.array([[300, 100, 50]])))

    # The same way is_inside is defined in shapes.InHull()
    delaunay = Delaunay(points=vertices)
    
    def reference_fun(x):
        return delaunay.find_simplex(x) >= 0

    # Generate Delaunay
    delaunay_box = shapes.Delaunay(coordinates=vertices)

    # Compare with general function
    standard_Shape_instance_test(
        shape=delaunay_box,
        reference=reference_fun, name='Delaunay',
        plot=plot, verbose=verbose)

    # Generate from Delaunay
    delaunay_box = shapes.Delaunay.import_instance(
        delaunay_instance=delaunay)

    # Compare with general function
    standard_Shape_instance_test(
        shape=delaunay_box,
        reference=reference_fun, name='Delaunay.import_instance',
        plot=plot, verbose=verbose)

    if verbose:
        print('Delaunay passed all tests')


def test_add_or(plot=False, verbose=False):
    ##########################################
    ## Generate three squares with Delaunay

    bbox_A = np.array([[-100, 34, 34.5],
                       [444, 650, 300]])

    bbox_B = np.array([[-200, 34, 34.5],
                       [1000, 650, 300]])

    vertices_A = shapes.bbox2box(bbox_A)
    vertices_B = shapes.bbox2box(bbox_B)

    # Generate Delaunay
    delaunay_A = shapes.Delaunay(coordinates=vertices_A)
    delaunay_B = shapes.Delaunay(coordinates=vertices_B)
    delaunay_ref = shapes.Delaunay(
        coordinates=np.concatenate([vertices_A, vertices_B]))

    delaunay_or = delaunay_A | delaunay_B

    # Compare with reference
    standard_Shape_instance_test(
        shape=delaunay_or, reference=delaunay_ref, name='Or-Delaunay',
        plot=plot, verbose=verbose)

    coord = (delaunay_ref.bbox[0, :]
             + np.random.rand(20000, 3) *(delaunay_ref.bbox[1, :] - delaunay_ref.bbox[0, :]))

    if plot:
        plt.plot(coord[:, 0], coord[:, 1], 'r.')
        plt.plot(coord[delaunay_ref(coord), 0], coord[delaunay_ref(coord), 1], 'y.', alpha=0.5)
        plt.plot(coord[delaunay_A(coord), 0], coord[delaunay_A(coord), 1], 'b.', alpha=0.5)
        plt.plot(coord[delaunay_B(coord), 0], coord[delaunay_B(coord), 1], 'g.', alpha=0.5)
        plt.show()

    if verbose:
        print('Add and Or operation passed all tests')


def test_and(plot=False, verbose=False):
    ##########################################
    ## Generate three squares with Delaunay

    bbox_A = np.array([[-100, 34, 34.5],
                       [444, 650, 300]])

    bbox_B = np.array([[50, 34, 34.5],
                       [300, 650, 300]])

    vertices_A = shapes.bbox2box(bbox_A)
    vertices_B = shapes.bbox2box(bbox_B)

    # Generate Delaunay
    delaunay_A = shapes.Delaunay(coordinates=vertices_A)
    delaunay_B = shapes.Delaunay(coordinates=vertices_B)
    delaunay_ref = shapes.Delaunay(
        coordinates=np.concatenate([vertices_A, vertices_B]))

    delaunay_and = delaunay_A & delaunay_B

    # Compare with reference
    standard_Shape_instance_test(
        shape=delaunay_and, reference=delaunay_B, name='And-Delaunay',
        plot=plot, verbose=verbose)

    if verbose:
        print('And operation passed all tests')


def test_sub(plot=False, verbose=False):
    ##########################################
    ## Generate three squares with Delaunay

    bbox_A = np.array([[-100, 34, 34.5],
                       [444, 650, 300]])

    bbox_B = np.array([[50, 34, 34.5],
                       [444, 650, 300]])

    bbox_C = np.array([[-100, 34, 34.5],
                       [50, 650, 300]]) # Change this

    def fun_c(x):
        return (np.all(x < bbox_C[1, :], axis=1)
                * np.all(x > bbox_C[0, :], axis=1))

    vertices_A = shapes.bbox2box(bbox_A)
    vertices_B = shapes.bbox2box(bbox_B)

    # Generate Delaunay
    delaunay_A = shapes.Delaunay(coordinates=vertices_A)
    delaunay_B = shapes.Delaunay(coordinates=vertices_B)

    delaunay_sub = delaunay_A - delaunay_B

    # Compare with reference
    standard_Shape_instance_test(
        shape=delaunay_sub, reference=fun_c, name='Subtraction-Delaunay',
        plot=plot, verbose=verbose)

    if verbose:
        print('Subtraction operation passed all tests')

def test_xor(plot=False, verbose=False):
    ##########################################
    ## Generate three squares with Delaunay

    bbox_A = np.array([[-100, 34, 34.5],
                       [0.12, 43, 43.134]])

    bbox_B = np.array([[-100, 34, 34.5],
                       [444, 650, 300]])

    vertices_A = shapes.bbox2box(bbox_A)
    vertices_B = shapes.bbox2box(bbox_B)

    # Generate Delaunay
    delaunay_A = shapes.Delaunay(coordinates=vertices_A)
    delaunay_B = shapes.Delaunay(coordinates=vertices_B)

    delaunay_xor = delaunay_B ^ delaunay_A

    # Not the best way, but - is tested alone, so its likely fine
    delaunay_ref = delaunay_B - delaunay_A
    coord = ((delaunay_A | delaunay_B).bbox[0, :]
             + (np.random.rand(20000, 3)
                * ((delaunay_A | delaunay_B).bbox[1, :]
                   - (delaunay_A | delaunay_B).bbox[0, :])))

    if plot:
        plt.plot(coord[:, 0], coord[:, 1], 'r.')
        plt.plot(coord[delaunay_xor(coord), 0], coord[delaunay_xor(coord), 1], 'y.', alpha=0.4)
        plt.plot(coord[delaunay_A(coord), 0], coord[delaunay_A(coord), 1], 'b.', alpha=0.4)
        plt.plot(coord[delaunay_B(coord), 0], coord[delaunay_B(coord), 1], 'g.', alpha=0.4)
        plt.plot(coord[delaunay_ref(coord), 0], coord[delaunay_ref(coord), 1], 'k.', alpha=0.4)

        plt.show()

    # Compare with reference
    standard_Shape_instance_test(
        shape=delaunay_xor, reference=delaunay_ref, name='Xor-Delaunay',
        plot=plot, verbose=verbose)

    if verbose:
        print('Xor operation passed all tests')

def test_operations(plot=False, verbose=False):

    ##########################################
    ## Generate three random shape with Delaunay
    # and then subtract the difference from the larger one

    vertices_A = (np.array([[-100, 50, -66]])
                  + np.random.rand(20, 3) * np.array([[500, 20, 100]]))
    vertices_B = (np.array([[-200, 10, -44]])
                  + np.random.rand(20, 3) * np.array([[250, 60, 150]]))

    # Generate Delaunay
    delaunay_A = shapes.Delaunay(coordinates=vertices_A)
    delaunay_B = shapes.Delaunay(coordinates=vertices_B)

    delaunay_larger = shapes.Delaunay(
        coordinates=np.concatenate([vertices_A, vertices_B]))

    delaunay_inter = delaunay_larger & (delaunay_A | delaunay_B) # and
    delaunay_diff = delaunay_larger - (delaunay_A | delaunay_B) #or
    delaunay_sum = delaunay_A | delaunay_B #Add
    delaunay_xor = delaunay_larger ^ delaunay_A

    with raises(AssertionError, match='\nArrays are not equal\nOperations-Delaunay*'):
        standard_Shape_instance_test(
            shape=delaunay_inter, reference=delaunay_diff,
            name='Operations-Delaunay',
            plot=plot, verbose=verbose)

    # Compare with reference
    standard_Shape_instance_test(
        shape=delaunay_sum, reference=delaunay_inter,
        name='Operations-Delaunay',
        plot=plot, verbose=verbose)

    standard_Shape_instance_test(
        shape=delaunay_xor,
        reference=delaunay_larger - (
            delaunay_A | (delaunay_A & delaunay_larger)),
        name='Operations-Delaunay',
        plot=plot, verbose=verbose)

    # Test if not intersection shapes raise error
    box_1 = shapes.Box(lower_left=[-7, -12], size=[1, 1])
    box_6 = shapes.Box(lower_left=[5, -12], size=[5, 5])
    with raises(
        NotImplementedError,
        match=('Intersection between non-intersecting shapes is not'
                + ' implemented')):
        empty_box = box_1 & box_6

    if verbose:
        print('Add operation passed all tests')


def test_rotation(plot=False, verbose=False):
    #####
    # Verifies a simple 90° rectangle rotation
    # Generate Rectangle
    bbox_A = np.array([[-50, -100, 30],
                       [50, 100, 200]])

    bbox_B = np.array([[-100, -50, 30],
                       [100, 50, 200]])

    vertices_A = shapes.bbox2box(bbox_A)
    vertices_B = shapes.bbox2box(bbox_B)

    # Generate Delaunay
    delaunay_A = shapes.Delaunay(coordinates=vertices_A)
    delaunay_B = shapes.Delaunay(coordinates=vertices_B)

    # Rotate A by 90°
    delaunay_A_rotated = shapes.rotate(
        shape=delaunay_A,
        angle=np.pi / 2,
        axis=(0, 0, 1),
        origin=np.mean(bbox_A, axis=0))

    check_for_bounded_instance(instance=delaunay_A)


    standard_Shape_instance_test(
        shape=delaunay_A_rotated, reference=delaunay_B, name='Rotation',
        plot=plot, verbose=verbose)

    if verbose:
        print('Rotation passed all tests')


def test_dilatation(plot=False, verbose=False):
    #####
    # Verifies a simple 90° rectangle rotation
    # Generate Rectangle
    bbox_A = np.array([[-50, -100, -30],
                       [50, 100, 30]])

    bbox_B = np.array([[-50 * 1.3, -25, -10],
                       [50 * 1.3, 25, 10]])

    bbox_C = bbox_A * 2

    vertices_A = shapes.bbox2box(bbox_A)
    vertices_B = shapes.bbox2box(bbox_B)
    vertices_C = shapes.bbox2box(bbox_C)

    # Generate Delaunay
    delaunay_A = shapes.Delaunay(coordinates=vertices_A)
    delaunay_B = shapes.Delaunay(coordinates=vertices_B)
    delaunay_C = shapes.Delaunay(coordinates=vertices_C)

    # Rotate A by 90°
    delaunay_A_dilatated = shapes.dilatate(
        shape=delaunay_A,
        dilatation=(1.3, 0.25, 1/3),
        center=np.mean(bbox_A, axis=0))

    delaunay_A_dilatated_C = shapes.dilatate(
        shape=delaunay_A,
        dilatation=2,
        center=np.mean(bbox_A, axis=0))

    check_for_bounded_instance(instance=delaunay_A)
    
    standard_Shape_instance_test(
        shape=delaunay_A_dilatated, reference=delaunay_B, name='Dilatate',
        plot=plot, verbose=verbose)

    standard_Shape_instance_test(
        shape=delaunay_A_dilatated_C, reference=delaunay_C, name='Dilatate',
        plot=plot, verbose=verbose)

    if verbose:
        print('Dilatate passed all tests')


def test_reflection(plot=False, verbose=False):
    #####
    # Verifies a simple 90° rectangle rotation
    # Generate Rectangle
    bbox_A = np.array([[-0, -0, -0],
                       [50, 100, 30]])

    bbox_B = np.array([[-50, -100, 0],
                       [0, 0, 30]])

    vertices_A = shapes.bbox2box(bbox_A)
    vertices_B = shapes.bbox2box(bbox_B)

    # Generate Delaunay
    delaunay_A = shapes.Delaunay(coordinates=vertices_A)
    delaunay_B = shapes.Delaunay(coordinates=vertices_B)

    # Rotate A by 90°
    delaunay_A_reflected = shapes.reflect(
        shape=delaunay_A,
        origin=[0, 0, 0],
        normal=(1, 0, 0)
    )

    delaunay_A_reflected = shapes.reflect(
        shape=delaunay_A_reflected,
        origin=[0, 0, 0],
        normal=(0, 1, 0)
    )

    check_for_bounded_instance(instance=delaunay_A)

    standard_Shape_instance_test(
        shape=delaunay_A_reflected, reference=delaunay_B, name='Reflect',
        plot=plot, verbose=verbose)

    if verbose:
        print('Reflect passed all tests')

def test_translation(plot=False, verbose=False):
    #####
    # Verifies that a random translation over a random polygon works
    # Generate Rectangle

    translation = np.random.rand(1, 3) * np.array([[50, 10, 100]])
    vertices_A = (np.array([[-100, 50, -66]])
                  + np.random.rand(20, 3) * np.array([[500, 20, 100]]))

    vertices_B = vertices_A + translation

    # Generate Delaunay
    delaunay_A = shapes.Delaunay(coordinates=vertices_A)
    delaunay_B = shapes.Delaunay(coordinates=vertices_B)

    # Rotate A by 90°
    delaunay_A_translated = shapes.translate(
        shape=delaunay_A, translation=translation[0, :])

    check_for_bounded_instance(instance=delaunay_A)

    standard_Shape_instance_test(
        shape=delaunay_A_translated, reference=delaunay_B, name='Translate',
        plot=plot, verbose=verbose)

    if verbose:
        print('Translate passed all tests')


def test_and_bbox(plot=False, verbose=False):

    rect1 = shapes.Box(lower_left=[0, 0, 0], size=[100, 200, 340])
    rect2 = shapes.Box(lower_left=[-10, -150, -50], size=[100, 200, 340])
    print(rect2.bbox)

    concatenated_bbox = np.array(
        [rect1.lower_left[0, :],
         (rect2.lower_left[0, :] + rect2.size[0, :])])

    assert_array_equal(
        concatenated_bbox,
        shapes.and_bbox(
            bbox_seq=(rect1.bbox, rect2.bbox)),
        err_msg='subtract_bbox failed test')

    # Test commutation
    assert_array_equal(
        concatenated_bbox,
        shapes.and_bbox(
            bbox_seq=(rect2.bbox, rect1.bbox)),
        err_msg='subtract_bbox failed test')

    translation = np.random.rand(1, 3) * np.array([[150, 10, 50]])
    vertices_A = (np.array([[-100, 50, -66]])
                  + np.random.rand(20, 3) * np.array([[500, 20, 100]]))

    vertices_B = vertices_A + translation

    delaunay_A = shapes.Delaunay(coordinates=vertices_A)
    delaunay_B = shapes.Delaunay(coordinates=vertices_B)

    bbox_glob = shapes.and_bbox(bbox_seq=(delaunay_A.bbox, delaunay_B.bbox))

    assert_array_equal(
        (delaunay_A & delaunay_B).bbox,
        bbox_glob,
        err_msg='bbox and didn"t pass sanity test')

    # Test if the and bbox is enough to capture and equivalent operation
    xor_op = delaunay_A ^ delaunay_B

    standard_bbox_test(
        shape=delaunay_A - xor_op,
        name='BBox and operation - xor eq test',
        verbose=verbose)


def test_subtract_bbox(plot=False, verbose=False):

    rect1 = shapes.Box(lower_left=[0, 0, 0], size=[100, 200, 340])
    rect2 = shapes.Box(lower_left=[-10, -150, -50], size=[100, 200, 340])
    print(rect2.bbox)

    concatenated_bbox = np.array(
        [[ 90,  50, 290],
         [ 100,  200, 340]])

    assert_array_equal(
        concatenated_bbox,
        shapes.subtract_bbox(bbox1=rect1.bbox, bbox2=rect2.bbox),
        err_msg='subtract_bbox failed test')

    concatenated_bbox = np.array(
        [[ -10,  -150, -50],
         [ 0,  0, 0]])

    assert_array_equal(
        concatenated_bbox,
        shapes.subtract_bbox(bbox1=rect2.bbox, bbox2=rect1.bbox),
        err_msg='subtract_bbox failed test')

    translation = np.random.rand(1, 3) * np.array([[150, 10, 50]])
    vertices_A = (np.array([[-100, 50, -66]])
                  + np.random.rand(20, 3) * np.array([[500, 20, 100]]))

    vertices_B = vertices_A + translation
    print(translation)
    # Generate Delaunay
    ##############
    # The subtract bbox routine should only work for rectangles
    # where the bbox is exact

    # delaunay_A = shapes.Delaunay(coordinates=vertices_A)
    # delaunay_B = shapes.Delaunay(coordinates=vertices_B)

    # bbox_AsubB = shapes.subtract_bbox(delaunay_A.bbox, delaunay_B.bbox)

    # standard_bbox_test(
    #     shape=shapes.General(delaunay_A - delaunay_B, bbox_AsubB),
    #     name='Bbox subtract',
    #     verbose=verbose)

    if verbose:
        print('subtract_bbox passed all tests')

def test_extruded():
    ''' Test Extruded class. 
    
    Tests extruding 1D -> 2D (box)
    Tests extruding 1D -> 3D (box)
    Tests extruding 2D -> 3D (box)
    '''

    a1 = shapes.Box(lower_left=(-100, ), size=(100, ))

    p_test = patterns.Rectangular.constant(element_size=(1, 1))

    a2 = shapes.Box(lower_left=(-100, -100), size=(100, 100))
    c_a2 = p_test(p_test.inside(a2))

    a1_2 = shapes.Extruded(shape=a1, axis=1, bounds=np.array([[-100], [0]]))
    c_a1_2 = p_test(p_test.inside(a1_2))
    assert_array_equal(c_a2, c_a1_2)

    a1_2 = shapes.extrude(shape=a1, axis=1, bounds=np.array([[-100], [0]]))
    c_a1_2 = p_test(p_test.inside(a1_2))
    assert_array_equal(c_a2, c_a1_2)

    p_test = patterns.Rectangular.constant(element_size=(1, 1, 1))

    a3 = shapes.Box(lower_left=(-100, -100, -100), size=(100, 100, 100))
    c_a3 = p_test(p_test.inside(a3))

    a1_3 = shapes.Extruded(
        shape=a1, axis=(1, 2), bounds=np.array([[-100, -100], [0, 0]]))
    c_a1_3 = p_test(p_test.inside(a1_3))
    assert_array_equal(c_a3, c_a1_3)

    a1_3 = shapes.extrude(
        shape=a1, axis=(1, 2), bounds=np.array([[-100, -100], [0, 0]]))
    c_a1_3 = p_test(p_test.inside(a1_3))
    assert_array_equal(c_a3, c_a1_3)

    a2_2 = shapes.Extruded(shape=a2, axis=1, bounds=np.array([[-100], [0]]))
    c_a2_2 = p_test(p_test.inside(a2_2))
    assert_array_equal(c_a3, c_a2_2)

    a2_2 = shapes.extrude(shape=a2, axis=1, bounds=np.array([[-100], [0]]))
    c_a2_2 = p_test(p_test.inside(a2_2))
    assert_array_equal(c_a3, c_a2_2)


    a2_2 = shapes.Extruded(shape=a2, axis=2, bounds=np.array([[-100], [0]]))
    c_a2_2 = p_test(p_test.inside(a2_2))
    assert_array_equal(c_a3, c_a2_2)

    a2_2 = shapes.extrude(shape=a2, axis=2, bounds=np.array([[-100], [0]]))
    c_a2_2 = p_test(p_test.inside(a2_2))
    assert_array_equal(c_a3, c_a2_2)    