from urllib.error import ContentTooShortError
import warnings

import numpy as np

from pescado import tools
from pescado import mesher

from numpy.testing import assert_array_equal
from pytest import raises

def test_voronoi_1d():

    precision = 14
    tol = 1e-14

    ###########################################################################
    # Generate 1D and 2D Grid
    ###########################################################################

    bbox_1d = np.array([[-2.,],
                 [10.,]])
    grid_pts_1d = tools.meshing.grid(bbox=bbox_1d, step=[0.3, ])

    bbox_2d = np.array([[-2., -1],
                 [10., 1]])
    grid_pts_2d = tools.meshing.grid(bbox=bbox_2d, step=[0.3, 1])

    args = np.arange(len(grid_pts_1d))
    np.random.shuffle(args)

    assert_array_equal(
        grid_pts_1d[np.arange(len(grid_pts_1d)), 0],
        grid_pts_2d[np.arange(len(grid_pts_1d)), 0],
        err_msg='1D and 2D testing arrays are not the same')

    ###########################################################################
    # Generate 1D and 2D voronoi_diagram
    ###########################################################################

    (points_1d, vertices_1d,
     ridge_points_1d, ridge_vertices_1d,
     ridges_1d, region_vertices_1d, region_1d) = mesher.voronoi_diagram_1d(
        points=grid_pts_1d[args])

    (points_2d, vertices_2d,
     ridge_points_2d, ridge_vertices_2d,
     ridges_2d, region_vertices_2d, region_2d) = mesher.voronoi_diagram_qhull(
        points=grid_pts_2d)

    points_1d = np.around(points_1d, decimals=precision)
    points_2d = np.around(points_2d, decimals=precision)

    vertices_1d = np.around(vertices_1d, decimals=precision)
    vertices_2d = np.around(vertices_2d, decimals=precision)

    ###########################################################################
    # Test vertices
    ###########################################################################

    args_ver_1d = np.argsort(vertices_1d[:, 0])

    ## Recover the index that are constant along 2D
    argsort = np.argsort(vertices_2d[:, 1])
    args_ver = argsort[len(vertices_1d): 2 * len(vertices_1d)]

    args_ver = args_ver[np.argsort(vertices_2d[args_ver, 0])]

    assert_array_equal(
        np.around(vertices_1d[args_ver_1d, 0], decimals=precision),
        np.around(vertices_2d[args_ver, 0], decimals=precision),
        err_msg='voronoi_diagram_1d did not pass vertex test')

    ###########################################################################
    # Check if point_region and the regions where set correctly
    ###########################################################################

    ## Find the point coordinates
    args_pts_1d = np.argsort(points_1d[:, 0])
    args_pts = np.arange(len(points_1d), 2 * len(points_1d))
    assert_array_equal(
        np.around(points_1d[args_pts_1d, 0], decimals=precision),
        np.around(points_2d[args_pts, 0], decimals=precision),
        err_msg='voronoi_diagram_1d did not pass points test')   

    # Keep only the correct vertices
    cleaned_rg_pts = [[] for i in range(len(args_pts))]
    for i, rg in enumerate(args_pts):
        cleaned_rg_pts[i] = np.intersect1d(
            region_vertices_2d[region_2d[rg]:region_2d[rg+1]], 
            np.insert(args_ver, obj=0, values=-1))
    
    ## Test the ones that are open
    assert ((-1 in cleaned_rg_pts[0])
            and (-1 in cleaned_rg_pts[-1])
            and (-1 == region_vertices_1d[
        region_1d[args_pts_1d[0]]:region_1d[args_pts_1d[0]+1]][0])
            and (-1 == region_vertices_1d[
        region_1d[args_pts_1d[-1]]:region_1d[args_pts_1d[-1]+1]][1]))

    ## Check if the vertices forming each region are the same
    for i in range(len(args_pts_1d) - 1):
        ver_2d = region_vertices_2d[
            region_2d[args_pts[i]]:region_2d[args_pts[i]+1]]
        if len(ver_2d) == 4: # Otherwise open cell
            cver = vertices_1d[region_vertices_1d[
                region_1d[args_pts_1d[i]]:region_1d[args_pts_1d[i]+1]]][:, 0]
            c_ver2d = vertices_2d[ver_2d][:, 0]
            assert len(np.setdiff1d(c_ver2d, cver)) == 0
            
    ###########################################################################
    # Test ridge_points
    ###########################################################################

    ## Test if it points surrounding ridge are first neig of each other
    for i, points in enumerate(ridge_points_1d):
        assert (points[1]
                in np.argsort(
                    np.abs(points_1d - points_1d[points[0]])[:, 0])[:3]), (
                'voronoi_diagram_1d did not pass ridge_points test')

    ###########################################################################
    # Test ridge_vertices
    ###########################################################################

    ## Test if vertex is in between the two points
    for i in range(len(ridges_1d) -1):
        assert np.abs(vertices_1d[
            ridge_vertices_1d[ridges_1d[i]:ridges_1d[i+1]]]
                      - np.mean(points_1d[ridge_points_1d[i]])) < tol, (
            'voronoi_diagram_1d did not pass ridge_vertices test')


def test_cell_type_fneig_points_ridges():

    ###########################################################################
    # Generate Grid
    ###########################################################################

    bbox_1d = np.array([[-2.,],
                 [10.,]])
    grid_pts_1d = tools.meshing.grid(bbox=bbox_1d, step=[0.3, ])

    bbox_2d = np.array([[-2., -1],
                 [10., 1]])
    grid_pts_2d = tools.meshing.grid(bbox=bbox_2d, step=[0.3, 1])

    bbox_3d = np.array([[-2., -1, -10.3],
                    [10., 1, 5.21]])
    grid_pts_3d = tools.meshing.grid(bbox=bbox_3d, step=[0.3, 1, 1.1])

    args = np.arange(len(grid_pts_1d))
    np.random.shuffle(args)

    ###########################################################################
    # Generate 1D and 2D voronoi_diagram
    ###########################################################################

    (points_1d, vertices_1d,
     ridge_points_1d, ridge_vertices_1d,
     ridges_1d, region_vertices_1d, region_1d) = mesher.voronoi_diagram_1d(
        points=grid_pts_1d[args])

    (points_2d, vertices_2d,
     ridge_points_2d, ridge_vertices_2d,
     ridges_2d, region_vertices_2d, region_2d) = mesher.voronoi_diagram_qhull(
        points=grid_pts_2d)
    
    # The 3D  voronoi_diagram should raise a warning.
    with warnings.catch_warnings(record=True) as warning_list:
        (points_3d, vertices_3d,
         ridge_points_3d, ridge_vertices_3d, ridges_3d,
         region_vertices_3d, region_3d) = mesher.voronoi_diagram_qhull(
            points=grid_pts_3d)

    if len(warning_list) == 0:
        raise RuntimeError(
            'No warning was raised, voronoi_diagram_qhull failed test.')
    else:
        for warning in warning_list:
            err_msg = (
                ('\n Some points were ignored when calculating the ridges'
                 + ' in the voronoi diagram.'))

            if ((str(warning.message)[:78] != err_msg[:78])
                or (warning.category is not RuntimeWarning)):

                raise RuntimeError(
                    ('\n Unexpected warning was raised. It goes as follows : \n'
                     + '{0} : {1}'.format(
                        str(warning.category), str(warning.message))))

    for reg, reg_vert, ridge_points, pts in zip(
        [region_1d, region_2d, region_3d],
        [region_vertices_1d, region_vertices_2d, region_vertices_3d],
        [ridge_points_1d, ridge_points_2d, ridge_points_3d],
        [points_1d, points_2d, points_3d]):

        ########################################################################
        # Test cell_type
        ########################################################################
        bounded_values, unbounded_values = mesher.voronoi.region_type(
            region_vertices=reg_vert, regions=reg)

        for pt in bounded_values:
            assert -1 not in reg_vert[reg[pt]:reg[pt+1]], (
                'Open region found for a point in cell_type bounded values')
        for pt in unbounded_values:
            assert -1 in reg_vert[reg[pt]:reg[pt+1]], (
                'Closed region found for a point in cell type unbounded values')

        ########################################################################
        # Test first_neighbours
        ########################################################################

        (neighbours, 
         ridges, points_neighbour) = mesher.voronoi.first_neighbours_qhull(
            ridge_points=ridge_points, npoints=len(pts))

        ######################################
        ## Test if ridges[points_neighbour[i]:points_neighbour[i+1]] 
        #  corresponds to the ridge
        # between i and neighbours[points_neighbour[i]:points_neighbour[i+1]] .
        ######################################
        for idx in range(len(pts)):
            int_neig_idx = np.arange(
                points_neighbour[idx],points_neighbour[idx+1], dtype=int)
            for i, (ridge, neig) in enumerate(
                zip(ridges[int_neig_idx], neighbours[int_neig_idx])):
                assert neig in ridge_points[ridge].ravel(), (
                    'The {0}th ridge '.format(i)
                    + ' is not the one separating the the point'
                    + ' {1} and its {0}th first neighbour (point {2}).'.format(
                        i, idx, neig)
                    + ' This goes against the ordering convention in '
                    + ' mesher.voronoi.first_neighbours_qhull')

        ######################################
        ## Test if every point sharing a ridge
        ## is first neig of the point it is sharing the ridge with
        ######################################
        for points in ridge_points:
            fneig = neighbours[
                points_neighbour[points[0]]:points_neighbour[points[0]+1]]
            assert points[0] not in fneig, (
                '{0} is counted as neig of {0}.'.format(points[0])
                + ' first_neighbours did not pass test;')

            assert points[1] in fneig, (
                '{0} neig of {1} is missing.'.format(points[1], points[0])
                + ' first_neighbours did not pass test;')

        ######################################
        ## Check that all fneigs share a ridge
        ## and that no other point than its fneigs share the same ridge.
        ######################################
        for pt in range(len(pts)):
            
            fneig = neighbours[
                points_neighbour[pt]:points_neighbour[pt+1]]
            
            pt_ridges = ridges[points_neighbour[pt]:points_neighbour[pt+1]]
            pt_found = np.zeros(len(pt_ridges), dtype=int)

            assert len(pt_ridges) == len(fneig)

            for i, neig in enumerate(fneig):
                neig_ridges = ridges[
                    points_neighbour[neig]:points_neighbour[neig+1]]
                res = np.searchsorted(pt_ridges[i] - neig_ridges, [0,])
                assert len(res) == 1
                assert pt_found[i] == 0
                pt_found[i] == 1

        ######################################
        ## Check that all neigs for bounded values are found
        ## 2, 4 and 6 for 1D, 2D and 3D
        ######################################
        dim = pts.shape[1]
        for i in bounded_values:
            fneig = neighbours[
                points_neighbour[i]:points_neighbour[i+1]]
            assert len(fneig) == 2 * dim, (
                'Some first neighbours were not found for bounded_values')

        ######################################
        ## Check that up to 1, 1 or  for bounded values are found
        ## 2, 4 and 6 for 1D, 2D and 3D
        ######################################
        for i in unbounded_values:
            fneig = neighbours[
                points_neighbour[i]:points_neighbour[i+1]]
            assert len(fneig) < 2 * dim, (
                'Too many first neighbours were found for unbounded values')
