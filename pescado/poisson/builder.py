
import time
import functools
import collections
import pickle
import copy

import numpy as np
from scipy import constants
import scipy.sparse as sparse

from pescado import mesher
from .problem import Problem
from pescado.tools import reduced_array as ra
from pescado.tools import meshing, plotting

from numpy.testing import assert_array_equal, assert_allclose

def convert_kwant(kwant_syst, kwant_lattice=None):
    ''' Convert kwant object into Pattern + kwant_tags

    Parameters
    -----------

    kwant_syst: instance of kwant.Builder()

    kwant_lattice: instance of kwant.lattice() (optional)

        If given, the function will see if there is a compatible
        mesher.patterns.Pattern() instance that can be generated
        instead of the default mesher.patterns.Finite()

        Optmization possible for kwant.lattice.Monoatomic
        with prime vectors defined along a single direction,
        e.g.

            prime_vectors = [[1, 0, 0], [0, 0.5, 0], [0, 0, 5]]

    Returns
    --------

    kwant_pat: instance of mesher.patterns.Pattern()

    kwant_coordinates: dictionary
        Keys : kwant.Sites instance

        values: np.array of floats
            sites coordinates obtained with kwant.Sites.pos

    '''

    kwant_pat = None
    if (str(type(kwant_lattice))
        == "<class 'kwant.lattice.Monatomic'>"):
        # Rectangular works if the
        # X, Y and Z axis are independant
        mask_pv = kwant_lattice.prim_vecs != 0.
        if np.all(np.sum(mask_pv, axis=1) == 1):
            kwant_pat = mesher.patterns.Rectangular.constant(
                    element_size=kwant_lattice.prim_vecs[mask_pv])

    # Compatibility with python 3.6
    kwant_coordinates = collections.OrderedDict({
        site:site.pos for site in kwant_syst.sites})

    if kwant_pat is None:
        kwant_pat = mesher.patterns.Finite(
            elements=kwant_coordinates)

    return kwant_pat, kwant_coordinates


def sparse_capa_matrix(
    points_idx=None, mesh=None, dielectric_constant=None, return_volume=False):
    ''' Generates a csc sparse matrix, scipy.sparse.csr_matrix

    Parameters
    -----------

    mesh: instance of pescado.mesher.mesh.Mesh

    dielectric_constant: sequence of floats with 'mesh.npoits' elements
        Absolute permittivity for every site in the mesh

        TODO: set it as SparseVector ?

    points_idx: np.array of intergers with shape(m, ) - optional

        m: number of points

        if not defined, all points in the mesh are used.

        Index of the mesh points from which to build the capacitance matrix,
        it is possible that some of the neighbouring points do not belong
        to 'points_idx'. Those points WILL NOT be ignored when calculating the capacitance matrix elements

    return_volume: boolean - default is False
        If True returns the volume associated with 'points_idx' as well. 
                            
                            Saves time

    Returns
    --------

    instance of scipy.sparse.csr_matrix defining the capacitance matrix with
    n rows and n columns :
        n: 'mesh'.npoints
        n: 'mesh'.npoints

    If return_volume is True
    np.ndarray of floats with shape(m, )
        Volumes of the mesh cells of points_idx

    '''

    if points_idx is None:
        points_idx = np.arange(0, mesh.npoints, dtype=int)

    property_dict, point_neighbours, neighbours = mesh._geometrical_property(
        points=points_idx, _property=['surface', 'volume', 'distance'])

    count_neig = ra.counts(point_neighbours)
    
    inside_neig = np.arange(len(neighbours))[neighbours != -1]
    outside_neig = np.arange(len(neighbours))[neighbours == -1]
    pos_out_in_points = np.searchsorted(
        point_neighbours, outside_neig, side='right') - 1
    num_outside_in_points = np.bincount(pos_out_in_points)
    
    count_neig[:max(pos_out_in_points)+1] -= num_outside_in_points
    poins_neighbours_inside = np.insert(
        np.cumsum(count_neig), obj=0, values=[0, ])
    number_non_zero = len(inside_neig) + len(points_idx)

    column = np.zeros(number_non_zero, dtype = int)
    row = np.zeros(number_non_zero, dtype = int)
    data = np.zeros(number_non_zero, dtype=float)

    surface = property_dict['surface']
    distance = property_dict['distance']

    geo_capa = -1 * 2 * np.true_divide(
        surface[inside_neig], distance[inside_neig])
    
    die_fact = (
        np.repeat(dielectric_constant, repeats=count_neig)
        * dielectric_constant[neighbours[inside_neig]]
        / (np.repeat(dielectric_constant, repeats=count_neig)
           + dielectric_constant[neighbours[inside_neig]]))
    
    capa_vect = die_fact * geo_capa

    column = np.zeros(number_non_zero, dtype = int)
    row = np.zeros(number_non_zero, dtype = int)
    data = np.zeros(number_non_zero, dtype=float)

    # Diagonal terms
    dif_ver = max(count_neig) - count_neig
    capa_vect_homogeneous = capa_vect
    if np.any(dif_ver != 0):
        capa_vect_homogeneous = np.insert(
            capa_vect_homogeneous, 
            obj=poins_neighbours_inside[1:][dif_ver != 0],
            values=np.zeros(sum(dif_ver != 0)))
        new_idx = ra.update_indices(
            elements=np.arange(len(dif_ver))[dif_ver != 0],
            counts=np.ones(sum(dif_ver != 0)),
            indices=poins_neighbours_inside)
        rep = np.ones(len(capa_vect_homogeneous), dtype=int)
        dif_ver[dif_ver != 0] -= 1
        rep[new_idx[1:] - 1] += dif_ver
        capa_vect_homogeneous = np.repeat(capa_vect_homogeneous, repeats=rep)

    column[:len(points_idx)] = points_idx
    row[:len(points_idx)] = points_idx
    data[:len(points_idx)] = -1 * np.sum(
        np.vstack(np.split(capa_vect_homogeneous, len(points_idx))), axis=1)

    column[len(points_idx):] = np.repeat(points_idx, repeats=count_neig)
    row[len(points_idx):] = neighbours[inside_neig]
    data[len(points_idx):] = capa_vect

    # Eventually if it all breaks
    #data = np.delete(data, np.arange(element_id, number_elements))
    #column = np.delete(column, np.arange(element_id, number_elements))
    #row = np.delete(row, np.arange(element_id, number_elements))
    # Check which type of sparse matrix migth be best to use
    sparse_mat = sparse.csr_matrix(
            (data, (row, column)), shape=(mesh.npoints, )*2)

    if return_volume:
        return sparse_mat, property_dict['volume']
    else:
        return sparse_mat


class ProblemBuilder():

    def __init__(self, mesh=None):
        ''' Creates an instance of pescado.poisson.ProblemBuilder

        Parameters
        -----------

        mesh : instance of pescado.mesher.Mesh
            Optional, if None creates a new instance of pescado.mesher.Mesh

        Returns
        --------

        instance of pescado.poisson.ProblemBuilder
        '''

        self.mesh = mesh

        ## Normalization for distance / charge / voltage
        self.distance_units = constants.nano #nm
        self.voltage_units = 1 #volts
        self.charge_units = constants.elementary_charge  # elementary charge

        self.vaccum_perm = (
            constants.epsilon_0
            * (self.distance_units * self.voltage_units / self.charge_units))

        self.dielectric_functions = list()

        self.reset_regions()
        self.kwant_coordinates = list()
        self.kwant_tags = list()

##### Mesh building functions
    def initialize_mesh(
            self, simulation_region=None, pattern=None, file_name=None):
        ''' Initializes an 'pescado.mesh.Mesh' instance. 
         It can use :
            i) 'simulation_region' and 'pattern' to initialize
         a Mesh instance, c.f. 'pescado.mesh.Mesh()'. 
            ii) 'file_name' to load a saved Mesh, c.f.
                'pescado.mesh.Mesh.load('file_name')'. 

        Parameters
        -----------

        simulation_region: instance of pescado.mesher.shapes.Shape 
            Defines the region occupied by the mesh

        pattern: instance of pescado.mesher.patterns.Pattern
            Defines the default pattern to fill up the
            mesh region

        file_name: string
            Path and file name of the saved Mesh

        '''

        if self.mesh is None:
            if (simulation_region is not None) and (pattern is not None):
                self.mesh = mesher.Mesh(
                    simulation_region=simulation_region, pattern=pattern)
            elif file_name is not None:
                self.mesh = mesher.Mesh.load(name=file_name)
            else:
                raise ValueError('Wrong inputs')
        else:
            raise RuntimeError(
                'An instance of "mesher.Mesh" already exists, '
                + 'please use the "reset_mesh" method instead of '
                + ' "initialize_mesh"')

    def refine_mesh(self, region, pattern=None, coordinates=None, ext_bd=None):
        ''' Refines part of the current mesh

        Parameters
        -----------

        region: instance of pescado.mesher.Shape
            Region in the mesh to be refined

        pattern: instance of pescado.mesher.Pattern
           The pattern used to refine the 'region' of the mesh.

        ext_bd: interger (optional, default None)

            Adds points forming the external boundary up to 'ext_bd'
            of the 'shape' instance in question.

            Uses pescado.mesher.external_boundary_shape(). Hence
            only convex shapes can be defined correctly.

        '''

        self.mesh.refine(
            region=region, pattern=pattern, 
            coordinates=coordinates, nth=ext_bd)

    def refine_mesh_kwant(
        self, region, kwant_syst, kwant_model_coordinate=None):
        ''' Add the points inside a kwant system to the mesh in
        pescado.poisson.ProblemBuilder.

        Parameters
        -----------

        region: instance of pescado.mesher.Shape
            Region in the mesh where the kwant system is defined

        kwant.builder: finalized instance of kwant.builder

        kwant_model_coordinate: np.array of shape(1, d)
            d: dimension of the 'region' and the 'self.mesh'

            example -> let region be 3D and kwant_syst 2D
                kwant_model_coordinate = np.array([[None, 0.0, None]])

                Adds kwant_syst along axis (0, 2) at axis 1 = 0.0

        '''

        if kwant_model_coordinate is not None:

            kwant_axis = np.arange(kwant_model_coordinate.shape[1])[
                kwant_model_coordinate[0, :] == None]
            
            # Recover and reshape kwant coordinates
            coord = copy.deepcopy(kwant_model_coordinate)
            coord[0, kwant_axis] = np.zeros(len(kwant_axis))
            coord = coord.astype(float)
            coord = coord.repeat(len(kwant_syst.sites), axis=0)

            for i, site in enumerate(kwant_syst.sites):
                coord[i, kwant_axis] = site.pos
        
            if region.bbox.shape[1] != kwant_model_coordinate.shape[1]:
                raise ValueError(
                    ('region has dimension '
                    +'{0} when kwant_model_coordinate has dimension {1}'.format(
                        region.shape[1], 
                        kwant_model_coordinate.shape[1])))
        else:
            coord = np.array([site.pos for site in kwant_syst.sites])
        
        # Keep those inside the region
        inside_idx = np.arange(len(coord))[region(coord)]

        kwant_coordinates = {kwant_syst.sites[i].tag:coord[i] 
                             for i in inside_idx}

        self.refine_mesh(
            region=region, coordinates=coord[inside_idx])

        # Recover the pattern index
        self.kwant_tags += [k for k in kwant_coordinates.keys()]
        self.kwant_coordinates += [v for v in kwant_coordinates.values()]

    def reset_mesh(self, simulation_region, pattern):
        ''' Reset self.mesh
        '''

        self.kwant_coordinates = list()
        self.kwant_tags = list()
        self.mesh = None

        self.initialize_mesh(
            simulation_region=simulation_region, pattern=pattern)

##### Region assignment functions
    def _update_regions(self, regions, region_type):
        ''' Update self.regions and self.regions_shapes

        Parameters
        -----------
        regions: sequence of 2 elements
            shape and tag

        type: string
            'neumann', 'dirichlet', 'helmholtz' or 'mutable'
        '''

        for region in regions:

            name = self._generate_region_name(
                regions=region, region_type=region_type)
            self.regions[region_type].append(name)
            self.regions_shapes.update({name: region[0]})

    def _generate_region_name(self, regions, region_type):
        ''' Returns a string naming 'sub_region' that is unique
        to the builder.

        Parameters
        -----------
        regions: sequence of 2 elements
            shape and tag

        type: string
            'neumann', 'dirichlet', 'helmholtz' or 'mutable'

        Returns
        --------
        string
        '''

        default_name = '{1}_{0}'.format(
            len(self.regions[region_type]), region_type)

        if len(regions) == 1:
            name = None
        else:
            name = regions[1]

        # Make sure I'm not adding any default name
        # that already exists.
        if (name is None
            or name[:-1] == '{0}_'.format(region_type)):
            name = default_name

        return name
    
    def set_metal(self, region, setup_name):
        ''' Sets the sites inside 'region' as metal and name them 
        'setup_name'. Setting them as metal means setting their type
        to Dirichlet ant their relative permittivity to 1e4
        
        Parameters
        -----------
            region : instance of pescado.mesher.Shapes
                Region where Neumann b.c.s is fixed,
                i.e. where the charge is fixed

            setup_name : immutable object - name of the region
        
        '''

        self.set_relative_permittivity(val=1e8, region=region)
        self.set_dirichlet(region=region, setup_name=setup_name)

    def set_neumann(self, region, setup_name):
        ''' Sets boundary condition to neumann (fixed charged)
        in the '*regions' inside the
        pescado.poisson.ProblemBuilder instance.

        Parameters
        -----------
            region : instance of pescado.mesher.Shapes
                Region where Neumann b.c.s is fixed,
                i.e. where the charge is fixed

            setup_name : immutable object - name of the region

        '''

        self._update_regions([(region, setup_name),],  region_type='neumann')

    def set_dirichlet(self, region, setup_name):
        ''' Sets boundary condition to dirichlet (fixed voltage)
        in the '*regions' inside the
        pescado.poisson.ProblemBuilder instance.

        Parameters
        -----------
            region : instance of pescado.mesher.Shapes
                Region where Dirichlet b.c.s is fixed,
                    i.e. where the potential is fixed

            setup_name : immutable object - name of the region

        '''

        self._update_regions([(region, setup_name),], region_type='dirichlet')

    def set_helmholtz(self, region, setup_name):
        ''' Sets boundary condition to quantum in the
        '*regions' inside the pescado.poisson.ProblemBuilder
        instance.

        Parameters
        -----------
            region : instance of pescado.mesher.Shapes
                Region where Helmholtz b.c.s is fixed,
                    i.e. where the charge at zero potential and the
                    charge dependance on the potentail (slope) is fixed

            setup_name : immutable object - name of the region
        '''

        self._update_regions([(region, setup_name),], region_type='helmholtz')

    def set_flexible(self, region, setup_name):
        ''' Sets boundary condition to quantum in the
        '*regions' inside the pescado.poisson.ProblemBuilder
        instance.

        Parameters
        -----------
            region : instance of pescado.mesher.Shapes
                Region of flexible b.c.s,
                i.e. where the b.c.s type can change from
                Neumann, Dirichlet or Helmholtz

            setup_name : immutable object - name of the region

        '''

        self._update_regions([(region, setup_name),], region_type='flexible')

    def reset_regions(self):
        ''' Reset all the regions
        '''

        self.regions_shapes = dict()
        self.regions = {'neumann':list(),
                        'dirichlet':list(),
                        'helmholtz':list(),
                        'flexible':list()}

    def delete_region(self, name):
        ''' Deletes a specific boundary condition
        '''

        # poping self.regions_shapes after self.regions[key] assures that if
        # there is the same name in another self.regions element,
        # then an error is raised

        deleted = False
        for key in self.regions.keys():
            if name in self.regions[key]:
                self.regions[key].remove(name)
                self.regions_shapes.pop(name)
                deleted = True

        if not deleted:
            raise ValueError('{0} not found'.format(name))


##### Rest
    def set_relative_permittivity(self, val, region):
        ''' Defines the relative permittivity for non default regions

        Parameters
        -----------

            val: int, float or function

                Relative permittivity for a given coordinate

                if function : val(float) -> float, int

            region: instance of pescado.mesher.shapes.Shape
                Region to fix 'val' as the relative permittivity

        Notes
        -----

        The relative permittivity is set sequentially.

        Therefore if 'region' intersects with another instance of
        shape previously used to set the relative permittivity,
        the intersecting points will be overloaded by the new 'val'.

        '''

        if isinstance(val, (float, int, np.integer, np.floating)):
            value_fun = lambda x: (np.ones(len(x), dtype=float) * val)

        self.dielectric_functions.append((value_fun, region))

    def _make_capacitance_matrix(
            self, mesh, return_volume=False):
        ''' Returns a capacitance matrix

        Parameters
        -----------
        mesh = instance of pescado.mesher.mesh.Mesh

        Returns
        --------
        instance of scipy.sparse.csr_matrix defining the capacitance matrix with
        n rows and n columns :

            n: 'mesh'.npoints
            n: 'mesh'.npoints
        '''
        
        # Find the non default dielectric constant values
        dielectric_constant = np.ones(mesh.npoints, dtype=float)
        coordinates = mesh.coordinates()
        for (val, region) in self.dielectric_functions:
            idx = np.arange(mesh.npoints)[
                region(coordinates)]
            dielectric_constant[idx] = val(coordinates[idx])

        dielectric_constant = dielectric_constant * self.vaccum_perm

        # Generate the capacitance matrix
        capa_matrix = sparse_capa_matrix(
            mesh=mesh, dielectric_constant=dielectric_constant, 
            return_volume=return_volume)

        return capa_matrix

    def _finalize_regions(self):
        ''' Returns a finalized 'regions' and 'regions_shapes'
        '''

        # Do not use _update_regions since default can change each time
        # finalized is used.
        regions = copy.deepcopy(self.regions)
        regions_shapes = copy.deepcopy(self.regions_shapes)

        default_shape = self.mesh._simulation_region

        def _and(x, y):
            return x | y
        if len(regions_shapes) > 0:
            default_shape -= functools.reduce(
                _and, [g for g in regions_shapes.values()])

        default_name = self._generate_region_name(
            (default_shape, 'default'), 'neumann')

        regions['neumann'].append(default_name)
        regions_shapes.update({default_name: default_shape})        

        return regions, regions_shapes
    
    def finalized(self, capa_matrix=None):
        ''' Returns an instance of pescado.poisson.Solver

        Parameters
        -----------

        capa_matrix: instance of scipy.sparse.csr_matrix - default None
            Contains the elements of the capacitance matrix

            If None then a capacitance matrix is constructed.

        '''

        regions, regions_shapes = self._finalize_regions()

        # OrderedDict is used
        kwant_indices = None
        if self.kwant_coordinates:
            kwant_indices_list = self.mesh.points(
                coordinates=np.array(self.kwant_coordinates))
            kwant_indices = {
                site:index
                for site, index in zip(
                    self.kwant_tags, kwant_indices_list)}

        if capa_matrix is None:
            capa_matrix, volume = self._make_capacitance_matrix(
                mesh=self.mesh, return_volume=True)
        else:
            volume = self.mesh.volumes(np.arange(self.mesh.npoints, dtype=int))

        return Problem(
            capacitance_matrix=capa_matrix,
            coordinates=self.mesh.coordinates(
                np.arange(self.mesh.npoints, dtype=int)),
            volume=volume,
            regions=regions,
            regions_shapes=regions_shapes,
            kwant_indices=kwant_indices)
    
    def plot_boundary_condition(
        self, region, grid_step, grid_center=None, direction=None, **kwargs):
        ''' Generates the function 'plot_fun'. It plots the boundary_condition inside 'region'.

        If direction is None: 'plot_fun(ax, **kwargs)'

        If direction is not None: it slices the boundary_condition at 
        a constant 'cut_position' along 'direction' : 
                    
                    'plot_fun(cut_position, ax, **kwargs)'

        Parameters
        -----------
        
        region: instance of 'pescado.mesher.shapes'
            Notice -> dimension of the 'region' must be smaller than 'd'

        grid_step: float
            Distance between grid points used for plotting

        grid_center: sequence of floats (default None)
            Coordinates of the center of the grid used for plotting

        direction: integer or sequence of integers (default is None)
            Slicing direction

        kwargs: transferred to pescado.tools.plotting.discrete_value_plot()

        Returns
        --------
        plot_fun:

            Parameters
            -----------
            cut_position: floats or sequence of floats (default is None)
                Same format as 'direction'

            cmap: str (default 'Set1')
                c.f. https://matplotlib.org/examples/color/colormaps_reference.html

                Ignored if 1D plot

            kwargs: arguments passed down to 
                i) ax.plot if 1D plot
                ii) ax.imshow if 2D plot

            Returns
            --------

            if 1D plot -> 'ax'
            if 2D plot -> 'ax' and 'ax.imshow(-)'        
        '''

        regions, regions_shapes = self._finalize_regions()
        values = np.ones(self.mesh.npoints, dtype=int) * -1
        coord = self.mesh.coordinates(np.arange(self.mesh.npoints, dtype=int))

        regions_keys = [i for i in regions.keys()]
        names_list = list()

        for i, names in enumerate(regions.values()):
            
            if len(names) > 0:
                names_list.append(regions_keys[i])
            
            for n in names:
                map_ = regions_shapes[n](coord)
                if np.any(values[map_] != -1):
                    raise RuntimeError('Error plotting bcs')
                values[map_] += len(names_list)

        region_plot_fun = plotting.region_plot(
                region=region, grid_step=grid_step, 
                grid_center=grid_center,
                values_coord=coord,
                direction=direction)

        return plotting.discrete_value_plot(
            region_plot_fun, region.bbox, values, names_list, **kwargs)
    
    def plot_system_regions(
        self, region, grid_step, 
        grid_center=None, direction=None, **kwargs):
        ''' Generates the function 'plot_fun'. It plots the boundary_condition inside 'region'.

        If direction is None: 'plot_fun(ax, **kwargs)'

        If direction is not None: it slices the boundary_condition at a constant 'cut_position' along 'direction' ; 
                    
                    'plot_fun(cut_position, ax, **kwargs)'

        Parameters
        -----------
        
        region: instance of 'pescado.mesher.shapes'
            Notice -> dimension of the 'region' must be smaller than 'd'

        grid_step: float
            Distance between grid points used for plotting

        grid_center: sequence of floats (default None)
            Coordinates of the center of the grid used for plotting

        direction: integer or sequence of integers (default is None)
            Slicing direction

        kwargs: transferred to pescado.tools.plotting.discrete_value_plot()

        Returns
        --------
        plot_fun:

            Parameters
            -----------
            cut_position: floats or sequence of floats (default is None)
                Same format as 'direction'

            ax: instance of plt.gca() (default is None)

            cmap: str (default 'Set1')
                c.f. https://matplotlib.org/examples/color/colormaps_reference.html

                Ignored if 1D plot

            kwargs: arguments passed down to 
                i) ax.plot if 1D plot
                ii) ax.imshow if 2D plot

            Returns
            --------

            if 1D plot -> 'ax'
            if 2D plot -> 'ax' and 'ax.imshow(-)'        
        '''

        regions, regions_shapes = self._finalize_regions()
        values = np.zeros(self.mesh.npoints, dtype=int)
        coord = self.mesh.coordinates(np.arange(self.mesh.npoints, dtype=int))

        for i, n in enumerate(regions_shapes):
            map_ = regions_shapes[n](coord)

            if np.any(values[map_] != 0):
                pos = np.unique(values[map_][values[map_] != 0])
                raise RuntimeError(
                    'Region {0} intersects with {1}'.format(
                        n, np.array([i for i in regions_shapes.keys()])[pos]))
            
            values[map_] += i

        region_plot_fun = plotting.region_plot(
                region=region, grid_step=grid_step, 
                grid_center=grid_center,
                values_coord=coord,
                direction=direction)
        
        names = [i for i in regions_shapes.keys()]

        return plotting.discrete_value_plot(
            region_plot_fun, region.bbox, values, names, **kwargs)
