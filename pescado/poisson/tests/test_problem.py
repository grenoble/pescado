import functools
import numpy as np

from numpy.testing import (
    assert_array_almost_equal, assert_array_equal, assert_allclose)

from pytest import raises

import kwant
from scipy import sparse

from pescado import poisson
from pescado.poisson import problem
from pescado import mesher
from pescado import tools


def test_matrix_slicing_operations():
    ''' Tests :
            Symmetric grouping - 'arrange_matrix_elements'
            Non-symmetric grouping - 'group_matrix'
            Sparse matrix slicing - 'slice_sparse'
    '''

    #####################
    # Generate reference matrix and sparse matrix
    #####################

    np.random.seed(134432)
    # Generate sparse matrix
    shape = [500, 500]

    ref_data = np.random.rand(*shape) * 324.3 + 0.1
    sparse_test = sparse.csc_matrix(ref_data)

    #####################
    # Test : Symmetric grouping - 'arrange_matrix_elements'
    #####################

    test_idx_num = 75
    arr = np.unique(
        np.random.randint(low=0, high=shape[0], size=test_idx_num))
    np.random.shuffle(arr)

    np.random.seed(14353)
    arr2 = np.unique(
        np.random.randint(low=0, high=shape[0], size=test_idx_num))
    np.random.shuffle(arr2)

    group_index = [arr, arr2]

    grouped_ele = problem.arrange_matrix_elements(
        group_index=group_index, matrix=sparse_test)

    for row, row_id in enumerate(group_index):
        for col, col_id in enumerate(group_index):
            check_group(
                ref_row=row_id,
                ref_col=col_id,
                ref_array=ref_data,
                group_info=grouped_ele[row, col])

    #####################
    # Test : Non-symmetric grouping - 'group_matrix'
    #####################

    test_idx_num = 100
    arr = np.unique(
        np.random.randint(low=0, high=shape[0], size=test_idx_num))
    np.random.shuffle(arr)

    np.random.seed(14353)
    test_idx_num = 150
    arr2 = np.unique(
        np.random.randint(low=0, high=shape[0], size=test_idx_num))
    np.random.shuffle(arr2)

    np.random.seed(24234)
    test_idx_num = 150
    arr3 = np.unique(
        np.random.randint(low=0, high=shape[0], size=test_idx_num))
    np.random.shuffle(arr3)

    np.random.seed(43244)
    test_idx_num = 300
    arr4 = np.unique(
        np.random.randint(low=0, high=shape[0], size=test_idx_num))
    np.random.shuffle(arr4)

    matrix_group = np.array(
        [[(arr, arr2), (arr, arr4), (arr3, arr4)],
         [(arr3, arr4), (arr2, arr4), tuple()]], dtype=object)

    grouped_ele = problem.group_matrix(
        matrix_group=matrix_group, matrix=sparse_test)

    for row in range(matrix_group.shape[0]):
        for col in range(matrix_group.shape[1]):
            if matrix_group[row, col]:
                check_group(
                    ref_row=matrix_group[row, col][0],
                    ref_col=matrix_group[row, col][1],
                    ref_array=ref_data,
                    group_info=grouped_ele[row, col])

    #####################
    # Test : Sparse matrix slicing - 'slice_sparse'
    #####################

    test_idx_num = 50
    arr = np.unique(
        np.random.randint(low=0, high=shape[0], size=test_idx_num))
    np.random.shuffle(arr)

    np.random.seed(14353)
    test_idx_num = 10
    arr2 = np.unique(
        np.random.randint(low=0, high=shape[0], size=test_idx_num))
    np.random.shuffle(arr2)

    sparse_section = problem.slice_sparse(
        rows=arr, columns=arr2, matrix=sparse_test).tocoo()

    assert_array_equal(
        sparse_section.todense(),
        ref_data[:, arr2][arr, :],
        err_msg='slice block failed basic regression test')


def check_group(ref_row, ref_col, ref_array, group_info):
    ''' Compares one element of the output of elements() method
    with reference.
    '''

    row_id = np.sort(ref_row)
    col_id = np.sort(ref_col)

    assert np.all(
        row_id == np.unique(group_info[1][0])), (
        ('The wrong elements (row) were selected when testing'
            +' group'))

    assert np.all(
        col_id == np.unique(group_info[1][1])), (
        ('The wrong elements (col) were selected when testing'
            +' group. \n {0}, {1}'.format(
                col_id, np.unique(group_info[1][1]))))

    assert_array_equal(
        group_info[0],
        ref_array[group_info[1]],
        err_msg='Block slicing did not pass test')


def test_dnh_lp():
    ''' Test left and right hand side for the linear problem
    without the flexible sites.
    '''

    pattern_coarse = mesher.patterns.Rectangular.constant(element_size=(50, 50))

    shape_coarse = mesher.shapes.Box(
        lower_left=(-600, -300), size=(1200, 600))

    pp_builder = poisson.ProblemBuilder()

    pp_builder.initialize_mesh(
         simulation_region=shape_coarse, pattern=pattern_coarse)

    mesh = pp_builder.mesh


    capacitance_mat = pp_builder._make_capacitance_matrix(mesh=mesh)

    # Get a randomized capacitance matrix but keep the same
    # neighbours mapping
    np.random.seed(134432)
    capacitance_mat = capacitance_mat.multiply(
        np.random.rand(*capacitance_mat.shape))

    dense_ref_capa_mat = np.array(capacitance_mat.todense())

    shape_dirichlet = mesher.shapes.Box(lower_left=(200, -60), size=(300, 200))

    shape_helmholtz = (
        mesher.shapes.Box(lower_left=(-100, -5), size=(300, 200))
        - shape_dirichlet)

    shape_flexible = (
        mesher.shapes.Box(lower_left=(-500, -5), size=(300, 200))
        - shape_helmholtz
        - shape_dirichlet)

    shape_neuman = (shape_coarse
                    - (shape_dirichlet | shape_flexible | shape_helmholtz))

    regions_index = [
        mesh.inside(shape_dirichlet), mesh.inside(shape_neuman),
        mesh.inside(shape_helmholtz), mesh.inside(shape_flexible)]

    grouped_capacitance = problem.arrange_matrix_elements(
        group_index=regions_index, matrix=capacitance_mat)

    rhs_dnh, lhs_dnh = problem.build_dnh_lp(
        group_index=regions_index,
        grouped_capa=grouped_capacitance,
        shape=capacitance_mat.shape)

    ################################
    # Check the rhs
    ################################

    col = 0
    for row, idx_row in enumerate(regions_index):
        sparse_cut = problem.slice_sparse(
            rows=idx_row, columns=regions_index[col],
            matrix=rhs_dnh)
        assert_array_equal(
            sparse_cut.todense(),
            dense_ref_capa_mat[:, regions_index[col]][idx_row, :],
            err_msg='rhs did not pass column 0 test')

    for i in [1, 2]:
        sparse_cut = problem.slice_sparse(
            rows=regions_index[i],
            columns=regions_index[i], matrix=rhs_dnh)
        assert_array_equal(
            sparse_cut.todense(),
            -1 * np.identity((len(regions_index[i]))),
            err_msg='rhs did not pass column 1 and 2 test')

    for col in [1, 2, 3]:
        for row, idx_row in enumerate(regions_index):
            if (col not in [1, 2]) and (row not in [1, 2]):
                sparse_cut = problem.slice_sparse(
                    rows=idx_row, columns=regions_index[col],matrix=rhs_dnh)

                assert np.sum(sparse_cut.todense() != 0) == 0, (
                    'unexpected non zero elements were found in rhs')

    ################################
    # Check the lhs
    ################################

    for col in [1, 2]:
        for row, idx_row in enumerate(regions_index):
            sparse_cut = problem.slice_sparse(
                rows=idx_row, columns=regions_index[col],
                matrix=lhs_dnh)

        assert_array_equal(
            sparse_cut.todense(),
            -1 * dense_ref_capa_mat[:, regions_index[col]][idx_row, :],
            err_msg='rhs did not pass column 0 test')

    sparse_cut = problem.slice_sparse(
        rows=regions_index[0],
        columns=regions_index[0], matrix=lhs_dnh)
    assert_array_equal(
        sparse_cut.todense(),
        np.identity(len(regions_index[0])),
        err_msg='rhs did not pass column 1 and 2 test')

    for col in [1, 3]:
        for row, idx_row in enumerate(regions_index):
            if (col != 1) and (row != 1):
                sparse_cut = problem.slice_sparse(
                    rows=idx_row, columns=regions_index[col],matrix=lhs_dnh)
                assert np.sum(sparse_cut.todense() != 0) == 0, (
                    'unexpected non zero elements were found in rhs')

    # ########################
    # # Check the flexible LP - Later when doing for LinearProblem.
    # ########################

    flexible_capacitance = functools.reduce(
        lambda x, y: x + y,
        [sparse.csc_matrix(group, capacitance_mat.shape)
         for group in grouped_capacitance[:, 3]])

    shape_flexible_D = (
        shape_flexible
        & mesher.shapes.Box(lower_left=(-600, -5), size=(150, 150)))

    shape_flexible_H = (
        shape_flexible
        & mesher.shapes.Box(lower_left=(-450, 150), size=(150, 150))
        - shape_flexible_D)

    shape_flexible_N = (
        shape_flexible - (shape_flexible_D | shape_flexible_H))

    flex_idx = [
        mesh.inside(shape_flexible_D),
        mesh.inside(shape_flexible_N),
        mesh.inside(shape_flexible_H)]

    matrix_group = np.empty((4, 3), dtype=np.ndarray)
    for row in range(4):
        for col in range(3):
            matrix_group[row, col] = (
                regions_index[row],
                flex_idx[col])

    grouped_flex_capa = problem.arrange_matrix_elements(
        matrix_group=matrix_group, matrix=flexible_capacitance)

    flex_sub_group = tools.SparseVector(
        indices=regions_index[3], values=np.zeros(len(regions_index[3])))
    for num, idx in enumerate(flex_idx):
        flex_sub_group[idx] = np.ones(len(idx)) * (num + 1)

    flex_rhs_dnh, flex_lhs_dnh = problem.build_flexible_lp(
        flex_sub_group=flex_sub_group,
        grouped_flex_capa=grouped_flex_capa,
        shape=flexible_capacitance.shape)

    # ################################
    # # Check the rhs
    # ################################

    col = 0 # All col 0 should match the capa matrix
    for group_idx in matrix_group[:, col]:
        sparse_cut = problem.slice_sparse(
            rows=group_idx[0], columns=group_idx[1],
            matrix=flex_rhs_dnh)

        assert_array_equal(
            sparse_cut.todense(),
            dense_ref_capa_mat[:, group_idx[1]][group_idx[0], :],
            err_msg='rhs did not pass column 0 test')

    for col in [1, 2]:
        row = col + 1
        sparse_cut = problem.slice_sparse(
            rows=matrix_group[row, col][1],
            columns=matrix_group[row, col][1],
            matrix=flex_rhs_dnh)

        assert_array_equal(
            sparse_cut.todense(),
            -1 * np.identity(len(matrix_group[row, col][1])),
            err_msg='rhs did not pass column 1 and 2 test')

    for col in [1, 2]:
        row = 3
        for i in range(3):
            if i != col:
                sparse_cut = problem.slice_sparse(
                    rows=matrix_group[row, i][1],
                    columns=matrix_group[row, col][1],
                    matrix=flex_rhs_dnh)

                assert np.sum(sparse_cut.todense() != 0) == 0, (
                    'Unexpected non zero elements were found in rhs')

            sparse_cut = problem.slice_sparse(
                rows=matrix_group[row, col][0],
                columns=matrix_group[i, col][0],
                matrix=flex_rhs_dnh)

            assert np.sum(sparse_cut.todense() != 0) == 0, (
                'Unexpected non zero elements were found in rhs')

        for row in range(3):
            sparse_cut = problem.slice_sparse(
                rows=matrix_group[row, col][0],
                columns=matrix_group[row, col][1],
                matrix=flex_rhs_dnh)

            assert np.sum(sparse_cut.todense() != 0) == 0, (
                'Unexpected non zero elements were found in rhs')

            sparse_cut = problem.slice_sparse(
                rows=matrix_group[row, col][0],
                columns=matrix_group[row, col][0],
                matrix=flex_rhs_dnh)

            assert np.sum(sparse_cut.todense() != 0) == 0, (
                'Unexpected non zero elements were found in rhs')

    # ################################
    # # Check the lhs
    # ################################

    for col in [1, 2]:
        for group_idx in matrix_group[:, col]:
            sparse_cut = problem.slice_sparse(
                rows=group_idx[0], columns=group_idx[1],
                matrix=flex_lhs_dnh)

            assert_array_equal(
                sparse_cut.todense(),
                -1 * dense_ref_capa_mat[:, group_idx[1]][group_idx[0], :],
                err_msg='lhs did not pass column 1 / 2 test')

    col = 0
    for row in range(4):
        if row == 3:
            sparse_cut = problem.slice_sparse(
                rows=matrix_group[row, col][1],
                columns=matrix_group[row, col][1],
                matrix=flex_lhs_dnh)

            assert_array_equal(
                sparse_cut.todense(),
                1 * np.identity(len(matrix_group[row, col][1])),
                err_msg='lhs did not pass column 1 and 2 test')

        if row != 3:
            sparse_cut = problem.slice_sparse(
                rows=matrix_group[row, col][0],
                columns=matrix_group[row, col][1],
                matrix=flex_lhs_dnh)

            assert np.sum(sparse_cut.todense() != 0) == 0, (
                'Unexpected non zero elements were found in Lhs')


def make_kwant(W, L, L_well):

    t=1.

    lat = kwant.lattice.square(a=1)
    kwant_syst = kwant.Builder()

    def potential(site, pot):
        (x, y) = site.pos
        if (L - L_well) / 2 < x < (L + L_well) / 2:
            return pot
        else:
            return 0

    def onsite(site, pot):
        return 4 * t + potential(site, pot)

    kwant_syst[(lat(x, y)
                for x in range(-L // 2, L // 2)
                for y in range(-W // 2, W // 2))] = onsite
    kwant_syst[lat.neighbors()] = -t

    return kwant_syst.finalized(), lat


def fun_(x):
    return np.abs(x) ** 1.2 * np.sign(x)

def inv_fun_(x):
    return np.abs(x) ** ( 1 / 1.2) * np.sign(x)


def test_double_assign():
    ''' Tests if double assigned points are caught by poisson.Problem;
    double assigned -> one mesh point assigned to two different types

    Also tests pp_builder.delete_region
    '''

    pattern_coarse = mesher.patterns.Rectangular.constant(
        element_size=(30, 30))

    shape_coarse = mesher.shapes.Box(
        lower_left=(-70, -40), size=(140, 60))

    ########################
    # Build the mesh using an instance of ProblemBuilder
    ########################

    pp_builder = poisson.ProblemBuilder()

    pp_builder.initialize_mesh(
         simulation_region=shape_coarse, pattern=pattern_coarse)

    ########################
    # Setting the b.c.s
    ########################
    shape_helmholtz = mesher.shapes.Box(
        lower_left=(-60, -40), size=(140, 60))

    shape_flexible = mesher.shapes.Box(
        lower_left=(-20, -40), size=(80, 80))

    names = ['gate', 'helm',  'mixed']
    pp_builder.set_flexible(region=shape_flexible, setup_name=names[2])
    pp_builder.set_helmholtz(region=shape_helmholtz, setup_name=names[1])

    # Check if the double assignment is caught by poisson.Problem
    with raises(RuntimeError):
        pp_problem = pp_builder.finalized()

    shape_flexible = mesher.shapes.Box(
        lower_left=(-50, -5), size=(200, 10)) - shape_helmholtz

    pp_builder.delete_region(names[2])
    pp_builder.set_flexible(region=shape_flexible, setup_name=names[2])

    pp_problem = pp_builder.finalized()


def test_PP_LP():
    ''' Test poisson.Problem and LinearProblem

    Do it in the same test to save time (create only one mesh)
    '''

    ########################
    # Kwant part
    ########################
    W=6
    L=30
    L_well=6

    kwant_syst, lattice = make_kwant(W=W, L=L, L_well=L_well)

    kwant_pat, kwant_coordinates = poisson.builder.convert_kwant(
        kwant_syst=kwant_syst, kwant_lattice=lattice)

    kwant_coord = np.array(list(kwant_coordinates.values()))
    
    # Tests ext = 3
    kwant_shape_ext = mesher.shapes.Delaunay(coordinates=kwant_coord)

    ext_kwant = 3
    kwant_bd_ext, kwant_bd_in = kwant_pat.boundary(kwant_shape_ext)
    neig_ = kwant_pat(kwant_pat.neighbours(kwant_bd_in, nth=ext_kwant)[0])

    # Tests ext = None
    kwant_shape = mesher.shapes.Delaunay(
        coordinates=neig_[kwant_shape_ext(neig_)])

    ########################
    # Define the patterns and Shapes
    ########################

    pattern_fine = mesher.patterns.Rectangular.constant(
        element_size=(6, 4))

    pattern_continuous = mesher.patterns.Rectangular(
        ticks=(fun_, ) * 2, tag2ticks=(inv_fun_, ) * 2)

    pattern_coarse = mesher.patterns.Rectangular.constant(
        element_size=(30, 30))

    shape_fine = mesher.shapes.Box(
        lower_left=(-30, -20), size=(60, 40))

    shape_coarse = mesher.shapes.Box(
        lower_left=(-150, -150), size=(300, 300))

    shape_continuous = mesher.shapes.Box(
        lower_left=(-50, -40), size=(100, 80))

    ########################
    # Build the mesh using an instance of ProblemBuilder
    ########################

    pp_builder = poisson.ProblemBuilder()

    pp_builder.initialize_mesh(
         simulation_region=shape_coarse, pattern=pattern_coarse)

    pp_builder.refine_mesh(
        region=shape_continuous, pattern=pattern_continuous)

    pp_builder.refine_mesh(
        region=shape_fine, pattern=pattern_fine)

    # Without external boundary
    pp_builder.refine_mesh_kwant(
        region=kwant_shape, kwant_syst=kwant_syst)

    ########################
    # Setting the capacitance matrix
    ########################

    shapes_inst = [shape_continuous, shape_fine, kwant_shape]
    values = [4.32, 4.234, 1.34]

    # The dielectric constant is set sequentially in the builder
    for j, (val, shape_inst) in enumerate(zip(values, shapes_inst)):
        pp_builder.set_relative_permittivity(val, shape_inst)

    ########################
    # Setting the b.c.s
    ########################

    shape_gate = mesher.shapes.Box(
        lower_left=(-50, 35), size=(100, 10))

    shape_ddop = mesher.shapes.Box(lower_left=(-50, 20), size=(100, 10))

    shape_helmholtz = mesher.shapes.Box(
        lower_left=(-20, -5), size=(40, 10))

    shape_flexible = mesher.shapes.Box(
        lower_left=(-50, -5), size=(100, 10)) - shape_helmholtz

    names = ['gate', 'helm',  'mixed', 'dop']
    pp_builder.set_neumann(region=shape_ddop, setup_name=names[3])
    pp_builder.set_metal(region=shape_gate, setup_name=names[0])
    pp_builder.set_flexible(region=shape_flexible, setup_name=names[2])
    pp_builder.set_helmholtz(region=shape_helmholtz, setup_name=names[1])

    pp_problem = pp_builder.finalized()

    ########################################################################
    #########               Test Poisson Problem                   #########
    ########################################################################

    ########################
    # test _arrange_region_index
    ########################

    regions_idx = pp_problem._arrange_region_index()

    assert_array_equal(
        pp_problem.points_inside(region=shape_gate),
        regions_idx[0],
        '_arrange_region_index failed dirichlet')

    assert len(np.setdiff1d(
        pp_problem.points_inside(
            region=shape_ddop
            | (shape_coarse
               - (shape_gate | shape_flexible | shape_helmholtz))),
        regions_idx[1])) == 0, ('_arrange_region_index failed neumann')

    assert len(np.setdiff1d(
        pp_problem.points_inside(region=shape_helmholtz),
        regions_idx[2])) == 0, ('_arrange_region_index failed helmholtz')

    assert len(np.setdiff1d(
        pp_problem.points_inside(region=shape_flexible),
        regions_idx[3])) == 0, ('_arrange_region_index failed flexible')

    pp_regions_idx = [
        pp_problem.dirichlet_indices,
        pp_problem.neumann_indices,
        pp_problem.helmholtz_indices,
        pp_problem.flexible_indices]

    ########################
    # Sanity test
    ########################

    for i, idx in enumerate(regions_idx):
        assert_array_equal(
            idx, pp_regions_idx[i],
            err_msg=(
                'pp_problem.regions index differs from that obtained'
                + ' with _arrange_region_index()'))

    linear_problem_inst = pp_problem._initialize_LP(regions_idx)

    #######################
    # Test lhs and rhs created by _initialize_LP
    #######################

    grouped_capacitance = problem.arrange_matrix_elements(
        group_index=regions_idx,
        matrix=pp_problem.capacitance_matrix)

    rhs_dnh, lhs_dnh = problem.build_dnh_lp(
        group_index=regions_idx,
        grouped_capa=grouped_capacitance,
        shape=pp_problem.capacitance_matrix.shape)

    assert_array_equal(
        rhs_dnh.data,
        pp_problem.linear_problem_inst.rhs_dnh.data,
        '_initialize_LP failed rhs_dnh non zero data')

    assert_array_equal(
        rhs_dnh.nonzero(),
        pp_problem.linear_problem_inst.rhs_dnh.nonzero(),
        '_initialize_LP failed rhs_dnh non zero indices')

    assert_array_equal(
        lhs_dnh.data,
        pp_problem.linear_problem_inst.lhs_dnh.data,
        '_initialize_LP failed lhs_dnh non zero data')

    assert_array_equal(
        lhs_dnh.nonzero(),
        pp_problem.linear_problem_inst.lhs_dnh.nonzero(),
        '_initialize_LP failed lhs_dnh non zero indices')

    #######################
    # test flexible_capacitance created by _initialize_LP
    #######################

    flex_capa = linear_problem_inst.flexible_capacitance

    flex_slice = problem.slice_sparse(
        rows=np.arange(pp_problem.npoints),
        columns=pp_regions_idx[3], matrix=pp_problem.capacitance_matrix)

    ref_flex_slice = problem.slice_sparse(
        rows=np.arange(pp_problem.npoints),
        columns=pp_regions_idx[3], matrix=pp_problem.capacitance_matrix)

    assert_array_equal(
        flex_slice.nonzero(), ref_flex_slice.nonzero(),
        err_msg='Flexible capacitance didn"t pass reference .nonzero() test')

    assert_array_equal(
        flex_slice.data, ref_flex_slice.data,
        err_msg='Flexible capacitance didn"t pass reference .data test')

    ########################
    # test kwant_indices()
    ########################

    kwant_idx = pp_problem.kwant_indices(pp_builder.kwant_tags)
    assert_array_equal(
        pp_problem.coordinates[kwant_idx],
        pp_builder.kwant_coordinates,
        err_msg='kwant_indices failed regression test')

    ########################
    # test kwant_coordinates()
    ########################

    kwant_coord = pp_problem.kwant_coordinates(pp_builder.kwant_tags)
    assert_array_equal(
        kwant_coord,
        pp_builder.kwant_coordinates,
        err_msg='kwant_coordinates failed regression test')

    ########################
    # test sparse_vector() and read_sparse_vector()
    ########################
    idx = np.sort(np.unique(
        np.random.randint(
            low=0, high=pp_problem.npoints, size=1000)))
    val = np.random.rand(len(idx)) * 112

    sv_index = tools.SparseVector(values=val, indices=idx)
    assert_array_equal(
        sv_index[idx], val,
        err_msg='poisson.Problem sparse_vector failed val/idx test')

    sv_index_den = tools.SparseVector(
        values=val * pp_problem.volume[idx], indices=idx)
    
    assert_array_equal(
        sv_index_den[idx], val * pp_problem.volume[idx],
        err_msg='poisson.Problem sparse_vector failed val(dens)/idx test')

    sv_shape = pp_problem.sparse_vector(
        val=3, region=(shape_ddop | shape_gate))

    idx = pp_problem.points_inside(region=(shape_ddop | shape_gate))
    assert_array_equal(
        sv_shape[idx], 3 * np.ones(len(idx)),
        err_msg='poisson.Problem sparse_vector failed val_const/shape test')

    res_val, res_idx = pp_problem.read_sparse_vector(
        sparse_vector_inst=sv_shape, region=(shape_ddop | shape_gate),
        return_indices=True)

    assert_array_equal(
        res_val, 3 * np.ones(len(idx)),
        err_msg='poisson.Problem read_sparse_vector failed test')

    assert_array_equal(
        res_idx, idx,
        err_msg='poisson.Problem read_sparse_vector failed test')

    fun = lambda x_vect: np.array(
        [np.sum([x_ * 1 for x_ in x]) for x in x_vect])
    sv_shape_fun = pp_problem.sparse_vector(
        val=fun, region=(shape_ddop | shape_gate))

    idx = pp_problem.points_inside(region=(shape_ddop | shape_gate))
    assert_array_equal(
        sv_shape_fun[idx],
        fun(pp_problem.coordinates[idx]),
        err_msg='poisson.Problem sparse_vector failed val_fun/shape test')

    res_val, res_idx = pp_problem.read_sparse_vector(
        sparse_vector_inst=sv_shape_fun, region=(shape_ddop | shape_gate),
        return_indices=True)

    assert_array_equal(
        res_val, fun(pp_problem.coordinates[idx]),
        err_msg='poisson.Problem read_sparse_vector failed test')

    assert_array_equal(
        res_idx, idx,
        err_msg='poisson.Problem read_sparse_vector failed test')

    sv_name = pp_problem.sparse_vector(val=3, name='dop')

    idx = pp_problem.points(name='dop')
    assert_array_equal(
        sv_name[idx], 3 * np.ones(len(idx)),
        err_msg='poisson.Problem sparse_vector failed val_con/name test')

    res_val, res_idx = pp_problem.read_sparse_vector(
        sparse_vector_inst=sv_name, name='dop',
        return_indices=True)

    assert_array_equal(
        res_val, 3 * np.ones(len(idx)),
        err_msg='poisson.Problem read_sparse_vector failed test')

    assert_array_equal(
        res_idx, idx,
        err_msg='poisson.Problem read_sparse_vector failed test')

    ########################
    # test constant_charge_vector()
    ########################
    idx = np.sort(np.unique(
        np.random.randint(
            low=0, high=pp_problem.npoints, size=1000)))
    val = 3.
    sv_integrated_idx = pp_problem.constant_charge_vector(
        integrated_value=val, indices=idx)

    assert_allclose(
        np.sum(sv_integrated_idx[idx]), val,
        err_msg='poisson.Problem constant_charge_vector failed val/idx test')

    idx = pp_problem.points_inside(region=(shape_ddop | shape_gate))

    sv_integrated_shape = pp_problem.constant_charge_vector(
        integrated_value=val, region=(shape_ddop | shape_gate))

    assert_array_almost_equal(
        np.sum(sv_integrated_shape[idx]), val,
        err_msg='poisson.Problem constant_charge_vector failed val/shape test')

    res_val, res_idx = pp_problem.read_sparse_vector(
        sparse_vector_inst=sv_integrated_shape,
        region=(shape_ddop | shape_gate),
        return_indices=True)

    assert_array_almost_equal(
        np.sum(sv_integrated_shape[idx]), val,
        err_msg='poisson.Problem read_sparse_vector failed test')

    assert_array_equal(
        res_idx, idx,
        err_msg='poisson.Problem read_sparse_vector failed test')

    idx = pp_problem.points(name='dop')

    sv_integrated_name = pp_problem.constant_charge_vector(
        integrated_value=val, name='dop')

    assert_array_almost_equal(
        np.sum(sv_integrated_name[idx]), val,
        err_msg='poisson.Problem constant_charge_vector failed val/idx test')

    res_val, res_idx = pp_problem.read_sparse_vector(
        sparse_vector_inst=sv_integrated_name, name='dop',
        return_indices=True)

    assert_array_almost_equal(
        np.sum(sv_integrated_name[idx]), val,
        err_msg='poisson.Problem read_sparse_vector failed test')

    assert_array_equal(
        res_idx, idx,
        err_msg='poisson.Problem read_sparse_vector failed test')

    ########################
    # assign_flexible() test in linear_problem
    # solve() test in linear_problem
    # freeze() test in linear_problem
    # reset() just resets the linear problem, tested
    # when testing the _initialize_LP
    # sub_region_idex tested when testing mesher
    # points_index tested when testing mesher
    ########################

    ########################################################################
    #########               Test Linear Problem                    #########
    ########################################################################

    linear_problem = pp_problem.linear_problem_inst

    flex_idx = linear_problem.regions_index[3]
    dirichlet = flex_idx[np.arange(0, len(flex_idx) // 3, dtype=int)]
    neumann = flex_idx[
        np.arange(len(flex_idx) // 3, (2 * len(flex_idx)) // 3, dtype=int)]
    helmholtz = flex_idx[
        np.arange((2 * len(flex_idx)) // 3, len(flex_idx), dtype=int)]

    ########################
    #   test assing_flexible()
    ########################

    linear_problem.assign_flexible(
        neumann_index=neumann, dirichlet_index=dirichlet,
        helmholtz_index=helmholtz)

    for num, idx in enumerate([dirichlet, neumann, helmholtz]):
        assert_array_equal(
            idx, linear_problem.flexible_sub_type.indices[
                linear_problem.flexible_sub_type.values == (num + 1)],
            err_msg='assign_flexible failed basic regression test')

    linear_problem.reset_flexible_configuration()
    assert np.all(linear_problem.flexible_sub_type.values == 0)

    linear_problem.assign_flexible(neumann_index=neumann)

    assert_array_equal(
        neumann, linear_problem.flexible_sub_type.indices[
                linear_problem.flexible_sub_type.values == 2],
        err_msg='assign_flexible failed basic regression test')
    assert_array_equal(
        np.concatenate([dirichlet, helmholtz]),
        linear_problem.flexible_sub_type.indices[
                linear_problem.flexible_sub_type.values == 0],
        err_msg='assign_flexible failed basic regression test')

    with raises(
        ValueError,
        match=('At least two parameters among neumann_index, '
               + 'dirichlet_index or helmholtz_index must be '
               + 'given if "contain_all" is True')):
        linear_problem.assign_flexible(
            neumann_index=neumann, contain_all=True)

    linear_problem.assign_flexible(
            neumann_index=neumann,  dirichlet_index=dirichlet, contain_all=True)

    for num, idx in enumerate([dirichlet, neumann, helmholtz]):
        assert_array_equal(
            idx, linear_problem.flexible_sub_type.indices[
                linear_problem.flexible_sub_type.values == num + 1],
            err_msg='assign_flexible failed basic regression test')

    ########################
    #   test build
    ########################

    linear_problem.assign_flexible(
        neumann_index=neumann, dirichlet_index=dirichlet,
        helmholtz_index=helmholtz, contain_all=True)

    flex_idx_config = [dirichlet, neumann, helmholtz]

    matrix_group = np.empty((4, 3), dtype=np.ndarray)
    for row in range(4):
        for col in range(3):
            matrix_group[row, col] = (
                linear_problem.regions_index[row],
                flex_idx_config[col])

    grouped_flex_capa = problem.arrange_matrix_elements(
        matrix_group=matrix_group, matrix=linear_problem.flexible_capacitance)

    flex_sub_group = tools.SparseVector(
        indices=flex_idx, values=np.zeros(len(flex_idx)))

    for num, idx in enumerate(flex_idx_config):
        flex_sub_group[idx] = np.ones(len(idx)) * (num + 1)

    flex_rhs, flex_lhs = problem.build_flexible_lp(
        flex_sub_group=flex_sub_group,
        grouped_flex_capa=grouped_flex_capa,
        shape=linear_problem.flexible_capacitance.shape)

    ref_lhs = linear_problem.lhs_dnh + flex_lhs
    ref_rhs = linear_problem.rhs_dnh + flex_rhs

    linear_problem.build()

    assert_array_equal(
        ref_lhs.todense(),
        linear_problem.lhs.todense(),
        err_msg='lhs in linear problem failed test')

    assert_array_equal(
        ref_rhs.todense(),
        linear_problem.rhs.todense(),
        err_msg='rhs in linear problem failed test')

    ########################
    #   test _prepare_input
    ########################
    v_idx = np.concatenate(
        [linear_problem.regions_index[0], flex_idx_config[0]])
    voltage = tools.SparseVector(indices=v_idx, values=np.arange(0, len(v_idx)))

    c_idx = np.concatenate(
        [linear_problem.regions_index[1], flex_idx_config[1],
         linear_problem.regions_index[2], flex_idx_config[2]])
    charge = tools.SparseVector(indices=c_idx, values=np.arange(0, len(c_idx)))

    input_ = linear_problem._prepare_input(voltage=voltage, charge=charge)
    print(v_idx)
    assert_array_equal(
        input_.todense()[v_idx, 0], voltage[v_idx][:, None],
        err_msg='_prepare_input() failed voltage test')
    assert_array_equal(
        input_.todense()[c_idx, 0], charge[c_idx][:, None],
        err_msg='_prepare_input() failed charge test')

    ########################
    #   test _read_output
    ########################
    c_idx = np.concatenate(
        [linear_problem.regions_index[0], flex_idx_config[0]])
    charge = tools.SparseVector(indices=c_idx, values=np.arange(0, len(c_idx)))

    v_idx = np.concatenate(
        [linear_problem.regions_index[1], flex_idx_config[1],
         linear_problem.regions_index[2], flex_idx_config[2]])
    voltage = tools.SparseVector(indices=v_idx, values=np.arange(0, len(v_idx)))

    test_output = functools.reduce(
        lambda x, y: x + y,
        [sparse.csr_matrix(
            (val, (ind, np.zeros(len(ind)))), input_.shape)
        for val, ind in zip(
            [voltage.values, charge.values],
            [voltage.indices, charge.indices])])

    out_voltage, out_charge = linear_problem._read_output(test_output.todense())

    assert_array_equal(
        out_voltage.values, voltage.values[:, None],
        err_msg='_read_output() failed voltage data test')
    assert_array_equal(
        out_voltage.indices, voltage.indices,
        err_msg='_read_output() failed voltage idx test')

    assert_array_equal(
        out_charge.values, charge.values[:, None],
        err_msg='_read_output() failed charge data test')
    assert_array_equal(
        out_charge.indices, charge.indices,
        err_msg='_read_output() failed charge idx test')

    ########################
    #   test _prepare_helmholtz
    ########################
    h_idx = np.concatenate(
        [linear_problem.regions_index[2], flex_idx_config[2]])
    helmholtz = tools.SparseVector(
        indices=h_idx, values=np.arange(1, len(h_idx) + 1))

    test_helm = linear_problem._prepare_helmholtz(
        helmholtz_density=helmholtz)

    assert_array_equal(
        test_helm.todense()[h_idx, h_idx],
        helmholtz[h_idx][None, :],
        err_msg='_prepare_helmholtz() failed helmhotlz density data test')

    non_zero_row, non_zero_col = test_helm.nonzero()

    print(np.setdiff1d(non_zero_row, h_idx))
    print(np.setdiff1d(h_idx, non_zero_row))
    print(np.setdiff1d(h_idx, non_zero_col))
    print(np.setdiff1d(non_zero_col, h_idx))
    # assert len(np.setdiff1d(non_zero_row, h_idx)) == 0, (
    #     '_prepare_helmholtz() failed helmhotlz density idx test')
    # assert len(np.setdiff1d(h_idx, non_zero_row)) == 0, (
    #     '_prepare_helmholtz() failed helmhotlz density idx test')
    # assert len(np.setdiff1d(non_zero_col, h_idx)) == 0, (
    #     '_prepare_helmholtz() failed helmhotlz density idx test')
    # assert len(np.setdiff1d(h_idx, non_zero_col)) == 0, (
    #     '_prepare_helmholtz() failed helmhotlz density idx test')


def test_save_and_load():
    ''' Tests Problem.save and Problem.load
    '''

    pb = poisson.ProblemBuilder()

    # Make the 2D mesh
    coarse = mesher.patterns.Rectangular.constant(element_size=(47.5,) * 2)
    sim_region = mesher.shapes.Box(lower_left=(-500, -500), size=(1001, 1001))

    pb.initialize_mesh(simulation_region=sim_region, pattern=coarse)

    average = mesher.patterns.Rectangular.constant(element_size=(25, 25))
    device_region = mesher.shapes.Box(lower_left=(-500, -100), size=(1000, 300))

    pb.refine_mesh(region=device_region, pattern=average)

    ## Set the dielectric permittivity

    # Dielectric
    dielectric = mesher.shapes.Box(lower_left=(-500, -500), size=(1000, 700))

    gas = mesher.shapes.Box(lower_left=(-500, -20), size=(1000, 40))

    pb.set_relative_permittivity(val=12, region=dielectric)

    # Gate
    gate = mesher.shapes.Box(lower_left=(-500, 200), size=(1000, 30))

    pb.set_metal(region=gate, setup_name='gate')

    bulk_gas = mesher.shapes.Box(lower_left=(-350, -20), size=(700, 40))

    pb.set_helmholtz(region=gas - bulk_gas, setup_name='filled_gas')
    pb.set_flexible(region=bulk_gas, setup_name='bulk_gas')

    pd = pb.finalized()

    pd.save(name='test_SL_problem')

    pd_loaded = problem.Problem.load(name='test_SL_problem')

    test_keys = [
        'capacitance_matrix', 'coordinates', 
        'volume','regions','regions_shapes','_kwant_indices']

    for key in test_keys:
        loaded = pd_loaded.__dict__[key]
        original = pd.__dict__[key]
        
        if key == 'capacitance_matrix':
            loaded = np.concatenate(
                (loaded.data, loaded.indices, loaded.indptr))
            original = np.concatenate(
                (original.data, original.indices, original.indptr))
        
        if key == 'regions_shapes':
            
            assert loaded.keys() == original.keys()
            
            loaded_coord = list()
            for sh in loaded.values():
                loaded_coord.append(
                    pd_loaded.__dict__['coordinates'][
                        sh(pd_loaded.__dict__['coordinates'])])
            
            original_coord = list()
            for sh in original.values():
                original_coord.append(
                    pd.__dict__['coordinates'][
                        sh(pd.__dict__['coordinates'])])
            
            loaded = np.concatenate(loaded_coord)
            original = np.concatenate(original_coord)
        if key == 'volume':
            loaded = loaded.values
            original = original.values
        
        assert_array_equal(loaded, original)

########################
# solve is tested in "test_physics"
########################

########################
# sovle_scipy is tested in "test_physics"
########################
