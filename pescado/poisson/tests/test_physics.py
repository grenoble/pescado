import sys, os

import numpy as np
from numpy.testing import assert_allclose, assert_almost_equal
from scipy import constants
import pickle

from pescado import mesher, poisson, tools
import matplotlib.pyplot as plt

from pytest import mark

file_path = (os.path.dirname(os.path.abspath(poisson.__file__))
             + '/tests/')


def compare_sv(reference, result, name, err_msg=''):
    ''' Compares two sparse vectors using assert_almost_equal
    '''

    assert_almost_equal(
        reference.indices, result.indices,
        err_msg=('Failed {0} indices compairison. {1}'.format(
            name, err_msg)))

    assert_almost_equal(
        reference.values, result.values,
        err_msg=('Failed {0} values compairison. {1}'.format(
            name, err_msg)))

def anlyt_volt_pcapa(
    V_up, V_down, d1, d2, d3,
    eps1=None, eps2=None, eps3=None, dop=None, helm_dens=None):
    ''' Analytical solution for the voltage profile in a parallel capacitance

    Parameters
    -----------
        V_up, V_down: float (in Si)
            Voltage at the gates

        d1; d2; d3: float (in Si)

        eps1; eps2; eps3: float
            Relative permittivity

        dop: float (in Si)
            3D dopant density

        helm_dens: float (in Si)
            3D helmholtz density

    Returns
    -------
        Function taking coordinate along z as input and returning
        the voltage value.

            Parameter
            ----------

                coord : np.array of floats with shape (m,)

            Returns
            ----------

                volt: np.array of floats with shape (m,)

    Solves

        ------------------------ V_up, d3
                eps3, rho=0
        ------------------------  d2
        eps2, rho=dop, helm_dens
        ------------------------  d1
                eps1, rho=0
        ------------------------ V_down, 0
    '''

    V_diff = V_up - V_down

    if ((eps1 is None) and (eps2 is None) and (eps3 is None)
        and (dop is None)):
        def volt(coord):
            return (
                V_diff * (coord / d3)
                 + (V_down * np.ones(len(coord))))
    elif (eps1 is not None) and (eps2 is  not None) and (eps3 is not None):

        eps1 = eps1 * constants.epsilon_0
        eps2 = eps2 * constants.epsilon_0
        eps3 = eps3 * constants.epsilon_0

        def volt(coord):
            volts = np.zeros(len(coord))

            reg_1 = np.arange(len(coord))[coord <= d1]
            reg_2 = np.arange(len(coord))[(coord > d1) * (coord <= d2)]
            reg_3 = np.arange(len(coord))[(coord > d2) * (coord <= d3)]

            A = (d1 * eps2 * eps3
                 + (d2 - d1) * eps3 * eps1
                 + (d3 - d2) * eps2 * eps1)

            c_1_I = V_diff * (eps2 * eps3) / A
            volts[reg_1] = (c_1_I * coord[reg_1]
                            + V_down * np.ones(len(reg_1)))

            c_1_II = V_diff * (eps1 * eps3) / A
            c_2_II = (
                V_down + V_diff * (d1 / A) * (eps2 * eps3  - eps1 * eps3))

            volts[reg_2] = (c_1_II * coord[reg_2]
                            + c_2_II * np.ones(len(reg_2)))

            c_1_III = V_diff * (eps1 * eps2) / A
            c_2_III = V_up - eps1 * eps2 * d3 * V_diff / A
            volts[reg_3] = (c_1_III * coord[reg_3]
                            + c_2_III * np.ones(len(reg_3)))

            return volts

    elif (eps1 is not None) and (dop is not None) and (helm_dens is None):

        eps1 = eps1 * constants.epsilon_0
        dop = dop * constants.elementary_charge

        def volt(coord):
            volts = np.zeros(len(coord))

            reg_1 = np.arange(len(coord))[coord <= d1]
            reg_2 = np.arange(len(coord))[(coord > d1) * (coord <= d2)]
            reg_3 = np.arange(len(coord))[(coord > d2) * (coord <= d3)]

            A = -dop / (2 * eps1)
            B = (V_up - V_down) / d3

            C_1_I = (A * (d2 / d3) * (d2 - 2 * d3)
                     - A * (d1 / d3) * (d1 - 2 * d3)
                     + B)

            C_2_I = V_down * np.ones(len(reg_1))

            C_1_II = (A / d3) * (d2 ** 2 - d1 ** 2 - 2 * d2 * d3) + B
            C_2_II = (A * (d1 ** 2) + V_down) * np.ones(len(reg_2))

            C_1_III = (A / d3) * (d2 ** 2 - d1 ** 2) + B
            C_2_III = (-A * (d2 ** 2 - d1 ** 2)
                       + V_down) * np.ones(len(reg_3))

            volts[reg_1] = C_1_I * coord[reg_1] + C_2_I
            volts[reg_2] = (A * (coord[reg_2] ** 2)
                            + C_1_II * coord[reg_2] + C_2_II)
            volts[reg_3] = C_1_III * coord[reg_3] + C_2_III

            return volts
    elif (eps1 is not None) and (dop is not None) and (helm_dens is not None):

        eps1 = eps1 * constants.epsilon_0
        dop = dop * constants.elementary_charge
        helm_dens = helm_dens * constants.elementary_charge

        def volt(coord):

            volts = np.zeros(len(coord))

            reg_1 = np.arange(len(coord))[coord <= d1]
            reg_2 = np.arange(len(coord))[(coord > d1) * (coord <= d2)]
            reg_3 = np.arange(len(coord))[(coord > d2) * (coord <= d3)]

            dop_fact = dop / helm_dens

            V_up_p = V_up + dop_fact
            V_down_p = V_down + dop_fact

            alpha = d2 - d3
            b = 1J * np.sqrt(np.abs(helm_dens) / eps1)

            e_d2_p = np.exp(b * d2)
            e_d2_n = np.exp(-1 * b * d2)

            e_d1_p = np.exp(b * d1)
            e_d1_n = np.exp(-1 * b * d1)

            A = (e_d2_p
                 * (np.exp(-2 * d1 * b) * (b * d1 + 1) * (b * alpha - 1)
                    / (b * d1 - 1)
                    - np.exp(-2 * d2 * b) * (b * alpha + 1)))

            fact = (e_d1_n * e_d2_p / (d1 * b - 1)) * (b * alpha - 1)
            C_2 = (1 / A) * (fact * V_down_p - V_up_p)
            C_1 = (C_2 * np.exp(-2 * d1 * b) * (b * d1 + 1)
                   - (e_d1_n * V_down_p)) / (b * d1 - 1)

            C_1_III = (C_1 * e_d2_p + C_2 * e_d2_n - V_up_p) / alpha
            C_2_III = (-d3 * C_1_III + V_up) * np.ones(len(reg_3))

            C_1_I = (1 / d1) * (C_1 * e_d1_p + C_2 * e_d1_n - V_down_p)
            C_2_I = V_down * np.ones(len(reg_1))

            volts[reg_1] = C_1_I * coord[reg_1] + C_2_I
            volts[reg_2] = (C_1 * np.exp(b * coord[reg_2])
                            + C_2 * np.exp(-1 * b * coord[reg_2])
                            - dop_fact)
            volts[reg_3] = C_1_III * coord[reg_3] + C_2_III

            return volts

    else:
        raise NotImplementedError()

    return volt

def parallel_capa_solve(
    pp_builder, shapes_list, shapes_gates,
    V_up, V_down, eps1, eps2=None, eps3=None, dop=None, 
    test_factorize=False):

    if eps2 is None:
        eps2 = eps1
    if eps3 is None:
        eps3 = eps1

    gate_up = shapes_gates[0]
    gate_down = shapes_gates[1]

    rel_die_list = [1, eps1, eps2, eps3]

    for rel_die, region in zip(rel_die_list, shapes_list):
        pp_builder.set_relative_permittivity(
            val=rel_die, region=region)

    # The regions to dirichlet (otherwise default Neumann)
    pp_builder.set_metal(
        region=(gate_up | gate_down), setup_name='gate')

    pp_problem = pp_builder.finalized()

    np.save('capa_test', pp_problem.capacitance_matrix.todense())

    sv_gate_outside = pp_problem.sparse_vector(
        val=V_up, region=gate_up)
    sv_gate_inside = pp_problem.sparse_vector(
        val=V_down, region=gate_down)

    sv_voltage = tools.SparseVector(indices=[], values=[])
    for sv in [sv_gate_outside, sv_gate_inside]:
        sv_voltage.extend(sv)

    sv_dopants = None
    if dop is not None:
        sv_dopants = pp_problem.sparse_vector(
            val=dop * 1e-9, region=shapes_list[2],
            density_to_charge=True)

    voltage, charge = pp_problem.solve(
        voltage=sv_voltage, charge=sv_dopants, method='mumps')

    if test_factorize:
        
        voltage_fact, charge_fact = pp_problem.solve(
            voltage=sv_voltage, charge=sv_dopants, 
            method='mumps', factorize=False)

        assert_allclose(
                voltage.values, voltage_fact.values,
                err_msg='Failed factorize test')

        assert_allclose(
                charge.values, charge_fact.values,
                err_msg='Failed factorize test')
        
    return pp_problem, voltage, charge


def parallel_capa_cmp_analytical(
    pp_problem, voltage_num, analytical_fun, mesh_idx,
    plot=False, tolerance=None):

    coordinates = pp_problem.coordinates[mesh_idx]
    analt_ref = analytical_fun(coordinates[:, 0] * 1e-9)

    if plot:
        plt.plot(coordinates[:], voltage_num, '.')
        plt.plot(coordinates[:], analt_ref, '-')
        plt.show()

    err, std = compare_reference(
        reference=analt_ref, numerical=voltage_num, tolerance=tolerance)

    return err, std


def parallel_capa_test(
    dim=None, system_size=(0, ), plot=False,
    element_size=None, tolerance=None,
    return_error=False, return_val=False):
    ''' Parallel capacitance test with 4 configurations.

    Notes
    -----
    Tests:

        Parallel capacitance with 3 different dielectrics
        Parallel capacitance with constant dielectric
        Parallel capacitance with dopant layer where the
        density profile is fixed.
        Parallel capacitance helmholtz.

        Parallel capacitance with dopant layer were
        the voltage profile is fixed.
    '''

    assert len(element_size) == dim

    V_up = -2
    V_down = 4

    eps1 = 15
    eps2 = 5
    eps3 = 9

    epgate=1
    ep1 = 5
    ep2 = 5
    ep3 = 5

    origin = -1 * epgate
    d0 = origin + epgate
    d1 = d0 + ep1
    d2 = d1 + ep2
    d3 = d2 + ep3

    if dim == 1:
        system_size = tuple()

    if return_error:
        error_dict = {
            '3_die':None,
            'constant_die':None,
            'dopant_layer':None,
            'helm':None,
            'dop_regression':None}

    if return_val:
        val_dict = {
            '3_die':None,
            'constant_die':None,
            'dopant_layer':None,
            'helm':None,
            'dop_regression':None}

    border = tuple([-1 * s / 2 for s in system_size])

    dop = 1e9
    helm_dens = 1e9

    anlytic_volt_1 = anlyt_volt_pcapa(
        V_up=V_up, V_down=V_down,
        d1=d1 * 1e-9, d2=d2 * 1e-9, d3=d3 * 1e-9,
        eps1=eps1, eps2=eps2, eps3=eps3)

    anlytic_volt_2 = anlyt_volt_pcapa(
        V_up=V_up, V_down=V_down,
        d1=d1 * 1e-9, d2=d2 * 1e-9, d3=d3 * 1e-9)

    anlytic_volt_3 = anlyt_volt_pcapa(
        V_up=V_up, V_down=V_down,
        d1=d1 * 1e-9, d2=d2 * 1e-9, d3=d3 * 1e-9,
        eps1=eps1, dop=dop * (1e9 ** 2))

    anlytic_volt_4 = anlyt_volt_pcapa(
        V_up=V_up, V_down=V_down,
        d1=d1 * 1e-9, d2=d2 * 1e-9, d3=d3 * 1e-9,
        eps1=eps1, dop=dop * (1e9 ** 2), helm_dens=helm_dens * (1e9 ** 2))
    ################################################################
    # Define the mesh elements
    ################################################################

    pattern_fine = mesher.patterns.Rectangular.constant(
        element_size=element_size)

    shape_all = mesher.shapes.Box(
        lower_left=(origin,) + border,
        size=(ep1 + ep2 + ep3 + (epgate * 2),) + system_size)

    pp_builder = poisson.ProblemBuilder()
    pp_builder.initialize_mesh(simulation_region=shape_all, pattern=pattern_fine)

    # Defining the overall system

    gate_up = mesher.shapes.Box(
        lower_left=(d3,) + border,
        size=(epgate,) + system_size)
    gate_down = mesher.shapes.Box(
        lower_left=(origin, ) + border,
        size=(epgate, ) + system_size)

    dielectric = (
        mesher.shapes.Box(
            lower_left=(d0, ) + border,
            size=(ep1, ) + system_size)
                  - gate_down)

    dielectric2 = (mesher.shapes.Box(
        lower_left=(d1, ) + border,
        size=(ep2, ) + system_size)
                   - dielectric)

    dielectric3 = (mesher.shapes.Box(
        lower_left=(d2 , ) + border,
        size=(ep3, ) + system_size)
                   - (dielectric | dielectric2 | gate_up))

    air = (shape_all - (gate_up | gate_down | dielectric | dielectric2))

    ################################################################
    # Varying dielectric constant test
    ################################################################
    shapes_list = [
        air, dielectric, dielectric2, dielectric3]
    shapes_gates = [gate_up, gate_down]

    pp_problem, voltage, charge = parallel_capa_solve(
        pp_builder=pp_builder, shapes_list=shapes_list,
        shapes_gates=shapes_gates, V_up=V_up, 
        V_down=V_down, eps1=eps1, eps2=eps2, eps3=eps3,
        test_factorize=True)

    voltage_die, idx_die = pp_problem.read_sparse_vector(
        sparse_vector_inst=voltage,
        region=(dielectric | dielectric2 | dielectric3),
        return_indices=True)

    err, std = parallel_capa_cmp_analytical(
        pp_problem, voltage_die, anlytic_volt_1, idx_die, plot=plot,
        tolerance=tolerance)

    if return_error:
        error_dict['3_die'] = (err, std)

    if return_val:
        val_dict['3_die'] = (
            voltage_die, pp_problem.coordinates[idx_die])

    ################################################################
    # Constant dielectric constant test
    ################################################################

    pp_builder.reset_regions()

    shapes_list = [air, dielectric, dielectric2, dielectric3]
    shapes_gates = [gate_up, gate_down]

    pp_problem, voltage, charge = parallel_capa_solve(
        pp_builder=pp_builder, shapes_list=shapes_list,
        shapes_gates=shapes_gates, V_up=V_up, V_down=V_down, eps1=eps1,
        test_factorize=True)

    voltage_die, idx_die = pp_problem.read_sparse_vector(
        sparse_vector_inst=voltage,
        region=(dielectric | dielectric2 | dielectric3),
        return_indices=True)

    err, std = parallel_capa_cmp_analytical(
        pp_problem, voltage_die, anlytic_volt_2, idx_die, plot=plot,
        tolerance=tolerance)

    if return_error:
        error_dict['constant_die'] = (err, std)

    if return_val:
        val_dict['constant_die'] = (
            voltage_die, pp_problem.coordinates[idx_die])

    ################################################################
    # flexible Dirichlet sites test
    ################################################################

    pp_builder.reset_regions()

    rel_die_list = [1, eps1, eps1, eps1]

    for rel_die, region in zip(rel_die_list, shapes_list):
        pp_builder.set_relative_permittivity(
            val=rel_die, region=region)

    for rel_die, region in zip([1e8, 1e8], shapes_gates):
        pp_builder.set_relative_permittivity(
            val=rel_die, region=region)
        
    gate_up = shapes_gates[0]
    gate_down = shapes_gates[1]

    # The regions to dirichlet (otherwise default Neumann)
    pp_builder.set_flexible(
        region=(gate_up | gate_down), setup_name='gate')

    pp_problem = pp_builder.finalized()

    sv_gate_outside = pp_problem.sparse_vector(
        val=V_up, region=gate_up)
    sv_gate_inside = pp_problem.sparse_vector(
        val=V_down, region=gate_down)

    sv_voltage = tools.SparseVector(indices=[], values=[])
    for sv in [sv_gate_outside, sv_gate_inside]:
        sv_voltage.extend(sv)

    pp_problem.assign_flexible(
        dirichlet_index=pp_problem.points(name='gate'))
    pp_problem.freeze()

    voltage_flexible, charge_flexible = pp_problem.solve(
        voltage=sv_voltage, method='mumps')

    compare_sv(reference=voltage, result=voltage_flexible,
               name='voltage', err_msg='flexible Dirichlet test')
    compare_sv(reference=charge, result=charge_flexible,
               name='charge', err_msg='flexible Dirichlet test')

    ################################################################
    # With dopants + helmholtz test - Fixed dopant profile
    ################################################################

    rel_die_list = [1, eps1, eps1, eps1]
    shapes_list = [
        air, dielectric, dielectric2, dielectric3]

    for rel_die, region in zip(rel_die_list, shapes_list):
        pp_builder.set_relative_permittivity(
            val=rel_die, region=region)

    pp_builder.reset_regions()

    pp_builder.set_metal(
        region=(gate_up | gate_down), setup_name='gate')
    pp_builder.set_helmholtz(region=dielectric2, setup_name='helm')

    pp_problem = pp_builder.finalized()

    sv_helm = pp_problem.sparse_vector(
        val=helm_dens * 1e-9, region=dielectric2, density_to_charge=True)
    sv_charge_dens = pp_problem.sparse_vector(
        val=dop * 1e-9, region=dielectric2, density_to_charge=True)

    sv_gate_outside = pp_problem.sparse_vector(val=V_up, region=gate_up)
    sv_gate_inside = pp_problem.sparse_vector(
        val=V_down, region=gate_down)

    sv_voltage = tools.SparseVector(indices=[], values=[])
    for sv in [sv_gate_outside, sv_gate_inside]:
        sv_voltage.extend(sv)

    voltage, charge = pp_problem.solve(
        voltage=sv_voltage, charge=sv_charge_dens,
        helmholtz_density=sv_helm, method='mumps')

    voltage_die, idx_die = pp_problem.read_sparse_vector(
        sparse_vector_inst=voltage,
        region=(dielectric | dielectric2 | dielectric3),
        return_indices=True)

    err, std = parallel_capa_cmp_analytical(
        pp_problem, voltage_die, anlytic_volt_4, idx_die, plot=plot,
        tolerance=tolerance)

    if return_error:
        error_dict['helm'] = (err, std)
    if return_val:
        val_dict['helm'] = (
            voltage_die, pp_problem.coordinates[idx_die])

    ################################################################
    # flexible Dirichlet / Neumann / Helmholtz sites test
    ################################################################

    pp_builder.reset_regions()

    rel_die_list = [1, eps1, eps1, eps1]

    for rel_die, region in zip(rel_die_list, shapes_list):
        pp_builder.set_relative_permittivity(
            val=rel_die, region=region)

    for rel_die, region in zip([1e8, 1e8], shapes_gates):
        pp_builder.set_relative_permittivity(
            val=rel_die, region=region)
        
    gate_up = shapes_gates[0]
    gate_down = shapes_gates[1]

    # The regions to dirichlet (otherwise default Neumann)
    pp_builder.set_flexible(
        region=(gate_up | gate_down), setup_name='gate')

    pp_builder.set_flexible(
        region=dielectric2, setup_name='dielectric2')

    pp_builder.set_flexible(
        region=(dielectric
               - (gate_up | gate_down)),
        setup_name='dielectric')

    pp_problem = pp_builder.finalized()

    pp_problem.assign_flexible(
        neumann_index=pp_problem.points(name='dielectric'),
        dirichlet_index=pp_problem.points(name='gate'),
        helmholtz_index=pp_problem.points(name='dielectric2'))
    pp_problem.freeze()

    sv_helm = pp_problem.sparse_vector(
        val=helm_dens * 1e-9, region=dielectric2, density_to_charge=True)
    sv_charge_dens = pp_problem.sparse_vector(
        val=dop * 1e-9, region=dielectric2, density_to_charge=True)

    sv_gate_outside = pp_problem.sparse_vector(val=V_up, region=gate_up)
    sv_gate_inside = pp_problem.sparse_vector(val=V_down, region=gate_down)

    sv_voltage = tools.SparseVector(indices=[], values=[])
    for sv in [sv_gate_outside, sv_gate_inside]:
        sv_voltage.extend(sv)

    voltage_flexible, charge_flexible = pp_problem.solve(
        voltage=sv_voltage, charge=sv_charge_dens,
        helmholtz_density=sv_helm, method='mumps')

    compare_sv(reference=voltage, result=voltage_flexible,
               name='voltage', err_msg='flexible D/N/H test')
    compare_sv(reference=charge, result=charge_flexible,
               name='charge', err_msg='flexible D/N/H test')

    ################################################################
    # With dopants test - Fixed dopant profile
    ################################################################

    pp_builder.reset_regions()

    shapes_list = [
        air, dielectric, dielectric2, dielectric3]
    shapes_gates = [gate_up, gate_down]

    pp_problem, voltage, charge = parallel_capa_solve(
        pp_builder=pp_builder, shapes_list=shapes_list,
        shapes_gates=shapes_gates, 
        V_up=V_up, V_down=V_down, eps1=eps1, dop=dop,
        test_factorize=True)

    voltage_die, idx_die = pp_problem.read_sparse_vector(
        sparse_vector_inst=voltage,
        region=(dielectric | dielectric2 | dielectric3),
        return_indices=True)

    err, std = parallel_capa_cmp_analytical(
        pp_problem, voltage_die, anlytic_volt_3, idx_die, plot=plot,
        tolerance=tolerance)

    if return_error:
        error_dict['dopant_layer'] = (err, std)
    if return_val:
        val_dict['dopant_layer'] = (
            voltage_die, pp_problem.coordinates[idx_die])

    ################################################################
    # flexible Dirichlet / Neumann sites test
    ################################################################

    pp_builder.reset_regions()

    rel_die_list = [1, eps1, eps1, eps1]

    for rel_die, region in zip(rel_die_list, shapes_list):
        pp_builder.set_relative_permittivity(
            val=rel_die, region=region)

    for rel_die, region in zip([1e8, 1e8], shapes_gates):
        pp_builder.set_relative_permittivity(
            val=rel_die, region=region)
        
    gate_up = shapes_gates[0]
    gate_down = shapes_gates[1]

    # The regions to dirichlet (otherwise default Neumann)
    pp_builder.set_flexible(
        region=(gate_up | gate_down), setup_name='gate')

    pp_builder.set_flexible(
        region=dielectric2, setup_name='dielectric2')

    pp_problem = pp_builder.finalized()

    pp_problem.assign_flexible(
        neumann_index=pp_problem.points(name='dielectric2'),
        dirichlet_index=pp_problem.points(name='gate'))
    pp_problem.freeze()

    sv_gate_outside = pp_problem.sparse_vector(val=V_up, region=gate_up)
    sv_gate_inside = pp_problem.sparse_vector(val=V_down, region=gate_down)

    sv_voltage = tools.SparseVector(indices=[], values=[])
    for sv in [sv_gate_outside, sv_gate_inside]:
        sv_voltage.extend(sv)

    sv_dopants = pp_problem.sparse_vector(
        val=dop * 1e-9, region=dielectric2, density_to_charge=True)

    voltage_flexible, charge_flexible = pp_problem.solve(
        voltage=sv_voltage, charge=sv_dopants, method='mumps')

    compare_sv(reference=voltage, result=voltage_flexible,
               name='voltage', err_msg='flexible D/N test')
    compare_sv(reference=charge, result=charge_flexible,
               name='charge', err_msg='flexible D/N test')

    ################################################################
    # With dopants test - Fixed voltage profile    ################################################################

    rel_die_list = [1, eps1, eps1, eps1]
    shapes_list = [
        air, dielectric, dielectric2, dielectric3]

    for rel_die, region in zip(rel_die_list, shapes_list):
        pp_builder.set_relative_permittivity(
            val=rel_die, region=region)

    pp_builder.reset_regions()
    pp_builder.set_dirichlet(region=dielectric2, setup_name='dielectric2')
    
    pp_builder.set_metal(region=(gate_up | gate_down), setup_name='gate')

    pp_problem = pp_builder.finalized()

    volt_die_2, idx_die2 = pp_problem.read_sparse_vector(
        sparse_vector_inst=voltage, region=dielectric2, return_indices=True)

    volt_die_2_sv = tools.SparseVector(
        values=volt_die_2, indices=idx_die2)

    sv_gate_outside = pp_problem.sparse_vector(val=V_up, region=gate_up)
    sv_gate_inside = pp_problem.sparse_vector(
        val=V_down, region=gate_down)

    sv_voltage = tools.SparseVector(indices=[], values=[])
    for sv in [sv_gate_outside, sv_gate_inside, volt_die_2_sv]:
        sv_voltage.extend(sv)

    voltage, charge = pp_problem.solve(voltage=sv_voltage, method='mumps')

    charge_die, idx_die = pp_problem.read_sparse_vector(
        sparse_vector_inst=charge, region=dielectric2,
        return_indices=True)

    coordinates = pp_problem.coordinates[idx_die]
    charge_expected = (
        dop * 1e-9 * np.array(pp_problem.volume[idx_die]))

    err, std  = compare_reference(
        charge_expected, charge_die, tolerance=tolerance)

    if return_error:
        error_dict['dop_regression'] = (err, std)
    if return_val:
        val_dict['dop_regression'] = (
            charge_die, pp_problem.coordinates[idx_die])

    res = list()
    if return_error:
        res.append(error_dict)
    if return_val:
        res.append(val_dict)

    return tuple(res)


def compare_reference(reference, numerical, tolerance=None):

    err = np.mean(np.abs(reference - numerical))
    std = np.max(np.abs(reference - numerical) - err)

    #print('Error of {0} +- {1}'.format(err, std))

    return err, std


def test_1D_parallel_capa(plot=False):

    dim = 1
    element_size = (0.5 , )
    tolerance=None

    error, value = parallel_capa_test(
        dim=dim, plot=plot, element_size=element_size,
        tolerance=tolerance, return_error=True,
        return_val=True)

    with open(file_path + '/reference_1d_parallel_capa', 'rb') as file:
        ref_data = pickle.load(file)

    error_ref = ref_data['error_mesh']
    mesh_ref = ref_data['mesh_ele']
    value_ref = ref_data['value_0.5']

    idx = 2
    print(mesh_ref)
    assert mesh_ref[idx] == element_size[0]

    # for key, err in error.items():

    #     assert_allclose(
    #         err, error_ref[key][idx], atol=1e-12, rtol=1,
    #         err_msg='{0} failed error regression test'.format(key))

    for key, val in value.items():
        for i in range(len(val)):
            assert_allclose(
                val[i], value_ref[key][i], rtol=1e-6,
                err_msg='{0} failed value regression test'.format(key))


def generate_ref_mesh_size_1d(save_data=False):

    dim = 1
    element_size = (0.5 , )
    tolerance=None

    error, value = parallel_capa_test(
        dim=dim, plot=False, element_size=element_size,
        tolerance=tolerance, return_error=True,
        return_val=True)

    error_mesh ={
            '3_die':list(),
            'constant_die':list(),
            'dopant_layer':list(),
            'helm':list(),
            'dop_regression':list()}

    mesh_ele = [1, 0.6, 0.5, 0.4, 0.2, 0.1]

    for i in mesh_ele:

        if i == 0.5:
            err = (error, )
        else:
            element_size = (i, )
            err = parallel_capa_test(
                dim=dim, plot=False, element_size=element_size,
                tolerance=tolerance, return_error=True)

        for key, val in err[0].items():
            error_mesh[key].append(val)

        print('Done {0}'.format(i))

    ref_data = {
        'error_mesh':error_mesh,
        'mesh_ele':mesh_ele,
        'value_0.5':value}

    if save_data:
        with open('reference_1d_parallel_capa', 'wb') as file:
            ref_data = pickle.dump(ref_data, file)


def generate_ref_mesh_size_2d(save_data=False):

    dim = 2
    element_size = (0.5, 5)
    system_size = (50, )

    tolerance=None

    error, value = parallel_capa_test(
        dim=dim, plot=False, element_size=element_size,
        tolerance=tolerance, return_error=True,
        return_val=True, system_size=system_size)

    error_mesh ={
            '3_die':list(),
            'constant_die':list(),
            'dopant_layer':list(),
            'helm':list(),
            'dop_regression':list()}

    mesh_ele = [1, 0.6, 0.5, 0.4]
    syst_size = [200, 100, 50]

    for i in mesh_ele:

        sys.stdout = open(os.devnull, 'w')
        for s in syst_size:
            element_size = (i, 5)
            err = parallel_capa_test(
                dim=dim, plot=False, element_size=element_size,
                tolerance=tolerance, return_error=True,
                system_size=(s, ))

            for key, val in err[0].items():
                error_mesh[key].append(val)

        sys.stdout = sys.__stdout__
        print('Done {0}'.format(i))

    ref_data = {
        'error_mesh':error_mesh,
        'mesh_ele':mesh_ele,
        'syst_size':syst_size,
        'value_0.5_50':value}

    if save_data:
        with open('reference_2d_parallel_capa', 'wb') as file:
            ref_data = pickle.dump(ref_data, file)

    print(error_mesh)


def test_2D_parallel_capa(plot=False):

    dim = 2
    element_size = (0.5, 5)
    tolerance=None
    system_size = (50, )

    error, value = parallel_capa_test(
        dim=dim, plot=plot, element_size=element_size,
        tolerance=tolerance, return_error=True,
        return_val=True, system_size=system_size)

    with open(file_path + '/reference_2d_parallel_capa', 'rb') as file:
        ref_data = pickle.load(file)

    error_ref = ref_data['error_mesh']
    mesh_ref = ref_data['mesh_ele']
    value_ref = ref_data['value_0.5_50']

    idx = 2
    assert mesh_ref[idx] == element_size[0]
    idx *= 3

    # for key, err in error.items():
    #     assert_allclose(
    #         err, error_ref[key][idx], atol=1e-13,
    #         err_msg='{0} failed error regression test'.format(key))

    for key, val in value.items():
        for i in range(len(val)):
            print(np.max(np.abs((val[i] - value_ref[key][i]) / val[i])))
            print(np.max(np.abs(val[i] - value_ref[key][i])))
            assert_allclose(
                val[i], value_ref[key][i], atol=1e-13, rtol=1e-6,
                err_msg='{0} failed value regression test'.format(key))


def generate_ref_mesh_size_3d(save_data=False):

    dim = 3
    element_size = (1, 10, 10)
    tolerance=None
    system_size = (50, 50)

    error, value = parallel_capa_test(
        dim=dim, plot=False, element_size=element_size,
        tolerance=tolerance, return_error=True,
        return_val=True, system_size=system_size)

    error_mesh ={
            '3_die':list(),
            'constant_die':list(),
            'dopant_layer':list(),
            'helm':list(),
            'dop_regression':list()}

    mesh_ele = [1, 0.6, 0.5, 0.4]
    syst_size = [200, 100, 50]

    for i in mesh_ele:

        sys.stdout = open(os.devnull, 'w')
        for s in syst_size:
            element_size = (i, 10, 10)
            err = parallel_capa_test(
                dim=dim, plot=False, element_size=element_size,
                tolerance=tolerance, return_error=True,
                system_size=(s, s))

            for key, val in err[0].items():
                error_mesh[key].append(val)
            print('Done {0}'.format(s))
        sys.stdout = sys.__stdout__
        print('Done {0}'.format(i))

    ref_data = {
        'error_mesh':error_mesh,
        'mesh_ele':mesh_ele,
        'syst_size':syst_size,
        'value_0.5_50_50':value}

    if save_data:
        with open('reference_3d_parallel_capa', 'wb') as file:
            ref_data = pickle.dump(ref_data, file)


#@mark.slow
def test_3D_parallel_capa(plot=False):

    dim = 3
    tolerance=None
    system_size = (50, 50) # Along invariant directios.

    with open(file_path + '/reference_3d_parallel_capa', 'rb') as file:
        ref_data = pickle.load(file)

    error_ref = ref_data['error_mesh']
    mesh_ref = ref_data['mesh_ele']
    value_ref = ref_data['value_0.5_50_50']

    idx = 0
    element_size = [mesh_ref[idx], 10, 10]

    error, value = parallel_capa_test(
        dim=dim, plot=plot, element_size=element_size,
        tolerance=tolerance, return_error=True,
        return_val=True, system_size=system_size)

    idx = 2
    # for key, err in error.items():
    #     assert_allclose(
    #         err, error_ref[key][idx], atol=1e-12, rtol=1,
    #         err_msg='{0} failed error regression test'.format(key))

    for key, val in value.items():
        for i in range(len(val)):
            assert_allclose(
                val[i], value_ref[key][i], atol=1e-13, rtol=1e-6,
                err_msg='{0} failed value regression test'.format(key))


def twod_cylinder(element_size=None, plot=False):
    ''' Empty cylinder with a dielectric in the middle.

        Analytical solution :
            V(r) = (V_int
                    + ln(r/d_int) / ln(d_ext / d_int) * (V_ext - V_int)

        Tests :

            Numerical with Analytical

            Invariance along psi
    '''

    V_int = -2
    V_ext = 4
    d_int = 7
    d_ext = 30

    if element_size is None:
        element_size=(1, 1)

    pattern_fine = mesher.patterns.Rectangular.constant(
        element_size=element_size)

    shape_all = mesher.shapes.Box(
        lower_left=(-35, -35), size=(70, 70))

    error_dict = {
        'random':None,
        'y_0':None,
        'x_0':None}

    val_dict = {
        'random':None,
        'y_0':None,
        'x_0':None}

    ##########
    # Sub_meshes

    die_int = mesher.shapes.Ellipsoid.hypersphere(
        center=(0, ) * 2, radius=d_int - 3)

    cylinder_int = (
        mesher.shapes.Ellipsoid.hypersphere(center=(0, ) * 2, radius=d_int)
        - die_int)

    die_mid = (
        mesher.shapes.Ellipsoid.hypersphere(center=(0, ) * 2, radius=d_ext)
        - (cylinder_int | die_int))

    cylinder_ext = (
        mesher.shapes.Ellipsoid.hypersphere(
            center=(0, ) * 2, radius=d_ext + 3)
        - (die_int | die_mid | cylinder_int))

    air_ext = (
        shape_all
        - (cylinder_ext | cylinder_int | die_mid | die_int))

    pp_builder = poisson.ProblemBuilder()

    pp_builder.initialize_mesh(
        simulation_region=shape_all, pattern=pattern_fine)

    ##########
    # Dielectric

    rel_die_list = [1, 8.2, 5.]
    shapes_list = [air_ext, die_mid, die_int]

    for rel_die, region in zip(rel_die_list, shapes_list):
        pp_builder.set_relative_permittivity(
            val=rel_die, region=region)

    ##########
    # b.c.s

    pp_builder.set_metal(
        region=cylinder_int, setup_name='cylinder_int')
    pp_builder.set_metal(
        region=cylinder_ext, setup_name='cylinder_ext')

    pp_builder.set_neumann(region=die_mid, setup_name='die_mid')
    pp_builder.set_neumann(region=die_int, setup_name='die_int')

    pp_problem = pp_builder.finalized()

    ##########
    # input voltage

    sv_gate_outside = pp_problem.sparse_vector(
        val=V_ext, region=cylinder_ext)
    sv_gate_inside = pp_problem.sparse_vector(
        val=V_int, region=cylinder_int)

    sv_voltage = tools.SparseVector(indices=[], values=[])
    for sv in [sv_gate_outside, sv_gate_inside]:
        sv_voltage.extend(sv)

    voltage, charge = pp_problem.solve(
        voltage=sv_voltage, method='mumps')

    ##########
    # Analytical reference

    def voltage_ref(coord):

        r = np.sqrt(np.sum(coord * coord, axis=1))
        v_r_ = (np.ones(len(r)) * V_int
                + (np.log(np.abs(r) / d_int)
                / np.log(d_ext / d_int)
                * (V_ext - V_int)))

        v_r_[np.abs(r) < d_int] = np.ones(np.sum(np.abs(r) < d_int)) * V_int

        v_r_[np.abs(r) > d_ext] = np.ones(np.sum(np.abs(r) > d_ext)) * V_ext

        return v_r_

    ##########
    # Compare random cut

    coord_cut_sin = np.vstack(
        [np.linspace(d_int, d_ext, 25), np.linspace(d_int, d_ext, 25)]).T
    coord_cut_sin[:, 0] = np.sin(
        np.linspace(0, 1, 25) * np.pi / 2) * (d_ext - d_int) + (d_int - 10)

    mesh_coord = np.empty(coord_cut_sin.shape, dtype=int)
    for i in range(len(coord_cut_sin)):
        mesh_coord[i, :] = pattern_fine(
            pattern_fine.around(center=coord_cut_sin[i], radius=[0.7, 0.7])[0])

    mesh_idx = pp_problem.points(coordinates=mesh_coord)

    if plot:
        plt.plot(np.sqrt(np.sum(mesh_coord ** 2, axis=1)), voltage[mesh_idx],
                 'rx', label='Simulation')
        plt.plot(np.sqrt(np.sum(mesh_coord ** 2, axis=1)),
                 voltage_ref(mesh_coord), 'b.', label='Anlytical')

        plt.title('Cut 1')
        plt.xlabel('Radius (nm)')
        plt.ylabel('Voltage (V)')
        plt.legend(loc='upper right')

        plt.show()

    err, std = compare_reference(
        reference=voltage_ref(mesh_coord), numerical=voltage[mesh_idx])

    error_dict['random'] = (err, std)
    val_dict['random'] = (voltage[mesh_idx], mesh_coord)

    voltage_die, idx_die = pp_problem.read_sparse_vector(
        sparse_vector_inst=voltage, region=(die_mid | die_int), return_indices=True)

    coordinates = pp_problem.coordinates[idx_die]
    ##########
    # Compare x = 0

    argsort = np.argsort(coordinates[:, 0])
    val, indx = tools.meshing.unique(coordinates[argsort][:, 0])

    val_0_idx = np.arange(0, len(val), dtype=int)[val == 0][0]

    coord_0_x = coordinates[argsort][indx[val_0_idx]:indx[val_0_idx + 1]]
    voltage_die_0 = voltage_die[argsort][indx[val_0_idx]:indx[val_0_idx + 1]]

    if plot:
        plt.plot(np.sqrt(np.sum(coord_0_x ** 2, axis=1)), voltage_die_0,
                'rx', label='Simulation')
        plt.plot(np.sqrt(np.sum(coord_0_x ** 2, axis=1)),
                voltage_ref(coord_0_x), 'b.', label='Anlytical')

        plt.title('Cut along x = 0')
        plt.xlabel('Radius (nm)')
        plt.ylabel('Voltage (V)')
        plt.legend(loc='upper right')

        plt.show()

    err, std = compare_reference(
        reference=voltage_ref(coord_0_x), numerical=voltage_die_0)

    error_dict['x_0'] = (err, std)
    val_dict['x_0'] = (voltage[mesh_idx], mesh_coord)

    ##########
    # Compare y = 0

    argsort = np.argsort(coordinates[:, 1])
    val, indx = tools.meshing.unique(coordinates[argsort][:, 1])

    val_0_idx = np.arange(0, len(val), dtype=int)[val == 0][0]

    coord_0_y = coordinates[argsort][indx[val_0_idx]:indx[val_0_idx + 1]]
    voltage_die_0 = voltage_die[argsort][indx[val_0_idx]:indx[val_0_idx + 1]]

    if plot:
        plt.plot(np.sqrt(np.sum(coord_0_y ** 2, axis=1)), voltage_die_0,
                'rx', label='Simulation')
        plt.plot(np.sqrt(np.sum(coord_0_y ** 2, axis=1)),
                 voltage_ref(coord_0_y), 'b.', label='Anlytical')

        plt.title('Cut along y = 0')
        plt.xlabel('Radius (nm)')
        plt.ylabel('Voltage (V)')
        plt.legend(loc='upper right')

        plt.show()

    err, std = compare_reference(
        reference=voltage_ref(coord_0_y), numerical=voltage_die_0)

    error_dict['y_0'] = (err, std)
    val_dict['y_0'] = (voltage[mesh_idx], mesh_coord)

    return error_dict, val_dict


def generate_cylinder_mesh(save_data=False):

    elements_size = [[1, 1], [0.5, 1], [1, 0.5],
                     [0.5, 0.5], [0.25, 0.5], [0.5, 0.25],
                     [0.25, 0.25], [0.1, 0.25], [0.25, .1]]

    error_ref = {'random':list(), 'y_0':list(), 'x_0':list()}

    val_ref = {'random':list(), 'y_0':list(), 'x_0':list()}

    for ele in elements_size:
        sys.stdout = open(os.devnull, 'w')
        error_dict, val_dict = twod_cylinder(
            element_size=ele, plot=False)

        for key, err in error_dict.items():
            error_ref[key].append(err)
            val_ref[key].append(val_dict[key])
        sys.stdout = sys.__stdout__
        print('Done {0}'.format(ele))

    ref_data = {
        'val_ref':val_ref,
        'error_ref':error_ref,
        'elements_size':elements_size}

    if save_data:
        with open('reference_2d_cylinder', 'wb') as file:
            ref_data = pickle.dump(ref_data, file)


def test_2D_cylinder(plot=False):

    with open(file_path + '/reference_2d_cylinder', 'rb') as file:
        ref_data = pickle.load(file)

    val_ref = ref_data['val_ref']
    error_ref = ref_data['error_ref']
    elements_size = ref_data['elements_size']

    idx = 0
    element_size = elements_size[idx]

    error_dict, val_dict = twod_cylinder(
            element_size=element_size, plot=False)


    for key, err in error_dict.items():

        assert_allclose(
            err, error_ref[key][idx], atol=1e-13,
            err_msg='{0} failed error regression test'.format(key))

    for key, val in val_dict.items():
        for i in range(len(val)):
            assert_allclose(
                val[i], val_ref[key][idx][i], atol=1e-13,
                err_msg='{0} failed value regression test'.format(key))


def volt_analytical_pt_charge(
    coord, charge_pos, eps, charge):

    coord = coord * 1e-9
    d_0_vect = np.ones(len(coord)) * charge_pos * 1e-9

    fact = ((charge * constants.elementary_charge)
            / (4 * np.pi * eps * constants.epsilon_0))

    r_sqr = np.sum(coord[:, [0, 2]] * coord[:, [0, 2]], axis=1)

    den_pos = 1 / np.sqrt(r_sqr + (coord[:, 1] - d_0_vect) ** 2)
    den_neg = -1 / np.sqrt(r_sqr + (coord[:, 1] + d_0_vect) ** 2)

    return fact * (den_pos + den_neg)


def point_charge(element_size=None, plot=False):

    epgate=2
    epdie = 20

    eps = 8

    origin = -epgate
    d0 = origin + epgate

    charge_pos = 10.
    V_up = 0
    charge_pt = -20

    system_size = 20
    border = system_size / 2

    # Defining the mesh
    if element_size is None:
        element_size = [1, 0.5, 1]

    pattern_fine = mesher.patterns.Rectangular.constant(
        element_size=element_size)

    element_size = [0.1, 0.1, 0.1]
    pattern_charge = mesher.patterns.Rectangular.constant(
        element_size=element_size)

    dielectric_charge = mesher.shapes.Box(
        lower_left=(-0.5, charge_pos - 0.5, -0.5), size=(1, 1, 1))

    shape_all = mesher.shapes.Box(
        lower_left=(-border, origin, -border),
        size=(system_size, epdie + epgate, system_size))

    pp_builder = poisson.ProblemBuilder()
    pp_builder.initialize_mesh(
        simulation_region=shape_all, pattern=pattern_fine)
    pp_builder.refine_mesh(region=dielectric_charge, pattern=pattern_charge)

    # Defining the overall system

    gate_up = mesher.shapes.Box(
        lower_left=(-border, origin, -border),
        size=(system_size, epgate, system_size))

    dielectric = (
        mesher.shapes.Box(
            lower_left=(-border, d0, -border),
            size=(system_size, epdie, system_size))
        - gate_up)
    
    air = (shape_all - (gate_up | dielectric))

    # Setting the dielectric constant
    
    rel_die_list = [1, eps]
    shapes_list = [air, dielectric]

    for rel_die, region in zip(rel_die_list, shapes_list):
        pp_builder.set_relative_permittivity(
            val=rel_die, region=region)

    # The regions to dirichlet (otherwise default Neumann)
    pp_builder.set_metal(region=gate_up, setup_name='gate')

    pp_problem = pp_builder.finalized()

    tag = pattern_charge.tags(coords=np.array([[0, charge_pos, 0]]))
    
    zero_idx = pp_problem.points(coordinates=np.array([[0, charge_pos, 0]]))

    sv_pt_charge = tools.SparseVector(
        values=charge_pt * np.ones(len(zero_idx)), indices=zero_idx)

    sv_voltage = pp_problem.sparse_vector(val=V_up, region=gate_up)

    voltage, charge = pp_problem.solve(
        voltage=sv_voltage, charge=sv_pt_charge, method='mumps')

    voltage_die, idx_die = pp_problem.read_sparse_vector(
        sparse_vector_inst=voltage,
        region=(dielectric), return_indices=True)
    
    coordinates = pp_problem.coordinates[idx_die]
    twodcut = np.arange(len(coordinates))[coordinates[:, 2] == 0]

    zeroth = np.where(coordinates[twodcut, 0] == 0)
    idx = twodcut[zeroth][coordinates[twodcut[zeroth]][:, 1] != charge_pos]

    volt_analytical = volt_analytical_pt_charge(
        coord=coordinates[idx],
        charge_pos=charge_pos, eps=eps, charge=charge_pt)
    
    if plot:
        plt.plot(coordinates[:, 1][idx],
                 voltage_die[idx], 'r.',
                 label='Numerical')
        plt.plot(coordinates[:, 1][idx],
                 volt_analytical, 'g.',
                 label='Analytical')
        plt.legend()
        plt.show()

        plt.plot(coordinates[:, 1][idx],
                 charge[idx], 'r.',
                 label='Numerical')
        plt.legend()
        plt.show()


    err, std  = compare_reference(
        volt_analytical, voltage_die[idx])

    return err, std, voltage_die, coordinates


def generate_point_charge_mesh(save_data=False):

    elements_size = [[5, 1, 5], [5, 0.5, 5], [5, 0.25, 5],
                     [2.5, 1, 2.5], [2.5, 0.5, 2.5], [2.5, 0.25, 2.5],
                     [1, 1, 1], [1, 0.5, 1], [1, 0.25, 1]]

    error_list = list()
    std_list = list()
    volt_list = list()
    coord_list = list()

    for ele in elements_size:
        sys.stdout = open(os.devnull, 'w')
        err, std, voltage_die, coord = point_charge(
            element_size=ele, plot=False)

        error_list.append(err)
        std_list.append(std)
        volt_list.append(voltage_die)
        coord_list.append(coord)

        sys.stdout = sys.__stdout__
        print('Done {0}'.format(ele))

    ref_data = {
        'error_list':error_list,
        'std_list':std_list,
        'volt_list':volt_list,
        'coord_list':coord_list,
        'elements_size':elements_size}

    if save_data:
        with open('reference_3d_point_charge', 'wb') as file:
            ref_data = pickle.dump(ref_data, file)


#@mark.slow
def test_point_charge(plot=False):

    with open(file_path + '/reference_3d_point_charge', 'rb') as file:
        ref_data = pickle.load(file)

    error_list = ref_data['error_list']
    std_list = ref_data['std_list']
    volt_list = ref_data['volt_list']
    coord_list = ref_data['coord_list']

    elements_size = ref_data['elements_size']

    idx = 0
    element_size = elements_size[idx]

    err, std, voltage_die, coord = point_charge(
            element_size=element_size, plot=plot)

    map_ = tools._meshing.where(coord_list[idx], coord)

    assert_allclose(
        coord[map_], coord_list[idx],
        err_msg='Failed coord regression test point_charge')
    assert_allclose(
        err, error_list[idx],
        err_msg='Failed error regression test point_charge')
    assert_allclose(
        std, std_list[idx],
        err_msg='Failed std regression test point_charge')
    assert_allclose(
        voltage_die[map_], volt_list[idx],
        err_msg='Failed voltage_die regression test point_charge')


def pot_2d_geo(h, c, vc, vs, x, y):
    """Analytically compute electrostatic potential inside a 2d plate capacitor
    with an additional central top gate.

         V = V_s   | V = V_c | V = V_s
         left      | central | right
    ...  side gate | gate    | side gate  ...
        ___________|_________|___________
                    <------->                   ^
                        c                       |
                                                |
    ...              interior             ...   | h
                                                |
                                                |
        _________________________________       v

    ...            bottom gate    V=0     ...

    The origin of the coordinate system horizontally in the center,
    vertically at the boundary between the interior and the bottom gate.
    """
    pi = np.pi
    arctan = np.arctan
    exp = np.exp
    cos = np.cos
    sin = np.sin

    temp = exp(pi / h * x)
    _v = 1 / temp / sin(pi / h * y)
    u = temp * cos(pi / h * y)
    del temp

    result = (vs - vc) / pi * arctan((-exp(pi * c / h) - u) * _v)
    result += (vc - vs) / pi * arctan((-exp(-pi * c / h) - u) * _v)

    result += vs / pi * arctan(-u * _v)
    result += vs / 2

    return result


#@mark.slow
def test_2d_geo():
    """Compare numerics with analytical result for potential inside a 2d
    plate capacitor with an additional central top gate.

    The geometry is a finite-width (w) version of the one shown in the
    docstring of analytic_2d_pot.

    """

    Box = mesher.shapes.Box

    # Dimensions of area between gates: half-width and height
    w, h = 20, 10

    # Half-width of the central gate
    c = 5

    # Number of layers of gate cells.
    nl = 2

    # Side length of excluded squares where central gate touches side gates,
    # as fraction of central gate width.
    ex = 0.5

    # Vertical spacing of the fine pattern.  This is so that the gates can thin
    # so that charge is not smeared out vertically which is nonphysical.
    fs = 1 / 1000

    # Ensure that the fine pattern will be commensurate with the coarse
    # (integer spaced) one.
    assert abs(1 / fs - round(1 / fs)) < 1e-10

    # Voltages of top gate: central and side.
    vc, vs = 1, -1

    # Absolute and relative error tolerances.
    atol = 0.01
    rtol = 0.01

    ######## Define meshing patterns. ########
    # Create coarse and fine patterns
    coarse = mesher.patterns.Rectangular.constant(element_size=(1, 1),
                                                  center=(0.5, 0.5))
    fine = mesher.patterns.Rectangular.constant(element_size=(1, fs),
                                                center=(0.5, fs / 2))

    ######## Define shapes. ########
    interior = Box(lower_left=(-w, -nl * fs), size=(2*w, h + (nl * 2 * fs)))

    bottom_gate = Box(lower_left=(-w, -nl * fs), size=(2*w, nl * fs))
    central_gate = Box(lower_left=(-c, h), size=(2*c, nl * fs))
    side_gate = (Box(lower_left=(-w, h), size=(w - c, nl * fs))
                 | Box(lower_left=(c, h), size=(w - c, nl * fs)))

    all_gates = bottom_gate | central_gate | side_gate

    bottom_counterpart = Box(lower_left=(-w, 0), size=(2*w, fs))
    top_counterpart = Box(lower_left=(-w, h - fs), size=(2*w, fs))

    all_counterparts = bottom_counterpart | top_counterpart

    ######## Build the Poisson problem. ########
    builder = poisson.ProblemBuilder()
    # Define the finite volume mesh.
    builder.initialize_mesh(simulation_region=interior, pattern=coarse)
    builder.refine_mesh(region=all_gates | all_counterparts, pattern=fine)

    # Set the sites in the gates as Dirichlet
    builder.set_metal(region=all_gates, setup_name='dir')

    # Construct the mesh, capacitance Matrix, linear problem.
    prob = builder.finalized()

    ######## Solve the Poisson problem. ########
    voltage = prob.sparse_vector(val=0, region=bottom_gate)
    voltage.extend(prob.sparse_vector(val=vc, region=central_gate))
    voltage.extend(prob.sparse_vector(val=vs, region=side_gate))

    result_u, result_q = prob.solve(voltage=voltage)

    cex = c * ex
    plot_area = prob.points_inside(
        Box(lower_left=(-w, 0), size=(2*w, h))
        - Box(lower_left=(-c - cex, h - cex), size=(2*cex, 2*cex))
        - Box(lower_left=(c - cex, h - cex), size=(2*cex, 2*cex)))

    ######## Compare with analytic result. ########
    coords = prob.coordinates[plot_area]
    u = pot_2d_geo(h, c, vc, vs, coords[:, 0], coords[:, 1])

    aerr = abs(result_u[plot_area] - u)

    rerr = abs(aerr / u)

    assert np.max(np.fmin(aerr / atol, rerr / rtol)) < 1

    # from matplotlib import pyplot
    # pyplot.scatter(coords[:, 0], coords[:, 1], c=aerr, s=10)
    # pyplot.colorbar()
    # pyplot.show()


#compare_reference(reference=v_r(coord_0_y), numerical=voltage_die_0)

# disable print
#sys.stdout = open(os.devnull, 'w')
# generate_ref_mesh_size_1d(save_data=True)
# #test_1D_parallel_capa()
# generate_ref_mesh_size_2d(save_data=True)
# #test_2D_parallel_capa(plot=True)
# generate_ref_mesh_size_3d(save_data=True)
# #test_3D_parallel_capa(plot=True)
# generate_point_charge_mesh(save_data=True)
# # test_point_charge(plot=False)
# generate_cylinder_mesh(save_data=True)
#test_2D_cylinder()
#test_point_charge(plot=False)
# test_1D_parallel_capa(plot=True)
# test_2D_parallel_capa(plot=True)
# test_3D_parallel_capa(plot=True)

# enable print
#sys.stdout = sys.__stdout__
