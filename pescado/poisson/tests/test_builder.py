import functools
import numpy as np

from numpy.testing import assert_allclose

from pytest import raises

import kwant

from pescado import poisson
from pescado import mesher
from pescado.tools import meshing, reduced_array

def make_kwant():

    W=10
    L=100
    L_well=20
    t=1.

    lat = kwant.lattice.square(a=1)
    kwant_syst = kwant.Builder()

    def potential(site, pot):
        (x, y) = site.pos
        if (L - L_well) / 2 < x < (L + L_well) / 2:
            return pot
        else:
            return 0

    def onsite(site, pot):
        return 4 * t + potential(site, pot)

    kwant_syst[(lat(x, y)
                for x in range(-L // 2, (L // 2) +1)
                for y in range(-W // 2, (W // 2) +1))] = onsite
    kwant_syst[lat.neighbors()] = -t
    
    ref_pattern = mesher.patterns.Rectangular.constant(element_size=(1, 1))
    kwant_shape = mesher.shapes.Box(
        lower_left=(-L // 2, -W // 2), size=(L, W))
    
    return kwant_syst.finalized(), kwant_shape, ref_pattern


def fun_(x):
    return np.abs(x) ** 1.2 * np.sign(x)


def inv_fun_(x):
    return np.abs(x) ** ( 1 / 1.2) * np.sign(x)


def test_pp_builder():

    ########################
    # Kwant part
    ########################

    kwant_syst, kwant_shape, kwant_ref_pat = make_kwant()

    ########################
    # Tests that  does not work with local functions
    ########################

    def local_fun_(x):
        return np.abs(x) ** 1.2 * np.sign(x)

    # To test the parallelization thing, later !!!
    pattern_continuous_local = mesher.patterns.Rectangular(
        ticks=(local_fun_, ) * 2, tag2ticks=(inv_fun_, ) * 2)
    shape_continuous_local = mesher.shapes.Box(
       lower_left=(-10, -10), size=(20, 20))

    pp_builder = poisson.ProblemBuilder()

    pp_builder.initialize_mesh(
        simulation_region=shape_continuous_local,
        pattern=pattern_continuous_local)

    # TODO: Before merge - Check what this error was !!! 
    # err_msg = "Can't pickle local object 'test_pp_builder.<locals>.local_fun_'"
    # with raises(AttributeError, match=err_msg):
    pp_builder.finalized()

    ########################
    # Define the patterns and Shapes
    ########################

    pattern_fine = mesher.patterns.Rectangular.constant(
        element_size=(1, 2))

    pattern_continuous = mesher.patterns.Rectangular(
        ticks=(fun_, ) * 2, tag2ticks=(inv_fun_, ) * 2)

    pattern_coarse = mesher.patterns.Rectangular.constant(
        element_size=(10, 20))

    shape_fine = mesher.shapes.Box(
        lower_left=(-5, -10), size=(10, 20))

    shape_coarse = mesher.shapes.Box(
        lower_left=(-50, -100), size=(100, 200))

    shape_continuous = mesher.shapes.Box(
        lower_left=(-25, -50), size=(50, 100))

    ########################
    # Build the mesh using an instance of poisson.ProblemBuilder
    ########################

    # If it fails to reset the mesh,
    # there will still be a local function in
    # pattern and therefore  will raise an AttributeError
    pp_builder.reset_mesh(
        simulation_region=shape_coarse, pattern=pattern_coarse)

    pp_builder.reset_regions()

    pp_builder.refine_mesh(
        region=shape_continuous, pattern=pattern_continuous)

    pp_builder.refine_mesh(
        region=shape_fine, pattern=pattern_fine)

    # # # # Without external boundary
    pp_builder.refine_mesh_kwant(
        region=kwant_shape, kwant_syst=kwant_syst)

    mesh = pp_builder.mesh

    ########################
    # Build the mesh with an instance of builder
    ########################

    msh_bdr = mesher.Mesh(
        simulation_region=shape_coarse, pattern=pattern_coarse)


    test = mesher.shapes.Box(lower_left=(-50, -5), size=(99, 9))
    pat = mesher.patterns.Rectangular.constant(element_size=(1, 1))

    msh_bdr.refine(region=shape_continuous, pattern=pattern_continuous)
    msh_bdr.refine(region=shape_fine, pattern=pattern_fine)
    msh_bdr.refine(region=kwant_shape, pattern=kwant_ref_pat)

    ########################
    # Test the mesh
    ########################
    npoints = 250
    compare_mesh(mesh_ref=msh_bdr, mesh_test=mesh, npoints=npoints)

    ########################
    # Test setting the capacitance matrix
    ########################

    shapes_inst = [shape_continuous, shape_fine, kwant_shape]
    values = [4.32, 4.234, 1.34]
    non_default_indices = list()
    dielectric_constant = np.ones(mesh.npoints, dtype=float)

    # The dielectric constant is set sequentially in the builder
    for j, (val, shape_inst) in enumerate(zip(values, shapes_inst)):

        pp_builder.set_relative_permittivity(val, shape_inst)

        if j < (len(shapes_inst) - 1):
            all_shapes = functools.reduce(
                lambda x, y: x | y,
                [g for i, g in enumerate(shapes_inst) if i > j])
            test_shape = shape_inst - all_shapes
        else:
            test_shape = shape_inst

        non_default_indices.append(mesh.inside(test_shape))
        dielectric_constant[non_default_indices[-1]] = np.ones(
            len(non_default_indices[-1])) * val

    test_indices = np.concatenate(
        [id_list for i, id_list in enumerate(non_default_indices) if i != 0])

    np.random.seed(134432)
    print(mesh.npoints)

    extra = np.setdiff1d(
        np.random.randint(low=0, high=mesh.npoints, size=npoints),
        test_indices)
    assert len(extra) > 0
    test_indices = np.concatenate([test_indices, extra])

    dielectric_constant = dielectric_constant * pp_builder.vaccum_perm

    try:
        test_capa = pp_builder._make_capacitance_matrix(
            mesh=mesh, parallelize=False)
    except Exception as e:
        if isinstance(e, AttributeError) and (str(e) == err_msg):
            raise e
        else:
            test_capa = pp_builder._make_capacitance_matrix(
                mesh=mesh)

    compare_capa_matrix(
        test_indices=test_indices, mesh=mesh,
        dielectric_constant=dielectric_constant,
        capa_matrix=test_capa, tol=1e-14)

    print('passed capa_test')

    ########################
    # Test setting the b.c.s
    ########################

    shape_gate = mesher.shapes.Box(
        lower_left=(25, -60), size=(20, 100))

    shape_helmholtz = mesher.shapes.Box(
        lower_left=(-20, -5), size=(40, 10))

    shape_mixed = mesher.shapes.Box(
        lower_left=(-25, -5), size=(50, 10)) - (shape_helmholtz | shape_gate)

    shape_ddop = (mesher.shapes.Box(lower_left=(-50, 20), size=(100, 50))
                  - (shape_helmholtz | shape_gate | shape_mixed))

    names = ['gate', 'helm',  'mixed', 'dop']
    pp_builder.set_neumann(region=shape_ddop, setup_name=names[3])
    pp_builder.set_metal(region=shape_gate, setup_name=names[0])
    pp_builder.set_flexible(region=shape_mixed, setup_name=names[2])
    pp_builder.set_helmholtz(region=shape_helmholtz, setup_name=names[1])

    pp_problem = pp_builder.finalized()

    for type_, name, sh_inst in zip(
        ['dirichlet', 'helmholtz',  'flexible', 'neumann'],
        [['gate',], ['helm',], ['mixed',], ['dop']],
        [shape_gate, shape_helmholtz, shape_mixed, shape_ddop]):

        print(pp_builder.regions[type_])
        assert pp_builder.regions[type_] == name, (
            'poisson.ProblemBuilder failed regions test')

        assert pp_builder.regions_shapes[name[0]] == sh_inst, (
            'poisson.ProblemBuilder failed regions_shapes test')

    pp_builder.delete_region(name='dop')
    assert 'dop' not in pp_builder.regions['dirichlet']

    with raises(KeyError):
        pp_builder.regions_shapes['dop']


def compare_capa_matrix(
    test_indices, mesh, dielectric_constant, capa_matrix, tol=1e-15):
    ''' Uses mesh to generate some capacitance matrix elements
    '''

    test_indices = np.sort(test_indices)
    neighbours, point_neighbours = mesh.neighbours(
        test_indices, reduced_array=True)

    number_non_zero = len(neighbours) + len(test_indices)

    column = np.zeros(number_non_zero, dtype = int)
    row = np.zeros(number_non_zero, dtype = int)
    data = np.zeros(number_non_zero, dtype=float)

    element_id = 0
    for i, idx in enumerate(test_indices):

        neig_idx = neighbours[point_neighbours[i]:point_neighbours[i+1]]
        vor_cell = mesh.voronoi(points=idx)
        
        # Some local voronoi neighbours migth not exist in the mesh.
        neig_coord = mesh.coordinates(neig_idx)
        vor_neig_coord = vor_cell.neighbours
        mesh2vor_neig_idx = meshing.kd_where(
            array=neig_coord, ref_array=vor_neig_coord)[0]

        surf = vor_cell.surface(neig_idx=mesh2vor_neig_idx)
        dist = vor_cell.distance(neig_idx=mesh2vor_neig_idx)

        geo_capa = -1 * 2 * np.true_divide(surf, dist)

        # Dielectric factor
        die_fact = (
            dielectric_constant[idx]
            * dielectric_constant[neig_idx]
            / (np.ones(len(neig_idx)) * dielectric_constant[idx]
                + dielectric_constant[neig_idx]))

        # Check sign here !!!
        capa_vect = die_fact * geo_capa

        # Diagonal element
        column[element_id] = idx
        row[element_id] = idx
        data[element_id] = -1 * np.sum(capa_vect)
        element_id += 1

        # The remaining neighbours
        row[element_id + np.arange(len(neig_idx))] = idx
        column[element_id + np.arange(len(neig_idx))] = neig_idx
        data[element_id + np.arange(len(neig_idx))] = capa_vect
        element_id += len(neig_idx)

    dense_capa = np.asarray(capa_matrix.todense())

    for row_, col_, val in zip(row, column, data):
        assert (dense_capa[row_, col_,] - val) < tol, (
            'capacitance matrix failed test for ({0}, {1})'.format(row_, col_))

    for i, idx in enumerate(test_indices):
        neig_idx = neighbours[point_neighbours[i]:point_neighbours[i+1]]
        non_zero = list(neig_idx) + [idx, ]
        assert np.all(dense_capa[idx, :][np.setdiff1d(
            np.arange(0, mesh.npoints), non_zero)] < tol), (
            'Some capacitance elements that should be zero are not')


def compare_mesh(mesh_ref, mesh_test, npoints):
    ''' Compares the coordinates, volume, surface and distance
    '''

    mesh_ref_coord = mesh_ref.coordinates()
    mesh_test_coord = mesh_test.coordinates()

    sort_idx_ref = meshing.sequencial_sort(
            mesh_ref_coord, order=np.arange(mesh_ref_coord.shape[1]),
            return_indices=True)

    inv_sort_idx_ref = np.argsort(sort_idx_ref)

    sort_idx_test = meshing.sequencial_sort(
            mesh_test_coord, order=np.arange(mesh_test_coord.shape[1]),
            return_indices=True)
    inv_sort_idx_test = np.argsort(sort_idx_test)

    assert_allclose(
        mesh_ref_coord[sort_idx_ref],
        mesh_test_coord[sort_idx_test],
        err_msg='Mesh failed coordinates test')

    assert_allclose(
        mesh_ref.volume(sort_idx_ref),
        mesh_test.volume(sort_idx_test),
        err_msg='Mesh failed volume test')

    neig_ref, idx_neig_ref = mesh_ref.neighbours(
        sort_idx_ref, reduced_array=True)
    neig_ref = inv_sort_idx_ref[neig_ref]
    ref_sorted_idx = reduced_array.argsort(
        indices=idx_neig_ref, values=neig_ref)

    neig_test, idx_neig_test = mesh_test.neighbours(
        sort_idx_test, reduced_array=True)
    neig_test = inv_sort_idx_test[neig_test]
    test_sorted_idx = reduced_array.argsort(
        indices=idx_neig_test, values=neig_test)
    
    assert_allclose(
         mesh_ref_coord[sort_idx_ref][neig_ref[ref_sorted_idx]],
         mesh_test_coord[sort_idx_test][neig_test[test_sorted_idx]], err_msg='Mesh failed neighbours test')
