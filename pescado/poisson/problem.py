import time
from collections.abc import Sequence
import copy
from distutils.log import warn
import functools

import pickle

from scipy import sparse

import numpy as np
from pescado import SparseVector
from pescado.poisson.solver import solve_scipy, Mumps
from pescado.tools import meshing, plotting

def group_matrix(matrix_group, matrix):
    ''' Arranges the 'matrix' into the groups defined by
     the (row, col) blocs in 'matrix_group'.

    Parameters
    -----------

    matrix_group: np.array of tuples
        Index defining the groups, overrides group_index

        Each tuple contains two elements, a numpy array with the row
        indices and a numpy array with the column indices. They do not need
        to have the same size.

        Example:

            matrix_group = [
                [(array_1, array_1), (array_1, array_2)],
                [(array_2, array_1), (array_2, array_2)]]

    matrix: instance of scipy.sparse.scr_matrix(), 2D matrix.

    Returns
    --------

    groups = numpy.ndarray of tuples with groupe_matrix.shape
        Definines the 'matrix' groups.

        groups[i, j] = (values, (row_indices, column_indices))

        with:
            values: the non zero values for
                'matrix'['group_index[i]', 'group_index[j]']

            row_indices: the elements in group_index[i] with non zero
                values in 'matrix'['group_index[i]', 'group_index[i]']

            column_indices: the elements in group_index[i] with non zero
                values in 'matrix'['group_index[j]', 'group_index[j]']

    '''
    # Check if the coordinates are unique
    for row in range(matrix_group.shape[0]):
        for col, group in enumerate(matrix_group[row, :]):
            if group:
                if  len(group[0]) != len(np.unique(group[0])):
                    raise ValueError(
                'row elements for group ({0}, {1}) are not unique'.format(
                    col, row))
                if  len(group[1]) != len(np.unique(group[1])):
                    raise ValueError(
                'col elements for group ({0}, {1}) are not unique'.format(
                    col, row))

    # convert matrix to_csr
    if not isinstance(matrix, sparse.csr_matrix):
        matrix_csr = sparse.csr_matrix(matrix)
    else:
        matrix_csr = matrix

    groups = np.empty(matrix_group.shape, dtype=tuple)
    for row in range(matrix_group.shape[0]):
        for col, group in enumerate(matrix_group[row, :]):
            if not group:
                groups[row, col] = (np.array([]), (np.array([]), np.array([])))
            elif (len(group[0]) == 0) or (len(group[1]) == 0):
                groups[row, col] = (np.array([]), (np.array([]), np.array([])))
            else:
                t = slice_sparse(
                    rows=group[0], columns=group[1], matrix=matrix_csr).tocoo()
                groups[row, col] = (t.data, (group[0][t.row], group[1][t.col]))

    return groups


def slice_sparse(matrix, rows, columns):
    ''' Selects the (row, col) block from a given 'sparse_array'
    using matrix multiplication

    Parameters
    -----------

        row: sequence of ints
        col: sequence of ints

        matrix: instance of scipy.sparse.csr_matrix

    Returns
    --------
        sparse array

    '''

    row_sparse, col_sparse = matrix.shape
    number_rows = len(rows)
    number_cols = len(columns)
    left_matrix = sparse.csr_matrix(([1]*number_rows,
                                  (range(number_rows), rows)),
                                 shape=(number_rows, row_sparse),
                                 dtype=bool)
    right_matrix = sparse.csc_matrix(([1]*number_cols,
                                   (columns, range(number_cols))),
                                  shape=(col_sparse, number_cols),
                                  dtype=bool)

    return left_matrix * matrix * right_matrix


def arrange_matrix_elements(
    matrix, group_index=None, matrix_group=None):
    ''' Arranges the 'matrix' into len('group_index') x
    len('group_index') groups defined by the indices in
    'group_index'.

    Parameters
    -----------

    matrix: instance of scipy.sparse.scr_matrix(), 2D matrix.

    group_index: sequence of np.arrays of intergers (optional - default None)
        Index defining the groups

        Example:

            group_index = [array_1, array_2]

        then it arranges the matrix elements into 4 groups

            matrix_group = [
                [(array_1, array_1), (array_1, array_2)],
                [(array_2, array_1), (array_2, array_2)]]

    matrix_group: np.array of tuples (optional - default None)
        Index defining the groups, overrides group_index

        Each tuple contains two elements, a numpy array with the row
        indices and a numpy array with the column indices. They do not need
        to have the same size.

        Example:

            matrix_group = [
                [(array_1, array_1), (array_1, array_2)],
                [(array_2, array_1), (array_2, array_2)]]

    Returns
    --------

    groups = numpy.ndarray of tuples with groupe_matrix.shape
        Definines the 'matrix' groups.

        groups[i, j] = (values, (row_indices, column_indices))

        with:
            values: the non zero values for
                'matrix'['group_index[i]', 'group_index[j]']

            row_indices: the elements in group_index[i] with non zero
                values in 'matrix'['group_index[i]', 'group_index[i]']

            column_indices: the elements in group_index[i] with non zero
                values in 'matrix'['group_index[j]', 'group_index[j]']

    '''
    if matrix_group is None:
        matrix_group = np.empty((len(group_index), ) * 2, dtype=tuple)
        for row, row_id in enumerate(group_index):
            for col, col_id in enumerate(group_index):
                matrix_group[row, col] = (row_id, col_id)

    return group_matrix(matrix_group, matrix)


def sym_identity_sparse(index, shape):
    ''' Returns a sparse matrix with 1 as value along the diagonal elements
    ('index', 'index')

    Parameters
    -----------

    index: numpy.ndarray of integers

    shape: sequence of two ints
        shape of the sparse.csr_matrix

    Returns
    --------

    instance of sparse.csr_matrix

    '''

    data = np.ones(len(index), dtype=float)
    return sparse.csr_matrix((data, (index, index)), shape=shape)


def build_dnh_lp(group_index, grouped_capa, shape):
    ''' Returns the rhs and lhs of the linear problem that depends only
    on the dirichlet, neumann and helmholtz decomposition.

    Parameters
    -----------

    group_index: sequence of 4 np.arrays of intergers
        Index defining the index for dirichlet, neumann, helmholtz and flexible
        regions.

    grouped_capa: numpy.ndarray of tuples
        For more info see output of arrange_matrix_elements()

    shape = sequence of ints
        Shape of the rhs and lhs sparse matrix

    Returns
    --------
        rhs_dnh: instance of sparse.csr_matrix

        lhs_dnh: instance of sparse.csr_matrix
    '''

    rhs_dnh = sparse.csr_matrix(
        (np.array([]), (np.array([]), np.array([]))),
        shape=shape)

    lhs_dnh = sparse.csr_matrix(
        (np.array([]), (np.array([]), np.array([]))),
        shape=shape)

    for row in range(4):
        for col in range(4):
            if (col == 0) and (row == col):
                lhs_dnh = sym_identity_sparse(
                    group_index[row], shape)
                rhs_dnh += sparse.csr_matrix(
                    grouped_capa[row, col], shape=shape)
            elif col == 0 and (row != col):
                rhs_dnh += sparse.csr_matrix(
                    grouped_capa[row, col], shape=shape)
            elif (col == row) and (row in [1, 2]):
                lhs_dnh -= sparse.csr_matrix(
                    grouped_capa[row, col], shape=shape)
                rhs_dnh -= sym_identity_sparse(
                    group_index[row], shape)
            elif (col != row) and col in [1, 2]:
                lhs_dnh -= sparse.csr_matrix(
                    grouped_capa[row, col], shape=shape)

    return rhs_dnh, lhs_dnh

def build_flexible_lp(flex_sub_group, grouped_flex_capa, shape):
    ''' Returns the rhs and lhs of the linear problem for the flexible sites

    Parameters
    -----------

    flex_sub_group: sequence of 3 np.arrays of intergers
        Index defining the index for dirichlet, neumann, helmholtz

    grouped_flex_capa: numpy.ndarray of tuples
        For more info see output of arrange_matrix_elements()

    shape: sequence of two integers
        Defines the shape of the sparse.csr_matrix

    Returns
    --------
        rhs_flex: instance of sparse.csr_matrix

        lhs_flex: instance of sparse.csr_matrix
    '''

    rhs_flex = sparse.csr_matrix(
        (np.array([]), (np.array([]), np.array([]))),
        shape=shape)

    lhs_flex = sparse.csr_matrix(
        (np.array([]), (np.array([]), np.array([]))),
        shape=shape)

    for row in range(4):
        for col in range(3):
            sub_group = flex_sub_group.indices[
                flex_sub_group.values == (col + 1)]

            if col == 0:
                rhs_flex += sparse.csr_matrix(
                    grouped_flex_capa[row, col], shape=shape)
            elif col in [1, 2]:
                lhs_flex -= sparse.csr_matrix(
                    grouped_flex_capa[row, col], shape=shape)

            if (col == 0) and (row == 3):
                lhs_flex += sym_identity_sparse(sub_group, shape)
            elif (col in [1, 2]) and (row == 3):
                rhs_flex -= sym_identity_sparse(sub_group, shape)

    return rhs_flex, lhs_flex


class Problem():

    def __init__(self,
        capacitance_matrix, coordinates, volume, regions, regions_shapes,
        kwant_indices):
        ''' Returns an instance of pescado.poisson.Problem

        Parameters
        -----------

        capacitance_matrix: scipy.sparse matrix

        coordinates: np.ndarray of floats with shape(m, d)
            m: number of points
            d: dimension

            real space coordinates of the mesh points. 

        volume: np.ndarray of floats with shape(m, )
            Volume of the point cell. 

        regions: dictionary
            key: region type ('dirichlet', 'neumann', 'helmholtz', 'flexible')
            val: list of strings with the sub_region names

        regions_shapes: dictionary
            key: sub_region name (from regions.values())
            val: sub_region shape

        kwant_indices: dicionary
            key: instance of kwant.Sites
            val: mesh index for the kwant site

        Returns
        -------

        instance of pescado.problem.Problem

        TODO: Discuss with Xavier wether precalculating the volume
        is a good idea.
        '''

        self.capacitance_matrix = capacitance_matrix
        self.regions = regions
        self.regions_shapes = regions_shapes

        self._kwant_indices = kwant_indices

        self.npoints = len(coordinates)
        self.coordinates = coordinates
        
        # Transform volume into a SparseVector
        self.volume = SparseVector(
            values=volume, indices=np.arange(len(volume)), dtype=float)
        
        regions_index = self._arrange_region_index()

        for num, idx in enumerate(regions_index):
            not_num = np.setdiff1d(np.arange(3), (num,))
            for ot_num, ot_idx in enumerate(regions_index[not_num]):
                rep = np.intersect1d(idx, ot_idx)
                if len(rep) > 0:
                    raise RuntimeError(
                        'Sites {0} were set to both {1} and {2} types'.format(
                            rep, num, not_num[ot_num]))

        (self.dirichlet_indices, self.neumann_indices, self.helmholtz_indices,
         self.flexible_indices) = regions_index

        self.linear_problem_inst = self._initialize_LP(regions_index)

    def points(self, name=None, coordinates=None, tol=1e-12):
        ''' Return the points tagged 'name' or the points for a 
         given set of 'coordinates'

        Parameters
        -----------

        name: string - optional (None)
            Tag given to the indices by 
            'set_neumann', 'set_dirichlet', 'set_flexible' or 'set_helmholtz'. 

        coordinates: np.array of floats (m, d) - optional (None)
                m: number of coordinates, d: dimension
                
        tol: float

        Returns
        --------
            points : np.array of integers
        '''

        if (name is not None) and (coordinates is not None):
            raise ValueError('Please give either "name" or "region"')
        elif (name is not None) and (coordinates is None):
            points = self.points_inside(self.regions_shapes[name])
        elif coordinates is not None:
            if len(coordinates.shape) != 2:
                raise ValueError('Wrong number of dimensions in "coordinates"')
            
            points = np.ones(len(coordinates), dtype=int) * -1
            points_, isin  = meshing.kd_where(
                coordinates, self.coordinates, dist=tol)
            points[isin] = points_
        else:
            raise ValueError
        
        return points 

    def _arrange_region_index(self):
        ''' Arranges the mesh indices into dirichlet, neumann, helmholtz and
        flexible

        Returns
        --------
        sequence with four numpy.arrays containing the indices for each type
        mentioned above in the following order:
            dirichlet neumann helmholtz flexible

        '''

        idx_list = np.empty((4,), dtype=np.ndarray)
        for num, type_ in enumerate(
            ['dirichlet', 'neumann', 'helmholtz', 'flexible']):
            if len(self.regions[type_]) > 0:
                idx_list[num] = np.concatenate(
                    [np.arange(self.npoints, dtype=int)[
                        self.regions_shapes[name](self.coordinates)]
                     for name in self.regions[type_]])
            else:
                idx_list[num] = np.array([])

        return idx_list

    def points_inside(self, region):
        ''' Return the indices of the mesh inside a 'region'.

        Parameters
        -----------

        region: instance of pescado.mesher.shapes.Shape
            Defines the real space region from which to recover the
            mesh 'indices'

        Returns
        --------
        sorted numpy.ndarray of integers containing the site indices

        '''
        return  np.arange(0, self.npoints, dtype=int)[region(self.coordinates)]

    def kwant_indices(self, kwant_sites=None):
        ''' Return the indices for the points 'kwant_tags'

        Parameters
        -----------

        kwant_sites: sequence of instances of kwant.Sites

        '''

        if kwant_sites is None:
            indices = np.array(list(self._kwant_indices.values()))
        else:
            indices = np.empty(
                (len(kwant_sites), ), dtype=int)

            for i, kwant_site in enumerate(kwant_sites):
                indices[i] = self._kwant_indices[kwant_site]

        return indices

    def kwant_coordinates(self, kwant_sites=None):
        ''' Return the coordinates for the points 'kwant_tags'

        Parameters
        -----------

        kwant_sites: sequence of instances of kwant.Sites

        '''

        if kwant_sites is None:
            coord = self.coordinates[self.kwant_indices()]
        else:
            coord = self.coordinates[
                self.kwant_indices(kwant_sites=kwant_sites)]

        return coord

    def _initialize_LP(self, regions_index):
        ''' Returns an instance of linear_problem

        Parameters
        -----------

        regions_index: sequence of np.arrray of int
            Contains the indices of the different regions in the system.

            Regions to be defined in the following order :
                Dirichlet - neumann - helmhotlz - flexible

        Returns
        --------
        instance of pescado.poisson.problem.LinearProblem

        '''

        # Find the different non-zero blocks within the capacitance matrix
        grouped_capacitance = arrange_matrix_elements(
            group_index=regions_index,
            matrix=self.capacitance_matrix)

        # Select those along the 'flexible' sites column that are not
        # zero.
        flexible_capacitance = functools.reduce(
            lambda x, y: x + y,
            [sparse.csr_matrix(group, self.capacitance_matrix.shape)
             for group in grouped_capacitance[:, 3]])

        # The Dirichlet, Neumann and Helmholtz lhs and rhs
        rhs_dnh, lhs_dnh = build_dnh_lp(
            group_index=regions_index,
            grouped_capa=grouped_capacitance,
            shape=self.capacitance_matrix.shape)

        return LinearProblem(
            rhs_dnh=rhs_dnh, lhs_dnh=lhs_dnh,
            flexible_capacitance=flexible_capacitance,
            regions_index=regions_index,
            volume=self.volume)

    def assign_flexible(
        self, neumann_index=None, dirichlet_index=None, helmholtz_index=None,
        contain_all=False):
        ''' From the list of mutable points, fix their type as 'dirichlet' or
        'neumann'.

        Parameters
        -----------

        neumann_index: sequence of m_n intergers (default None)

        dirichlet_index: sequence of m_d intergers (default None)

        helmholtz_index: sequence of m_h intergers (default None)

        contain_all: boolean (default False)
            If True then 'neumann_index', 'dirichlet_index' and
            'helmholtz_index' must, tohgheter, contain all the flexible sites.

            If only two are given (e.g. 'neumann_index' and 'dirichlet_index')
            then the rest is set to the third region (e.g. 'helmholtz_index')

            For more info see Notes

        Notes
        ------

        If contain_all is True

            If only two are given, the third type is assigned as default
            for any of the flexible points not in the two parameters.

            If three are given, then all of the flexible points must
            be assigned. Otherwise ValueError is raised.

        '''

        self.linear_problem_inst.assign_flexible(
            neumann_index=neumann_index,
            dirichlet_index=dirichlet_index,
            helmholtz_index=helmholtz_index,
            contain_all=contain_all)

    def solve(
        self, voltage, charge=None, charge_density=None,
        helmholtz_density=None, method='mumps', **kwargs):
        '''

        TODO: Check with Xavier.
        '''

        return self.linear_problem_inst.solve(
            voltage=voltage, charge=charge,
            charge_density=charge_density, helmholtz_density=helmholtz_density,
            method=method, **kwargs)

    def freeze(self):
        ''' Freezes the Neumann / Dirichlet / Helmholtz configuration

        Calls LinearProblem.build() method
        '''

        self.linear_problem_inst.build()

    def reset(self):
        ''' Resets the instance of LinearProblem inside Problem.

        TODO: Check with Xavier
        '''


        updated_regions_index = self._arrange_region_index()

        self.linear_problem_inst = self._initialize_LP(
            updated_regions_index)

    def sparse_vector(
        self, val, region=None, name=None,
        dtype=None, density_to_charge=False):
        ''' Returns an instance of pescado.poisson.SparseVector
        with the 'val' fixed to the points in 'indices' or 'shape'

        Parameters
        -----------

        val: float, integer, or function
            Values of the points in 'indices'

            If it is a function f(r), then:
                f(r) = val
                    r: the mesh point real space coordinates
                    val: the scalar value at r

        region: instance of pescado.mesher.shapes.Shape - optional (None)
            Defines the real space region from which to recover the
            mesh 'indices'

        name: string - optional (None)
            Tag given to the indices by 
            'set_neumann', 'set_dirichlet', 'set_flexible' or 'set_helmholtz'.

        dtype: np.dtype
            Parameter passed to tools.sparse_vector.SparseVector()
            Defines the dtype of the .values in SparseVector

        density_to_charge: boolean - optional (False)
            If True it will multiple 'val' by the cell volume.

            That is:

                If True : values = 'val' * self.volume(
                    indices='indices')

        Returns
        --------
        instance of pescado.tools.sparse_vector.Sparse_vector

            Containing the 'val' and 'indices' array.

            The default value is set to None in the SparseVector instance.

        '''
        if (region is not None) and (name is None):
            indices = self.points_inside(region=region)
        else:
            indices = self.points(name=name)

        if isinstance(val, (float, int, np.floating, np.integer)):
            values = np.ones((len(indices), ), dtype=float) * val
        else:
            values = val(self.coordinates[indices])

        if density_to_charge:
            values = values * self.volume[indices]

        return SparseVector(
            indices=indices, values=values, dtype=dtype)

    def constant_charge_vector(
        self, integrated_value, indices=None, name=None, region=None):
        ''' Returns an instance of pescado.poisson.SparseVector
        where the np.sum(SparseVector.values) is equal to the
        'integrated_value' and the indices are the ones inside 'region',
        inside the sub_region tagged 'name' or in the parameter 'indices'.

        Parameters
        -----------

        integrated_value: float, integer
            Total value for the charge inside 'region' or the sub_region
            tagged 'name'.

        indices: sequence of integers - optional (None)
            Global indices of the points in the mesh

        region: instance of pescado.mesher.shapes.Shape - optional (None)
            Defines the real space region from which to recover the
            mesh 'indices'

        name: string - optional (None)
            Name of the sub_region from which to recover the
            mesh 'indices'

        Returns
        --------
        instance of pescado.tools.sparse_vector.Sparse_vector

            SparseVector.values[i] = (
                volume[i] * 'integrated_value') / np.sum(volume)

            Hence 'integrated_value' = np.sum(SparseVector.values)

            The default value is set to None in the SparseVector instance.

        Notes
        ------

        The charge is distributed to the 'indices' according to their volume
        relative to the total volume defined by the specified region.

        '''

        if ((indices is None)
            and ((region is not None) and (name is None))):
            indices = self.points_inside(region=region)
        elif ((indices is None)
            and ((region is None) and (name is not None))):
            indices = self.points(name=name)
        else:
            indices = np.asarray(indices)

        vol = self.volume[indices]
        values = (vol / np.sum(vol)) * integrated_value

        return SparseVector(
            indices=indices, values=values, dtype=np.float64)

    def read_sparse_vector(
        self, sparse_vector_inst, region=None, name=None,
        return_indices=False):
        ''' Returns the values of the sparse array inside a given 'region'
        or a give region called by 'name'.

        Parameters
        -----------

        sparse_vector_inst: instance of pescado.tools.sparse_vector.SparseVector

        region: instance of pescado.mesher.shapes.Shape - optional (None)

        name: string - optional (None)

        return_indices: boolean - optional (False)

        Returns
        -------

        values: np.ndarray of floats

        If return_indices is True

        indices: np.ndarray of integers

        '''

        if region is None:
            region = self.regions_shapes[name]

        non_default_idx = sparse_vector_inst.indices[region(
             self.coordinates[sparse_vector_inst.indices])]

        if sparse_vector_inst.default is None:
            indices = non_default_idx
            values = sparse_vector_inst[indices]
        else:
            # self.points_inside is already sorted.
            all_idx = self.points_inside(region=region)

            default_idx, default_pos, ignore = np.intersect1d(
                all_idx, non_default_idx, return_indices=True)

            values = (np.ones((len(all_idx), ), dtype=float)
                      * sparse_vector_inst.default)

            values[default_pos] = sparse_vector_inst[non_default_idx]

            indices = all_idx

        if return_indices:
            return values, indices
        else:
            return values

    def plot_sparse_vector(
            self, sparse_vector, region, grid_step, 
            grid_center=None, direction=None):
        ''' Generates the function 'plot_fun'. It plots the values of the 'sparse_vector' inside 'region'.

        If direction is None: 'plot_fun(ax, **kwargs)'

        If direction is not None: it slices the values of the 'sparse_vector'
        at a constant 'cut_position' along 'direction' ; 
                    
                    'plot_fun(cut_position, ax, **kwargs)'

        Parameters
        -----------
        sparse_vector: instance of sparse vector
            Same behaviour as sparse vector output of 'self.solve()

        region: instance of 'pescado.mesher.shapes'
            Notice -> dimension of the 'region' must be the same as the slicing
                dimension. 2D cut, 2D region, etc ...

        grid_step: float
            Distance between grid points used for plotting

        grid_center: sequence of floats (default None)
            Coordinates of the center of the grid used for plotting
        
        direction: integer or sequence of integers (default is None)
            Slicing direction

            Example: Take a 3D system, then if:
                direction = 2 -> make a 2D plot along (0, 1)
                direction = (1, 2) -> make a 1D slice along 0

        Returns
        --------
        plot_fun:

            Parameters
            -----------
            cut_position: floats or sequence of floats (default is None)
                Same format as 'direction'

            ax: instance of plt.gca() (default is None)

            cmap: str (default 'Set1')
                c.f. https://matplotlib.org/examples/color/colormaps_reference.html

                Ignored if 1D plot

            kwargs: arguments passed down to 
                i) ax.plot if 1D plot
                ii) ax.imshow if 2D plot

            Returns
            --------

            if 1D plot -> 'ax'
            if 2D plot -> 'ax' and 'ax.imshow(-)'        
        '''

        values = sparse_vector[np.arange(self.npoints, dtype=int)]
        sv_plot = plotting.region_plot(
                region=region, grid_step=grid_step, grid_center=grid_center,
                values_coord=self.coordinates, direction=direction)
        
        def plot_fun(cut_position=None, ax=None, **kwargs):    
            return sv_plot(
                values=values, cut_position=cut_position, ax=ax, **kwargs)
        
        return plot_fun
    
    def save(self, name):
        ''' Saves current mesh to 'name' using pickle. 
        
        It saves the self.__init__() attributes. 
        
        Notice, among them there is regions_shapes. It contains instances of
        pescado.mesher.shapes.Shape. There are two considerations: 
            
            i) To save them they can't contain a lambda function. Pickle can't
            handle them by default;
            
            i) To load a saved Problem one needs to import shapes.

        Parameters
        -----------
        
        file_name: string; 
            Path and file name where to save the current Problem instance.

        '''

        data = {
            'capacitance_matrix':self.capacitance_matrix,
            'coordinates':self.coordinates,
            'volume':self.volume.values,
            'regions':self.regions,
            'regions_shapes':self.regions_shapes,
            'kwant_indices':self._kwant_indices}
        
        with open(name, 'wb') as f:
            pickle.dump(data, file=f)
    
    @classmethod
    def load(cls, name):
        ''' Initializes an instance of Problem from 'name'. 

        Parameters
        -----------

        name: string; 
            Path and file name of the saved Problem instance. 

        Returns
        --------
        instance of pescado.poisson.Problem
        '''

        with open(name, 'rb') as f:
            data = pickle.load(f)

        return cls(**data)
    
    # def save(self,
    #          sparse_vector_inst, shape=None, name_file='mesh_with_values',
    #          name_values = 'Values', only_boundary=True):
    #     ''' Write coordinates of 3D voronoi cells and the values stored inside
    #         a SparseVector in a .vtu file.

    #         The data could be later visualized using Paraview or Mayavi for example

    #     Parameters
    #     ----------
    #     sparse_vector_inst : pescado.tools.SparseVector with values inputs

    #     shape : instance of 'pescado.continuous.shapes.Shape', optional
    #         If shape = None, the voronoi cells and the corresponding values
    #         of the whole mesh will be writen in the .vtu file

    #     name_file : string, optional
    #         The default is 'mesh_with_values'.

    #     name_values : string, optional
    #         The default is 'Value'.

    #     only_boundary : boolean, optional
    #         The default is False.
    #         If True only save the coordinates and the values stored at the
    #         boundary of "shape".

    #     Returns
    #     -------
    #     None.

    #     '''
    #     try:

    #         import vtk

    #         if only_boundary:
    #             idx = self.mesh_inst.boundary(shape)[0]
    #             val = sparse_vector_inst[idx]
    #         else:
    #             val, idx = self.read_sparse_vector(
    #                 sparse_vector_inst, shape, return_indices=True)

    #         values = vtk.vtkFloatArray()
    #         values.SetNumberOfComponents(1)
    #         values.SetName(name_values)

    #         points = vtk.vtkPoints()
    #         ugrid = vtk.vtkUnstructuredGrid()
    #         n_vertices = 0

    #         vrn = self.mesh_inst._voronoi(idx)

    #         for i in range(idx.size):
    #             vertices = vrn[i].vertices
    #             for j in range(vertices.shape[0]):
    #                 points.InsertNextPoint(vertices[j])
    #                 values.InsertNextTuple([val[i]])

    #             ridges = np.asarray(vrn[i].ridges) + n_vertices
    #             ridgesId = vtk.vtkIdList()
    #             ridgesId.InsertNextId(len(ridges))

    #             for ridge in ridges:
    #                 ridgesId.InsertNextId(ridge.size)
    #                 for k in ridge:
    #                     ridgesId.InsertNextId(k)
    #             ugrid.InsertNextCell(vtk.VTK_POLYHEDRON, ridgesId)

    #             n_vertices += len(vertices)

    #         ugrid.SetPoints(points)
    #         ugrid.GetPointData().SetScalars(values)

    #         writer = vtk.vtkXMLUnstructuredGridWriter()
    #         writer.SetInputData(ugrid)
    #         writer.SetFileName(name_file + '.vtu')
    #         writer.SetDataModeToAscii()
    #         writer.Update()

    #     except ModuleNotFoundError as e:
    #         warn('Vtk module not found, can"t use save function : {0}'.format(
    #             e))


class LinearProblem():

    def __init__(self,
        rhs_dnh, lhs_dnh, flexible_capacitance, regions_index, volume):
        ''' Returns an instance of pescado.poisson.LinearProblem

        Parameters
        -----------

        capacitance_matrix: scipy.sparse matrix

        '''

        self.rhs_dnh = rhs_dnh
        self.lhs_dnh = lhs_dnh
        self.flexible_capacitance = flexible_capacitance
        self.regions_index = regions_index
        self.volume = volume

        self.rhs = copy.deepcopy(self.rhs_dnh)
        self.lhs = copy.deepcopy(self.lhs_dnh)

        self.method = None
        self.solver = None

        # Which among flexible are Dirichlet, Neumann or Helmholtz
        self.reset_flexible_configuration()
        self.flexible_glossary = [
            'undefined', 'dirichlet', 'neumann', 'helmholtz']

    def reset_flexible_configuration(self):
        ''' Reset flexible sites configuration.

        It reverses the operations in assign_flexible.
        '''

        self.flexible_sub_type = SparseVector(
            indices=self.regions_index[3],
            values=np.zeros(len(self.regions_index[3])))

    def assign_flexible(
        self, neumann_index=None, dirichlet_index=None, helmholtz_index=None,
        contain_all=False):
        ''' From the list of flexible points, fix their type as 'neumann_index',
        'dirichlet_index' or 'helmholtz_index'.

        Parameters
        -----------

        neumann_index: sequence of m_n intergers (default None)

        dirichlet_index: sequence of m_d intergers (default None)

        helmholtz_index: sequence of m_h intergers (default None)

        contain_all: boolean (default False)
            If True then 'neumann_index', 'dirichlet_index' and
            'helmholtz_index' must, tohgheter, contain all the flexible sites.

            If only two are given (e.g. 'neumann_index' and 'dirichlet_index')
            then the rest is set to the third region (e.g. 'helmholtz_index')

            For more info see Notes

        Notes
        ------

        If contain_all is True

            If only two are given, the third type is assigned as default
            for any of the flexible points not in the two parameters.

            If three are given, then all of the flexible points must
            be assigned. Otherwise ValueError is raised.

        '''

        flex_sub_regions = [dirichlet_index, neumann_index, helmholtz_index]

        if contain_all: # Reset flexible_sub_type
            self.reset_flexible_configuration()
            map__ = [sub_reg is None for sub_reg in flex_sub_regions]
            if sum(map__) == 1:
                def_idx = np.arange(0, 3)[map__][0]
                flex_sub_regions[def_idx] = np.setdiff1d(
                    self.regions_index[3],
                    np.concatenate([flex_sub_regions[i]
                    for i in range(3) if i != def_idx]))
            elif sum(map__) > 1:
                raise ValueError(
                    'At least two parameters among neumann_index, '
                    + 'dirichlet_index or helmholtz_index must be '
                    + 'given if "contain_all" is True')
        else:
            for i, sub_region in enumerate(flex_sub_regions):
                if sub_region is None:
                    flex_sub_regions[i] = np.array([])

        for sub_region_type, sub_region in enumerate(flex_sub_regions):
            if len(np.setdiff1d(sub_region, self.regions_index[3])) != 0:
                raise ValueError(
                    ('The indices {0} in {1}_index'.format(
                        np.setdiff1d(sub_region, self.regions_index[
                            3]),
                        self.flexible_glossary[sub_region_type])
                     + ' do not belong to the flexible region'))
            else:
                self.flexible_sub_type[sub_region] = np.ones(
                    (len(sub_region), ), dtype=int) * (sub_region_type + 1)

        if contain_all:
            if np.any(self.flexible_sub_type.values == 0):
                raise ValueError(
                    'The sites {0} in flexible were not set'.format(
                        self.regions_index[3][
                            self.flexible_sub_type.values == 0]))

    def build(self):
        ''' Constructs the rhs and lhs
        '''

        matrix_group = np.empty((4, 3), dtype=np.ndarray)
        for row in range(4):
            for col in range(3):
                matrix_group[row, col] = (
                    self.regions_index[row],
                    self.flexible_sub_type.indices[
                        self.flexible_sub_type.values == (col + 1)])

        grouped_flex_capa = arrange_matrix_elements(
            matrix_group=matrix_group, matrix=self.flexible_capacitance)

        rhs_flex, lhs_flex = build_flexible_lp(
            flex_sub_group=self.flexible_sub_type,
            grouped_flex_capa=grouped_flex_capa,
            shape=self.flexible_capacitance.shape)

        self.lhs = self.lhs_dnh + lhs_flex
        self.rhs = self.rhs_dnh + rhs_flex

    def _prepare_input(self, voltage, charge):
        ''' Returns an input scipy.sparse.csr_matrix

        Parameters
        -----------

            voltage: pescado.tools.SparseVector with voltage inputs

            charge: pescado.tools.SparseVector with charge inputs

        Returns
        --------
        scipy.sparse.csr_matrix
        with shape = (len(self.flexible_capacitance.shape[0]), )

        '''

        shape_ = self.flexible_capacitance.shape

        volt_ind = list()
        charge_ind = list()

        for region_num, ind_list in enumerate(self.regions_index[:-1]):

            flex_ind = self.flexible_sub_type.indices[
                    self.flexible_sub_type.values == region_num + 1]

            if region_num == 0:
                volt_ind.append(ind_list)
                volt_ind.append(flex_ind)
            else:
                charge_ind.append(ind_list)
                charge_ind.append(flex_ind)

        volt_ind = np.concatenate(volt_ind)
        charge_ind = np.concatenate(charge_ind)

        input_ = functools.reduce(
            lambda x, y: x + y,
            [sparse.csr_matrix(
                (val, (ind, np.zeros(len(ind)))), (shape_[0], 1))
            for val, ind in zip(
                [voltage[volt_ind], charge[charge_ind]],
                [volt_ind, charge_ind])])

        return input_

    def _read_output(self, output):
        ''' Returns a 'voltage' and 'charge' pescado.tools.SparseVector
        containing the output values.

        Parameters
        -----------

            output: np.ndarray

        Returns
        --------

            voltage: pescado.tools.SparseVector with voltage outputs

            charge: pescado.tools.SparseVector with charge outputs
        '''

        volt_ind = list()
        charge_ind = list()

        for region_num, ind_list in enumerate(self.regions_index[:-1]):

            flex_ind = self.flexible_sub_type.indices[
                    self.flexible_sub_type.values == region_num + 1]

            if region_num != 0:
                volt_ind.append(ind_list)
                volt_ind.append(flex_ind)
            else:
                charge_ind.append(ind_list)
                charge_ind.append(flex_ind)

        volt_ind = np.concatenate(volt_ind).astype(int)
        charge_ind = np.concatenate(charge_ind).astype(int)

        voltage = SparseVector(
            indices=volt_ind, values=output[volt_ind])
        charge = SparseVector(
            indices=charge_ind, values=output[charge_ind])

        return voltage, charge

    def _prepare_helmholtz(self, helmholtz_density):
        ''' Returns a sparse.csr_matrix with the helmholtz density

        Parameters
        -----------

            helmholtz_density: pescado.tools.SparseVector
                with the helmholtz_density outputs

        Returns
        --------
            sparse.csr_matrix with shape self.flexible_capacitance.shape
            with non zero values at the helmholtz indices.

        '''

        shape_ = self.flexible_capacitance.shape

        helmholtz_density_sparse = sparse.csr_matrix(
            (helmholtz_density[self.regions_index[2]],
             (self.regions_index[2], self.regions_index[2])),
            shape_)

        helmholtz_flex_idx = self.flexible_sub_type.indices[
                 self.flexible_sub_type.values == 3]

        helmholtz_density_sparse += sparse.csr_matrix(
            (helmholtz_density[helmholtz_flex_idx],
             (helmholtz_flex_idx, helmholtz_flex_idx)),
            shape_)

        return helmholtz_density_sparse

    def solve(
        self, voltage, charge=None, charge_density=None,
        helmholtz_density=None, method='mumps', **kwargs):
        '''
        '''

        t0 = time.time()
        if charge and charge_density:
            error_id = np.intersect1d(
                charge_density.indices, charge.indices)
            if len(error_id) != 0:
                raise ValueError(
                    'The values for the sites {0} '.format(error_id)
                    + 'have already been defined in "charge"')

        charge_in = SparseVector(
            indices=np.array([]), values=np.array([]), default=0)
        if charge:
            charge_in[charge.indices] = charge.values
        if charge_density:
            charge_in[charge_density.indices] = (
                charge_density.values * self.volume[charge_density.indices])

        if helmholtz_density:
            helmholtz_density_sparse = self._prepare_helmholtz(
                helmholtz_density=helmholtz_density)
            lhs = self.lhs + helmholtz_density_sparse
        else:
            lhs = copy.deepcopy(self.lhs)

        input_ = self._prepare_input(voltage=voltage, charge=charge_in)
        rhs = self.rhs * input_
        solvers = {'scipy':solve_scipy,
                   'mumps':Mumps()}

        if (self.solver is None) or (self.method != method):
            self.solver = solvers[method]
            self.method = method

        t1 = time.time()
        voltage_out, charge_out = self._read_output(
            output=self.solver(A=lhs, B=rhs, **kwargs))
        t2 = time.time()
        voltage_out.extend(voltage)

        charge_out.extend(charge_in)
        charge_out.default = 0.

        # tf = time.time()
        # print('Total time in solve {0}'.format(tf - t0))
        # print('Time spent solving the linear problem {0}'.format(
        #     t2 - t1))

        return voltage_out, charge_out
