from scipy import sparse

def solve_scipy(A, B, **kwargs):
    ''' Return 'X', a SparseVector solving AX=B with scipy.sparse.linalg.spsolve

    Parameters
    -----------

        A: scipy.sparse.csc_matric

        B: scipy.sparse.csc_matric

    Returns
    -------

        X: output of the chosen solver (see Notes)

    Notes
    ------
        For scipy the output is given
    '''

    if 'use_umfpack' in kwargs.keys():
        use_umfpack = kwargs.pop('use_umfpack')
    else:
        use_umfpack = False

    output = sparse.linalg.spsolve(
        A, B, use_umfpack=use_umfpack, **kwargs)

    return output

class Mumps():
    '''
        Wrapper around kwant.linalg.mumps
    '''

    def __init__(self):
        super().__init__()
        self.mumps_solver_obj = None

    def __call__(self, A, B, **kwargs):
        '''
            Uses kwant interface of the MUMPS parse solver library

            Parameters:
            -----------

                A: scipy.sparse.csc_matric

                B: scipy.sparse.csc_matric

                mums_solver_obj: Instance of kwant.linalg.mumps.MUMPSContext

                factorize: bool
                    If True LU decomposition is made (default)
                    If not the previous LU decomposition is used

                verbose: bool
                    Parameter sent to kwant.linalg.mumps.MUMPSContext.
                    Default False.

            Returns:
            --------
                out: np.ndarray
        '''

        factorize = True
        verbose = False

        if 'mumps_solver_obj' in kwargs.keys():
            self.mumps_solver_obj = kwargs.pop('mumps_solver_obj')
        if 'factorize' in kwargs.keys():
            factorize = kwargs.pop('factorize')
        if 'verbose' in kwargs.keys():
            verbose = kwargs.pop('verbose')

        if self.mumps_solver_obj is None:
            try:
                self.mumps_solver_obj = mumps.MUMPSContext(
                        verbose=verbose)
            except NameError:
                from kwant.linalg import mumps
                self.mumps_solver_obj = mumps.MUMPSContext(
                        verbose=verbose)

        # Temporary fix to mumps error - See Iusse 56
        # Convert data to complex type
        A.data = A.data.astype(complex)

        if factorize:
            self.mumps_solver_obj.factor(A)

        return self.mumps_solver_obj.solve(B).real[:, 0]
